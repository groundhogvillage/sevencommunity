-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 07, 2017 at 09:04 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sevencomm`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('04e6c9adeac48211efa9ca1a5931d3fb1bea38bd', '::1', 1491296017, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313239363031363b),
('14c144478ab0f96b29aba90fcd301e52bae2f97f', '::1', 1491538357, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533383139333b557365725f49447c733a313a2231223b557365726e616d657c733a353a2261646d696e223b5065726d697373696f6e7c733a373a2263726561746f72223b436f6d6d756e6974797c4e3b),
('1749a802bea57dadf41becc735b522edaec04723', '::1', 1490087527, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038373233323b),
('1d6fc751fa2a821888159836c4817989eec7a5ef', '::1', 1490088594, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038383439343b),
('2a5779e46786b1d20d9e49fe43a6005df33352de', '::1', 1490092476, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303039323434353b),
('2bce127383b2d28947d95236e75fe482197e06e5', '::1', 1490087179, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038363932333b),
('3a28dc7594bcc40b8eb5e64bdbc1086d32ffba1d', '::1', 1491534594, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533343338303b557365725f49447c733a313a2231223b557365726e616d657c733a353a2261646d696e223b5065726d697373696f6e7c733a373a2263726561746f72223b436f6d6d756e6974797c4e3b),
('3c42891a923658eacdf322baf8542fcc82a790f6', '::1', 1490091397, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303039313039383b),
('40fd3b489c4bdcef461374d26469551da996745a', '::1', 1490153858, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303135333539383b557365725f49447c733a313a2231223b557365726e616d657c733a353a2261646d696e223b5065726d697373696f6e7c733a373a2263726561746f72223b436f6d6d756e6974797c4e3b),
('45cf5bf3d043f089319792e693961d3f72651555', '::1', 1491534363, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533343036333b),
('482f45f50849e5674c2b3cf8e9881607e280d2a8', '::1', 1491537518, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533373531383b557365725f49447c733a313a2231223b557365726e616d657c733a353a2261646d696e223b5065726d697373696f6e7c733a373a2263726561746f72223b436f6d6d756e6974797c4e3b),
('50803044abe8404bdc67f4fb833aaf4bcc821125', '::1', 1490085571, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038353330333b),
('5371c3331dbb2979902e3928f21f4f875400f54a', '::1', 1490091074, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303039303738333b),
('763262e9738713d37d3e2f4ff2982ded2d0e9701', '::1', 1490084739, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038343732363b),
('7a6b814219b36c73e9e223505dfc0f3f8c9f83e6', '::1', 1490089001, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038383832333b),
('7bccc8206a809eb02fd4de7ec990caaeef809dff', '::1', 1490088209, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038383037363b),
('831d8aa33979e839011b5812b6f6d29b53e89ede', '::1', 1491534969, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533343931333b557365725f49447c733a313a2231223b557365726e616d657c733a353a2261646d696e223b5065726d697373696f6e7c733a373a2263726561746f72223b436f6d6d756e6974797c4e3b),
('8423064c5af181833b81a872f4fcdc347df77a1f', '::1', 1491536944, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533363739373b557365725f49447c733a313a2231223b557365726e616d657c733a353a2261646d696e223b5065726d697373696f6e7c733a373a2263726561746f72223b436f6d6d756e6974797c4e3b),
('8af6c1217509205b7219e4ee593b7a42adcf5d11', '::1', 1491536522, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533363437323b557365725f49447c733a313a2231223b557365726e616d657c733a353a2261646d696e223b5065726d697373696f6e7c733a373a2263726561746f72223b436f6d6d756e6974797c4e3b),
('8d3f608ea42960ad72029fbcac0969d6a267f9cb', '::1', 1490087546, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038373534363b),
('94573f2f82b5bfce38658b475372caf67f36dca3', '::1', 1490086221, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038353634313b),
('ac91a9d9c1f1b0c3904326bd29f03b22b665616e', '::1', 1491531969, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533313933303b),
('b1abe2542ee81520dfea9bd2b932bb9663e2bef7', '::1', 1490091738, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303039313435323b),
('b21f316693a573b4331e99b64c1d12b8721acc4d', '::1', 1490086820, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038363535343b),
('c536b3e4af0ed2805c1bee0f892c86bb75d5bba6', '::1', 1491539842, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533393834323b557365725f49447c733a313a2231223b557365726e616d657c733a353a2261646d696e223b5065726d697373696f6e7c733a373a2263726561746f72223b436f6d6d756e6974797c4e3b),
('c82702f8d6b57619cb00557ded7602c66618870d', '::1', 1491533185, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439313533333138353b),
('cb4ab129b1d2b8535d99fe5a46b7a6688bb6829b', '::1', 1490086523, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303038363232323b);

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `path` text COLLATE utf8_unicode_ci NOT NULL,
  `file_order` int(11) NOT NULL DEFAULT '1',
  `page_id` int(11) NOT NULL,
  `sub_page_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `forgetpassword`
--

CREATE TABLE `forgetpassword` (
  `fgpw_ID` int(11) NOT NULL,
  `fgpw_Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fgpw_expire` datetime NOT NULL,
  `fgpw_link` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `visited` int(11) NOT NULL DEFAULT '0',
  `showhide` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = show, 1= hide',
  `frequency_update` int(11) NOT NULL DEFAULT '0',
  `withdraw` tinyint(1) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `name`, `short_description`, `description`, `lat`, `lon`, `create_datetime`, `update_datetime`, `visited`, `showhide`, `frequency_update`, `withdraw`, `page_id`) VALUES
(1, 'สถานีพัฒนาและส่งเสริมการอนุรักษณ์สัตว์ป่าเขาพระเทว อำเภอถลาง จังหวัดภูเก็ต', '“คืนชะนีสู่ป่า”  เป็นโครงการช่วยเหลือชะนี รวมทั้งสัตว์ป่าอื่นๆ ให้สามารถกลับคืนสู่ธรรมชาติได้มากที่สุด ', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">สถานีพัฒนาและส่งเสริมการอนุรักษณ์สัตว์ป่าเขาพระเทว อำเภอถลาง จังหวัดภูเก็ต</span></p>\r\n<p><span style="font-family: arial, sans, sans-serif; font-size: 13px;">หมู่ 3 104/3 ตำบล ป่าคลอก อำเภอ ถลาง จังหวัด ภูเก็ต 83110&lrm;</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ติดต่อ :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">076 260 492</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">เวลาเปิด-ปิด :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px; text-align: center;">08.00-17.30 น. ของทุกวัน</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ชะนี&rdquo; ถูกจัดเป็นสัตว์ป่าสงวนและคุ้มครอง จำพวกสัตว์เลี้ยงลูกด้วยนม ตาม พ.ร.บ.สงวนและคุ้มครองสัตว์ป่า พ.ศ.2503 แต่ก็เป็นสัตว์อีกชนิดหนึ่งที่มนุษย์ยังอุตส่าห์หามาเป็น สัตว์เลี้ยงและยังนำไปแสดงลีลาบนเวทีละครสัตว์ เลียนแบบพฤติกรรมมนุษย์ เรียกเก็บเงินจากคนดูได้เป็นกอบเป็นกำ โดยเฉพาะชะนีที่ถูกนำไปแสดงเป็น &ldquo;นางโชว์&rdquo; ที่จังหวัดภูเก็ต<br /><br />เบื้องหลังของชีวิตชะนีเหล่านี้ เมื่อหมดประโยชน์ หรือเจ้าของเลิกรัก เลิกใช้งานแล้ว น่าสงสารยิ่งนัก เพราะแทบไม่มีค่าอะไรเหลืออยู่เลย<br /><br />หลายตัวถูกปล่อยให้อดตาย ถูกกักขังในกรง ไม่ยอมปล่อยออกไปไหน เพราะมีสภาพแก่ เนื้อหนังเหี่ยว ขนร่วง บางตัวถูกนำไปปล่อยทิ้งตามยถากรรม แล้วแต่จะหากิน ซึ่งในไม่ช้าก็ต้องอดตาย เพราะชะนีที่ถูกเลี้ยงมาตลอดชีวิต จะไม่รู้จักหาอาหารกินเอง เละไม่สามารถอาศัยอยู่ในป่าได้<br /><br />เพื่อรักษาชีวิตและสืบสานพันธุ์ของสัตว์ป่าสงวนและคุ้มครองชนิดนี้ มูลนิธิช่วยชีวิตสัตว์ป่าแห่งประเทศไทย เจ้าของโครงการ คืนชะนีสู่ป่าจังหวัดภูเก็ต หรือ THE GIBBON Rehabilitation Project-GRP มีที่ทำการตั้งอยู่ บริเวณน้ำตกบางแป จังหวัดภูเก็ต ได้จัด &ldquo;โครงการคืนชะนีสู่ป่า&rdquo; ขึ้น<br /><br />โครงการ &ldquo;คืนชะนีสู่ป่า&rdquo; เป็นการช่วยเหลือชะนี รวมทั้งสัตว์ป่าอื่นๆ ให้สามารถกลับคืนสู่ธรรมชาติได้มากที่สุด รวมทั้งให้ความรู้ ความเข้าใจแก่ชุมชนท้องถิ่นและผู้สนใจ เกี่ยวกับการคุ้มครองชะนีที่มูลนิธิช่วยชีวิตสัตว์ป่าแห่งประเทศไทย ที่น้ำตกบางแป จังหวัดภูเก็ต มีชะนีพันธุ์ต่างๆ ที่ได้รับการอนุบาลไว้ เพื่อนำกลับสู่ป่า</span></p>', '8.0417519', '98.39403360000006', '2017-03-09 01:24:46', '2017-03-17 16:49:26', 0, 0, 17, 0, 2),
(2, 'น้ำตกโตนไทร', 'อุทยานน้ำตกแห่งชาติโตนไทร อยู่ในเขตป่าสงวนแห่งชาติเขาพระแถว  เป็นน้ำตก ขนาดเล็ก มีสัตว์ป่าหลายชนิด ที่อาศัยอยู่ตามธรรมชาติ', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">ต.เทพกษัตริย์ อ.ถลาง จ.ภูเก็ต</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">เวลาเปิด-ปิด :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">08.00-17.30 น. ของทุกวัน</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">น้ำตกโตนไทร ตั้งอยู่ในตำบลเทพกษัตรี อำเภอถลาง บริเวณน้ำตกเป็นสวนรุกขชาติ ร่มรื่น เป็นน้ำตก ขนาดเล็ก น้ำจะไหลแรงอยู่ในฤดูฝน อุทยานน้ำตกแห่งชาติโตนไทร อยู่ในเขตป่าสงวนแห่งชาติเขาพระแถว ในวนอุทยานมีป่าที่อุดมสมบูรณ์ที่สุด มีสัตว์ป่าหลายชนิด ที่อาศัยอยู่ตามธรรมชาติ วนอุทยานได้รับการตกแต่งให้เป็นที่พักผ่อนหย่อนใจ มีต้นไม้ใหญ่ ๆ ให้ร่มเงา ต้นไม้เหล่านี้มีอายุไม่ต่ำกว่า ๑๐๐ ปี นอกจากนี้ยังมีต้นปาล์มหลังขาวขึ้นเต็มไปหมด โดยปาล์มหลังขาวนี้เป็นต้นไม้สกุลใหม่ที่ค้นพบในภูเก็ตเท่านั้น ในบริเวณน้ำตกโตนไทร ยังมีนกหลากหลายสานพันธ์ นับร้อยชนิดเลยทีเดียว</span></p>', '8.028113', '98.36270689999992', '2017-03-09 23:54:36', '2017-03-16 21:16:30', 0, 0, 4, 0, 3),
(3, 'วัดพระทอง', 'วัดพระทอง เป็นวัดที่มีพระพุทธรูปผุดขึ้นจากพื้นดินเพียงครึ่งองค์ ', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">ซอย วัดพระทอง ตำบล เทพกระษัตรี อำเภอ ถลาง ภูเก็ต&nbsp;</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">เวลาเปิด-ปิด :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">เปิดให้เข้าชมฟรีทุกวัน เวลา 07.00-17.00 น.</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">วัดนี้มีพระพุทธรูปผุดขึ้นจากพื้นดินเพียงครึ่งองค์ เมื่อคราวศึกพระเจ้าปะดุง ยกพลมาตีเมืองถลาง พ.ศ. 2328 ทหารพม่าพยายามขุดพระผุดเพื่อนำกลับไปพม่า แต่ขุดลงไปคราวใดก็มีฝูงแตนไล่ต่อยจนต้องละความพยายาม ต่อมาชาวบ้านได้นำทองหุ้มพระพุทธรูปที่ผุดจากพื้นดินเพียงครึ่งองค์ ดังปรากฏอยู่จนถึงปัจจุบัน นอกจากนี้ยังเป็นที่ตั้งของ &ldquo;พิพิธภัณฑสถานวัดพระทอง&rdquo; เป็นที่เก็บรวบรวมโบราณวัตถุข้าวของเครื่องใช้ของชาวภูเก็ต เช่น &ldquo;จังซุ่ย&rdquo; เสื้อกันฝนชาวเหมืองแร่ดีบุก รองเท้าตีนตุกของสตรีเชื้อสายจีนที่ต้องมัดเท้าให้เล็กตามค่านิยมของสังคมสมัยนั้น&nbsp;</span></p>', '8.0336438', '98.33679770000003', '2017-03-09 23:55:23', '2017-03-16 21:16:21', 0, 0, 5, 0, 3),
(4, 'หมู่บ้านวัฒนธรรมถลาง บ้านแขนน', 'เป็นแหล่งเรียนรู้สอนวัฒนธรรมท้องถิ่น วัฒนธรรมถลาง ศึกษาวิถีชุมชน ทั้งเรื่องอาชีพ หัตถกรรม การแสดง อาหารพื้นเมือง ของว่างพื้นเมือง ขนมโบราณ', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">หมู่บ้านวัฒนธรรม บ้านแขนน ตั้งอยู่ที่หมู่ 2 ตำบลเทพกษัตรี อำเภอถลาง จังหวัดภูเก็ต</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ติดต่อ :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">โทร.มาจองล่วงหน้าอย่างน้อย 1 สัปดาห์&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">หมายเลขโทรศัพท์&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">086-6821371 , 081-89568674</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">เวลาเปิด-ปิด :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">โทร.มาจองล่วงหน้าอย่างน้อย 1 สัปดาห์&nbsp;</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">-เป็นแหล่งเรียนรู้สอนวัฒนธรรมท้องถิ่น วัฒนธรรมถลาง, วัฒนธรรมสมัยท้าวเทพกษัตรี &ndash; ท้าวศรีสุนทร<br />-เป็นแหล่งศึกษาดูงานทางด้านวัฒนธรรม วิถีชุมชน ทั้งเรื่องอาชีพ หัตถกรรม การแสดง อาหารพื้นเมือง ของว่างพื้นเมือง ขนมโบราณ<br />-เป็นแหล่งท่องเที่ยวเชิงเกษตร และการท่องเที่ยวเชิงชุมชน วัฒนธรรม ประเพณี วิถีชีวิต<br />-เป็นแหล่งศึกษาดูงานเรื่อง เศรษฐกิจพอเพียงใช้พื้นที่ว่างให้เกิดประโยชน์ เก็บผักป่า พืชสมุนไพรไว้กิน เหลือไว้ขาย ปลูกทุกอย่างที่กินได้ ไว้แบ่งปันใช้ในครอบครัวเหลือจำหน่ายเสริมรายได้ นำผักพื้นบ้านพืชสมุนไพรพื้นเมืองมาปรุงเป็นอาหาร กินเป็นอาหาร กินเป็นยา กินเป็นภูมิคุ้มกัน<br />-เป็นแหล่งดูการจัดการกิจกรรมการลงแขกโบราณ จิตอาสา อยู่ร่วมกันเป็นเครือญาติ ร่วมกันทำงานแบ่งปันผลประโยชน์ แบ่งรายได้ สร้างงาน สร้างกิจกรรมให้เกิดขึ้นในหมู่บ้าน<br /></span></p>', '8.0292504', '98.3498478', '2017-03-09 23:55:53', '2017-03-16 21:16:08', 0, 0, 4, 0, 3),
(5, 'น้ำตกบางแป', 'เป็นน้ำตกขนาดเล็ก มีสวนรุกขชาติที่ร่มรื่น และสถานอนุบาลชะนี', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{"><span data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง : หมู่ 3 104/3 ตำบล ป่าคลอก อำเภอ ถลาง จังหวัด ภูเก็ต 83110&lrm;</span></span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{"><span data-sheets-value="{" data-sheets-userformat="{">ติดต่อ :&nbsp;โทร. 076 260 492</span></span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{"><span data-sheets-value="{" data-sheets-userformat="{">เวลาเปิด-ปิด :&nbsp;</span></span><span style="font-family: arial, sans, sans-serif; font-size: 13px; text-align: center;">08.00-17.30 น. ของทุกวัน</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">น้ำตกบางแป ไปจากตัวเมืองภูเก็ตถึงอนุสาวรีย์ทาวเทพกษัตรี ท้าวศรีสุนทร แล้วเลี้ยวขวาไปทางตำบลป่าคลอก 7 กิโลเมตร เป็นน้ำตกขนาดเล็ก มีสวนรุกขชาติที่ร่มรื่น และสถานอนุบาลชะนี ซึ่งเป็นโครงการเพื่อฟื้นฟูสภาพร่างกายและจิตใจของชะนี ที่ถูกจับมาเลี้ยงให้พร้อมที่จะกลับคืนสู้ป่า น้ำตกบางแป เป็นน้ำตกขนาดเล็ก เมื่อเทียบกับน้ำตกโตนไทร บริเวณตัวน้ำตก จะมีโขดหินค่อนข้างมากโดยเฉพาะตามธารน้ำ มีแอ่งน้ำ สามารถเล่นน้ำได้ และน้ำไหลแรงตลอดทั้งปี มีความร่มรื่นด้วยต้นไม้ขนาดใหญ่</span></p>', '8.0394155', '98.3911318', '2017-03-10 13:41:04', '2017-03-16 21:15:27', 0, 0, 5, 0, 2),
(6, 'ฟาร์มแพะ', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">ม.3 ต.ป่าคลอก บ.บางโรง อ.ถลาง จ.ภูเก็ต 83110&nbsp;</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ติดต่อ :&nbsp;0-7621-2213, 0-7621-1036</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">เวลาเปิด-ปิด :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px; text-align: center;">10:00 - 16:30 น. ของทุกวัน</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">การเที่ยวชมฟาร์มแพะ เป็นฟาร์มแพะเนื้อและแพะนม ที่ชาวบ้านเลี้ยงอย่างสะอาดและถูกสุขลักษณะ และยังสามารถร่วมรีดนมแพะได้ด้วย</span></p>', '8.05101', '98.4062401', '2017-03-15 21:34:17', '2017-03-16 21:15:15', 0, 0, 2, 0, 2),
(7, 'กลุ่มเลี้ยงหอยมุก เกาะนาคาใหญ่', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">เกาะนาคา อยู่ในเขตตำบลป่าคลอก อำเภอถลาง จังหวัดภูเก็ต&nbsp;</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">เกาะนาคา ตั้งอยู่ทางทิศตะวันออกเฉียงเหนือของเกาะภูเก็ต เป็นเกาะเล็กๆ มีพื้นที่ 50 ไร่ ความงามที่เป็นเอกลักษณ์เฉพาะตัว คือ ความเป็นธรรมชาติที่สมบูรณ์ เนื่องจาก เกาะแห่งนี้เป็นสถานที่ซึ่งใช้เลี้ยงหอยมุก อันเป็นสัญลักษณ์แห่งท้องทะเลอันดามัน ทำให้เกาะแห่งนี้ ต้องรักษาสภาพแวดล้อมทางทะเลให้คงเดิมที่สุดเพื่อให้ผลผลิตมุกที่มีคุณภาพสูง</span></p>', '8.041089', '98.4593135', '2017-03-15 21:35:59', '2017-03-16 21:15:06', 0, 0, 5, 0, 2),
(8, 'วัดพระนางสร้าง', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">หมู่ที่ 1 ตำบลเทพกระษัตรี อำเภอถลาง จังหวัดภูเก็ต</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">วัดพระนางสร้างเป็นวัดเก่าแก่มีมานานแล้ว เชื่อกันว่าสร้างก่อนพม่าเข้าทำศึกเมืองถลาง พ.ศ. 2328 มีเรื่องเล่าสืบต่อกันมาว่า พระนางเลือดขาวเป็นผู้สร้างวัด พระนางเลือดขาวเป็นผู้ศรัทธาในพระพุทธศาสนามาก เป็นมเหสีของเจ้าผู้ครองนคร แต่ถูกคนใกล้ชิดกลั่นแกล้งว่ามีชู้ จึงต้องโทษประหารชีวิต แต่พระนางเลือดขาวขอไปนมัสการพระบรมธาตุที่ลังกาก่อน คณะของนางเลือดขาวลงเรือไปถึงลังกา เมื่อกลับมาได้นำพระพุทธรูปและโบราณวัตถุหลายอย่างมาด้วย ตอนเดินทางกลับพระนางเลือดขาวได้แวะพักที่เกาะถลาง แล้วสร้างวัดไว้เป็นที่ระลึก ปลูกต้นประดู่และต้นตะเคียน พร้อมทั้งนำของมีค่าทางพุทธศาสนาเช่นพระพุทธรูปฝังไว้ในเจดีย์ด้วย ชาวบ้านจึงเรียกกันว่าวัดพระนางสร้าง เมื่อสร้างวัดเสร็จแล้วจึงออกเดินทางกลับไปยังเมืองตน</span></p>', '8.026759199999999', '98.3332388', '2017-03-15 21:54:37', '2017-03-16 21:15:58', 0, 0, 2, 0, 3),
(9, 'เกาะโหลน', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">เกาะโหลน จังหวัด ภูเก็ต&nbsp;</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">มนต์เสน่ห์ในเรื่องของธรรมชาติที่งดงาม ประกอบกับอาคารบ้านเรือนที่ยังมีความเก่าแก่หลงเหลืออยู่มากก็ทำให้เมืองท่องเที่ยวระดับโลกอย่างภูเก็ตนั้นมีชื่อเสียงขจรขจายไปอย่างมากในหมู่นักท่องเที่ยวชาวไทย และบรรดานักท่องเที่ยวชาวต่างชาติ ที่ต่างก็ต้องการมาเที่ยวยังสถานที่ท่องเที่ยวระดับโลกที่มีความน่าสนใจหลากหลาย โดยอย่างที่ เกาะโหลน ก็ถือว่าเป็นอีกสถานที่ท่องเที่ยวที่มีแต่คนชื่นชมอย่างมากในความสวยงาม จนทำให้มันกลายเป็นอีกหนึ่งจุดที่มีความน่าสนใจเป็นอย่างมากในการมาเที่ยวภูเก็ต</span></p>', '7.7875371', '98.3549372', '2017-03-16 20:14:21', '2017-03-16 21:17:34', 0, 0, 5, 0, 4),
(10, 'rawoy ร้านอาหารซีฟู้ด', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">เกาะโหลน จังหวัด ภูเก็ต&nbsp;</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ร้านอาหารซีฟู้ดชื่อดังแห่งหนึ่งของเกาะโหลน ที่มีชื่อร้านว่า ร้าน rawoy</span></p>', '7.7875371', '98.3549372', '2017-03-16 20:18:17', '2017-03-16 21:17:25', 0, 0, 2, 0, 4),
(11, 'ผ้าบาติก', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง : เกาะโหลน จังหวัด ภูเก็ต</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">กิจกรรมบาติก Runway Industry Search<br />ปล่อยจินตนาการให้โลดแล่นบนผืนผ้าบาติกออกมาให้สวย<br />ที่สุดพร้อมทั้งระดมความคิดสร้างมูลค่าเพิ่มให้กับผ้าบาติกจนกลายเป็น<br />ผลิตภัณฑ์ใหม่ที่น่าใช้ มาดูกันว่าทีมไหนไอเดียจะปังที่สุด!&nbsp;</span></p>', '7.7875371', '98.3549372', '2017-03-16 20:21:19', '2017-03-16 21:16:57', 0, 0, 1, 0, 4),
(12, 'โฮมสเตย์ เกาะโหลน', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง : เกาะโหลน จังหวัด ภูเก็ต</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">เกาะโหลน เป็นเกาะที่มีแหล่งท่องเที่ยวที่สวยงาม เป็นเกาะที่ยังคงมีบรรยากาศเงียบสงบ นักท่องเที่ยวไม่มากนัก เหมาะสำหรับผู้ที่ต้องการหลีกหนีความวุ่นวายในจังหวัดภูเก็ต เนื่องจากยังไม่ได้รับการพัฒนามากนัก โดยทางชายฝั่งทิศตะวันออกเฉียงเหนือของเกาะ มีชายหาดที่ยาวมาก มักเป็นที่ชื่นชอบของนักท่องเที่ยวที่รักสันโดษเป็นอย่างยิ่ง&nbsp;</span></p>', '7.7875371', '98.3549372', '2017-03-16 20:22:21', '2017-04-07 11:12:37', 0, 0, 9, 0, 4),
(13, 'จุดชมวิวบางเทาภูวิว', '', '<p>ที่ตั้ง : ตำบล เชิงทะเล อำเภอ ถลาง จังหวัดภูเก็ต</p>', '7.9763467', '98.2968383', '2017-03-16 20:24:33', '2017-03-16 21:18:08', 0, 0, 1, 0, 5),
(14, 'ตลาดนัดทินเล', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">ตำบล เชิงทะเล อำเภอ ถลาง จังหวัดภูเก็ต</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">เวลาเปิด-ปิด :&nbsp;เปิดบริการเวลา 16:00-22:00 น. วันพุธ ศุกร์ เสาร์ อาทิตย์</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ตลาดนัดทินเลประกอบด้วยกองทัพสินค้า อาหาร เครื่องดื่ม งานศิลปะ งานไอเดีย งานแฮนด์เมด Food Truck บาร์ มากกว่า 300 ร้านค้า เก้าอี้นั่งทานอาหารกว่า 400 ที่นั่ง บนพื้นที่กว่า 7 ไร่ ที่จอดรถกว่า 500 คันหลากหลาย และครอบคลุมทุกความต้องการ รอต้อนรับทั้งคนท้องถิ่น นักท่องเที่ยวชาวไทย และต่างชาติ โดยเฉพาะพื้นที่เชิงทะเล บางเทา หาดสุรินทร์ กมลา ป่าตอง บ้านเคียน ถลาง ป่าคลอก อนุสาวรีย์ท้าวเทพกษัตรีย์-ท้าวศรีสุนทร รวมถึงพื้นที่ใกล้เคียง<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="font-family: arial, sans, sans-serif; font-size: 13px;">นอกจากพื้นที่จำหน่ายสินค้า และอาหาร ตลาดนัดทินเลยังมีพื้นที่จัดงานอีเว้นท์ จัดโปรโมชั่นสินค้าจัดกิจกรรมต่างๆ โดยเปิดโอกาสให้น้องๆ เยาวชนได้มาใช้พื้นที่แสดงออกในทางที่ดี ไม่ว่าจะเป็นการเล่นดนตรี ร้อง เล่น เต้นรำ</span></p>', '7.9857515', '98.3010424', '2017-03-16 20:26:24', '2017-03-16 21:18:00', 0, 0, 2, 0, 5),
(15, 'ไร่สัปปะรด', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง : ตำบล เชิงทะเล อำเภอ ถลาง จังหวัดภูเก็ต</span></p>', '7.9771528', '98.2967285', '2017-03-16 20:29:21', '2017-03-16 21:17:52', 0, 0, 1, 0, 5),
(16, 'ข้าวยำบ้านบางเทา Phuket', '', '<p>ที่ตั้ง : ตำบล เชิงทะเล อำเภอ ถลาง จังหวัด ภูเก็ต</p>', '7.9857515', '98.3010424', '2017-03-16 20:33:24', '2017-03-16 21:17:44', 0, 0, 1, 0, 5),
(17, 'สะพานสารสิน', '', '<p><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">ที่ตั้ง : อำเภอ ถลาง จังหวัด ภูเก็ต</span></p>', '8.2016017', '98.2979814', '2017-03-16 20:39:44', '2017-03-16 21:18:37', 0, 0, 1, 0, 6),
(18, 'ประติมากรรมสึนามิ', 'สวนสาธารณะกมลา', '<p>ที่ตั้ง : ซอย ริมหาด ตำบล กมลา อำเภอ กะทู้ จังหวัด ภูเก็ต</p>', '7.9513355', '98.2837503', '2017-03-16 20:43:39', '2017-03-16 21:25:44', 0, 0, 1, 0, 7),
(19, 'ฟาร์มม้า', 'ฟาร์มม้า กมลา อิมรอน ฟาร์ม', '<p>ที่ตั้ง : ตำบล กมลา อำเภอ กะทู้ จังหวัด ภูเก็ต</p>\r\n<p>เวลาเปิด-ปิด : ทุกวัน</p>', '7.9488022', '98.2949044', '2017-03-16 20:45:27', '2017-03-16 21:25:57', 0, 0, 1, 0, 7),
(20, 'ป่าชายเลน', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง : อำเภอ ถลาง จังหวัด ภูเก็ต</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">เป็นป่าซึ่งเขียวชอุ่มตลอดปี เป็นบริเวณคลองน้ำจืดที่ทะเลท่วมถึง ป่าชายเลนแห่งนี้เกิดจากการกำบังพื้นที่ของแม่น้ำลำธารจากหลายสายไหลลงสู่ทะเล โดยเฉพาะบริเวณปากแม่น้ำ ป่าชายเลนผืนนี้มีอาณาเขตประมาณ 1 ตารางกิโลเมตร นับว่าเป็นป่าชายเลนธรรมชาติผืนใหญ่สุดท้ายของจังหวัดภูเก็ตก็ว่าได้ ป่าผืนนี้นอกจากจะเป็นแหล่งที่อยู่อาศัย เพาะพันธุ์ หลบภัย และหากินของสัตว์ป่าและสัตว์น้ำชนิดต่างๆ อย่างเช่น ลิงแสม นกนางนวลแกลบสีกุหลาบ นกคอสั้นตีนไว นกกินเปี้ยว นกชายเลนปากแอ่น นกชายเลนปากแอ่นหางลาย นกอีก๋อยเล็ก นกกวัก นกอัญชันอกเทา นกออก เหยี่ยวแดง อีกา เหี้ย งูพังกา งูกะปะ ปลาเก๋า ปลากระบอก ปลาดุกทะเล ปลาจวด ปลาตีน ปลาหัวตะกั่ว ปลาทับตะเภา ปลากด ปลาเข็ม กุ้ง หอย ปู และอื่นๆ&nbsp;</span></p>', '8.1908789', '98.2895221', '2017-03-16 20:59:47', '2017-03-16 21:18:29', 0, 0, 1, 0, 6),
(21, 'หาดทรายแก้ว', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง : อำเภอ ถลาง จังหวัด ภูเก็ต </span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">สำหรับเมืองท่องเที่ยวริมทะเลของประเทศไทยนั้นมีอยู่อย่างมากมาย แต่สำหรับเกาะภูเก็ตแล้ว ที่นี่ถือเป็นแหล่งทรัพยากรทางด้านการท่องเที่ยวที่มีค่ามากที่สุดของประเทศ แถมมันยังติดอันดับเป็นเมืองท่องเที่ยวยอดนิยมของโลกอีกด้วย ทำให้ในแต่ละปีนั้นจะมีนักท่องเที่ยวต่างชาติที่จะมาเที่ยวภูเก็ตกันเป็นจำนวนมาก และแน่นอนว่าสถานที่ท่องเที่ยวที่มีความน่าสนใจภายในภูเก็ต อย่าง หาดทรายแก้ว ก็ได้รับความสนใจในการมาเที่ยวชมกันอย่างคึกคักทุกวันเลยทีเดียว ทำให้สร้างรายได้จำนวนมหาศาลเข้าเมืองภูเก็ตและเมืองไทย</span></p>', '8.193793', '98.2918555', '2017-03-16 21:00:49', '2017-03-16 21:18:20', 0, 0, 1, 0, 6),
(22, 'หลาดใหญ่', 'Let’s Phuketian, be Phuketian', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">ถนนถลาง เทศบาลนครภูเก็ต ตำบลตลาดใหญ่ อำเภอเมือง จังหวัดภูเก็ต</span></p>\r\n<p><span style="font-family: arial, sans, sans-serif; font-size: 13px;">เวลาเปิด-ปิด : &nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">ทุกวันอาทิตย์ เวลา 16.00 &ndash; 22.00 น.&nbsp;</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">โครงการถนนน่าเดิน &ldquo;หลาดใหญ่&rdquo; (Lard Yai) ที่จัดขึ้นเพื่อส่งเสริมการท่องเที่ยวเชิงอนุรักษ์ด้านศิลปวัฒนธรรม เผยแพร่วิถีชีวิตของชุมชนย่านการค้า และเพิ่มแหล่งท่องเที่ยวให้จังหวัดภูเก็ต สำหรับนักท่องเที่ยวทั้งคนไทย และต่างชาติ พร้อมส่งเสริมเศรษฐกิจในย่านการค้าเมืองเก่า ทำให้ชุมชนมีรายได้เพิ่มขึ้น โครงการดังกล่าวจัดตั้งภายใต้แนวคิด Let&rsquo;s Phuketian, be Phuketian หรือ &ldquo;ให้ชาวภูเก็ต เป็นชาวภูเก็ต&rdquo; คือให้เกิดการรับรู้ และมีความเข้าใจถึงรากฐานของวิถีชีวิต และเพื่อให้ประชาชนย่านเมืองเก่าได้เปิดบ้านต้อนรับนักท่องเที่ยว และประชาชนในจังหวัดได้มาลิ้มรสอาหารพื้นเมือง หรือซื้อหาสินค้า นอกจากนี้ได้มีการเล่าเรื่องวัฒนธรรมผ่านกิจกรรมต่างๆ บนถนนน่าเดิน ซึ่งได้ถูกแบ่งออกเป็น 4 โซน ได้แก่ โซนกิจกรรม โซนของที่ระลึก โซนของนักเรียน นักศึกษา และโซนจำหน่ายอาหาร</span></p>', '7.8846898', '98.3894151', '2017-03-16 21:06:55', '2017-03-16 21:14:47', 0, 0, 2, 0, 8),
(23, 'ลานมังกร', 'เป็นสวนสาธารณะที่เทศบาลนครภูเก็ตร่วมกับประชาชนสร้างขึ้นเนื่องในปีมหามงคลสมเด็จพระนางเจ้าฯ พระบรมราชินีนาถ ทรงเจริญพระชนมพรรษาครบ 6 รอบ (พ.ศ.2547) ', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">ที่ตั้ง : ตำบล ตลาดใหญ่ อำเภอเมืองภูเก็ต ภูเก็ต</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{" data-sheets-userformat="{">สวนเฉลิมพระเกียรติ 72 พรรษา มหาราชินี หรือลานมังกรทองเป็นสวนสาธารณะที่เทศบาลนครภูเก็ตร่วมกับประชาชนสร้างขึ้นเนื่องในปีมหามงคลสมเด็จพระนางเจ้าฯ พระบรมราชินีนาถ ทรงเจริญพระชนมพรรษาครบ 6 รอบ (พ.ศ.2547) <br /> <br />จากพื้นที่เดิมของการไฟฟ้าภูเก็ต มีการปรับแต่งภูมิทัศน์ให้กลายเป็นสวนสาธารณะ จุดเด่นคือด้านทิศใต้ติดถนนถลางได้จัดสร้างประติมากรรมพญามังกรทะเล (ฮ่ายเหล็งอ๋อง) สัตว์มงคลที่ยิ่งใหญ่ตามตำราโหวงเฮ้งของจีน ลักษณะเป็นมังกรสีทองขนาดใหญ่โดดเด่นสะดุดตาผู้ผ่านไปมา คนภูเก็ตจึงเรียกกันติดปากว่า ลานมังกรทอง</span></p>', '7.8861269', '98.39191800000003', '2017-03-16 21:11:52', '2017-03-16 21:14:35', 0, 0, 1, 0, 8),
(24, 'ศาลเจ้าแสงธรรม', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;\\u0e28\\u0e32\\u0e25\\u0e40\\u0e08\\u0e49\\u0e32\\u0e41\\u0e2a\\u0e07\\u0e18\\u0e23\\u0e23\\u0e21 \\u0e19\\u0e2d\\u0e01\\u0e08\\u0e32\\u0e2a\\u0e16\\u0e32\\u0e19\\u0e17\\u0e35\\u0e48\\u0e15\\u0e31\\u0e49\\u0e07\\u0e2d\\u0e31\\u0e19\\u0e2a\\u0e31\\u0e19\\u0e42\\u0e14\\u0e29\\u0e41\\u0e25\\u0e30\\u0e40\\u0e07\\u0e35\\u0e22\\u0e1a\\u0e2a\\u0e07\\u0e1a\\u0e2d\\u0e22\\u0e39\\u0e48\\u0e43\\u0e19\\u0e43\\u0e08\\u0e01\\u0e25\\u0e32\\u0e07\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\\u0e41\\u0e25\\u0e49\\u0e27 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e33\\u0e04\\u0e31\\u0e0d\\u0e04\\u0e37\\u0e2d\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e27\\u0e22\\u0e07\\u0e32\\u0e21\\u0e02\\u0e2d\\u0e07\\u0e2a\\u0e16\\u0e32\\u0e1b\\u0e31\\u0e15\\u0e22\\u0e01\\u0e23\\u0e23\\u0e21\\u0e43\\u0e19\\u0e23\\u0e39\\u0e1b\\u0e41\\u0e1a\\u0e1a\\u0e08\\u0e35\\u0e19\\u0e1b\\u0e23\\u0e30\\u0e40\\u0e1e\\u0e13\\u0e35\\u0e41\\u0e25\\u0e49\\u0e27 \\u0e42\\u0e14\\u0e22\\u0e15\\u0e31\\u0e27\\u0e2d\\u0e32\\u0e04\\u0e32\\u0e23\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e23\\u0e39\\u0e1b\\u0e41\\u0e1a\\u0e1a\\u0e40\\u0e01\\u0e4b\\u0e07\\u0e08\\u0e35\\u0e19\\u0e02\\u0e19\\u0e32\\u0e14\\u0e40\\u0e25\\u0e47\\u0e01 \\u0e01\\u0e30\\u0e17\\u0e31\\u0e14\\u0e23\\u0e31\\u0e14 \\u0e2a\\u0e48\\u0e27\\u0e19\\u0e1a\\u0e19\\u0e2b\\u0e25\\u0e31\\u0e07\\u0e04\\u0e32\\u0e21\\u0e35\\u0e1b\\u0e39\\u0e19\\u0e1b\\u0e31\\u0e49\\u0e19\\u0e23\\u0e39\\u0e1b\\u0e21\\u0e31\\u0e07\\u0e01\\u0e23\\u0e41\\u0e25\\u0e30\\u0e15\\u0e38\\u0e4a\\u0e01\\u0e15\\u0e32\\u0e08\\u0e35\\u0e19\\u0e0b\\u0e36\\u0e48\\u0e07\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e17\\u0e35\\u0e48\\u0e19\\u0e34\\u0e22\\u0e21\\u0e43\\u0e19\\u0e21\\u0e13\\u0e11\\u0e25\\u0e2e\\u0e01\\u0e40\\u0e01\\u0e35\\u0e49\\u0e22\\u0e19\\u0e1b\\u0e23\\u0e30\\u0e14\\u0e31\\u0e1a\\u0e15\\u0e01\\u0e41\\u0e15\\u0e48\\u0e07\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e2a\\u0e27\\u0e22\\u0e07\\u0e32\\u0e21 \\u0e19\\u0e2d\\u0e01\\u0e08\\u0e32\\u0e19\\u0e35\\u0e49\\u0e20\\u0e32\\u0e22\\u0e43\\u0e19\\u0e28\\u0e32\\u0e25\\u0e40\\u0e08\\u0e49\\u0e32\\u0e22\\u0e31\\u0e07\\u0e21\\u0e35\\u0e20\\u0e32\\u0e1e\\u0e40\\u0e02\\u0e35\\u0e22\\u0e19\\u0e25\\u0e32\\u0e22\\u0e40\\u0e2a\\u0e49\\u0e19\\u0e2a\\u0e35\\u0e14\\u0e33\\u0e43\\u0e19\\u0e15\\u0e32\\u0e23\\u0e32\\u0e07\\u0e2a\\u0e35\\u0e48\\u0e40\\u0e2b\\u0e25\\u0e35\\u0e48\\u0e22\\u0e21\\u0e40\\u0e15\\u0e47\\u0e21\\u0e1d\\u0e32\\u0e14\\u0e49\\u0e32\\u0e19\\u0e02\\u0e49\\u0e32\\u0e07\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2a\\u0e2d\\u0e07\\u0e14\\u0e49\\u0e32\\u0e19\\u0e14\\u0e33\\u0e40\\u0e19\\u0e34\\u0e19\\u0e40\\u0e23\\u0e37\\u0e48\\u0e2d\\u0e07 \\u0e0b\\u0e34\\u0e22\\u0e34\\u0e48\\u0e19\\u0e01\\u0e38\\u0e49\\u0e22 \\u0e41\\u0e2b\\u0e48\\u0e07\\u0e23\\u0e32\\u0e0a\\u0e27\\u0e07\\u0e28\\u0e4c\\u0e01\\u0e38\\u0e49\\u0e07 \\u0e2d\\u0e31\\u0e19\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e15\\u0e33\\u0e19\\u0e32\\u0e19\\u0e41\\u0e2b\\u0e48\\u0e07\\u0e2d\\u0e14\\u0e35\\u0e15\\u0e02\\u0e2d\\u0e07\\u0e40\\u0e17\\u0e1e\\u0e2d\\u0e4b\\u0e2d\\u0e07\\u0e0b\\u0e38\\u0e19\\u0e15\\u0e48\\u0e32\\u0e22\\u0e2a\\u0e32\\u0e22 \\u0e40\\u0e21\\u0e37\\u0e48\\u0e2d\\u0e04\\u0e23\\u0e31\\u0e49\\u0e07\\u0e40\\u0e2a\\u0e27\\u0e22\\u0e0a\\u0e32\\u0e15\\u0e34\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e21\\u0e19\\u0e38\\u0e29\\u0e22\\u0e4c\\u0e21\\u0e32\\u0e01\\u0e48\\u0e2d\\u0e19&quot;}" data-sheets-userformat="{&quot;2&quot;:961,&quot;3&quot;:{&quot;1&quot;:0},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0}">ที่ตั้ง : ถ.พังงา อ.เมือง จ.ภูเก็ต</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;\\u0e28\\u0e32\\u0e25\\u0e40\\u0e08\\u0e49\\u0e32\\u0e41\\u0e2a\\u0e07\\u0e18\\u0e23\\u0e23\\u0e21 \\u0e19\\u0e2d\\u0e01\\u0e08\\u0e32\\u0e2a\\u0e16\\u0e32\\u0e19\\u0e17\\u0e35\\u0e48\\u0e15\\u0e31\\u0e49\\u0e07\\u0e2d\\u0e31\\u0e19\\u0e2a\\u0e31\\u0e19\\u0e42\\u0e14\\u0e29\\u0e41\\u0e25\\u0e30\\u0e40\\u0e07\\u0e35\\u0e22\\u0e1a\\u0e2a\\u0e07\\u0e1a\\u0e2d\\u0e22\\u0e39\\u0e48\\u0e43\\u0e19\\u0e43\\u0e08\\u0e01\\u0e25\\u0e32\\u0e07\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\\u0e41\\u0e25\\u0e49\\u0e27 \\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e33\\u0e04\\u0e31\\u0e0d\\u0e04\\u0e37\\u0e2d\\u0e04\\u0e27\\u0e32\\u0e21\\u0e2a\\u0e27\\u0e22\\u0e07\\u0e32\\u0e21\\u0e02\\u0e2d\\u0e07\\u0e2a\\u0e16\\u0e32\\u0e1b\\u0e31\\u0e15\\u0e22\\u0e01\\u0e23\\u0e23\\u0e21\\u0e43\\u0e19\\u0e23\\u0e39\\u0e1b\\u0e41\\u0e1a\\u0e1a\\u0e08\\u0e35\\u0e19\\u0e1b\\u0e23\\u0e30\\u0e40\\u0e1e\\u0e13\\u0e35\\u0e41\\u0e25\\u0e49\\u0e27 \\u0e42\\u0e14\\u0e22\\u0e15\\u0e31\\u0e27\\u0e2d\\u0e32\\u0e04\\u0e32\\u0e23\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e23\\u0e39\\u0e1b\\u0e41\\u0e1a\\u0e1a\\u0e40\\u0e01\\u0e4b\\u0e07\\u0e08\\u0e35\\u0e19\\u0e02\\u0e19\\u0e32\\u0e14\\u0e40\\u0e25\\u0e47\\u0e01 \\u0e01\\u0e30\\u0e17\\u0e31\\u0e14\\u0e23\\u0e31\\u0e14 \\u0e2a\\u0e48\\u0e27\\u0e19\\u0e1a\\u0e19\\u0e2b\\u0e25\\u0e31\\u0e07\\u0e04\\u0e32\\u0e21\\u0e35\\u0e1b\\u0e39\\u0e19\\u0e1b\\u0e31\\u0e49\\u0e19\\u0e23\\u0e39\\u0e1b\\u0e21\\u0e31\\u0e07\\u0e01\\u0e23\\u0e41\\u0e25\\u0e30\\u0e15\\u0e38\\u0e4a\\u0e01\\u0e15\\u0e32\\u0e08\\u0e35\\u0e19\\u0e0b\\u0e36\\u0e48\\u0e07\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e17\\u0e35\\u0e48\\u0e19\\u0e34\\u0e22\\u0e21\\u0e43\\u0e19\\u0e21\\u0e13\\u0e11\\u0e25\\u0e2e\\u0e01\\u0e40\\u0e01\\u0e35\\u0e49\\u0e22\\u0e19\\u0e1b\\u0e23\\u0e30\\u0e14\\u0e31\\u0e1a\\u0e15\\u0e01\\u0e41\\u0e15\\u0e48\\u0e07\\u0e2d\\u0e22\\u0e48\\u0e32\\u0e07\\u0e2a\\u0e27\\u0e22\\u0e07\\u0e32\\u0e21 \\u0e19\\u0e2d\\u0e01\\u0e08\\u0e32\\u0e19\\u0e35\\u0e49\\u0e20\\u0e32\\u0e22\\u0e43\\u0e19\\u0e28\\u0e32\\u0e25\\u0e40\\u0e08\\u0e49\\u0e32\\u0e22\\u0e31\\u0e07\\u0e21\\u0e35\\u0e20\\u0e32\\u0e1e\\u0e40\\u0e02\\u0e35\\u0e22\\u0e19\\u0e25\\u0e32\\u0e22\\u0e40\\u0e2a\\u0e49\\u0e19\\u0e2a\\u0e35\\u0e14\\u0e33\\u0e43\\u0e19\\u0e15\\u0e32\\u0e23\\u0e32\\u0e07\\u0e2a\\u0e35\\u0e48\\u0e40\\u0e2b\\u0e25\\u0e35\\u0e48\\u0e22\\u0e21\\u0e40\\u0e15\\u0e47\\u0e21\\u0e1d\\u0e32\\u0e14\\u0e49\\u0e32\\u0e19\\u0e02\\u0e49\\u0e32\\u0e07\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2a\\u0e2d\\u0e07\\u0e14\\u0e49\\u0e32\\u0e19\\u0e14\\u0e33\\u0e40\\u0e19\\u0e34\\u0e19\\u0e40\\u0e23\\u0e37\\u0e48\\u0e2d\\u0e07 \\u0e0b\\u0e34\\u0e22\\u0e34\\u0e48\\u0e19\\u0e01\\u0e38\\u0e49\\u0e22 \\u0e41\\u0e2b\\u0e48\\u0e07\\u0e23\\u0e32\\u0e0a\\u0e27\\u0e07\\u0e28\\u0e4c\\u0e01\\u0e38\\u0e49\\u0e07 \\u0e2d\\u0e31\\u0e19\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e15\\u0e33\\u0e19\\u0e32\\u0e19\\u0e41\\u0e2b\\u0e48\\u0e07\\u0e2d\\u0e14\\u0e35\\u0e15\\u0e02\\u0e2d\\u0e07\\u0e40\\u0e17\\u0e1e\\u0e2d\\u0e4b\\u0e2d\\u0e07\\u0e0b\\u0e38\\u0e19\\u0e15\\u0e48\\u0e32\\u0e22\\u0e2a\\u0e32\\u0e22 \\u0e40\\u0e21\\u0e37\\u0e48\\u0e2d\\u0e04\\u0e23\\u0e31\\u0e49\\u0e07\\u0e40\\u0e2a\\u0e27\\u0e22\\u0e0a\\u0e32\\u0e15\\u0e34\\u0e40\\u0e1b\\u0e47\\u0e19\\u0e21\\u0e19\\u0e38\\u0e29\\u0e22\\u0e4c\\u0e21\\u0e32\\u0e01\\u0e48\\u0e2d\\u0e19&quot;}" data-sheets-userformat="{&quot;2&quot;:961,&quot;3&quot;:{&quot;1&quot;:0},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0}">เวลาเปิด-ปิด :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">ทุกวัน 08.30-17.00 น.</span></p>\r\n<p>ศาลเจ้าแสงธรรม นอกจาสถานที่ตั้งอันสันโดษและเงียบสงบอยู่ในใจกลางเมืองแล้ว ความสำคัญคือความสวยงามของสถาปัตยกรรมในรูปแบบจีนประเพณีแล้ว โดยตัวอาคารเป็นรูปแบบเก๋งจีนขนาดเล็ก กะทัดรัด ส่วนบนหลังคามีปูนปั้นรูปมังกรและตุ๊กตาจีนซึ่งเป็นที่นิยมในมณฑลฮกเกี้ยนประดับตกแต่งอย่างสวยงาม นอกจานี้ภายในศาลเจ้ายังมีภาพเขียนลายเส้นสีดำในตารางสี่เหลี่ยมเต็มฝาด้านข้างทั้งสองด้านดำเนินเรื่อง ซิยิ่นกุ้ย แห่งราชวงศ์กุ้ง อันเป็นตำนานแห่งอดีตของเทพอ๋องซุนต่ายสาย เมื่อครั้งเสวยชาติเป็นมนุษย์มาก่อน</p>', '7.8851085', '98.3858602', '2017-03-16 21:14:26', '2017-03-16 21:14:26', 0, 0, 0, 0, 8),
(25, 'อาหารพื้นบ้าน', '', '', '7.8846898', '98.3894151', '2017-03-16 21:19:45', '2017-03-16 21:19:45', 0, 0, 0, 0, 8),
(26, 'จุดชมวิวกมลา', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;\\u0e40\\u0e2b\\u0e47\\u0e19\\u0e27\\u0e34\\u0e27\\u0e17\\u0e31\\u0e49\\u0e07\\u0e2b\\u0e21\\u0e14\\u0e02\\u0e2d\\u0e07\\u0e0a\\u0e32\\u0e22\\u0e2b\\u0e32\\u0e14\\u0e01\\u0e21\\u0e25\\u0e32\\u0e41\\u0e25\\u0e30\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e1a\\u0e49\\u0e32\\u0e19&quot;}" data-sheets-userformat="{&quot;2&quot;:963,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16773836},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0}">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">ตำบล กมลา อำเภอ กะทู้ จังหวัด ภูเก็ต</span></p>\r\n<p><span style="font-family: arial, sans, sans-serif; font-size: 13px;">เวลาเปิด-ปิด : ทุกวัน</span></p>\r\n<p><span style="font-family: arial, sans, sans-serif; font-size: 13px;">เห็นวิวทั้งหมดของชายหาดกมลาและหมู่บ้าน</span></p>', '7.9630946', '98.2828929', '2017-03-16 21:26:51', '2017-03-16 21:26:51', 0, 0, 0, 0, 7),
(27, 'สบู่เหลว ขมิ้นผสมน้ำผึ้ง', '', '<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;\\u0e23\\u0e49\\u0e32\\u0e19\\u0e04\\u0e49\\u0e32\\u0e0a\\u0e38\\u0e21\\u0e0a\\u0e19\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e01\\u0e21\\u0e25\\u0e32&quot;}" data-sheets-userformat="{&quot;2&quot;:963,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16773836},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0}">ที่ตั้ง :&nbsp;</span><span style="font-family: arial, sans, sans-serif; font-size: 13px;">ซอย ริมหาด ตำบล กมลา อำเภอ กะทู้ จังหวัด ภูเก็ต<br /></span></p>\r\n<p><span style="font-family: arial, sans, sans-serif; font-size: 13px;">เวลาเปิด-ปิด : ทุกวัน</span></p>\r\n<p><span style="font-size: 13px; font-family: arial, sans, sans-serif;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;\\u0e23\\u0e49\\u0e32\\u0e19\\u0e04\\u0e49\\u0e32\\u0e0a\\u0e38\\u0e21\\u0e0a\\u0e19\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e01\\u0e21\\u0e25\\u0e32&quot;}" data-sheets-userformat="{&quot;2&quot;:963,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16773836},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0}">ร้านค้าชุมชนตำบลกมลา</span></p>', '7.9513355', '98.2837503', '2017-03-16 21:28:04', '2017-03-16 21:28:04', 0, 0, 0, 0, 7),
(28, 'wqr', 'rqw', '<p>rqw</p>', '7.900577973795131', '98.38213920593262', '2017-03-17 00:28:58', '2017-03-17 04:06:15', 0, 0, 1, 1, 3),
(29, 'qwr', '', '', '7.900577973795131', '98.38213920593262', '2017-03-17 00:31:16', '2017-03-17 00:31:18', 0, 0, 0, 1, 2),
(30, 'ewrew', '', '', '7.900577973795131', '98.38213920593262', '2017-03-17 00:32:53', '2017-03-17 00:32:59', 0, 0, 0, 1, 2),
(31, 'ewrewr', '', '', '7.900577973795131', '98.38213920593262', '2017-03-17 00:33:57', '2017-03-17 00:34:03', 0, 0, 0, 1, 2),
(32, 'wqr', '', '', '7.900577973795131', '98.38213920593262', '2017-03-17 00:36:19', '2017-03-17 04:06:10', 0, 0, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `item_en`
--

CREATE TABLE `item_en` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description_en` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_en`
--

INSERT INTO `item_en` (`id`, `name_en`, `short_description_en`, `description_en`, `update_datetime`, `item_id`) VALUES
(1, 'The Gibbon Rehabilitation Project', '', '', '2017-03-17 16:49:26', 1),
(2, 'est', 'est', '<p>est</p>', '0000-00-00 00:00:00', 2),
(3, 'r23r', '32r32r', '<p>32r32</p>', '0000-00-00 00:00:00', 3),
(4, 'qwr', 'rqw', '<p>rwqrwqr</p>', '0000-00-00 00:00:00', 4),
(5, 'Bang Pae Waterfall', 'qaqa', '<p>aqaqa</p>', '2017-03-15 21:29:40', 5),
(6, 'Goat Farm', '', '', '0000-00-00 00:00:00', 6),
(7, '', '', '', '0000-00-00 00:00:00', 7),
(8, '', '', '', '0000-00-00 00:00:00', 8),
(9, 'Lone Island', '', '', '0000-00-00 00:00:00', 9),
(10, 'rawoy', '', '', '0000-00-00 00:00:00', 10),
(11, '', '', '', '0000-00-00 00:00:00', 11),
(12, '', '', '', '0000-00-00 00:00:00', 12),
(13, '', '', '', '0000-00-00 00:00:00', 13),
(14, '', '', '', '0000-00-00 00:00:00', 14),
(15, '', '', '', '0000-00-00 00:00:00', 15),
(16, '', '', '', '0000-00-00 00:00:00', 16),
(17, '', '', '', '0000-00-00 00:00:00', 17),
(18, '', '', '', '0000-00-00 00:00:00', 18),
(19, '', '', '', '0000-00-00 00:00:00', 19),
(20, '', '', '', '0000-00-00 00:00:00', 20),
(21, '', '', '', '0000-00-00 00:00:00', 21),
(22, '', '', '', '0000-00-00 00:00:00', 22),
(23, '', '', '', '0000-00-00 00:00:00', 23),
(24, '', '', '', '0000-00-00 00:00:00', 24),
(25, '', '', '', '0000-00-00 00:00:00', 25),
(26, '', '', '', '0000-00-00 00:00:00', 26),
(27, '', '', '', '0000-00-00 00:00:00', 27),
(28, 'rqw', 'qrw', '<p>qrw</p>', '0000-00-00 00:00:00', 28),
(29, '', '', '', '0000-00-00 00:00:00', 29),
(30, '', '', '', '0000-00-00 00:00:00', 30),
(31, '', '', '', '0000-00-00 00:00:00', 31),
(32, '', '', '', '0000-00-00 00:00:00', 32),
(33, NULL, NULL, NULL, '0000-00-00 00:00:00', 12),
(34, NULL, NULL, NULL, '0000-00-00 00:00:00', 12),
(35, NULL, NULL, NULL, '0000-00-00 00:00:00', 12),
(36, NULL, NULL, NULL, '0000-00-00 00:00:00', 12),
(37, NULL, NULL, NULL, '0000-00-00 00:00:00', 12);

-- --------------------------------------------------------

--
-- Table structure for table `item_picture`
--

CREATE TABLE `item_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` text COLLATE utf8_unicode_ci NOT NULL,
  `picture_order` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_picture`
--

INSERT INTO `item_picture` (`id`, `name`, `path`, `picture_order`, `item_id`) VALUES
(3, 'item_15032017211257.jpg', 'item_15032017211257.jpg', 0, 5),
(4, 'item_15032017212831.jpg', 'item_15032017212831.jpg', 0, 1),
(5, 'item_17032017002858.jpg', 'item_17032017002858.jpg', 0, 28),
(7, 'item_17032017003116.jpg', 'item_17032017003116.jpg', 0, 29),
(8, 'item_17032017003253.jpg', 'item_17032017003253.jpg', 0, 30),
(9, 'item_17032017003357.jpg', 'item_17032017003357.jpg', 0, 31);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `link` text COLLATE utf8_unicode_ci,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` int(11) NOT NULL,
  `withdraw` tinyint(1) NOT NULL DEFAULT '0',
  `attr_name` tinyint(1) NOT NULL DEFAULT '1',
  `attr_description` tinyint(1) NOT NULL DEFAULT '1',
  `attr_link` tinyint(1) NOT NULL DEFAULT '1',
  `attr_picture` tinyint(1) NOT NULL DEFAULT '1',
  `attr_slide` tinyint(1) NOT NULL DEFAULT '1',
  `attr_file` tinyint(1) NOT NULL DEFAULT '1',
  `attr_subpage` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `name`, `description`, `link`, `create_datetime`, `update_datetime`, `type`, `withdraw`, `attr_name`, `attr_description`, `attr_link`, `attr_picture`, `attr_slide`, `attr_file`, `attr_subpage`) VALUES
(1, 'หน้าหลัก', '<p>จังหวัดภูเก็ต เป็นจังหวัดที่มีเอกลักษณ์และมีวัฒนธรรมอันโดดเด่น มีแหล่งท่องเที่ยวสำคัญระดับโลก รวมทั้งความหลากหลายทางวัฒนธรรม ซึ่งสิ่งที่กล่าวนี้เป็นทรัพยากรการท่องเที่ยวที่สำคัญของจังหวัดภูเก็ต &nbsp;ปัจจุบันการท่องเที่ยวโดยชุมชน (Community Based Tourism: CBT) เป็นการบริหารจัดการและดำเนินการท่องเที่ยวโดยชุมชน &nbsp;เป็นการท่องเที่ยวประเภทหนึ่งที่นักท่องเที่ยวให้ความสนใจเพิ่มมากขึ้น ซึ่งการท่องเที่ยวชุมชนมีความเป็นเอกลักษณ์ และมีความน่าสนใจในการสะท้อนให้เห็นถึงวิถีชีวิต ศิลปวัฒนธรรม ประเพณีของชุมชน &nbsp;การท่องเที่ยวโดยชุมชน เป็นหนึ่งในการท่องเที่ยวอย่างยั่งยืน และการประชาสัมพันธ์คือสิ่งสำคัญที่เป็นสื่อกลางในการเผยแพร่ข้อมูลข่าวสารไปสู่สาธารณะชนให้นักท่องเที่ยวทั้งชาวไทยและชาวต่างประเทศเทศรับทราบข้อมูลกิจกรรมด้านการท่องเที่ยวของจังหวัดอย่างต่อเนื่อง ทันต่อสถานการณ์ และเป็นการกระตุ้นเศรษฐกิจในภาพรวมชุมชนจังหวัดภูเก็ต ดังนั้น เพื่อเป็นการส่งเสริมการท่องเที่ยวโดยชุมชน สำนักงานท่องเที่ยวและกีฬาจังหวัดภูเก็ต ได้เห็นถึงความสำคัญของการท่องเที่ยวโดยชุมชนในจังหวัดภูเก็ต &nbsp;ซึ่งเป็นอีกหนึ่งพื้นที่ที่ควรจะส่งเสริมให้เกิดการท่องเที่ยวโดยชุมชน การเข้าถึงแหล่งท่องเที่ยวต่าง ๆ จำเป็นต้องอาศัยแผนที่และสื่อในการดึงดูดความสนใจ โดยแสดงผลตำแหน่งที่ตั้งแหล่งท่องเที่ยวในระบบแผนที่ ร่วมกับการจัดทำเว็บไซต์ แอพพลิเคชั่นเพื่อแสดงผลแผนที่แหล่งท่องเที่ยวผ่านเครือข่ายอินเตอร์เน็ต เพื่อให้นักท่องเที่ยวสามารถเข้าถึงแหล่งท่องเที่ยวได้ง่ายขึ้น และสร้างช่องทางการกระจายรายได้สู่ชุมชนอย่างเป็นรูปธรรมมากขึ้น</p>', NULL, '2017-03-05 11:39:57', '2017-03-19 03:43:06', 1, 0, 1, 1, 0, 0, 0, 0, 0),
(2, 'ตำบลป่าคลอก', '<h4><strong>- ไข่มุกนาคา ภูผาน้ำตก ยกย่องเกษตร ของเขตชนบท สะกดว่าพอเพียง ร้อยเรียง ตามรอยพ่อ -</strong></h4>\r\n<p>ตำบลป่าคลอก เมื่อประมาณ 200 ปีที่ผ่านมามีสภาพเป็นป่าทึบ ต้นไม้สูงใหญ่ สัตว์ป่านานาชนิดชุกชุม ต่อมาพม่าต้องการเมืองขึ้นจึงได้ยกพล 9 ทัพ เข้ามาตีเมืองต่าง ๆ และพม่าได้ยกพลมาตีเมืองถลางส่วนหนึ่งยกทัพมาแถบบริเวณชายทะเล ได้เกิดการปะทะกันฆ่าผู้คนล้มตายเกลื่อนกลาด พม่าได้จับเด็กแล้วใช้ไฟคลิกเผา ต่อมาคุณหญิงจัน-คุณหยิงมุกได้รวบรวมคนมาต่อสู้กับพม่า จนพม่าขาดเสบียงยกทัพกลับไปจึงเรียกว่า "ป่าคลอก" จนถึงปัจจุบัน</p>', NULL, '2017-03-05 11:56:20', '2017-03-17 16:53:48', 2, 0, 1, 1, 0, 1, 0, 0, 0),
(3, 'บ้านแขนน (หมู่บ้านวัฒนธรรมถลาง)', '<h4><strong>- สืบสานวัฒนธรรม กิจกรรมโนราบิก เรียนรู้วิถีชีวิตพื้นถิ่นถลาง -</strong></h4>\r\n<p>หมู่บ้านแขนน ตั้งอยู่ หมู่ที่ 2 ตำบลเทพกระษัตรี อำเภอถลาง จังหวัดภูเก็ต &nbsp;มีเนื้อที่ประมาณ 5 ตารางกิโลเมตร &nbsp;พื้นที่บางส่วนติดกับอุทยานแห่งชาติเขาพระแทว เป็นหมู่บ้านเก่าแก่ตั้งแต่สมัยอยุธยา เพราะมีหลักฐานทางโบราณสถาน คือ หลักสถูปโบราณ ๓ องค์ มีวัดร้าง ๑ แห่ง และประวัติศาสตร์เกี่ยวกับวีรสตรีไทยท้าวเทพกระษัตรี &ndash; ท้าวศรีสุนทร &nbsp;ตอนสู้รบกับพม่าเพื่อรักษาเมืองถลาง &nbsp;เดิมบ้านแขนนมีพื้นที่กว้างขวางมีอาณาเขตตั้งแต่ หมู่ที่ ๑ บ้านบ่อกรวด ไปจรดเทือกเขาพระแทว แต่ต่อมามีการแยกพื้นที่ออกเป็น ๒ ส่วน คือ หมู่ที่ ๒ บ้านแขนน และหมู่ที่ ๑๑ บ้านควน &nbsp;สำหรับชื่อที่มาของหมู่บ้าน คำว่า บ้านแขนน มาจากข้อสันนิษฐาน ๒ ข้อ ดังนี้</p>\r\n<p dir="ltr">ข้อแรก &nbsp;บ้านแขนนน่าจะมาจากคำว่า &ldquo;บ้านบางแน่น&rdquo; เมื่อพูดเร็วๆและพูดสั้นๆ ตามสำเนียงคนภูเก็ต จึงกลายเป็นคำว่า บ้านแขนน &nbsp;ซึ่งไม่มีความหมาย ส่วนที่มาของบ้านบางแน่น น่าจะมาจากเขตหมู่บ้าน มีคลองขนาดเล็ก(น้ำบาง) ไหลผ่านจำนวนหลายสาย ซึ่งไหลมาจากเทือกเขาพระแทว เช่น บางอู บางใหญ่ และบางไอ้จุก &nbsp;ทำให้เกิดความหนาแน่นของคลองหลายสาย</p>\r\n<p dir="ltr">ข้อที่สอง &nbsp;บ้านแขนนน่าจะมาจากคำว่า &ldquo;บ้านเสน่ห์&rdquo; เพราะในสมัยโบราณบ้านแขนนมีชื่อเสียงโด่งดังในเรื่องการทำเสน่ห์ยาแฝด มีหมอเสน่ห์ผู้มีวิชาความรู้ที่ชำนาญเรื่องการทำเสน่ห์ให้คนรักคนหลง และต่อมาเรียกเพี้ยนตามสำเนียงคนภูเก็ตจากบ้านเสน่ห์เป็น บ้านแหนน และบ้านแขนน ในปัจจุบัน</p>', NULL, '2017-03-06 00:57:00', '2017-03-17 17:02:45', 3, 0, 1, 1, 0, 1, 0, 0, 0),
(4, 'ตำบลราไวย์ (เกาะโหลน)', '<h4 dir="ltr"><strong>- อาหารสด ทะเลใส ราไวย์ ราโว้ย โวยวาย เกาะโหลน -</strong></h4>\r\n<p>ในอดีตเมื่อ 100 กว่าปีก่อน ได้มีชาวบ้านจาก จังหวัดสตูล ชื่อ โต๊ะนางู้ เดินทางมาตั้งบ้านเรือนอยู่บนเกาะ โหลน ซึ่งมีสภาพเป็นป่า ต่อมาคนภายนอกเห็นวา่ มีคนอยู่ก็พามากันสร้างบ้านเรือน บ้านเกาะโหลนจัดตั้งเป็นทางการเมื่อปี พ.ศ 2480 มีผู้ใหญ่บ้านคนแรกชื่อนายดล สองเมือง มีประชากร 33 ครัวเรือน และที่เรียกวา่ บ้านเกาะ โหลน ผู้นำชุมชนเล่าว่าพอถึงฤดูทำนาชาวบ้านใกล้เคียงกลัวว่าควายที่เลี้ยงไวจ้ะกินข้าวในนา ก็พาควายที่เลี้ยงไว้กรรเชียงเรือ จูงควายว่ายน้ามาปล่อยไว้ที่เกาะโหลน พอหมดฤดูทำนาเจ้าของควายก็จะมาพาควายกลับแต่มีควายที่หลุดเชือก เป็นควาย เถื่อนอยู่ที่เกาะโหลน ควายเถื่อนพวกนี้กินต้นไม้บนเกาะทำให้โลนเตียน จึงได้เรียกว่า&ldquo;บ้านเกาะโหลน&rdquo;</p>', NULL, '2017-03-06 00:57:05', '2017-03-19 06:13:45', 4, 0, 1, 1, 0, 1, 0, 0, 0),
(5, 'บ้านบางเทา-เชิงทะเล', '<p><strong>- หาดสวย ทะเลใส สับปรดหวานฉ่ำ ข้าวยำ รสเด็ด -</strong></p>\r\n<p dir="ltr">ตำบลเชิงทะเล เดิมเป็นตำบลที่อยู่ติดบริเวณชายหาด น้ำทะเลท่วมไม่ถึง มีพื้นที่ติดกับบริเวณชายหาด จึงเรียกว่าตำบลเชิงทะเล ปัจจุบันมีหาดสุรินทร์ที่เป็นแหล่งท่องเที่ยวสำคัญ ผู้คนส่วนใหญ่ประกอบอาชีพการเกษตร ประมง ค้าขาย และรับจ้าง มีทั้งชุมชนชาวพุธและอิสลามอยู่ร่วมกัน มีวิถีชีวิตและวัฒนธรรมที่หลากหลาย มีการผลิตสินค้าเกษตรและมีการแปรรูปผลิตภัณฑ์ทางการเกษตรอีกด้วย</p>', NULL, '2017-03-06 00:57:10', '2017-03-19 06:13:50', 5, 0, 1, 1, 0, 1, 0, 0, 0),
(6, 'บ้านท่าฉัตรไชย', '<p><strong>-ตำนานรักสารสิน ถิ่นมอเกล็น แดนวัฒนธรรม -</strong></p>\r\n<p><span style="font-weight: 400;">บ้านท่าฉัตรไชย เดิม เรียกบ้านแหลมหลา ในสมัยรัชกาลที่ ๗ พระบาทสมเด็จพระปกเกล้า เจ้าอยู่หัว เสด็จประพาสจังหวัดภูเก็ต บริเวณแหลมหลา ได้สร้างศาลาส าหรับประทับรับเสด็จ บน หลังคาที่ฉัตร เป็นเครื่องหมายของพระมหากษัตริย์ไทย ศาลาดังกล่าวอยู่บริเวณริมทะเล ซึ่งเรียกว่า ท่า จึงเป็นที่มาของ&rdquo;ท่าฉัตรไชย&rdquo; ต่อมาชาวบ้านมักจะเรียก ท่าฉัตรไชยมากกว่า แหลมหลา จึง เรียกชื่อเป็นทางราชการว่า &ldquo;บ้านท่าฉัตรไชย&rdquo; หมู่ที่ ๕ ต าบลไม้ขาว อ าเภอถลาง จังหวัดภูเก็ตจนถึง ปัจจุบันนี้ </span></p>', NULL, '2017-03-06 00:57:15', '2017-03-19 06:13:56', 6, 0, 1, 1, 0, 1, 0, 0, 0),
(7, 'ตำบลกมลา', '<p><strong>- อ้อมกอดภูเขา อิงแอบทะเล วิถีวัฒนธรรมหลาย กิจกรรมอาชาบำบัด -</strong></p>\r\n<p dir="ltr">ตำบลกมลา เป็นตำบลในอำเภอกะทู้ จังหวัดภูเก็ตเป็นชื่อเดียวกับ กราบาลา หรืออ่าวลึกชื่อกำมะรา นี้ได้ใช้มาจนกระทั่ง ทำเนียบท้องที่ของกระทรวงมหาดไทย พ.ศ.2486 หลังจากนั้นจึงมีผู้เกิดความคิดขึ้นมาว่า กำมะรา แปลไม่ได้ จึงได้เปลี่ยนให้เป็น กมลาเป็นภาษาไทยผศมอินเดียที่แปลว่า ดอกบัว</p>', NULL, '2017-03-06 00:57:26', '2017-03-19 06:14:06', 8, 0, 1, 1, 0, 1, 0, 0, 0),
(8, 'ย่านเมืองเก่าภูเก็ต', '<p><strong>- ยลเมืองเก่า เล่าความหลัง สัมผัสวิถี เสน่ห์เมืองทุ่งคา -</strong>&nbsp;</p>\r\n<p dir="ltr">ภูเก็ต ดินแดนแห่งไข่มุกอันดามันอุดมสมบูรณ์ไปด้วยทรัพยากรทั้งทางบกและทางทะเล สวรรค์ ของนักท่องเที่ยวที่ต้องมาเยือนซักครั้งในชีวิต นอกจากภูเก็ตจะเป็นที่ต้องตาต้องใจของ นักท่องเที่ยว ยังมีชุมชนย่านเมืองเก่าที่หล่อหลอมวัฒนธรรมหลากหลายรูปมารวมกัน จนเป็นชุมชนย่านเมืองเก่า ผ่านเข้าไปในย่านเมืองเก่าภูเก็ตจะสะดุดตากับตึกแถวแบบโบราณเรียงรายสองฝั่งถนน ที่เป็น ร่องรอยประวัติศาสตร์อันรุ่งเรืองสมัยรัชกาลที่ 5 ยุคสมัยการทำาเหมืองแร่เฟื่องฟู ให้ได้สัมผัส เสน่ห์สถาปัตยกรรมชิโน-โปรตุกีส และชมความงดงามแบบคลาสสิค ชาวบ้านย่านเมืองเก่า จึงริเริ่มเก็บรวบรวมข้อมูลหลักฐานทางประวัติศาสตร์ของอาคารชิโนโปรตุกีสพร้อมทั้งอนุรักษ์ และพัฒนาเมืองเก่าให้ลูกหลานได้เห็นคุณค่าสืบต่อไป &nbsp;ย่านเมืองเก่าเป็นแหล่งรวมสถาปัตยกรรมแบบชิโนโปรตุกีส ที่ยังคงรักษาไว้จนถึงปัจจุบัน มีรูปแบบวัฒนธรรมเฉพาะที่เรียกว่า &ldquo;เพอรานากัน&rdquo; มีพิพิธภัณฑ์ภูเก็ตไทยหัว เป็นแหล่งรวบรวมเรื่องราวของย่านเมืองเก่าและวิถีชีวิตวัฒนธรรมชาวจีนโพ้นทะเลที่อพยพ มาทำาเหมือง เป็นแหล่งท่องเที่ยวที่มีให้เลือกหลากหลายอารมณ์ชวนต้องมนต์เสน่ห์ของเมืองเก่า ภูเก็ต</p>', NULL, '2017-03-06 00:58:14', '2017-03-19 06:14:01', 7, 0, 1, 1, 0, 1, 0, 0, 0),
(9, 'เกี่ยวกับการท่องเที่ยวโดยชุมชน', '<p>&nbsp;สมาชิก</p>\r\n<p dir="ltr">1.นายสมพร แทนสกุล ( บ้านท่าฉัตรไชย )</p>\r\n<p dir="ltr">2.นายประเสริฐ ฤิทธิ์รักษา&nbsp;( ป่าคลอก )</p>\r\n<p dir="ltr">3.นางธัญลักษณ์ จริยะเลอพงษ์&nbsp;( บ้านแขนน )</p>\r\n<p dir="ltr">4.นายสมยศ ปาทาน&nbsp;( ย่านเมืองเก่า )</p>\r\n<p dir="ltr">5.นายสนธยา คงทิพย์ ( เชิงทะเล )</p>\r\n<p dir="ltr">6.นายสยามพงษ์ คงราช ( กมลา )</p>\r\n<p dir="ltr">7.นายทรงสิทธิ์ บุญผล ( เกาะโหลน )</p>\r\n<p dir="ltr"><span id="docs-internal-guid-21771db0-d23f-48cc-241f-eedfcd581321">8.ดร.อภิรมย์ พรหมจรรยา ( ที่ปรึกษาโครงการ )</span></p>', NULL, '2017-03-06 01:48:06', '2017-03-19 03:43:13', 10, 0, 1, 1, 0, 0, 0, 0, 0),
(10, 'ติดต่อ', '<h4><span style="font-size: 11pt; font-family: Arial; color: #000000; background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">สำนักงานการท่องเที่ยวและกีฬาจังหวัดภูเก็ต : </span>076-217054</h4>\r\n<h4><span style="font-size: 11pt; font-family: Arial; color: #000000; background-color: #ffffff; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">การท่องเที่ยวแห่งประเทศไทย สำนักงานภูเก็ต : </span>076-217138</h4>\r\n<h4 dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; color: #000000; background-color: #ffffff; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">ศูนย์แก้ไขปัญหาการหลอกลวงและช่วยเหลือนักท่องเที่ยว : </span>076-327100</h4>\r\n<h3>ติดต่อหัวหน้าชุมชน</h3>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">ชุมชนบ้านท่าฉัตรไชย</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">-นายสมพร &nbsp;แทนสกุล 087-2849676</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">ชุมชนตำบลป่าคลอก</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">-นายประเสริฐ &nbsp;ฤทธิ์รักษา &nbsp;084-3099131</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">ชุมชนบ้านแขนน (หมู่บ้านวัฒนธรรมถลาง)</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">-ว่าที่ ร.ต.ไตรบัญญัติ &nbsp;จริยะเลอพงษ์ &nbsp;086-6821371</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">-นางธัญลักษณ์ จริยะเลอพงษ์ &nbsp;081-8956864</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">ชุมชนย่านเมืองเก่าภูเก็ต</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">- นายสมยศ &nbsp;ปาทาน &nbsp;&nbsp;084-3053960</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">- นายดอน &nbsp;ลิ้มนันทพิสิฐ 081-8920618</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span id="docs-internal-guid-2faca225-d238-c8bc-2604-7966c7e08b77"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;"><span style="background-color: transparent; font-size: 11pt;">ชุมชนบ้านบางเทา-เชิงทะเล</span></span></span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">-นายสนธยา คงทิพย์ &nbsp;062-2287896</span>&nbsp;&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">ชุมชนตำบลกมลา</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; color: #000000; background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">-นายสยามพงษ์ &nbsp;คงราช &nbsp;&nbsp;&nbsp;081-5693363</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">ชุมชนตำบลราไวย์ (เกาะโหลน)</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">-นายทรงสิทธิ์ &nbsp;บุญผล 085-4290021</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>', NULL, '2017-03-06 01:48:18', '2017-03-16 20:54:13', 9, 0, 1, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `page_cn`
--

CREATE TABLE `page_cn` (
  `id` int(11) NOT NULL,
  `name_cn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_cn` text COLLATE utf8_unicode_ci,
  `link_cn` text COLLATE utf8_unicode_ci,
  `update_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `page_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page_cn`
--

INSERT INTO `page_cn` (`id`, `name_cn`, `description_cn`, `link_cn`, `update_datetime`, `page_id`) VALUES
(1, NULL, NULL, NULL, '2017-03-05 11:39:57', 1),
(2, NULL, NULL, NULL, '2017-03-05 11:56:20', 2),
(3, NULL, NULL, NULL, '2017-03-06 00:57:00', 3),
(4, NULL, NULL, NULL, '2017-03-06 00:57:05', 4),
(5, NULL, NULL, NULL, '2017-03-06 00:57:10', 5),
(6, NULL, NULL, NULL, '2017-03-06 00:57:15', 6),
(7, NULL, NULL, NULL, '2017-03-06 00:57:26', 7),
(8, NULL, NULL, NULL, '2017-03-06 00:58:14', 8),
(9, NULL, NULL, NULL, '2017-03-06 01:48:06', 9),
(10, NULL, NULL, NULL, '2017-03-06 01:48:18', 10),
(11, NULL, NULL, NULL, '2017-03-15 20:32:12', 9),
(12, NULL, NULL, NULL, '2017-03-15 20:32:30', 9),
(13, NULL, NULL, NULL, '2017-03-15 20:47:53', 10),
(14, NULL, NULL, NULL, '2017-03-15 20:49:57', 10),
(15, NULL, NULL, NULL, '2017-03-15 20:51:30', 10),
(16, NULL, NULL, NULL, '2017-03-15 20:53:20', 10),
(17, NULL, NULL, NULL, '2017-03-15 20:53:39', 10),
(18, NULL, NULL, NULL, '2017-03-15 20:53:50', 10),
(19, NULL, NULL, NULL, '2017-03-15 20:53:52', 10),
(20, NULL, NULL, NULL, '2017-03-15 20:55:24', 9),
(21, NULL, NULL, NULL, '2017-03-15 20:55:32', 9),
(22, NULL, NULL, NULL, '2017-03-15 20:57:59', 2),
(23, NULL, NULL, NULL, '2017-03-15 20:58:34', 2),
(24, NULL, NULL, NULL, '2017-03-15 20:59:59', 2),
(25, NULL, NULL, NULL, '2017-03-15 23:00:22', 3),
(26, NULL, NULL, NULL, '2017-03-15 23:01:50', 3),
(27, NULL, NULL, NULL, '2017-03-15 23:03:51', 3),
(28, NULL, NULL, NULL, '2017-03-15 23:04:37', 3),
(29, NULL, NULL, NULL, '2017-03-15 23:04:51', 2),
(30, NULL, NULL, NULL, '2017-03-15 23:05:00', 2),
(31, NULL, NULL, NULL, '2017-03-15 23:06:18', 3),
(32, NULL, NULL, NULL, '2017-03-15 23:07:05', 2),
(33, NULL, NULL, NULL, '2017-03-15 23:11:09', 3),
(34, NULL, NULL, NULL, '2017-03-16 09:42:09', 4),
(35, NULL, NULL, NULL, '2017-03-16 09:43:01', 4),
(36, NULL, NULL, NULL, '2017-03-16 09:43:15', 4),
(37, NULL, NULL, NULL, '2017-03-16 20:07:20', 2),
(38, NULL, NULL, NULL, '2017-03-16 20:07:38', 3),
(39, NULL, NULL, NULL, '2017-03-16 20:07:54', 2),
(40, NULL, NULL, NULL, '2017-03-16 20:08:15', 3),
(41, NULL, NULL, NULL, '2017-03-16 20:10:47', 4),
(42, NULL, NULL, NULL, '2017-03-16 20:34:21', 5),
(43, NULL, NULL, NULL, '2017-03-16 20:34:34', 5),
(44, NULL, NULL, NULL, '2017-03-16 20:36:29', 5),
(45, NULL, NULL, NULL, '2017-03-16 20:37:27', 4),
(46, NULL, NULL, NULL, '2017-03-16 20:48:29', 10),
(47, NULL, NULL, NULL, '2017-03-16 20:50:20', 9),
(48, NULL, NULL, NULL, '2017-03-16 20:54:13', 10),
(49, NULL, NULL, NULL, '2017-03-16 20:57:24', 10),
(50, NULL, NULL, NULL, '2017-03-16 21:02:57', 6),
(51, NULL, NULL, NULL, '2017-03-16 21:03:11', 6),
(52, NULL, NULL, NULL, '2017-03-16 21:04:06', 6),
(53, NULL, NULL, NULL, '2017-03-16 21:20:27', 8),
(54, NULL, NULL, NULL, '2017-03-16 21:20:36', 8),
(55, NULL, NULL, NULL, '2017-03-16 21:24:59', 8),
(56, NULL, NULL, NULL, '2017-03-16 21:29:16', 7),
(57, NULL, NULL, NULL, '2017-03-16 21:29:25', 7),
(58, NULL, NULL, NULL, '2017-03-16 21:33:44', 7),
(59, NULL, NULL, NULL, '2017-03-17 16:49:06', 2),
(60, NULL, NULL, NULL, '2017-03-17 16:53:39', 2),
(61, NULL, NULL, NULL, '2017-03-17 16:53:48', 2),
(62, NULL, NULL, NULL, '2017-03-17 16:53:57', 3),
(63, NULL, NULL, NULL, '2017-03-17 16:54:09', 4),
(64, NULL, NULL, NULL, '2017-03-17 16:54:17', 5),
(65, NULL, NULL, NULL, '2017-03-17 16:54:25', 6),
(66, NULL, NULL, NULL, '2017-03-17 16:54:33', 8),
(67, NULL, NULL, NULL, '2017-03-17 16:54:42', 7),
(68, NULL, NULL, NULL, '2017-03-17 17:02:45', 3),
(69, NULL, NULL, NULL, '2017-03-19 03:43:02', 9),
(70, NULL, NULL, NULL, '2017-03-19 03:43:06', 1),
(71, NULL, NULL, NULL, '2017-03-19 03:43:13', 9),
(72, NULL, NULL, NULL, '2017-03-19 06:13:35', 2),
(73, NULL, NULL, NULL, '2017-03-19 06:13:40', 3),
(74, NULL, NULL, NULL, '2017-03-19 06:13:45', 4),
(75, NULL, NULL, NULL, '2017-03-19 06:13:50', 5),
(76, NULL, NULL, NULL, '2017-03-19 06:13:56', 6),
(77, NULL, NULL, NULL, '2017-03-19 06:14:01', 8),
(78, NULL, NULL, NULL, '2017-03-19 06:14:06', 7),
(79, NULL, NULL, NULL, '2017-03-22 10:33:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_en`
--

CREATE TABLE `page_en` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_en` text COLLATE utf8_unicode_ci,
  `link_en` text COLLATE utf8_unicode_ci,
  `update_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `page_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page_en`
--

INSERT INTO `page_en` (`id`, `name_en`, `description_en`, `link_en`, `update_datetime`, `page_id`) VALUES
(1, 'Index', '', NULL, '2017-03-19 03:43:06', 1),
(2, 'Tambol Pa Klog Community', '', NULL, '2017-03-15 20:59:59', 2),
(3, 'Baan Kanan Community Based Tourism (Thalang Cultural and Traditional village)', '', NULL, '2017-03-15 23:00:22', 3),
(4, 'Tambol Rawai of Lone Island Community', '', NULL, '2017-03-16 09:43:15', 4),
(5, 'Baan Bangtao-Cherng Talay Community', '', NULL, '2017-03-16 20:34:34', 5),
(6, 'Baan Tha Chatchai Community', '', NULL, '2017-03-16 21:03:11', 6),
(7, 'Tambol Kamala Community', '', NULL, '2017-03-16 21:29:25', 7),
(8, 'The Old Phuket Town Community', '', NULL, '2017-03-16 21:20:36', 8),
(9, 'About Community Based Tourism', '', NULL, '2017-03-15 20:32:30', 9),
(10, 'Contact', '<h3><span style="font-family: Arial; font-size: 14.6667px; white-space: pre-wrap;">Community</span></h3>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">Tha Chatchai Community</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">-นายสมพร &nbsp;แทนสกุล 087-2849676</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">Tambol Pa Klog Community</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">-นายประเสริฐ &nbsp;ฤทธิ์รักษา &nbsp;084-3099131</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">Baan Kanan Community (Thalang Cultural and Traditional village)</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">-ว่าที่ ร.ต.ไตรบัญญัติ &nbsp;จริยะเลอพงษ์ &nbsp;086-6821371</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">-นางธัญลักษณ์ จริยะเลอพงษ์ &nbsp;081-8956864</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">The Old Phuket Town Community </span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">- นายสมยศ &nbsp;ปาทาน &nbsp;&nbsp;084-3053960</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">- นายดอน &nbsp;ลิ้มนันทพิสิฐ 081-8920618</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;<span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">Baan Bangtao-Cherng Talay Community</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;">-นายสนธยา คงทิพย์ &nbsp;062-2287896</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">Tambol Kamala Community</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; color: #000000; background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">-นายสยามพงษ์ &nbsp;คงราช &nbsp;&nbsp;&nbsp;081-5693363</span></p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">Tambol Rawai of Lone Island </span><span style="background-color: transparent; font-family: Arial; font-size: 11pt; white-space: pre-wrap;">Community</span>&nbsp;</p>\r\n<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 11pt; font-family: Arial; color: #000000; background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">-นายทรงสิทธิ์ &nbsp;บุญผล 085-4290021</span></p>', NULL, '2017-03-16 20:57:24', 10),
(11, NULL, NULL, NULL, '2017-03-22 10:33:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_type`
--

CREATE TABLE `page_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page_type`
--

INSERT INTO `page_type` (`id`, `name`, `description`) VALUES
(1, 'index', NULL),
(2, 'com1', 'ป่าคลอก'),
(3, 'com2', 'บ้านแขนน'),
(4, 'com3', 'เกาะโหลน'),
(5, 'com4', 'เชิงทะเล'),
(6, 'com5', 'ท่าฉัตรไชย'),
(7, 'com6', 'เมืองเก่า'),
(8, 'com7', 'กมลา'),
(9, 'contact', NULL),
(10, 'about', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE `picture` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `name_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_en` text COLLATE utf8_unicode_ci,
  `path` text COLLATE utf8_unicode_ci NOT NULL,
  `picture_order` int(11) NOT NULL DEFAULT '1',
  `page_id` int(11) NOT NULL,
  `sub_page_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `picture`
--

INSERT INTO `picture` (`id`, `name`, `description`, `name_en`, `description_en`, `path`, `picture_order`, `page_id`, `sub_page_id`) VALUES
(2, 'ชุมชนป่าคลอก', NULL, NULL, NULL, 'picture_20170316080546.jpg', 1, 2, NULL),
(4, 'นายประเสริฐ  ฤทธิ์รักษา', '<p>ติดต่อ : 084-3099131</p>', '', '', 'picture_20170315090604.png', 1, 2, 1),
(5, 'ชุมชนบ้านแขนน (หมู่บ้านวัฒนธรรมถลาง)', NULL, NULL, NULL, 'picture_20170316080933.jpg', 1, 3, NULL),
(6, 'ว่าที่ ร.ไตรบัญญัติ  จริยะเลอพงษ์', '<p>ติดต่อ :&nbsp;086-6821371</p>', '', '', 'picture_20170320125622.jpg', 1, 3, 2),
(7, 'นางธัญลักษณ์ จริยะเลอพงษ์', '<p>ติดต่อ : 081-8956864</p>', '', '', 'picture_20170315111527.png', 1, 3, 2),
(9, 'ชุมชนราไวย์(เกาะโหลน)', NULL, NULL, NULL, 'picture_20170316081101.jpg', 1, 4, NULL),
(10, 'นายทรงสิทธิ์  บุญผล', '<p>ติดต่อ : 085-4290021</p>', NULL, NULL, 'picture_20170320125918.jpg', 1, 4, 3),
(11, 'ชุมชนบ้านบางเทา-เชิงทะเล', NULL, NULL, NULL, 'picture_20170316083507.jpg', 1, 5, NULL),
(12, 'นายสนธยา คงทิพย์', '<p>ติดต่อ : 062-2287896</p>', NULL, NULL, 'picture_20170316083603.jpg', 1, 5, 4),
(13, 'ชุมชนบ้านท่าฉัตรไชย', NULL, NULL, NULL, 'picture_20170316090347.jpg', 1, 6, NULL),
(14, 'นายสมพร  แทนสกุล', '<p>ติดต่อ :&nbsp;087-2849676</p>', NULL, NULL, 'picture_20170316090459.png', 1, 6, 5),
(15, 'ชุมชนย่านเมืองเก่าภูเก็ต', NULL, NULL, NULL, 'picture_20170316092101.jpg', 1, 8, NULL),
(16, 'นายสมยศ  ปาทาน', '<p>ติดต่อ :&nbsp;084-3053960</p>', NULL, NULL, 'picture_20170316092202.png', 1, 8, 6),
(17, 'นายดอน  ลิ้มนันทพิสิฐ', '<p>ติดต่อ :&nbsp;081-8920618</p>', NULL, NULL, 'picture_20170320125644.jpg', 1, 8, 6),
(18, 'ชุมชนตำบลกมลา', NULL, NULL, NULL, 'picture_20170316093003.jpg', 1, 7, NULL),
(19, 'นายสยามพงษ์  คงราช', '<p>ติดต่อ :&nbsp;081-5693363</p>', NULL, NULL, 'picture_20170316093049.png', 1, 7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `path` text COLLATE utf8_unicode_ci NOT NULL,
  `slide_order` int(11) NOT NULL DEFAULT '1',
  `page_id` int(11) NOT NULL,
  `sub_page_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subpage`
--

CREATE TABLE `subpage` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `link` text COLLATE utf8_unicode_ci,
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `page_id` int(11) NOT NULL,
  `withdraw` tinyint(1) NOT NULL DEFAULT '0',
  `attr_name` tinyint(1) NOT NULL DEFAULT '1',
  `attr_description` tinyint(1) NOT NULL DEFAULT '1',
  `attr_link` tinyint(1) NOT NULL DEFAULT '1',
  `attr_picture` tinyint(1) NOT NULL DEFAULT '1',
  `attr_slide` tinyint(1) NOT NULL DEFAULT '1',
  `attr_file` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subpage`
--

INSERT INTO `subpage` (`id`, `name`, `description`, `link`, `create_datetime`, `update_datetime`, `page_id`, `withdraw`, `attr_name`, `attr_description`, `attr_link`, `attr_picture`, `attr_slide`, `attr_file`) VALUES
(1, 'ตัวแทนชุมชน', NULL, NULL, '2017-03-05 11:58:48', '2017-03-06 01:17:42', 2, 0, 1, 0, 0, 1, 0, 0),
(2, 'ตัวแทนชุมชน', NULL, NULL, '2017-03-06 01:09:30', '2017-03-06 01:17:35', 3, 0, 1, 0, 0, 1, 0, 0),
(3, 'ตัวแทนชุมชน', NULL, NULL, '2017-03-06 01:12:28', '2017-03-06 01:17:47', 4, 0, 1, 0, 0, 1, 0, 0),
(4, 'ตัวแทนชุมชน', NULL, NULL, '2017-03-06 01:12:47', '2017-03-06 01:17:53', 5, 0, 1, 0, 0, 1, 0, 0),
(5, 'ตัวแทนชุมชน', NULL, NULL, '2017-03-06 01:13:15', '2017-03-06 01:17:58', 6, 0, 1, 0, 0, 1, 0, 0),
(6, 'ตัวแทนชุมชน', NULL, NULL, '2017-03-06 01:13:33', '2017-03-06 01:18:02', 8, 0, 1, 0, 0, 1, 0, 0),
(7, 'ตัวแทนชุมชน', NULL, NULL, '2017-03-06 01:14:02', '2017-03-06 01:18:08', 7, 0, 1, 0, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subpage_cn`
--

CREATE TABLE `subpage_cn` (
  `id` int(11) NOT NULL,
  `name_cn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_cn` text COLLATE utf8_unicode_ci,
  `link_cn` text COLLATE utf8_unicode_ci,
  `update_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `page_id` int(11) NOT NULL,
  `subpage_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subpage_cn`
--

INSERT INTO `subpage_cn` (`id`, `name_cn`, `description_cn`, `link_cn`, `update_datetime`, `page_id`, `subpage_id`) VALUES
(1, NULL, NULL, NULL, '2017-03-05 11:58:48', 2, 1),
(2, NULL, NULL, NULL, '2017-03-05 19:05:53', 2, 1),
(3, NULL, NULL, NULL, '2017-03-05 19:06:47', 2, 1),
(4, NULL, NULL, NULL, '2017-03-05 19:07:29', 2, 1),
(5, NULL, NULL, NULL, '2017-03-06 01:09:30', 3, 2),
(6, NULL, NULL, NULL, '2017-03-06 01:12:28', 4, 3),
(7, NULL, NULL, NULL, '2017-03-06 01:12:47', 5, 4),
(8, NULL, NULL, NULL, '2017-03-06 01:13:15', 6, 5),
(9, NULL, NULL, NULL, '2017-03-06 01:13:33', 8, 6),
(10, NULL, NULL, NULL, '2017-03-06 01:14:02', 7, 7),
(11, NULL, NULL, NULL, '2017-03-15 21:00:33', 2, 1),
(12, NULL, NULL, NULL, '2017-03-15 21:00:44', 2, 1),
(13, NULL, NULL, NULL, '2017-03-16 09:44:23', 4, 3),
(14, NULL, NULL, NULL, '2017-03-16 20:08:50', 3, 2),
(15, NULL, NULL, NULL, '2017-03-16 20:09:09', 3, 2),
(16, NULL, NULL, NULL, '2017-03-16 20:35:22', 5, 4),
(17, NULL, NULL, NULL, '2017-03-16 21:04:21', 6, 5),
(18, NULL, NULL, NULL, '2017-03-16 21:21:22', 8, 6),
(19, NULL, NULL, NULL, '2017-03-16 21:30:20', 7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `subpage_en`
--

CREATE TABLE `subpage_en` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci,
  `link_en` text COLLATE utf8_unicode_ci,
  `update_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `page_id` int(11) NOT NULL,
  `subpage_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subpage_en`
--

INSERT INTO `subpage_en` (`id`, `name_en`, `description_en`, `link_en`, `update_datetime`, `page_id`, `subpage_id`) VALUES
(1, 'Community Agent', NULL, NULL, '2017-03-15 21:00:44', 2, 1),
(2, 'Community Agent', NULL, NULL, '2017-03-16 20:08:50', 3, 2),
(3, 'Community Agent', NULL, NULL, '2017-03-16 09:44:23', 4, 3),
(4, 'Community Agent', NULL, NULL, '2017-03-16 20:35:22', 5, 4),
(5, 'Community Agent', NULL, NULL, '2017-03-16 21:04:21', 6, 5),
(6, 'Community Agent', NULL, NULL, '2017-03-16 21:21:22', 8, 6),
(7, 'Community Agent', NULL, NULL, '2017-03-16 21:30:20', 7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `Token_ID` int(11) NOT NULL,
  `Token` text COLLATE utf8_unicode_ci NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Date_Create` datetime NOT NULL,
  `Date_Update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `User_ID` int(11) NOT NULL,
  `User_Email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `User_Firstname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `User_Lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Permission` enum('creator','admin','agent','user') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Community` int(11) DEFAULT NULL,
  `User_Phone_Number` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `User_Display_Picture` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `User_Deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`User_ID`, `User_Email`, `Username`, `Password`, `User_Firstname`, `User_Lastname`, `Permission`, `Community`, `User_Phone_Number`, `User_Display_Picture`, `User_Deleted`) VALUES
(1, 'p.kittichet@gmail.com', 'admin', 'c987354747bb89e1726c81afbb675691', 'admin2', 'admin2', 'creator', NULL, '0987654321', 'Baba_man.jpg', 0),
(2, 'test@test.com', 'test', 'c987354747bb89e1726c81afbb675691', 'test', 'test', 'admin', NULL, '0987654312', 'Baba_man1.jpg', 0),
(3, '1234@wqewqe.com', 'test2', 'c987354747bb89e1726c81afbb675691', 'test2', 'test2', 'agent', 3, '1234', 'Shophouse_02.jpg', 0),
(6, '262@456.com', 'test3', 'f9df54b45a0efa6b8b12264a0222345a', '123', '213', 'agent', 3, '0987654321', '', 0),
(7, 'noemail1@noemail.com', 'ท่าฉัตรไชย', '59edac1d29397270ec6dc18fa79aa576', 'นายสมพร', 'แทนสกุล', 'agent', 5, '087-2849676', '', 0),
(8, 'noemail2@noemail.com', 'ป่าคลอก', '5a9b3e45fc1500af57f499545fbd98c8', 'นายประเสริฐ', 'ฤทธิ์รักษา', 'agent', 1, '084-3099131', '1.png', 0),
(9, 'noemail3@noemail.com', 'บ้านแขนน', '08bbf987810b10881998709ef09a4e68', 'ว่าที่ ร.ต.ไตรบัญญัติ', 'จริยะเลอพงษ์', 'agent', 2, '086-6821371', '11.png', 0),
(10, 'qe@qwq.com', 'qwq', '3e7d7d6478daf7fdd72366a405dcfb83', '-', '-', 'agent', 1, 'q', '', 0),
(11, 'noemail4@noemail.com', 'บ้านแขนน2', '6ca33633a4b2a0d90ebc21a47dfe9fb0', 'นางธัญลักษณ์', 'จริยะเลอพงษ์', 'agent', 2, '081-8956864', '2.png', 0),
(12, 'noemail5@noemail.com', 'เชิงทะเล', '37e77c7b36ba597553bb86a775d149b2', 'นายสนธยา', 'คงทิพย์', 'agent', 4, '062-2287896', 'เชิงทะเล-นายสนธยา.jpg', 0),
(13, 'noemail6@noemail.com', 'กมลา', '2e8c80c21d8ec3e1b9f212ca35f41a03', 'นายสยามพงษ์', 'คงราช', 'agent', 7, '081-5693363', '', 0),
(14, 'noemail7@noemail.com', 'ย่านเมืองเก่า', '84ae32623e9a50e1388af6196404e4b8', 'นายสมยศ', 'ปาทาน', 'agent', 6, '084-3053960', '', 0),
(15, 'noemail8@noemail.com', 'ย่านเมืองเก่า2', '27fed30249ab953f37c68e01362ca6b9', 'นายดอน', 'ลิ้มนันทพิสิฐ', 'agent', 6, '081-8920618', 'เมืองเก่า-นายดอน.jpg', 0),
(16, 'noemail9@noemail.com', 'เกาะโหลน', 'f7cd1a9c01c984ed26ecc31913f96f28', 'นายทรงสิทธิ์', 'บุญผล', 'agent', 3, '085-4290021', 'เกาะโหลน-นายทรงสิทธฺ์.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `web_login`
--

CREATE TABLE `web_login` (
  `id` int(11) NOT NULL,
  `Web_Login_Time` datetime NOT NULL,
  `User_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `web_login`
--

INSERT INTO `web_login` (`id`, `Web_Login_Time`, `User_ID`) VALUES
(1, '2017-04-07 10:09:53', 1),
(2, '2017-03-20 00:46:23', 2),
(3, '2017-03-10 15:39:44', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgetpassword`
--
ALTER TABLE `forgetpassword`
  ADD PRIMARY KEY (`fgpw_ID`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_en`
--
ALTER TABLE `item_en`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_picture`
--
ALTER TABLE `item_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_cn`
--
ALTER TABLE `page_cn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_en`
--
ALTER TABLE `page_en`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_type`
--
ALTER TABLE `page_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subpage`
--
ALTER TABLE `subpage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subpage_cn`
--
ALTER TABLE `subpage_cn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subpage_en`
--
ALTER TABLE `subpage_en`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`Token_ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`User_ID`);

--
-- Indexes for table `web_login`
--
ALTER TABLE `web_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forgetpassword`
--
ALTER TABLE `forgetpassword`
  MODIFY `fgpw_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `item_en`
--
ALTER TABLE `item_en`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `item_picture`
--
ALTER TABLE `item_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `page_cn`
--
ALTER TABLE `page_cn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `page_en`
--
ALTER TABLE `page_en`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `page_type`
--
ALTER TABLE `page_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subpage`
--
ALTER TABLE `subpage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subpage_cn`
--
ALTER TABLE `subpage_cn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `subpage_en`
--
ALTER TABLE `subpage_en`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `Token_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `User_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `web_login`
--
ALTER TABLE `web_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
