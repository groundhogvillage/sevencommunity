<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buildmenu {

  private $CI = NULL;
	private $arr_ip = NULL;

	function __construct()
	{
		//__construct will running first went this class load.
		$this->CI =& get_instance();
		$this->CI->load->model('select_model');
  }

	public function buildmenuadmin($main = NULL,$sub = NULL)
	{
    $menuarr = array(
                      "index" => "index",
                      "user" => "userlist",
                      "1" => "index",
                      '2' => "com1",
                      "3" => "com2",
                      "4" => "com3",
                      "5" => "com4",
                      "6" => "com5",
                      "7" => "com6",
                      "8" => "com7",
                      "9" => "contact",
                      "10" => "about"

                    );
   $menuarrname = array(
                    "index" => "ข้อมูลของชุมชน",
                    "user" => "Manage User",
                    "1" => "Home",
                    '2' => "ป่าคลอก",
                    "3" => "บ้านแขนน",
                    "4" => "เกาะโหลน",
                    "5" => "เชิงทะเล",
                    "6" => "ท่าฉัตรไชย",
                    "7" => "เมืองเก่า",
                    "8" => "กมลา",
                    "9" => "Contact",
                    "10" => "About"

                  );

		$menusetting = '<ul class="nav">';
		foreach ($menuarr as $key => $value) {
			if($main==NULL||$main=="")
			{
				$main = "index";
			}

			if($key==$main)
			{
				$activemain = "current";
			}
			else
			{
				$activemain = "";
			}

			if($key!="index"&&$key!="user")
			{
				$furl = "cms";
				$iconf = '<i class="glyphicon glyphicon-pencil"></i>';
				$page = $this->getpagebytype($key);
				$data['page'] = $page['page'];

				if($sub == "itemadd")
				{
					$setactive = "current";
          $setactive2 = "";
				}
        else if($sub == "newsadd")
				{
          $setactive = "";
					$setactive2 = "current";
				}
				else
				{
          $setactive = "";
					$setactive2 = "";
				}

				if($key>=2&&$key<=8)
				{
					$setadditem =  '<li class="'.$setactive.'"><a id="additem_'.$key.'" href="'.base_url().'community/itemcommlist?main='.$data['page']['id'].'&type='.$key.'" style="cursor:pointer;"> เพิ่มสถานที่ </a></li>';
          $setadditem .= '<li class="'.$setactive2.'"><a id="additem_'.$key.'" href="'.base_url().'community/newscommlist?main='.$data['page']['id'].'&type='.$key.'" style="cursor:pointer;"> เพิ่มข่าวสาร </a></li>';
        }
				else
				{
					$setadditem = '';
				}

				$setsubmenu = $this->getallsubpage($data['page']['id']);
				$setsubpagemenu_row = $setsubmenu['subpage_row'];
				$setsubpagemenu = $setsubmenu['subpage'];
				if($setsubpagemenu_row>0)
				{
					$havesub = "submenu";
					if(($sub!=NULL&&$sub!=""&&$sub!="0"&&$main==$key) || $main == $key || ($main == $key&&$sub == "itemadd")  || ($main == $key&&$sub == "newsadd") )
					{
						$activesubmenu = "display: block;";
						$opensubmenu = "open";
					}
					else
					{
						$activesubmenu = "display: none;";
						$opensubmenu = "";
					}

					if($main == $key&&($sub==NULL||$sub==""||$sub=="0"))
					{
						$setactive = "current";
					}
					else
					{
						$setactive = "";
					}

					$submenusetting = '<ul style="'.$activesubmenu.'">';
					$setmainsub = '<li class="'.$setactive.'"><a href="'.base_url().$furl.'/'.$value.'">Main</a>';
					$submenusetting .= $setadditem;
					$submenusetting .= $setmainsub;
						foreach ($setsubpagemenu as $keys => $values)
						{
							if($sub == $values["id"])
							{
								$setactive = "current";
							}
							else
							{
								$setactive = "";
							}
							$submenusetting .= '<li class="'.$setactive.'"><a id="sub_'.$values["id"].'" href="'.base_url().'cms/subpage?main='.$values["page_id"].'&sub='.$values["id"].'&type='.$key.'" style="cursor:pointer;"> '.$values["name"].'</a></li>';
						}
						$submenusetting .= '</ul>';
				}
				else if($key>=2&&$key<=8&&$setsubpagemenu_row<=0)
				{
					$havesub = "submenu";
					if($main==$key || ($main == $key&&$sub == "itemadd")  || ($main == $key&&$sub == "newsadd"))
					{
						$activesubmenu = "display: block;";
						$opensubmenu = "open";
					}
					else
					{
						$activesubmenu = "display: none;";
						$opensubmenu = "";
					}

					if($main == $key)
					{
						$setactive = "current";
					}
					else
					{
						$setactive = "";
					}

					$setmainsub = '<li class="'.$setactive.'"><a href="'.base_url().$furl.'/'.$value.'">Main</a>';

					$submenusetting = '<ul style="'.$activesubmenu.'">';
          $submenusetting .= $setadditem;
					$submenusetting .= $setmainsub;
					$submenusetting .= '</ul>';
				}
				else
				{
					$havesub = "";
					$submenusetting = "";
				}
			}
			else
			{
				$havesub = "";
				$submenusetting = "";
				$opensubmenu = "";
				$furl = "main";
				$iconf = "";
				$setsubpagemenu_row = 0;
			}

			if($key>=2&&$key<=8)
			{
				$menusetting .= '<li class="'.$activemain.' '.$havesub.' '.$opensubmenu.'"><a href="#">'.$iconf.$menuarrname[$key].'<span class="caret pull-right"></span></a>';
			}
			else if($setsubpagemenu_row<=0)
			{
				$menusetting .= '<li class="'.$activemain.' '.$havesub.' '.$opensubmenu.'"><a href="'.base_url().$furl.'/'.$value.'">'.$iconf.$menuarrname[$key].'</a>';
			}
			else
			{
				$menusetting .= '<li class="'.$activemain.' '.$havesub.' '.$opensubmenu.'"><a href="#">'.$iconf.$menuarrname[$key].'<span class="caret pull-right"></span></a>';
			}

			$menusetting .= $submenusetting;
			$menusetting .= '</li>';

		}
    $menusetting .= "<li><a href='".base_url()."'>กลับไปสู่หน้าเว็ปไซค์</a></li>";
		$menusetting .= "</ul>";

		return $menusetting;
	}

  public function buildmenuagent($main = NULL,$sub = NULL,$atcomm = NULL)
	{
    $menuarr = array(
                      "index" => "index"
                    );
   $menuarrname = array(
                     "index" => "ข้อมูลของชุมชน"
                  );

   if($atcomm=="1")
   {
     $menuarr = array_merge($menuarr, array( "comm" => "com1") );
     $menuarrname = array_merge($menuarrname, array( "comm" =>  "ป่าคลอก") );
   }
   else if($atcomm=="2")
   {
     $menuarr = array_merge($menuarr, array( "comm" => "com2") );
     $menuarrname = array_merge($menuarrname, array( "comm" =>  "บ้านแขนน") );
   }
   else if($atcomm=="3")
   {
     $menuarr = array_merge($menuarr, array( "comm" => "com3") );
     $menuarrname = array_merge($menuarrname, array( "comm" =>  "เกาะโหลน") );
   }
   else if($atcomm=="4")
   {
     $menuarr = array_merge($menuarr, array( "comm" => "com4") );
     $menuarrname = array_merge($menuarrname, array( "comm" =>  "เชิงทะเล") );
   }
   else if($atcomm=="5")
   {
     $menuarr = array_merge($menuarr, array( "comm" => "com5") );
     $menuarrname = array_merge($menuarrname, array( "comm" =>  "ท่าฉัตรไชย") );
   }
   else if($atcomm=="6")
   {
     $menuarr = array_merge($menuarr, array( "comm" => "com6") );
     $menuarrname = array_merge($menuarrname, array( "comm" =>  "เมืองเก่า") );
   }
   else if($atcomm=="7")
   {
     $menuarr = array_merge($menuarr, array( "comm" => "com7") );
     $menuarrname = array_merge($menuarrname, array( "comm" =>  "กมลา") );
   }

		$menusetting = '<ul class="nav">';
		foreach ($menuarr as $key => $value) {
			if($main==NULL||$main=="")
			{
				$main = "index";
			}

			if($key==$main)
			{
				$activemain = "current";
			}
			else
			{
				$activemain = "";
			}

			if($key!="index"&&$key!="user")
			{
				$furl = "cms";
				$iconf = '<i class="glyphicon glyphicon-pencil"></i>';
				$page = $this->getpagebytype($atcomm+1);
				$data['page'] = $page['page'];

				if($sub == "itemadd")
				{
					$setactive = "current";
          $setactive2 = "";
				}
        else if($sub == "newsadd")
        {
          $setactive = "";
          $setactive2 = "current";
        }
				else
				{
					$setactive = "";
          $setactive2 = "";
				}

				if($key=="comm")
				{
					$setadditem =  '<li class="'.$setactive.'"><a id="additem_'.$key.'" href="'.base_url().'community/itemcommlist?main='.$data['page']['id'].'&type='.($atcomm+1).'" style="cursor:pointer;"> เพิ่มสถานที่ </a></li>';
          $setadditem .=  '<li class="'.$setactive2.'"><a id="additem_'.$key.'" href="'.base_url().'community/newscommlist?main='.$data['page']['id'].'&type='.($atcomm+1).'" style="cursor:pointer;"> เพิ่มข่าวสาร </a></li>';
				}
				else
				{
					$setadditem = '';
				}

				$setsubmenu = $this->getallsubpage($data['page']['id']);
				$setsubpagemenu_row = $setsubmenu['subpage_row'];
				$setsubpagemenu = $setsubmenu['subpage'];
				if($setsubpagemenu_row>0)
				{
					$havesub = "submenu";
					if(($sub!=NULL&&$sub!=""&&$sub!="0"&&$main==$key) || $main == $key || ($main == $key&&$sub == "itemadd") || ($main == $key&&$sub == "newsadd") )
					{
						$activesubmenu = "display: block;";
						$opensubmenu = "open";
					}
					else
					{
						$activesubmenu = "display: none;";
						$opensubmenu = "";
					}

					if($main == $key&&($sub==NULL||$sub==""||$sub=="0"))
					{
						$setactive = "current";
					}
					else
					{
						$setactive = "";
					}

					//$submenusetting = '<ul style="'.$activesubmenu.'">';
					//$setmainsub = '<li class="'.$setactive.'"><a href="'.base_url().$furl.'/'.$value.'">Main</a>';
					$submenusetting .= $setadditem;
					// .= $setmainsub;
						foreach ($setsubpagemenu as $keys => $values)
						{
							if($sub == $values["id"])
							{
								$setactive = "current";
							}
							else
							{
								$setactive = "";
							}
							$submenusetting .= '<li class="'.$setactive.'"><a id="sub_'.$values["id"].'" href="'.base_url().'cms/subpage?main='.$values["page_id"].'&sub='.$values["id"].'&type='.($atcomm+1).'" style="cursor:pointer;"> '.$values["name"].'</a></li>';
						}
					//	$submenusetting .= '</ul>';
				}
				else if($key=="comm"&&$setsubpagemenu_row<=0)
				{
					$havesub = "submenu";
					if($main==$key || ($main == $key&&$sub == "itemadd") || ($main == $key&&$sub == "newsadd"))
					{
						$activesubmenu = "display: block;";
						$opensubmenu = "open";
					}
					else
					{
						$activesubmenu = "display: none;";
						$opensubmenu = "";
					}

					if($main == $key)
					{
						$setactive = "current";
					}
					else
					{
						$setactive = "";
					}

					//$setmainsub = '<li class="'.$setactive.'"><a href="'.base_url().$furl.'/'.$value.'">Main</a>';

					//$submenusetting = '<ul style="'.$activesubmenu.'">';
          $submenusetting .= $setadditem;
					//$submenusetting .= $setmainsub;
				//	$submenusetting .= '</ul>';
				}
				else
				{
					$havesub = "";
					$submenusetting = "";
				}
			}
			else
			{
				$havesub = "";
				$submenusetting = "";
				$opensubmenu = "";
				$furl = "main";
				$iconf = "";
				$setsubpagemenu_row = 0;
			}

			if($key=="comm")
			{
				//$menusetting .= '<li class="'.$activemain.' '.$havesub.' '.$opensubmenu.'"><a href="#">'.$iconf.$menuarrname[$key].'<span class="caret pull-right"></span></a>';
			}
			else if($setsubpagemenu_row<=0)
			{
				$menusetting .= '<li class="'.$activemain.' '.$havesub.' '.$opensubmenu.'"><a href="'.base_url().$furl.'/'.$value.'">'.$iconf.$menuarrname[$key].'</a>';
			}
			else
			{
				$menusetting .= '<li class="'.$activemain.' '.$havesub.' '.$opensubmenu.'"><a href="#">'.$iconf.$menuarrname[$key].'<span class="caret pull-right"></span></a>';
			}

			$menusetting .= $submenusetting;
			$menusetting .= '</li>';

		}
    $menusetting .= "<li><a href='".base_url()."'>กลับไปสู่หน้าเว็ปไซค์</a></li>";
		$menusetting .= "</ul>";

		return $menusetting;
	}


	function getpagebytype($type)
	{
	  $retdata['page'] = $this->CI->select_model->select_where("*","page",array("type = '".$type."'","withdraw = 0"));
	  if($retdata['page']->num_rows())
	  {
	    $retdata['page_row'] = $retdata['page']->num_rows();
	    $retdata['page'] = $retdata['page']->row_array();
	  }
	  else
	  {
	    $retdata['page_row'] = 0;
	    $retdata['page'] = $this->emptypage();
	    $retdata['page']['type'] = $type;
	  }
	  return $retdata;
	}

  function getallsubpage($id)
  {
    $getdata = $this->CI->select_model->select_where("*","subpage",array("page_id = '".$id."'","withdraw = 0"));
    if($getdata->num_rows())
    {
      $setdata['subpage_row'] = $getdata->num_rows();
      $setdata['subpage'] = $getdata->result_array();
      return $setdata;
    }
    else
    {
      $setdata['subpage_row'] = 0;
      $setdata['subpage'] = $this->emptysubpage();
      return $setdata;
    }
  }

  private function emptypage()
  {
    return array(
      "id" => NULL,
      "name" => NULL,
      "description" => NULL,
      "link" => NULL,
      "create_datetime" => NULL,
      "type" => NULL
    );
  }

  private function emptysubpage()
  {
    return array(
      "id" => NULL,
      "name" => NULL,
      "description" => NULL,
      "link" => NULL,
      "create_datetime" => NULL,
      "update_datetime" => NULL,
      "page_id" => NULL,
      "withdraw" => NULL
    );
  }

}
