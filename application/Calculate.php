<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculate_bak extends MY_Calculate_Ipanel {

	public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Bangkok');
        $this->load->model('select_model');
        $this->load->model('update_model');
        $this->load->model('login_model');
        $this->load->model('delete_model');
        if(!$this->session->userdata("Permission")||!$this->session->userdata("Username")||!$this->session->userdata("User_ID"))
		{
			redirect(base_url());
		}
    }

    function checksaleprovince($Project_ID)
    {
    	$jcarr = array("branch");
    	$jvarr = array("branch.Branch_ID = user.Branch_ID");
    	$selprov = $this->select_model->selectwherejoin("*","user","User_ID = '".$this->session->userdata('User_ID')."'",$jcarr,$jvarr,NULL,NULL);
    	if($selprov->num_rows())
		{
			$selprov = $selprov->row_array();
		}
		else
		{
			$selprov = NULL;
		};

    	if($selprov != NULL)
    	{
    		$selprov = $selprov['Province_ID'];
    	}
    	else
    	{
    		redirect(base_url());
    	}
    }

    function calculate_ipanel_apc($Project_ID)
    {
    	$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$le = $editipanel['Ipanel_Pools_Length'];
		$wi = $editipanel['Ipanel_Pools_Width'];
		$dep1 = $editipanel['Ipanel_Pools_Dept1'];
		$dep2 = $editipanel['Ipanel_Pools_Dept2'];
		$dep3 = $editipanel['Ipanel_Pools_Dept3'];
		$alldep = 0;
		$area = 0;
		$perimeter = 0;
		$capacity = 0;

		if($editipanel['Ipanel_Pools_Dept1']!=""&&$editipanel['Ipanel_Pools_Dept1']!=0&&$editipanel['Ipanel_Pools_Dept1']!=NULL)
		{
			$alldep++;
		}
		if($editipanel['Ipanel_Pools_Dept2']!=""&&$editipanel['Ipanel_Pools_Dept2']!=0&&$editipanel['Ipanel_Pools_Dept2']!=NULL)
		{
			$alldep++;
		}
		if($editipanel['Ipanel_Pools_Dept3']!=""&&$editipanel['Ipanel_Pools_Dept3']!=0&&$editipanel['Ipanel_Pools_Dept3']!=NULL)
		{
			$alldep++;
		}

		if($editipanel['Ipanel_Pools_Shape']=="Ovoid")
		{
			$Area = $le*$wi*0.917;
			$Perimeter = (2*($le+$wi))*0.91;
			$Capacity = ($Area*($dep1+$dep2+$dep3))/$alldep ;
		}
		else if($editipanel['Ipanel_Pools_Shape']=="Liberty")
		{
			$Area = $le*$wi*0.81;
			$Perimeter = (2*($le+$wi))*0.85;
			$Capacity = ($Area*($dep1+$dep2+$dep3))/$alldep ;
		}
		else
		{
			$Area = $le*$wi;
			$Perimeter = (2*($le+$wi));
			$Capacity = ($Area*($dep1+$dep2+$dep3))/$alldep ;
		}
		if($exipanel_row>0)
		{
			if($editipanel['Ipanel_Pools_Extra_Shape']=="Rectangular")
			{	
				$sumwl = 0;
				$sum2w = 0;

				foreach ($exipanel as $key => $value) {

					if($value['Ipanel_Extra_Area_Length']==""||$value['Ipanel_Extra_Area_Length']==null)
					{
						$value['Ipanel_Extra_Area_Length'] = 0;
					}
					if($value['Ipanel_Extra_Area_Width']==""||$value['Ipanel_Extra_Area_Width']==null)
					{
						$value['Ipanel_Extra_Area_Width'] = 0;
					}

					$sumwl = $sumwl+($value['Ipanel_Extra_Area_Length']*$value['Ipanel_Extra_Area_Width']);
					$sum2w = $sum2w+$value['Ipanel_Extra_Area_Width'];

				}

				$sum2w = 2*$sum2w;

				$Area = $Area+$sumwl;
				$Perimeter = $Perimeter+$sum2w;
				$Capacity = (($Area)*($dep1+$dep2+$dep3))/$alldep ;

			}
			else if($editipanel['Ipanel_Pools_Extra_Shape']=="Semicircle")
			{
				$sumrad = 0;
				$sumradup2 = 0;

				foreach ($exipanel as $key => $value) {

					if($value['Ipanel_Extra_Area_Radius']==""||$value['Ipanel_Extra_Area_Radius']==null)
					{
						$value['Ipanel_Extra_Area_Radius'] = 0;
					}

					$sumrad = $sumrad+$value['Ipanel_Extra_Area_Radius'];
					$sumradup2 = $sumradup2+($value['Ipanel_Extra_Area_Radius']*$value['Ipanel_Extra_Area_Radius']);
					
				}

				$Area = $Area+(  ( pi()/2 ) * $sumradup2 );
				$Perimeter = $Perimeter+( ( pi()-2 ) * $sumrad );
				$Capacity = (($Area)*($dep1+$dep2+$dep3))/$alldep ;
			}
		}

		return array($Area,$Perimeter,$Capacity);
    }

	function sum_spa_swim_heat_scul_pump($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$badu4716standardqty=0;
		$badu4722standardqty=0;
		$allheatpump=0;
		$sfp1500qty=0;

		if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']!=""&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']!=0)
		{
			if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']>0&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=4)
			{
				$badu4716standardqty += 1;
			}
			else
			{
				$badu4722standardqty += ceil($editipaneloption['Ipanel_Pool_Option_Spa_Jet']/6);
			}
		}

		$baduheatpump = 0;

		if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL)
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}

		if($heatpump!=NULL)
		{
			/*$allheatpump=0;
			$fixnum = 1;
			$heatpump_arr = array();

			foreach ($heatpump as $key => $value) 
			{
				array_push($heatpump_arr, $value['Supplier_Product_Code']);	
			}

			$heatpump_arr = array_count_values($heatpump_arr);

			$nodupheatpump = $this->unique_multidim_array($heatpump,"Supplier_Product_Code");


			foreach ($nodupheatpump as $key => $value) 
			{
				foreach ($heatpump_arr as $key2 => $value2) {
					if($key2==$value['Supplier_Product_Code'])
					{
						$heatpumpqty = $value2;
						$allheatpump += $heatpumpqty;
						break;
					}
					else
					{
						$heatpumpqty = $fixnum;
						$allheatpump += $fixnum;
						break;
					}
				}
			}*/
			if(sizeof($heatpump)==1)
			{
				$baduheatpump = 1;
			}
			else if(sizeof($heatpump)>1)
			{
				$baduheatpump = ceil(sizeof($heatpump)/2);
			}

		}

		if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
		{
			$sfp1500qty = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
		}

		if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>0&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!="")
		{
			if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/16")
			{
				$badu4716standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
			}
			elseif($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/22")
			{
				$badu4722standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
			}
		}

		$sum = $badu4716standardqty+$badu4722standardqty+$baduheatpump+$sfp1500qty;

		return $sum;
	}

	function calcable_10x2_5($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$sfp1500qty = 0;
		$badu4722standardqty = 0;
		$badu4716standardqty = 0;

		if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
		{
			$sfp1500qty = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
		}

		if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>0&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!="")
		{
			if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/16")
			{
				$badu4716standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
			}
			elseif($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/22")
			{
				$badu4722standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
			}
		}

		if($editipanel['Ipanel_Pools_Where']=="home")
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
		}
		else
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
		}

		$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

		$te20count = $Use_Filtration[0];
		$te25count = $Use_Filtration[1];
		$bikinicount = $Use_Filtration[2];

		return ceil( ($te20count+$te25count+$sfp1500qty+$badu4716standardqty+$badu4722standardqty)*20 );
	}



	function calculate_ipanel_structure($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate

		$ipanelstructure=NULL;
		$bolt=NULL;
		$panelclip=NULL;
		$struct60=NULL;
		$struct120=NULL;
		$fiberpanel=NULL;
		$outsidecerve=NULL;

		$te20count=0;
		$te25count=0;
		$bikinicount=0;

		$sumpricestructure=0;

		$ipanel_structure = array();
		$arrcount = 0;

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FST10-00102'){
					$ipanelstructure = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='RSS30-00300'){
					$bolt = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00802'){
					$panelclip = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00300'){
					$struct60 = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00310'){
					$struct120 = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00502'){
					$fiberpanel = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00401'){
					$outsidecerve = $value;
					$arrcount++;
				}
			}

			$ipanelstructure['QTY']=0;
			$bolt['QTY']=0;
			$panelclip['QTY']=0;
			$struct60['QTY']=0;
			$struct120['QTY']=0;
			$fiberpanel['QTY']=0;
			$outsidecerve['QTY']=0;

			if($editipanel['Ipanel_Pools_Where']=="home")
			{
				$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
			}
			else
			{
				$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
			}

			$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

			$te20count = $Use_Filtration[0];
			$te25count = $Use_Filtration[1];
			$bikinicount = $Use_Filtration[2];

			$ipanelstructure['QTY'] = $Perimeter;
			$ipanelstructure['QTY'] = $ipanelstructure['QTY']-($te20count*0.75);
			$ipanelstructure['QTY'] = $ipanelstructure['QTY']-($te25count*0.75);

			if($editipanel['Ipanel_Pools_Shape']=="Ovoid")
			{
				$ipanelstructure['QTY'] = $ipanelstructure['QTY']-1;
			}
			if($editipanel['Ipanel_Pools_Shape']=="Rectangular")
			{
				$ipanelstructure['QTY'] = $ipanelstructure['QTY']-1;
			}

			$panelclip['QTY'] = ceil($ipanelstructure['QTY']/1.5)*5;

			if($editipanel['Ipanel_Pools_Shape']=="Ovoid")
			{
				$fiberpanel['QTY'] = $fiberpanel['QTY']+(1*4);
			}
			if($editipanel['Ipanel_Pools_Shape']=="Rectangular")
			{
				$fiberpanel['QTY'] = $fiberpanel['QTY']+(1*4);
			}

			$bolt['QTY'] = ($panelclip['QTY']*1.2)+($fiberpanel['QTY']+$outsidecerve['QTY']+$te25count+$te20count)*10;

			$struct60['QTY'] = ceil($ipanelstructure['QTY']/1.5)+$fiberpanel['QTY']+$outsidecerve['QTY']+$te25count+$te20count+1+$editipanelcomponent['Ipanel_Pool_Component_By_Pass_Jet']; //(1 = encappump + standardpump)
			$struct120['QTY'] = $struct60['QTY'];

			array_push($ipanel_structure, $ipanelstructure);
			array_push($ipanel_structure, $panelclip);
			array_push($ipanel_structure, $fiberpanel);
			array_push($ipanel_structure, $outsidecerve);
			array_push($ipanel_structure, $bolt);
			array_push($ipanel_structure, $struct60);
			array_push($ipanel_structure, $struct120);

			$sumpricestructure = $sumpricestructure+($ipanelstructure['QTY']*$ipanelstructure['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($panelclip['QTY']*$panelclip['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($fiberpanel['QTY']*$fiberpanel['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($outsidecerve['QTY']*$outsidecerve['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($bolt['QTY']*$bolt['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($struct60['QTY']*$struct60['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($struct120['QTY']*$struct120['Supplier_Product_Price']);

			return array($ipanel_structure, $sumpricestructure);

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Structure</td></tr>";
			//end head
			/*foreach ($ipanel_structure as $key => $value) {
				echo "<tr style='border: 1px solid black;'>";
				echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
				echo "</tr>";
			}
			

			echo $sumpricestructure;*/
		}
		else
		{
			return array(NULL, NULL);
		}

	}

	function calculate_ipanel_lightblue_wall($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate
		$wallliner = NULL;
		$walllinerlockstrip = NULL;
		$walllinerlockinsert = NULL;
		$infinitylinerlock = NULL;

		$sumpricewallliner = 0;

		$arraypoolwall = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI00-00509'){
					$wallliner = $value;
				}
				if($value['Supplier_Product_Code']=='FLI21-00301'){
					$walllinerlockstrip = $value;
				}
				if($value['Supplier_Product_Code']=='FLI21-00302'){
					$walllinerlockinsert = $value;
				}
				if($value['Supplier_Product_Code']=='FST10-00601'){
					$infinitylinerlock = $value;
				}
			}

			$infinitylinerlock['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] > 0&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!="")
			{
				
				$infinityedge_liner = ($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']*2)+2;

				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Build']=="BuildOnSite")
				{
					$infinityedge_liner += 2;

					$infinitylinerlock['QTY'] = $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']+2;
				}
			}
			else
			{
				$infinityedge_liner = 0;
			}


			$wallliner['QTY'] = ceil($Perimeter + 1 + $infinityedge_liner); 
			$walllinerlockstrip['QTY'] = ceil($Perimeter + 1);
			$walllinerlockinsert['QTY'] = ceil($Perimeter + 1);

			array_push($arraypoolwall, $wallliner);
			array_push($arraypoolwall, $walllinerlockstrip);
			array_push($arraypoolwall, $walllinerlockinsert);
			array_push($arraypoolwall, $infinitylinerlock);

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Light Blue Wall Liner</td></tr>";
			//end head
			$sumpricewallliner = $sumpricewallliner+($wallliner['QTY']*$wallliner['Supplier_Product_Price']);
			$sumpricewallliner = $sumpricewallliner+($walllinerlockstrip['QTY']*$walllinerlockstrip['Supplier_Product_Price']);
			$sumpricewallliner = $sumpricewallliner+($walllinerlockinsert['QTY']*$walllinerlockinsert['Supplier_Product_Price']);

			return array($arraypoolwall, $sumpricewallliner);


		}
		else
		{
			return array(NULL, NULL);
		}
	}

	function calculate_ipanel_lightblue_floor($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate
		$floorliner = NULL;

		$sumpricefloorliner = 0;

		$arrayfloorliner = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI00-00508'){
					$floorliner = $value;
				}
			}

			$floorliner['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']==NULL||$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] =="")
			{
				$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']  = 0;
			}

			$floorliner['QTY'] = ceil( (($Area+($Perimeter/2))/1.3) + $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] );

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Light Blue Floor Liner</td></tr>";
			//end head

			$sumpricefloorliner = $sumpricefloorliner+($floorliner['QTY']*$floorliner['Supplier_Product_Price']);

			array_push($arrayfloorliner, $floorliner);

			return array($arrayfloorliner, $sumpricefloorliner);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_davinci_wall($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate
		$wallliner = NULL;
		$walllinerlockstrip = NULL;
		$walllinerlockinsert = NULL;

		$sumpricewallliner = 0;

		$arrdavinciwall = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI00-00509'){
					$wallliner = $value;
				}
				if($value['Supplier_Product_Code']=='FLI21-00301'){
					$walllinerlockstrip = $value;
				}
				if($value['Supplier_Product_Code']=='FLI21-00302'){
					$walllinerlockinsert = $value;
				}
			}

			$wallliner['QTY'] = 0;
			$walllinerlockstrip['QTY'] = 0;
			$walllinerlockinsert['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] > 0)
			{
				$infinityedge_liner = ($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']*2)+2;
			}
			else
			{
				$infinityedge_liner = 0;
			}

			if($editipanel['Ipanel_Pools_Series']!="davinciblue")
			{
				$wallliner['QTY'] = ceil($Perimeter + 1 + $infinityedge_liner);
			}
			else
			{
				$wallliner['QTY'] = 0;
			}

			$walllinerlockstrip['QTY'] = ceil($Perimeter + 1 +$infinityedge_liner);
			$walllinerlockinsert['QTY'] = ceil($Perimeter + 1 +$infinityedge_liner);

			array_push($arrdavinciwall, $wallliner);
			array_push($arrdavinciwall, $walllinerlockstrip);
			array_push($arrdavinciwall, $walllinerlockinsert);

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Light Blue Wall Liner</td></tr>";
			//end head

			$sumpricewallliner = $sumpricewallliner+($wallliner['QTY'] *$wallliner['Supplier_Product_Price']);
			$sumpricewallliner = $sumpricewallliner+($walllinerlockstrip['QTY']*$walllinerlockstrip['Supplier_Product_Price']);
			$sumpricewallliner = $sumpricewallliner+($walllinerlockinsert['QTY']*$walllinerlockinsert['Supplier_Product_Price']);

			return array($arrdavinciwall,$sumpricewallliner);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_davinci_floor($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate
		$floorliner = NULL;

		$sumpricefloorliner = 0;

		$arrdavincifloor = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI00-00900'){
					$floorliner = $value;
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']==NULL||$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] =="")
			{
				$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']  = 0;
			}
			else
			{
				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Build']=="BuildOnSite")
				{
					$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] += 2;
				}
			}

			$floorliner['QTY'] = ceil( (($Area+($Perimeter/2))/1.6) + $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] );

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Davinchi Liner</td></tr>";
			//end head
			$sumpricefloorliner = $sumpricefloorliner+($floorliner['QTY']*$floorliner['Supplier_Product_Price']);

			array_push($arrdavincifloor, $floorliner);

			return array($arrdavincifloor,$sumpricefloorliner);
		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_filtration($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL)
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}

		
		//set calculate
		$te20panel = NULL;
		$te25panel = NULL;
		$bikinipanel = NULL;
		$badu4716standard = NULL;
		$badu4722standard = NULL;
		$badu4716encapsulate = NULL;
		$badu4722encapsulate = NULL;
		$badusinglepump = NULL;
		$badudoublepump = NULL;
		$standardpoolcontrol = NULL;
		$x20cl = NULL;
		$x30cl = NULL;

		$sumpriceflitration = 0;

		$arrfiltration = array();

		if($editipanel['Ipanel_Pools_Where']=="home")
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
		}
		else
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
		}

		if($product_row>0)
		{

			$Use_Filtration = $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);	

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FFI20-00715'){
					$te20panel = $value;
				}
				if($value['Supplier_Product_Code']=='FFI20-00708'){
					$te25panel = $value;
				}
				if($value['Supplier_Product_Code']=='FFI21-00504')
				{
					$bikinipanel = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00200'){
					$badu4716standard = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00201'){
					$badu4722standard = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00203'){
					$badu4716encapsulate = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00204'){
					$badu4722encapsulate = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00117'){
					$badusinglepump = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00122'){
					$badudoublepump = $value;
				}
				if($value['Supplier_Product_Code']=='FPU10-00104'){
					$standardpoolcontrol = $value;
				}
				if($value['Supplier_Product_Code']=='FAC30-00200'){
					$x20cl = $value;
				}
				if($value['Supplier_Product_Code']=='FAC30-00201'){
					$x30cl = $value;
				}
			}

			$te20panel['QTY'] = $Use_Filtration[0];
			$te25panel['QTY'] = $Use_Filtration[1];
			$bikinipanel['QTY'] = $Use_Filtration[2];
			$badu4716standard['QTY'] = 0;
			$badu4722standard['QTY'] = 0;
			$badu4716encapsulate['QTY'] = 0;
			$badu4722encapsulate['QTY'] = 0;
			$badusinglepump['QTY'] = 0;
			$badudoublepump['QTY'] = 0;
			$standardpoolcontrol['QTY'] = 0;
			$x20cl['QTY'] = 0;
			$x30cl['QTY'] = 0;

			if($editipanelcomponent['Ipanel_Pool_Component_Filter_Pump']=='Standard')
			{
				$badu4716standard['QTY'] = $te20panel['QTY'];
				$badu4722standard['QTY'] = $te25panel['QTY'];
				if($bikinipanel['QTY']>0)
				{
					$badu4716standard['QTY'] = $bikinipanel['QTY'];
				}
			}

			if($editipanelcomponent['Ipanel_Pool_Component_Filter_Pump']=='Endcapsulated')
			{
				$badu4716encapsulate['QTY'] = $te20panel['QTY'];
				$badu4722encapsulate['QTY'] = $te25panel['QTY'];
				if($bikinipanel['QTY']>0)
				{
					$badu4716encapsulate['QTY'] = $bikinipanel['QTY'];
				}
			}

			if($editipanelcomponent['Ipanel_Pool_Component_Control_Panel']=='salt')
			{
				$x20cl['QTY'] = $te20panel['QTY'];
				$x30cl['QTY'] = $te25panel['QTY'];
				if($bikinipanel['QTY']>0)
				{
					$x20cl['QTY'] = $bikinipanel['QTY'];
				}
			}
			else
			{
				if($te20panel['QTY']>=1)
				{
					$standardpoolcontrol['QTY'] = $te20panel['QTY'];
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=0)
			{
				$badu4716standard['QTY'] = $badu4716standard['QTY'];
				$badu4722standard['QTY'] = $badu4722standard['QTY'];
			}
			if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']>0&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=4)
			{
				$badu4716standard['QTY'] += 1;
			}
			else
			{
				$badu4722standard['QTY'] += ceil($editipaneloption['Ipanel_Pool_Option_Spa_Jet']/6);
			}

			if($heatpump!=NULL)
			{
				if(sizeof($heatpump)==1)
				{
					$badu4716standard['QTY'] += 1;
				}
				else if(sizeof($heatpump)>1)
				{
					$badu4722standard['QTY'] += ceil(sizeof($heatpump)/2);
				}

			}

		if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>0&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!=NULL)
			{
				if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/16")
				{
					$badu4716standard['QTY'] += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
				}
				elseif($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/22")
				{
					$badu4722standard['QTY'] += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
				}
			}

			if(($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY']-$te20panel['QTY']-$te25panel['QTY'])/2>0.45)
			{
				$badudoublepump['QTY'] = floor( (($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY']-$te20panel['QTY']-$te25panel['QTY'])/2) );
			}
			else
			{
				$badudoublepump['QTY'] = 0;
			}

			if(($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY']-$te20panel['QTY']-$te25panel['QTY'])/2>0.45)
			{
				$badusinglepump['QTY'] = ceil( (($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY']-$te20panel['QTY']-$te25panel['QTY'])/2) - $badudoublepump['QTY'] );
			}
			else
			{
				$badusinglepump['QTY'] = 0;
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Flitration</td></tr>";
			//end head

			$sumpriceflitration = $sumpriceflitration+($te20panel['QTY']*$te20panel['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($te25panel['QTY']*$te25panel['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($bikinipanel['QTY']*$bikinipanel['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badu4716standard['QTY']*$badu4716standard['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badu4722standard['QTY']*$badu4722standard['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badu4716encapsulate['QTY']*$badu4716encapsulate['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badu4722encapsulate['QTY']*$badu4722encapsulate['Supplier_Product_Price']);

			$useheatpump = array();

			if($heatpump!=NULL)
			{
				
				$fixnum = 1;
				$heatpump_arr = array();

				foreach ($heatpump as $key => $value) 
				{
					array_push($heatpump_arr, $value['Supplier_Product_Code']);	
				}

				$heatpump_arr = array_count_values($heatpump_arr);

				$nodupheatpump = $this->unique_multidim_array($heatpump,"Supplier_Product_Code");

				$useheatpump = array_values($nodupheatpump);

				foreach ($useheatpump as $key => $value) 
				{
					//$useheatpump[$key]['QTY'] = 0;
					foreach ($heatpump_arr as $key2 => $value2) {
						if($key2==$value['Supplier_Product_Code'])
						{
							$thisqty = $value2;
							$useheatpump[$key]['QTY'] = $value2;
							break;
						}
						else
						{
							$thisqty = $fixnum;
							$useheatpump[$key]['QTY'] = $fixnum;
							break;
						}
					}
					$sumpriceflitration = $sumpriceflitration+($thisqty*$value['Supplier_Product_Price']);
				}
			}

			$sumpriceflitration = $sumpriceflitration+($badusinglepump['QTY']*$badusinglepump['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badudoublepump['QTY']*$badudoublepump['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($standardpoolcontrol['QTY']*$standardpoolcontrol['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($x20cl['QTY']*$x20cl['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($x30cl['QTY']*$x30cl['Supplier_Product_Price']);

			array_push($arrfiltration, $te20panel);
			array_push($arrfiltration, $te25panel);
			array_push($arrfiltration, $bikinipanel);
			array_push($arrfiltration, $badu4716standard);
			array_push($arrfiltration, $badu4722standard);
			array_push($arrfiltration, $badu4716encapsulate);
			array_push($arrfiltration, $badu4722encapsulate);
			if($heatpump!=NULL)
			{
				foreach ($useheatpump as $key => $value) {
					array_push($arrfiltration, $value);
				}
			}
			array_push($arrfiltration, $badusinglepump);
			array_push($arrfiltration, $badudoublepump);
			array_push($arrfiltration, $standardpoolcontrol);
			array_push($arrfiltration, $x20cl);
			array_push($arrfiltration, $x30cl);

			
			return array($arrfiltration,$sumpriceflitration);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_underwaterlight($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$arrled = array();

		$colorled = NULL;
		$whiteled = NULL;
		$abs = NULL;

		$sumpriceled = 0;

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI51-00502'){
					$whiteled = $value;
				}
				if($value['Supplier_Product_Code']=='FLI51-00512'){
					$colorled = $value;
				}
				if($value['Supplier_Product_Code']=='FLI52-00101'){
					$abs = $value;
				}
			}

			$colorled['QTY'] = 0;
			$whiteled['QTY'] = 0;
			$abs['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Under_Water_Light']=="white")
			{
				$whiteled['QTY'] = $editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'];
			}
			elseif($editipaneloption['Ipanel_Pool_Option_Under_Water_Light']=="color")
			{
				$colorled['QTY'] = $editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'];
			}

			$abs['QTY'] = $colorled['QTY']+$whiteled['QTY'];

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>UNDER WATER LIGHT</td></tr>";
			//end head
			$sumpriceled = $sumpriceled+($colorled['QTY']*$colorled['Supplier_Product_Price']);
			$sumpriceled = $sumpriceled+($whiteled['QTY']*$whiteled['Supplier_Product_Price']);
			$sumpriceled = $sumpriceled+($abs['QTY']*$abs['Supplier_Product_Price']);

			array_push($arrled, $colorled);
			array_push($arrled, $whiteled);
			array_push($arrled, $abs);

			return array($arrled,$sumpriceled);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_spaoption($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$hmcp = NULL;
		$spajet = NULL;
		$hmcrp = NULL;
		$hmcrpf6 = NULL;

		$sumpricespaoption = 0;

		$arrspa = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FPU10-00102'){
					$hmcp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC20-00300'){
					$spajet = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00201'){
					$hmcrp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00202'){
					$hmcrpf6 = $value;
				}
			}

			$hmcp['QTY'] = 0;
			$spajet['QTY'] = 0;
			$hmcrp['QTY'] = 0;
			$hmcrpf6['QTY'] = 0;

			$spajet['QTY'] = $editipaneloption['Ipanel_Pool_Option_Spa_Jet'];


			if($spajet['QTY'] == NULL)
			{
				$spajet['QTY'] = 0;
			}
			if($spajet['QTY']>1)
			{
				$hmcp['QTY'] = 1;
			}

			if($spajet['QTY']>0&&$spajet['QTY']<=4)
			{
				$hmcrp['QTY'] = 1;
			}
			elseif($spajet['QTY']>4)
			{
				$hmcrpf6['QTY'] = 1;
			}
			else
			{
				$hmcrp['QTY'] = 0;
				$hmcrpf6['QTY'] = 0;
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>SPA OPTION</td></tr>";
			//end head

			$sumpricespaoption = $sumpricespaoption+($hmcp['QTY']*$hmcp['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($spajet['QTY']*$spajet['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($hmcrp['QTY']*$hmcrp['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($hmcrpf6['QTY']*$hmcrpf6['Supplier_Product_Price']);

			array_push($arrspa, $hmcp);
			array_push($arrspa, $spajet);
			array_push($arrspa, $hmcrp);
			array_push($arrspa, $hmcrpf6);

			return array($arrspa,$sumpricespaoption);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_inletandfitting($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL)
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}
		
		$onga = NULL;
		$returneyeball = NULL;
		$eyeball = NULL;
		$speedupjet = NULL;

		$sumpriceinletandfitting = 0;

		$arrinlet = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI40-00800'){
					$onga = $value;
				}
				if($value['Supplier_Product_Code']=='FLI40-00102'){
					$returneyeball = $value;
				}
				if($value['Supplier_Product_Code']=='FLI40-00100'){
					$eyeball = $value;
				}
				if($value['Supplier_Product_Code']=='FFI60-00512'){
					$speedupjet = $value;
				}
			}

			$onga['QTY'] = 0;
			$returneyeball['QTY'] = 0;
			$eyeball['QTY'] = 0;
			$speedupjet['QTY'] = 0;

			$returneyeball['QTY'] = (ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15)*3)+$editipanelcomponent['Ipanel_Pool_Component_By_Pass_Jet'];

			if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
			{
				$eyeball['QTY'] = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
				$speedupjet['QTY'] = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
			}

			if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>=1)
			{
				$onga['QTY'] += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']*2;
			}

			if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
			{
				$onga['QTY'] += $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit']*2;
			}

			if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']>0&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=4)
			{
				$onga['QTY'] += 2;
			}
			else
			{
				$onga['QTY'] += (ceil($editipaneloption['Ipanel_Pool_Option_Spa_Jet']/6))*2;
			}

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>0)
			{
				$onga['QTY'] += 2;
			}

			if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']=="saparate")
			{
				$onga['QTY'] += ((ceil(sizeof($heatpump)/2))*2);
				$returneyeball['QTY'] += ((ceil(sizeof($heatpump)/2))*3);
			}
			else if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']=="bypass")
			{
				$eyeball['QTY'] += ((sizeof($heatpump))*2);
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>INLET & FITTING</td></tr>";
			//end head

			$sumpriceinletandfitting = $sumpriceinletandfitting+($onga['QTY']*$onga['Supplier_Product_Price']);
			$sumpriceinletandfitting = $sumpriceinletandfitting+($returneyeball['QTY']*$returneyeball['Supplier_Product_Price']);
			$sumpriceinletandfitting = $sumpriceinletandfitting+($eyeball['QTY']*$eyeball['Supplier_Product_Price']);
			$sumpriceinletandfitting = $sumpriceinletandfitting+($speedupjet['QTY']*$speedupjet['Supplier_Product_Price']);

			array_push($arrinlet, $onga);
			array_push($arrinlet, $returneyeball);
			array_push($arrinlet, $eyeball);
			array_push($arrinlet, $speedupjet);

			return array($arrinlet,$sumpriceinletandfitting);

		}
		else
		{
			return array(NULL,NULL);
		}
	}


	function calculate_ipanel_infinityedge($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$waterfall = NULL;
		$waterfallcontrol = NULL;

		$sumpriceinfinityedge = 0;

		$arrinfinity  = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FAC40-01502'){
					$waterfall = $value;
				}
				if($value['Supplier_Product_Code']=='FPU10-00109'){
					$waterfallcontrol = $value;
				}
			}

			$waterfall['QTY'] = 0;
			$waterfallcontrol['QTY'] = 0;

			if($editipanel['Ipanel_Pools_Series']!="davinciblue")
			{
				$waterfall['QTY'] = 0;
			}
			else
			{
				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Build']=="Compact")
				{
					$waterfall['QTY'] = floor($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']+1);
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>0)
			{
				$waterfallcontrol['QTY'] = 1;
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>INFINITY EDGE</td></tr>";
			//end head
			$sumpriceinfinityedge = $sumpriceinfinityedge+($waterfall['QTY']*$waterfall['Supplier_Product_Price']);
			$sumpriceinfinityedge = $sumpriceinfinityedge+($waterfallcontrol['QTY']*$waterfallcontrol['Supplier_Product_Price']);

			array_push($arrinfinity, $waterfall);
			array_push($arrinfinity, $waterfallcontrol);

			return array($arrinfinity, $sumpriceinfinityedge);
		}
		else
		{
			return array(NULL, NULL);
		}
	}

	function calculate_ipanel_otheroption($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$inteliswimjet = NULL;
		$sfp1500 = NULL;
		$ltppump = NULL;
		$ppppump = NULL;

		$sfppumproom = NULL;
		$sproomltp = NULL;
		$sproomppp = NULL;

		$doublepumproomltpsfp = NULL;
		$dproomltp = NULL;
		$dproomppp = NULL;

		$ltpandsfp = NULL;
		$pppandsfp = NULL;
		$pppandltp = NULL;

		
		$stainlesssteel3step = NULL;

		$sumpriceotheroption = 0;

		$arrother = array();

		if($editipaneloption['Ipanel_Pool_Option_Logo']!=NULL)
		{
			$uselogo = $this->getlogo($editipaneloption['Ipanel_Pool_Option_Logo']);
		}
		else
		{
			$uselogo = NULL;
		}

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FFI21-00303'){
					$inteliswimjet = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-10002'){
					$ltppump = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-12501'){
					$ppppump = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-20003'){
					$sfp1500 = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00100'){
					$sproomltp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00101'){
					$sproomppp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00103'){
					$sfppumproom = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00133'){
					$dproomltp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00134'){
					$dproomppp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00107'){
					$doublepumproomltpsfp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00108'){
					$ltpandsfp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00120'){
					$pppandsfp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00127'){
					$pppandltp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC10-00204'){
					$stainlesssteel3step = $value;
				}
			}

			$inteliswimjet['QTY'] = 0;
			$sfp1500['QTY'] = 0;
			$ltppump['QTY'] = 0;
			$ppppump['QTY'] = 0;

			$sfppumproom['QTY'] = 0;
			$sproomltp['QTY'] = 0;
			$sproomppp['QTY'] = 0;

			$doublepumproomltpsfp['QTY'] = 0;
			$dproomltp['QTY'] = 0;
			$dproomppp['QTY'] = 0;

			$ltpandsfp['QTY'] = 0;
			$pppandsfp['QTY'] = 0;
			$pppandltp['QTY'] = 0;
			$stainlesssteel3step['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="aquaflow")
			{
				$inteliswimjet['QTY'] = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
			}

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>=1&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']<=6)
			{
				$ltppump['QTY'] = ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
			}
			else if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>=7&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']<=10)
			{
				$ppppump['QTY'] = ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
			}
			else if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>=11&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']<=14)
			{
				$sfp1500['QTY'] = ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
			}
			else
			{
				$sfp1500['QTY'] = floor($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
				$otherpump = $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] - floor($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
				if($otherpump>=1&&$otherpump<=6)
				{
					$ltppump['QTY'] = 1;
				}
				else if($otherpump>=7&&$otherpump<=10)
				{
					$ppppump['QTY'] = 1;
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
			{
				$sfp1500['QTY'] += $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
			}

			if(($sfp1500['QTY']/2)>0.45)
			{
				$doublepumproomltpsfp['QTY'] = floor ($sfp1500['QTY']/2);
			}

			if(($sfp1500['QTY']/2)>0.45)
			{
				$sfppumproom['QTY'] = ceil($sfp1500['QTY']/2)-$doublepumproomltpsfp['QTY'];
			}

			if($ppppump['QTY']>0||$ltppump['QTY']>0)
			{
				if($sfppumproom['QTY']>=1)
				{
					$sfppumproom['QTY'] = $sfppumproom['QTY'] -1;

					$ltpandsfp['QTY'] = 1;
					$pppandsfp['QTY'] = 1;
				}
				else
				{
					$sproomltp['QTY'] = 1;
					$sproomppp['QTY'] = 1;
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Ladder']=="Stainless")
			{
				$stainlesssteel3step['QTY'] = 	$editipaneloption['Ipanel_Pool_Option_Ladder_Unit'];
			}

			if($robot!=NULL)
			{
				$robot = $robot[0];
				$robot['QTY'] = 1;
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>OTHER OPTION</td></tr>";
			//end head

			$sumpriceotheroption = $sumpriceotheroption+($inteliswimjet['QTY']*$inteliswimjet['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($ppppump['QTY']*$ppppump['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($ltppump['QTY']*$ltppump['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sfp1500['QTY']*$sfp1500['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sproomltp['QTY']*$sproomltp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sproomppp['QTY']*$sproomppp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sfppumproom['QTY']*$sfppumproom['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($ltpandsfp['QTY']*$ltpandsfp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($pppandsfp['QTY']*$pppandsfp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($doublepumproomltpsfp['QTY']*$doublepumproomltpsfp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($stainlesssteel3step['QTY']*$stainlesssteel3step['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($robot['QTY']*$robot['Supplier_Product_Price']);

			$setlogo = $uselogo[0];

			if($editipaneloption['Ipanel_Pool_Option_Logo']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Logo']!="")
			{
				foreach ($uselogo[0] as $key => $value) {

					foreach ($uselogo[1] as $key1 => $value1) {

						if($key1==$value['Supplier_Product_Code'])
						{
							$setlogo[$key]['QTY'] = $value1;
							$thisqty = $value1;
							break;
						}
						else
						{
							$setlogo[$key]['QTY'] = 0;
							$thisqty = 0;
						}
					}

					$sumpriceotheroption = $sumpriceotheroption+($thisqty*$value['Supplier_Product_Price']);

				}
			}

			array_push($arrother, $inteliswimjet);
			array_push($arrother, $ppppump);
			array_push($arrother, $ltppump);
			array_push($arrother, $sfp1500);
			array_push($arrother, $sproomltp);
			array_push($arrother, $sproomppp);
			array_push($arrother, $sfppumproom);
			array_push($arrother, $ltpandsfp);
			array_push($arrother, $pppandsfp);
			array_push($arrother, $doublepumproomltpsfp);
			array_push($arrother, $stainlesssteel3step);
			array_push($arrother, $robot);
			
			if($editipaneloption['Ipanel_Pool_Option_Logo']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Logo']!="")
			{
				foreach ($setlogo as $key => $value) {
					array_push($arrother, $value);
				}
			}

			return array($arrother,$sumpriceotheroption);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_maintenent($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$vacuum12 = NULL;
		$vacuum15 = NULL;
		$vacuumhead = NULL;
		$leaf = NULL;
		$filterbag = NULL;
		$telescopic = NULL;
		$wall = NULL;
		$thermomiter = NULL;
		$testkid = NULL;

		$sumpricemaintenent = 0;

		$arrmaintain = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FPM00-00112'){
					$vacuum12 = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00123'){
					$vacuum15 = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00203'){
					$vacuumhead = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00303'){
					$leaf = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00400'){
					$filterbag = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00503'){
					$telescopic = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00604'){
					$wall = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00702'){
					$thermomiter = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00812'){
					$testkid = $value;
				}
			}

			$vacuum12['QTY'] = 0;
			$vacuum15['QTY'] = 0;
			$vacuumhead['QTY'] = 1;
			$leaf['QTY'] = 1;
			$filterbag['QTY'] = 1;
			$telescopic['QTY'] = 1;
			$wall['QTY'] = 1;
			$thermomiter['QTY'] = 1;
			$testkid['QTY'] = 1;

			if($editipanel['Ipanel_Pools_Length']<9)
			{
				$vacuum12['QTY'] = 1;
			}
			else
			{
				$vacuum15['QTY'] = 1;
			}

			if($editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories']>0)
			{
				$vacuumhead['QTY'] = $vacuumhead['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$leaf['QTY'] = $leaf['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$filterbag['QTY'] = $filterbag['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$telescopic['QTY'] = $telescopic['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$wall['QTY'] = $wall['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$thermomiter['QTY'] = $thermomiter['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$testkid['QTY'] = $testkid['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Maintenent Accessories</td></tr>";
			//end head

			$sumpricemaintenent = $sumpricemaintenent+($vacuum12['QTY']*$vacuum12['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($vacuum15['QTY']*$vacuum15['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($vacuumhead['QTY']*$vacuumhead['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($leaf['QTY']*$leaf['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($filterbag['QTY']*$filterbag['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($telescopic['QTY']*$telescopic['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($wall['QTY']*$wall['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($thermomiter['QTY']*$thermomiter['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($testkid['QTY']*$testkid['Supplier_Product_Price']);

			array_push($arrmaintain, $vacuum12);
			array_push($arrmaintain, $vacuum15);
			array_push($arrmaintain, $vacuumhead);
			array_push($arrmaintain, $leaf);
			array_push($arrmaintain, $filterbag);
			array_push($arrmaintain, $telescopic);
			array_push($arrmaintain, $wall);
			array_push($arrmaintain, $thermomiter);
			array_push($arrmaintain, $testkid);

			return array($arrmaintain,$sumpricemaintenent);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_chemical($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$longlasting = NULL;
		$chlorine = NULL;
		$php = NULL;
		$phm = NULL;
		$jdclean = NULL;

		$sumpricechem = 0;

		$arrchem = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FCH00-00101'){
					$longlasting = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00111'){
					$chlorine = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00200'){
					$php = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00210'){
					$phm = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00300'){
					$jdclean = $value;
				}
			}

			$longlasting['QTY'] = 1;
			$chlorine['QTY'] = 1;
			$php['QTY'] = 1;
			$phm['QTY'] = 1;
			$jdclean['QTY'] = 1;

			if($editipanelcomponent['Ipanel_Pool_Component_Chemicals']>0)
			{
				$longlasting['QTY'] = $longlasting['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
				$chlorine['QTY'] = $chlorine['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
				$php['QTY'] = $php['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
				$phm['QTY'] = $phm['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
				$jdclean['QTY'] = $jdclean['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Chemical Treatment</td></tr>";
			//end head

			$sumpricechem = $sumpricechem+($longlasting['QTY']*$longlasting['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($chlorine['QTY']*$chlorine['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($php['QTY']*$php['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($phm['QTY']*$phm['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($jdclean['QTY']*$jdclean['Supplier_Product_Price']);

			array_push($arrchem,$longlasting);
			array_push($arrchem,$chlorine);
			array_push($arrchem,$php);
			array_push($arrchem,$phm);
			array_push($arrchem,$jdclean);

			return array($arrchem, $sumpricechem);

		}
		else
		{
			return array(NULL,NULL);
		}
	}
	function structure_boq_ipanel($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$distanct = $this->calkm($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$wherearr = array("structure.Pool_Type = 'ipanel'");
		$joincarr = array("product_unit","structure_by_province");
		$joinvarr = array("structure.Product_Unit_ID = product_unit.Product_Unit_ID","structure.Structure_ID = structure_by_province.Structure_ID");

		$boqipanel = $this->select_model->selectwherejoin("*","structure",$wherearr,$joincarr,$joinvarr,NULL,NULL);

		if($boqipanel->num_rows())
		{
			$boqipanel_row = $boqipanel->num_rows();
			$boqipanel = $boqipanel->result_array();
		}
		else
		{
			$boqipanel = NULL;
			$boqipanel_row=0;
		}

		$returnboqipanel = array();

		$elecpvcmatsum = 0;
		$elecpvclabsum = 0;
		$allpipe = 0;

		foreach ($boqipanel as $keyboq => $valueboq) {
			if($valueboq['Structure_Code']==1)
			{
				if( ($distanct/1000)<=50 )
				{
					//$valueboq['Structure_Labour_Unit_Cost'] = 2000;
					$valueboq['Structure_Labour_Unit_Cost'] = 0;
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 3000;
				}
				$valueboq['QTY'] = 1;
			}
			else if($valueboq['Structure_Code']==2)
			{
				if( ($distanct/1000)<=50 )
				{
					//$valueboq['Structure_Labour_Unit_Cost'] = 7000;
					$valueboq['Structure_Labour_Unit_Cost'] = 0;
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 10000;
				}
				$valueboq['QTY'] = 1;
			}
			else if($valueboq['Structure_Code']==3)
			{
				$valueboq['QTY'] = 0;
			}
			else if($valueboq['Structure_Code']==4)
			{
				$mac = 0;
				$men = 0;
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Standard')
				{
					
					if($editipanelcomponent['Ipanel_Pool_Component_Dig']=='Machine')
					{
						$mac = 1;
					}

					if($editipanelcomponent['Ipanel_Pool_Component_Dig']=='Men')
					{
						$men = 1;
					}

					$valueboq['Structure_Labour_Unit_Cost'] = (350*$mac)+(600*$men);
				}
				
				$valueboq['QTY'] = ceil( ($Capacity+($Area*0.2)+($Perimeter*1.4*0.8))*($men+$mac) );
			}
			else if($valueboq['Structure_Code']==5)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Inground')
				{
					$mac = 0;
					$men = 0;
					if($editipanelcomponent['Ipanel_Pool_Component_Dig']=='Machine')
					{
						$mac = 1;
					}

					if($editipanelcomponent['Ipanel_Pool_Component_Dig']=='Men')
					{
						$men = 1;
					}

					$valueboq['Structure_Labour_Unit_Cost'] = (150*$mac)+(200*$men);
				}

				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Stand')
				{
					$concrete = ceil( (($Perimeter/2)+$Area)*0.05 );
					$concretebasementandwall = ceil( ((($Area+(0.5*$Perimeter))*0.15)+($Perimeter/8))*1.05 );

					if($editipanel['Ipanel_Pools_Where']=="home")
					{
						$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
					}
					else
					{
						$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
					}

					$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

					$te20count = $Use_Filtration[0];
					$te25count = $Use_Filtration[1];
					$bikinicount = $Use_Filtration[2];

					$valueboq['QTY'] = ceil(($Capacity+(($concrete+$concretebasementandwall)*2.4))+(2*($te20count+$te25count)));
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==6)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = ceil($Area/6.25);
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==7)
			{
				$valueboq['QTY'] = ceil( (($Perimeter/2)+$Area)*0.05 );
			}
			else if($valueboq['Structure_Code']==8)
			{
				$valueboq['QTY'] = ceil( (($Perimeter/2)+$Area)*0.05 );
			}
			else if($valueboq['Structure_Code']==9)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te25count = $Use_Filtration[1];

				$valueboq['QTY'] = ceil( ($Perimeter+4)+(( $te25count )*2) );;
			}
			else if($valueboq['Structure_Code']==10)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = ceil($Area/6.25)*4;
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==11)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']!='Customer')
				{
					$valueboq['QTY'] = ceil( ((($Perimeter/2)+($Area))*2)*1.2 );
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==12)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = ceil( ((($Perimeter/2)+($Area))*2)*1.2 );
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==13)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = ceil($Area/6.25)*2;
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==14)
			{
				$valueboq['QTY'] = ceil(($Perimeter*4)/7);
			}
			else if($valueboq['Structure_Code']==15)
			{
				$valueboq['QTY'] =  ceil(($Perimeter/9.5)*3);
			}
			else if($valueboq['Structure_Code']==16)
			{
				$sum1to16 = 0;

				if($editipanelcomponent['Ipanel_Pool_Component_Work']!='Customer')
				{
					$sum1to16 += ceil( ((($Perimeter/2)+($Area))*2)*1.2 );
				}
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$sum1to16 += ceil( ((($Perimeter/2)+($Area))*2)*1.2 );
				}
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$sum1to16 += ceil($Area/6.25)*2;
				}

				$sum1to16 += ceil(($Perimeter*4)/7);
				$sum1to16 += ceil(($Perimeter/9.5)*3);

				$valueboq['QTY'] = $sum1to16;

			}
			else if($valueboq['Structure_Code']==17)
			{
				$valueboq['QTY'] = $Perimeter;
			}
			else if($valueboq['Structure_Code']==18)
			{
				$valueboq['QTY'] = ceil( ((($Area+(0.5*$Perimeter))*0.15)+($Perimeter/8))*1.05 );
			}
			else if($valueboq['Structure_Code']==19)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = (($Area+(0.5*$Perimeter))*0.05)+(ceil($Area/6.25)*0.2);
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==20)
			{

				$valueboq['QTY'] = 0;

				if($editipaneloption['Ipanel_Pool_Option_Spa_Seat']!=NULL||$editipaneloption['Ipanel_Pool_Option_Spa_Seat']!="")
				{
					$valueboq['QTY'] += $editipaneloption['Ipanel_Pool_Option_Spa_Seat'];
				}

				if($editipaneloption['Ipanel_Pool_Option_Ladder']=="Concrete")
				{
					$valueboq['QTY'] += $editipaneloption['Ipanel_Pool_Option_Ladder_Unit'];
				}

				if($valueboq['QTY']>0)
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 3000+($valueboq['QTY']-1)*1000/$valueboq['QTY'];
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 0;
				}

			}
			else if($valueboq['Structure_Code']==21)
			{
				$valueboq['QTY'] = 0;

				if($editipaneloption['Ipanel_Pool_Option_Kid_Pool']!=NULL||$editipaneloption['Ipanel_Pool_Option_Kid_Pool']!="")
				{
					$valueboq['QTY'] += $editipaneloption['Ipanel_Pool_Option_Kid_Pool'];
				}

				if($valueboq['QTY']>0)
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 3000+($valueboq['QTY']-1)*1000/$valueboq['QTY'];
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==22)
			{
				$valueboq['QTY'] = 0;

				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!=NULL||$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!="")
				{
					$valueboq['QTY'] += $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'];
				}

				if($valueboq['QTY']>0)
				{
					if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Build']=="BuildOnSite")
					{
						$valueboq['Structure_Labour_Unit_Cost'] = (3000+($valueboq['QTY']-1)*1000)/$valueboq['QTY'];
					}
					else
					{
						$valueboq['Structure_Labour_Unit_Cost'] = 0;
					}
					
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==23)
			{
				$valueboq['QTY'] = $Area;
			}
			else if($valueboq['Structure_Code']==24)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']!='Above')
				{
					$excavation = ceil( ($Capacity+($Area*0.2)+($Perimeter*1.4*0.8))*(1) );
					$sandorconcrete = ceil( (($Perimeter/2)+$Area)*0.05 );
					$concretebasementandwall = ceil( ((($Area+(0.5*$Perimeter))*0.15)+($Perimeter/8))*1.05 );
					$valueboq['QTY']  = ceil( $excavation - ($Capacity+($sandorconcrete*2)+$concretebasementandwall) );
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==25)
			{
				$valueboq['QTY'] = 0;

				//find Badu pump

				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}
				
				$Use_Filtration = $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);	

				$te20panelqty = $Use_Filtration[0];
				$te25panelqty = $Use_Filtration[1];
				$bikinipanelqty = $Use_Filtration[2];

				if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL)
				{
					$heatpump = $this->getheatpump($Area);
				}
				else
				{
					$heatpump = NULL;
				}

				$badu4716standardqty = 0;
				$badu4722standardqty = 0;
				$badu4716encapsulateqty = 0;
				$badu4722encapsulateqty = 0;
				$badudoublepumpqty = 0;
				$badusinglepumpqty = 0;
				$doublepumproomltpsfpqty = 0;
				$sfppumproomqty = 0;
				$sfp1500qty = 0;

				if($editipanelcomponent['Ipanel_Pool_Component_Filter_Pump']=='Standard')
				{
					$badu4716standardqty = $te20panelqty;
					$badu4722standardqty = $te25panelqty;
					if($bikinipanelqty>0)
					{
						$badu4716standardqty = $bikinipanelqty;
					}
				}

				if($editipanelcomponent['Ipanel_Pool_Component_Filter_Pump']=='Endcapsulated')
				{
					$badu4716encapsulateqty = $te20panelqty;
					$badu4722encapsulateqty = $te25panelqty;
					if($bikinipanelqty>0)
					{
						$badu4716encapsulateqty = $bikinipanelqty;
					}
				}

				if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=0)
				{
					$badu4716standardqty = $badu4716standardqty;
					$badu4722standardqty = $badu4722standardqty;
				}
				if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']>0&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=4)
				{
					$badu4716standardqty += 1;
				}
				else
				{
					$badu4722standardqty += ceil($editipaneloption['Ipanel_Pool_Option_Spa_Jet']/6);
				}

				if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>0&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!=NULL)
				{
					if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/16")
					{
						$badu4716standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
					}
					elseif($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/22")
					{
						$badu4722standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
					}
				}

				if($heatpump!=NULL)
				{
					if(sizeof($heatpump)==1)
					{
						$badu4716standardqty += 1;
					}
					else if(sizeof($heatpump)>1)
					{
						$badu4722standardqty += ceil(sizeof($heatpump)/2);
					}

				}

				if(($badu4716standardqty+$badu4722standardqty+$badu4716encapsulateqty+$badu4722encapsulateqty-$te20panelqty-$te25panelqty)/2>0.45)
				{
					$badudoublepumpqty = floor( (($badu4716standardqty+$badu4722standardqty+$badu4716encapsulateqty+$badu4722encapsulateqty-$te20panelqty-$te25panelqty)/2) );
				}
				else
				{
					$badudoublepumpqty = 0;
				}

				if(($badu4716standardqty+$badu4722standardqty+$badu4716encapsulateqty+$badu4722encapsulateqty-$te20panelqty-$te25panelqty)/2>0.45)
				{
					$badusinglepumpqty = ceil( (($badu4716standardqty+$badu4722standardqty+$badu4716encapsulateqty+$badu4722encapsulateqty-$te20panelqty-$te25panelqty)/2) - $badudoublepumpqty );
				}
				else
				{
					$badudoublepumpqty = 0;
				}

				//end find badu pump

				//find sfp pump
				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>1)
				{
					$sfp1500qty = ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
				}

				if(($sfp1500qty/2)>0.45)
				{
					$doublepumproomltpsfpqty = floor ($sfp1500qty/2);
				}
				else
				{
					$doublepumproomltpsfpqty = 0;
				}

				if(($sfp1500qty/2)>0.45)
				{
					$sfppumproomqty = ceil($sfp1500qty/2)-$doublepumproomltpsfpqty;
				}
				else
				{
					$sfppumproomqty = 0;
				}
				//end find sfp pump

				//echo "badusinglepumpqty : ".$badusinglepumpqty." badudoublepumpqty : ".$badudoublepumpqty." sfppumproomqty : ".$sfppumproomqty." doublepumproomltpsfpqty : ".$doublepumproomltpsfpqty;

				$valueboq['QTY'] = $badusinglepumpqty + $badudoublepumpqty + $sfppumproomqty + $doublepumproomltpsfpqty;

			}
			else if($valueboq['Structure_Code']==26)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				$valueboq['QTY'] = $te20count+$te25count+$bikinicount;
			}
			else if($valueboq['Structure_Code']==27)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				$valueboq['QTY'] = $te20count+$te25count+$bikinicount;
			}
			else if($valueboq['Structure_Code']==28)
			{
				$valueboq['QTY'] = 20;

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==29)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				$valueboq['QTY'] = ceil( ($te20count*10) );

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==30)
			{
				$valueboq['QTY'] = 20;

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==31)
			{
				if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']!="")
				{
					$valueboq['QTY'] = $editipaneloption['Ipanel_Pool_Option_Spa_Jet']*1.5;
				}
				else
				{
					$valueboq['QTY'] = 0;
				}

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==32)
			{
				if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL)
				{
					$heatpump = $this->getheatpump($Area);
				}
				else
				{
					$heatpump = NULL;
				}

				if($heatpump!=NULL)
				{
					$allheatpump=0;
					$fixnum = 1;
					$heatpump_arr = array();

					foreach ($heatpump as $key => $value) 
					{
						array_push($heatpump_arr, $value['Supplier_Product_Code']);	
					}

					$heatpump_arr = array_count_values($heatpump_arr);


					$nodupheatpump = $this->unique_multidim_array($heatpump,"Supplier_Product_Code");


					foreach ($nodupheatpump as $key => $value) 
					{
						foreach ($heatpump_arr as $key2 => $value2) {
							if($key2==$value['Supplier_Product_Code'])
							{
								$heatpumpqty = $value2;
								$allheatpump += $heatpumpqty;
								break;
							}
							else
							{
								$heatpumpqty = $fixnum;
								$allheatpump += $fixnum;
								break;
							}
						}
					}
				}
				// $valueboq['QTY'] = $allheatpump;

				$valueboq['QTY'] = 0;

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==33)
			{
				$valueboq['QTY'] = ceil( ($this->sum_spa_swim_heat_scul_pump($Project_ID))*( 10+($Perimeter/2) ) );

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==34)
			{
				$valueboq['QTY'] = 1;
				$valueboq['Structure_Labour_Unit_Cost'] = $allpipe*0.2;
			}
			else if($valueboq['Structure_Code']==35)
			{
				$cable2x2_5 = ceil( ($editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'])*(($Perimeter*0.5)+20) );
				$cable2x4 = 0;
				$cable3x1_5 = $this->sum_spa_swim_heat_scul_pump($Project_ID)*15;
				$cable10x2_5 = $this->calcable_10x2_5($Project_ID);

				$valueboq['QTY'] = $cable2x2_5+$cable2x4+$cable3x1_5+$cable10x2_5;

				$elecpvcmatsum = $valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost'];
				$elecpvclabsum = $valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost'];
			}
			else if($valueboq['Structure_Code']==36)
			{
				$valueboq['QTY'] = ceil( ($editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'])*(($Perimeter*0.5)+20) );
			}
			else if($valueboq['Structure_Code']==37)
			{
				$valueboq['QTY'] = 0;
			}
			else if($valueboq['Structure_Code']==38)
			{

				$valueboq['QTY'] = $this->sum_spa_swim_heat_scul_pump($Project_ID)*15;

			}
			else if($valueboq['Structure_Code']==39)
			{
				$valueboq['QTY'] = $this->calcable_10x2_5($Project_ID);
			}
			else if($valueboq['Structure_Code']==40)
			{
				$valueboq['Structure_Labour_Unit_Cost'] = ($elecpvcmatsum + $elecpvclabsum) *0.2;
				$valueboq['QTY'] = 1;

			}
			else if($valueboq['Structure_Code']==41)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>0)
				{
					$infin = $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']+2;
				}
				else
				{
					$infin = 0;
				}

				$valueboq['QTY'] = ceil( ($Perimeter+(3.5*($te20count+$te25count+$bikinicount))+$infin)+1);

				if($editipanel['Ipanel_Pools_Shape']=='Rectangular')
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 200;
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 250;
				}
				
			}
			else
			{
				$valueboq['QTY'] = 0;
			}


			$returnboqipanel[$keyboq] = $valueboq;
		}

		return $returnboqipanel;
	}

	public function getsystemandfinal($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$distanct = $this->calkm($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$wherearr = array("system_final.Pool_Type = 'ipanel'");
		$joincarr = array("product_unit","system_final_by_province");
		$joinvarr = array("system_final.Product_Unit_ID = product_unit.Product_Unit_ID","system_final.System_Final_ID = system_final_by_province.System_Final_ID");

		$system_final = $this->select_model->selectwherejoin("*","system_final",$wherearr,$joincarr,$joinvarr,NULL,NULL);

		if($system_final->num_rows())
		{
			$system_final_row = $system_final->num_rows();
			$system_final = $system_final->result_array();
		}
		else
		{
			$system_final = NULL;
			$system_final_row=0;
		}

		$returnsystemandfinal = array();

		$Perimeter = 42;
		$Area = 98;
		$Capacity = 110;


		foreach ($system_final as $syskey => $sysvalue) {

			if($sysvalue['System_Final_Code']==1)
			{
				$sysvalue['QTY'] = $Area;
			}
			elseif($sysvalue['System_Final_Code']==2)
			{
				$sysvalue['QTY'] = $Perimeter;
			}
			elseif($sysvalue['System_Final_Code']==3)
			{
				$sysvalue['QTY'] = 1;
			}
			elseif($sysvalue['System_Final_Code']==4)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				$sysvalue['QTY'] = $te20count+$te25count;
			}
			elseif($sysvalue['System_Final_Code']==5)
			{
				$sysvalue['QTY'] = $this->sum_spa_swim_heat_scul_pump($Project_ID);
			}
			elseif($sysvalue['System_Final_Code']==6)
			{
				if($editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit']!=""&&$editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit']!=0)
				{
					$sysvalue['QTY'] = $editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'];
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
				
			}
			elseif($sysvalue['System_Final_Code']==7)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Control_Panel']=="salt")
				{
					$sysvalue['QTY'] = ceil(1*$Capacity*4/50);
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
			}
			elseif($sysvalue['System_Final_Code']==8)
			{
				$sysvalue['QTY'] = 1;
			}
			elseif($sysvalue['System_Final_Code']==9)
			{
				
				if( ($distanct/1000)<=50 )
				{
					$sysvalue['QTY'] = 0;
					//$sysvalue['System_Final_Labour_Cost'] = 0;
				}
				else
				{
					$sysvalue['QTY'] = 1;
					//$sysvalue['System_Final_Labour_Cost'] = 10000;
				}
			}
			elseif($sysvalue['System_Final_Code']==10)
			{
				if($editipaneloption['Ipanel_Pool_Option_Ladder']=="Stainless")
				{
					$sysvalue['QTY'] = $editipaneloption['Ipanel_Pool_Option_Ladder_Unit'];
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
			}
			elseif($sysvalue['System_Final_Code']==11)
			{
				if($editipaneloption['Ipanel_Pool_Option_Ladder_Handrail']!=""&&$editipaneloption['Ipanel_Pool_Option_Ladder_Handrail']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Ladder_Handrail']!=0)
				{
					$sysvalue['QTY'] = $editipaneloption['Ipanel_Pool_Option_Ladder_Handrail'];
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
			}
			elseif($sysvalue['System_Final_Code']==12)
			{
				if($editipaneloption['Ipanel_Pool_Option_Logo']!=""&&$editipaneloption['Ipanel_Pool_Option_Logo']!=NULL)
				{
					$uselogo = json_decode($editipaneloption['Ipanel_Pool_Option_Logo']); 
   					$uselogosize = sizeof($uselogo);
					$sysvalue['QTY'] = $uselogosize;
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
			}
			elseif($sysvalue['System_Final_Code']==13)
			{
				$sysvalue['QTY'] = ceil(($distanct/1000));
			}
			else
			{
				$sysvalue['QTY'] = 0;
			}


			$returnsystemandfinal[$syskey] = $sysvalue;
		}

		return $returnsystemandfinal;
	}

	public function checkformula()
	{
		$Project_ID = $this->input->get('pid');

		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$distanct = $this->calkm($Project_ID);

		/*if($distanct!=0)
		{
			echo $distanct/1000;
		}*/

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$fliterpumpunit = $this->getfliterpumpunit_with_row()[0];
		$fliterpumpunit_row = $this->getfliterpumpunit_with_row()[1];
		
		$spapumpunit = $this->getspapumpunit_with_row()[0];
		$spapumpunit_row = $this->getspapumpunit_with_row()[1];
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];
		
		echo "Project ID = ".$Project_ID;
		echo "<br/>";

		print_r("editipanel : " );
		echo "<br/>";

		echo 'Ipanel_Pools_Series : '.$editipanel['Ipanel_Pools_Series'];
		echo "<br/>";
		echo 'Ipanel_Pools_Shape : '.$editipanel['Ipanel_Pools_Shape'];
		echo "<br/>";
		echo 'Ipanel_Pools_Length : '.$editipanel['Ipanel_Pools_Length'];
		echo "<br/>";
		echo 'Ipanel_Pools_Width : '.$editipanel['Ipanel_Pools_Width'];
		echo "<br/>";
		echo 'Ipanel_Pools_Dept1 : '.$editipanel['Ipanel_Pools_Dept1'];
		echo "<br/>";
		echo 'Ipanel_Pools_Dept2 : '.$editipanel['Ipanel_Pools_Dept2'];
		echo "<br/>";
		echo 'Ipanel_Pools_Dept3 : '.$editipanel['Ipanel_Pools_Dept3'];
		echo "<br/>";
		echo 'Ipanel_Pools_Extra_Shape : '.$editipanel['Ipanel_Pools_Extra_Shape'];
		echo "<br/>";
		print_r("exipanel : " );
		echo "<br/>";
		if($exipanel_row>0)
		{
			foreach ($exipanel as $key => $value) {
				echo 'Ipanel_Extra_Area_Position : '.$value['Ipanel_Extra_Area_Position'];
				echo "<br/>";
				echo 'Ipanel_Extra_Area_Length : '.$value['Ipanel_Extra_Area_Length'];
				echo "<br/>";
				echo 'Ipanel_Extra_Area_Width : '.$value['Ipanel_Extra_Area_Width'];
				echo "<br/>";
				echo 'Ipanel_Extra_Area_Radius : '.$value['Ipanel_Extra_Area_Radius'];
				echo "<br/>";
			}
		}


		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		print_r("calculate ; ");
		echo "<br/>";
		echo "Area : ".$Area;
		echo "<br/>";
		echo "Perimeter : ".$Perimeter;
		echo "<br/>";
		echo "Water Capacity : ".$Capacity;
		echo "<br/>";
		print_r("editipanelcomponent : " );
		echo "<br/>";

		if($editipanelcomponent['Ipanel_Pool_Component_Filtration']!=NULL)
		{
			foreach ($flitrationunit as $key => $value) {
				if($value['Supplier_Product_Code'] == $editipanelcomponent['Ipanel_Pool_Component_Filtration'])
				{
					$showflitrationunit = $editipanelcomponent['Ipanel_Pool_Component_Filtration']." - ".$value['Supplier_Product_Name_ENG']." = ".$value['Supplier_Product_Price'];
				}
			}
		}
		else
		{
			$showflitrationunit = NULL;
		}


		if($editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']!=NULL)
		{
			foreach ($robot as $key => $value) {
				if($value['Supplier_Product_Code'] == $editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner'])
				{
					$showrobot = $editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']." - ".$value['Supplier_Product_Name_ENG']." = ".$value['Supplier_Product_Price'];
				}
			}
		}
		else
		{
			$showrobot = NULL;
		}

		if($editipanel['Ipanel_Pools_Where']=="home")
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
		}
		else
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
		}
			

		echo 'Ipanel_Pool_Component_Filtration : '.$showflitrationunit;
		echo "<br/>";
		echo 'Ipanel_Pool_Component_Filter_LED_Light : '.$editipanelcomponent['Ipanel_Pool_Component_Filter_LED_Light'];
		echo "<br/>";
		echo 'Ipanel_Pool_Component_Filtration_Unit : '.$Ipanel_Pool_Component_Filtration_Unit;
		echo "<br/>";
		echo 'Ipanel_Pool_Component_Filter_Pump : '.$editipanelcomponent['Ipanel_Pool_Component_Filter_Pump'];
		echo "<br/>";
		echo 'Ipanel_Pool_Component_Control_Panel : '.$editipanelcomponent['Ipanel_Pool_Component_Control_Panel'];
		echo "<br/>";
		echo 'Ipanel_Pool_Component_Coping : '.$editipanelcomponent['Ipanel_Pool_Component_Coping'];
		echo "<br/>";
		echo 'Ipanel_Pool_Component_Coping : '.$editipanelcomponent['Ipanel_Pool_Component_Coping'];
		echo "<br/>";
		echo 'Ipanel_Pool_Component_Chemicals : '.$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
		echo "<br/>";

		print_r("editipaneloption : " );
		echo "<br/>";

		echo 'Ipanel_Pool_Option_Under_Water_Light : '.$editipaneloption['Ipanel_Pool_Option_Under_Water_Light'];
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Under_Water_Light_Unit : '.$editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'];
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Spa_Jet : '.$editipaneloption['Ipanel_Pool_Option_Spa_Jet'];
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Spa_Seat : '.$editipaneloption['Ipanel_Pool_Option_Spa_Seat'];
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Swim_Jet_System : '.$editipaneloption['Ipanel_Pool_Option_Swim_Jet_System'];
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Infinity_Eage_Set : '.$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'];
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Pool_Heating_System : '.$editipaneloption['Ipanel_Pool_Option_Pool_Heating_System'];
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Ladder : '.$editipaneloption['Ipanel_Pool_Option_Ladder'];
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Ladder_Unit : '.$editipaneloption['Ipanel_Pool_Option_Ladder_Unit'];
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Logo : ';
		print_r($editipaneloption['Ipanel_Pool_Option_Logo']);
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Robotic_Cleaner : '.$showrobot;
		echo "<br/>";
		echo 'Ipanel_Pool_Option_Other : ';
		print_r($editipaneloption['Ipanel_Pool_Option_Other']);
		echo "<br/>";

		$ipanel_structure = $this->calculate_ipanel_structure($this->input->get('pid'));
		
		if($editipanel['Ipanel_Pools_Series']=="smartpool"||$editipanel['Ipanel_Pools_Series']=="ecopool")
		{
			$wall =	$this->calculate_ipanel_lightblue_wall($this->input->get('pid'));
			$floor = $this->calculate_ipanel_lightblue_floor($this->input->get('pid'));
		}
		else
		{
			$wall =	$this->calculate_ipanel_davinci_wall($this->input->get('pid'));
			$floor = $this->calculate_ipanel_davinci_floor($this->input->get('pid'));
		}

		$filtration = $this->calculate_ipanel_filtration($this->input->get('pid'));
		$underwaterlight = $this->calculate_ipanel_underwaterlight($this->input->get('pid'));
		$spaoption = $this->calculate_ipanel_spaoption($this->input->get('pid'));
		$inletandfitting = $this->calculate_ipanel_inletandfitting($this->input->get('pid'));
		$infinityedge = $this->calculate_ipanel_infinityedge($this->input->get('pid'));
		$otheroption = $this->calculate_ipanel_otheroption($this->input->get('pid'));
		$maintenent = $this->calculate_ipanel_maintenent($this->input->get('pid'));
		$chemical = $this->calculate_ipanel_chemical($this->input->get('pid'));

		$use_ipanel_structure = $ipanel_structure[0];
		$cost_ipanel_structure = $ipanel_structure[1];

		$use_wall = $wall[0];
		$cost_wall = $wall[1];

		$use_floor = $floor[0];
		$cost_floor = $floor[1];

		$use_filtration = $filtration[0];
		$cost_filtration = $filtration[1];

		$use_underwaterlight = $underwaterlight[0];
		$cost_underwaterlight = $underwaterlight[1];

		$use_spaoption = $spaoption[0];
		$cost_spaoption = $spaoption[1];
		
		$use_inletandfitting = $inletandfitting[0];
		$cost_inletandfitting = $inletandfitting[1];

		$use_infinityedge = $infinityedge[0];
		$cost_infinityedge = $infinityedge[1];
		
		$use_otheroption = $otheroption[0];
		$cost_otheroption = $otheroption[1];

		$use_maintenent = $maintenent[0];
		$cost_maintenent = $maintenent[1];

		$use_chemical = $chemical[0];
		$cost_chemical = $chemical[1];

		$alljdprodsummary = $cost_ipanel_structure+$cost_wall+$cost_floor+$cost_filtration+$cost_underwaterlight+$cost_underwaterlight+$cost_spaoption+$cost_inletandfitting+$cost_infinityedge+$cost_otheroption+$cost_maintenent+$cost_chemical;

		$ipanelboq = $this->structure_boq_ipanel($this->input->get('pid'));
		$getsystemandfinal = $this->getsystemandfinal($this->input->get('pid'));

		echo "<table style=border: 1px solid black;>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>"." Code "."</td>";
		echo "<td style='border: 1px solid black;'>"." Descriptiom "."</td>";
		echo "<td style='border: 1px solid black;'>"." Unit "."</td>";
		echo "<td style='border: 1px solid black;'>"." Quontity "."</td>";
		echo "<td style='border: 1px solid black;'>"." Price/Unit (Baht) "."</td>";
		echo "<td style='border: 1px solid black;'>"." Total (Baht) "."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." Structure "."</td>";
		echo "</tr>";
		foreach ($use_ipanel_structure as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_ipanel_structure."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." WALL "."</td>";
		echo "</tr>";
		foreach ($use_wall as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_wall."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." FLOOR "."</td>";
		echo "</tr>";
		foreach ($use_floor as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_floor."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." Filtration "."</td>";
		echo "</tr>";
		foreach ($use_filtration as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_filtration."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." Underwaterlight "."</td>";
		echo "</tr>";
		foreach ($use_underwaterlight as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_underwaterlight."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." Spaoption "."</td>";
		echo "</tr>";
		foreach ($use_spaoption as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_spaoption."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." Inletandfitting "."</td>";
		echo "</tr>";
		foreach ($use_inletandfitting as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_inletandfitting."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." Infinityedge "."</td>";
		echo "</tr>";
		foreach ($use_infinityedge as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_infinityedge."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." Otheroption "."</td>";
		echo "</tr>";
		foreach ($use_otheroption as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_otheroption."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." Maintenent "."</td>";
		echo "</tr>";
		foreach ($use_maintenent as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_maintenent."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='6' style='border: 1px solid black;'>"." Chemical "."</td>";
		echo "</tr>";
		foreach ($use_chemical as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
			echo "</tr>";
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$cost_chemical."</td>";
		echo "</tr>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'>"." Summary "."</td>";
		echo "<td style='border: 1px solid black;'>".$alljdprodsummary."</td>";
		echo "</tr>";
		echo "</table>";

		echo "<br/><br/><br/><br/>";

		echo "<h3>Structure BOQ</h3>";

		//print_r($ipanelboq);

		$totlemat = 0;
		$totlelabour = 0;
		$totleall = 0;

		$eleccable2x2_5qty = 0;
		$eleccable2x4qty = 0;
		$eleccable3x1_5qty = 0;
		$eleccable10x2_5qty = 0;

		$maincopping = 0;

		echo "<table style=border: 1px solid black;>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>"." Description "."</td>";
		echo "<td style='border: 1px solid black;'>"." Q'TY "."</td>";
		echo "<td style='border: 1px solid black;'>"." Unit "."</td>";
		echo "<td style='border: 1px solid black;'>"." Materials Unit Cost "."</td>";
		echo "<td style='border: 1px solid black;'>"." Materials Total "."</td>";
		echo "<td style='border: 1px solid black;'>"." Labour Change Unit "."</td>";
		echo "<td style='border: 1px solid black;'>"." Labour Change Total "."</td>";
		echo "<td style='border: 1px solid black;'>"." Total "."</td>";
		echo "</tr>";
		foreach ($ipanelboq as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['Structure_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Structure_Materials_Unit_Cost']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Structure_Materials_Unit_Cost']*$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Structure_Labour_Unit_Cost']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Structure_Labour_Unit_Cost']."</td>";
			echo "<td style='border: 1px solid black;'>".(($value['QTY']*$value['Structure_Labour_Unit_Cost'])+($value['Structure_Materials_Unit_Cost']*$value['QTY']))."</td>";
			echo "</tr>";
			if($value['Structure_Code']=="36"){
				$eleccable2x2_5qty = $value['QTY'];
			}
			if($value['Structure_Code']=="37"){
				$eleccable2x4qty = $value['QTY'];
			}
			if($value['Structure_Code']=="38"){
				$eleccable3x1_5qty = $value['QTY'];
			}
			if($value['Structure_Code']=="39"){
				$eleccable10x2_5qty = $value['QTY'];
			}
			if($value['Structure_Code']=="41"){
				$maincopping = $value['QTY'];
			}
			$totlemat += $value['Structure_Materials_Unit_Cost']*$value['QTY'];
			$totlelabour += $value['Structure_Labour_Unit_Cost']*$value['QTY'];
			$totleall += ($value['QTY']*$value['Structure_Labour_Unit_Cost'])+($value['Structure_Materials_Unit_Cost']*$value['QTY']);
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>ยอดรวม</td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$totlemat."</td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$totlelabour."</td>";
		echo "<td style='border: 1px solid black;'>".$totleall."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'> Profit (13.5%) </td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".($totleall+($totleall*0.135))."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'> Vat (7%) </td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".($totleall+($totleall*0.135)+(($totleall+$totleall*0.135)*0.07))."</td>";
		echo "</tr>";

		echo "</table>";

		echo "<br/><br/>";

		echo "<h3>System And Final</h3>";

		//print_r($ipanelboq);

		$totlematsaf = 0;
		$totlelaboursaf = 0;
		$totleallsaf = 0;

		echo "<table style=border: 1px solid black;>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>"." Description "."</td>";
		echo "<td style='border: 1px solid black;'>"." Q'TY "."</td>";
		echo "<td style='border: 1px solid black;'>"." Unit "."</td>";
		echo "<td style='border: 1px solid black;'>"." Materials Unit Cost "."</td>";
		echo "<td style='border: 1px solid black;'>"." Materials Total "."</td>";
		echo "<td style='border: 1px solid black;'>"." Labour Change Unit "."</td>";
		echo "<td style='border: 1px solid black;'>"." Labour Change Total "."</td>";
		echo "<td style='border: 1px solid black;'>"." Total "."</td>";
		echo "</tr>";
		foreach ($getsystemandfinal as $key => $value) {
			echo "<tr style='border: 1px solid black;'>";
			echo "<td style='border: 1px solid black;'>".$value['System_Final_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['System_Final_Materials_Cost']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['System_Final_Materials_Cost']*$value['QTY']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['System_Final_Labour_Cost']."</td>";
			echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['System_Final_Labour_Cost']."</td>";
			echo "<td style='border: 1px solid black;'>".(($value['QTY']*$value['System_Final_Labour_Cost'])+($value['System_Final_Materials_Cost']*$value['QTY']))."</td>";
			echo "</tr>";
			$totlematsaf += $value['System_Final_Materials_Cost']*$value['QTY'];
			$totlelaboursaf += $value['System_Final_Labour_Cost']*$value['QTY'];
			$totleallsaf += ($value['QTY']*$value['System_Final_Labour_Cost'])+($value['System_Final_Materials_Cost']*$value['QTY']);
		}
		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>ยอดรวม</td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$totlematsaf."</td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$totlelaboursaf."</td>";
		echo "<td style='border: 1px solid black;'>".$totleallsaf."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'> Profit (13.5%) </td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".($totleallsaf+($totleallsaf*0.135))."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'> Vat (7%)</td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".($totleallsaf+($totleallsaf*0.135)+(($totleallsaf+$totleallsaf*0.135)*0.07))."</td>";
		echo "</tr>";

		echo "</table>";

		echo "<br/><br/>";

		echo "<h3>Summary</h3>";
		echo "<table style=border: 1px solid black;>";
		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>"." Item "."</td>";
		echo "<td style='border: 1px solid black;'>"." Description "."</td>";
		echo "<td style='border: 1px solid black;'>"." Unit "."</td>";
		echo "<td style='border: 1px solid black;'>"." QTY "."</td>";
		echo "<td style='border: 1px solid black;'>"." Price/Unit "."</td>";
		echo "<td style='border: 1px solid black;'>"." Totle Price "."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>"." 1 "."</td>";
		echo "<td style='border: 1px solid black;'>"." รายการอุปกรณ์จาก เจ.ดี. พูลส์  "."</td>";
		echo "<td style='border: 1px solid black;'>"." Set "."</td>";
		echo "<td style='border: 1px solid black;'>"." 1 "."</td>";
		echo "<td style='border: 1px solid black;'>".$alljdprodsummary."</td>";
		echo "<td style='border: 1px solid black;'>".$alljdprodsummary."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." Discount "."</td>";
		echo "<td style='border: 1px solid black;'>"." Set "."</td>";
		echo "<td style='border: 1px solid black;'>"." 1 "."</td>";
		echo "<td style='border: 1px solid black;'>"." 0% "."</td>";
		echo "<td style='border: 1px solid black;'>"." 0 "."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." Summary "."</td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".$alljdprodsummary."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>"." 2 "."</td>";
		echo "<td style='border: 1px solid black;'>"." Construction Cost "."</td>";
		echo "<td style='border: 1px solid black;'>"." Set "."</td>";
		echo "<td style='border: 1px solid black;'>"." 1 "."</td>";
		echo "<td style='border: 1px solid black;'>".($totleall+($totleall*0.135)+(($totleall+$totleall*0.135)*0.07))."</td>";
		echo "<td style='border: 1px solid black;'>".($totleall+($totleall*0.135)+(($totleall+$totleall*0.135)*0.07))."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>"." 3 "."</td>";
		echo "<td style='border: 1px solid black;'>"." Equipment installation and Hand Over. "."</td>";
		echo "<td style='border: 1px solid black;'>"." Set "."</td>";
		echo "<td style='border: 1px solid black;'>"." 1 "."</td>";
		echo "<td style='border: 1px solid black;'>".($totleallsaf+($totleallsaf*0.135)+(($totleallsaf+$totleallsaf*0.135)*0.07))."</td>";
		echo "<td style='border: 1px solid black;'>".($totleallsaf+($totleallsaf*0.135)+(($totleallsaf+$totleallsaf*0.135)*0.07))."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>"." 4 "."</td>";
		echo "<td style='border: 1px solid black;'>"." Transportation "."</td>";
		echo "<td style='border: 1px solid black;'>"." Set "."</td>";
		echo "<td style='border: 1px solid black;'>"." 1 "."</td>";
		echo "<td style='border: 1px solid black;'>"." 10000 "."</td>";
		echo "<td style='border: 1px solid black;'>"." 10000 "."</td>";
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'>"." 5 "."</td>";
		echo "<td style='border: 1px solid black;'>"." Local Equipment "."</td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "</tr>";

		$sumallle = 0;

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." -Electric Cable.NYY 3X1.5sq.mm(Option Pump) "."</td>";
		echo "<td style='border: 1px solid black;'>"." Meter "."</td>";
		echo "<td style='border: 1px solid black;'>".$eleccable3x1_5qty."</td>";
		echo "<td style='border: 1px solid black;'>"." 36.64 "."</td>";
		echo "<td style='border: 1px solid black;'>".($eleccable3x1_5qty*36.64)."</td>";
		echo "</tr>";

		$sumallle += $eleccable3x1_5qty*36.64;

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." -Electric Cable. NYY 2X2.5sq.mm(Under Water Light) "."</td>";
		echo "<td style='border: 1px solid black;'>"." Meter "."</td>";
		echo "<td style='border: 1px solid black;'>".$eleccable2x2_5qty."</td>";
		echo "<td style='border: 1px solid black;'>"." 39 "."</td>";
		echo "<td style='border: 1px solid black;'>".($eleccable2x2_5qty*39)."</td>";
		echo "</tr>";

		$sumallle += $eleccable2x2_5qty*39;

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." -Electric Cable. NYY 2X4sq.mm(Under Water Light) "."</td>";
		echo "<td style='border: 1px solid black;'>"." Meter "."</td>";
		echo "<td style='border: 1px solid black;'>".$eleccable2x4qty."</td>";
		echo "<td style='border: 1px solid black;'>"." 53 "."</td>";
		echo "<td style='border: 1px solid black;'>".($eleccable2x4qty*53)."</td>";
		echo "</tr>";

		$sumallle += $eleccable2x4qty*53;

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." -Electric Cable. JZ 10X2.5sq.mm(Filtration System) "."</td>";
		echo "<td style='border: 1px solid black;'>"." Meter "."</td>";
		echo "<td style='border: 1px solid black;'>".$eleccable10x2_5qty."</td>";
		echo "<td style='border: 1px solid black;'>"." 250 "."</td>";
		echo "<td style='border: 1px solid black;'>".($eleccable10x2_5qty*250)."</td>";
		echo "</tr>";

		$sumallle += $eleccable10x2_5qty*250;

		$Granite_India = 0;
		$Granite_China = 0;
		$Sand = 0;

		if($editipanelcomponent['Ipanel_Pool_Component_Coping']=="Granite_India")
		{
			$Granite_India = $maincopping;
		}
		elseif($editipanelcomponent['Ipanel_Pool_Component_Coping']=="Granite_China")
		{
			$Granite_China = $maincopping;
		}
		elseif($editipanelcomponent['Ipanel_Pool_Component_Coping']=="Sand")
		{
			$Sand = $maincopping - $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'];
			$Granite_China = $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'];
		}

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." -Copping "."</td>";
		echo "<td style='border: 1px solid black;'>"." Meter "."</td>";
		echo "<td style='border: 1px solid black;'>".$Sand."</td>";
		echo "<td style='border: 1px solid black;'>"." 396 "."</td>";
		echo "<td style='border: 1px solid black;'>".($Sand*396)."</td>";
		echo "</tr>";

		$sumallle += $Sand*396; 

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." -หินขอบสระแกรนิตดำจีน "."</td>";
		echo "<td style='border: 1px solid black;'>"." Meter "."</td>";
		echo "<td style='border: 1px solid black;'>".$Granite_China."</td>";
		echo "<td style='border: 1px solid black;'>"." 960 "."</td>";
		echo "<td style='border: 1px solid black;'>".($Granite_China*960)."</td>";
		echo "</tr>";

		$sumallle += $Granite_China*960;

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." -Infinity Coping Granite "."</td>";
		echo "<td style='border: 1px solid black;'>"." Meter "."</td>";
		echo "<td style='border: 1px solid black;'>".$Granite_India."</td>";
		echo "<td style='border: 1px solid black;'>"." 1300 "."</td>";
		echo "<td style='border: 1px solid black;'>".($Granite_India*1300)."</td>";
		echo "</tr>";

		$sumallle += $Granite_India*1300;

		if($editipanelcomponent['Ipanel_Pool_Component_Control_Panel']=='salt')
		{
			$salt = $Capacity*4/50;
		}
		else
		{
			$salt = 0;
		}

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." Salt (bag 50) "."</td>";
		echo "<td style='border: 1px solid black;'>"." Bag "."</td>";
		echo "<td style='border: 1px solid black;'>".ceil($salt)."</td>";
		echo "<td style='border: 1px solid black;'>"." 600 "."</td>";
		echo "<td style='border: 1px solid black;'>".(ceil($salt)*600)."</td>";
		echo "</tr>";

		$sumallle += ceil($salt)*600;

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." Refil Water "."</td>";
		echo "<td style='border: 1px solid black;'>"." M3 "."</td>";
		echo "<td style='border: 1px solid black;'>".$Capacity."</td>";
		echo "<td style='border: 1px solid black;'>"." 250 "."</td>";
		echo "<td style='border: 1px solid black;'>".($Capacity*250)."</td>";
		echo "</tr>";

		$sumallle += $Capacity*250;

		echo "<tr style='border: 1px solid black;'>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>"." Local Equipment Summary "."</td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'></td>";
		echo "<td style='border: 1px solid black;'>".($sumallle+10000)."</td>"; //+transport
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'>COST</td>";
		echo "<td style='border: 1px solid black;'>".(  $alljdprodsummary + ($totleall+($totleall*0.135)+(($totleall+$totleall*0.135)*0.07))+
		($totleallsaf+($totleallsaf*0.135)+(($totleallsaf+$totleallsaf*0.135)*0.07)) + ($sumallle+10000) )."</td>"; //+transport
		echo "</tr>";

		echo "<tr style='border: 1px solid black;'>";
		echo "<td colspan='5' style='border: 1px solid black;'>Recommend Retail Price</td>";
		echo "<td style='border: 1px solid black;'>".( ( $alljdprodsummary + ($totleall+($totleall*0.135)+(($totleall+$totleall*0.135)*0.07))+
		($totleallsaf+($totleallsaf*0.135)+(($totleallsaf+$totleallsaf*0.135)*0.07)) + ($sumallle+10000) ) *2 )."</td>"; //+transport
		echo "</tr>";

		echo "</table>";
	}
}