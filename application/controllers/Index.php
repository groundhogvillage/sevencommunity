<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct()
  {
      parent::__construct();
      date_default_timezone_set('Asia/Bangkok');
      $this->load->model('select_model');
      $this->load->model('update_model');
      $this->load->model('login_model');
      $this->load->model('delete_model');

      $this->sitename = "Sevencommunity";
  }

	public function index()
	{
		$wherearr = array("item.withdraw = 0","item.showhide = 0");
		$wherearr2 = array("news.withdraw = 0","news.showhide = 0");

		$pageindex = $this->getpagebytype(1);
		$data['pageindex_row'] = $pageindex['page_row'];
		$data['pageindex'] = $pageindex['page'];

		$pageabout = $this->getpagebytype(10);
		$data['pageabout_row'] = $pageabout['page_row'];
		$data['pageabout'] = $pageabout['page'];

		$page1 = $this->getpagebytypewithpic(6);
		$data['page1_row'] = $page1['page_row'];
		$data['page1'] = $page1['page'];

		$page2 = $this->getpagebytypewithpic(2);
		$data['page2_row'] = $page2['page_row'];
		$data['page2'] = $page2['page'];

		$page3 = $this->getpagebytypewithpic(3);
		$data['page3_row'] = $page3['page_row'];
		$data['page3'] = $page3['page'];

		$page4 = $this->getpagebytypewithpic(7);
		$data['page4_row'] = $page4['page_row'];
		$data['page4'] = $page4['page'];

		$page5 = $this->getpagebytypewithpic(5);
		$data['page5_row'] = $page5['page_row'];
		$data['page5'] = $page5['page'];

		$page6 = $this->getpagebytypewithpic(8);
		$data['page6_row'] = $page6['page_row'];
		$data['page6'] = $page6['page'];

		$page7 = $this->getpagebytypewithpic(4);
		$data['page7_row'] = $page7['page_row'];
		$data['page7'] = $page7['page'];

		$listitem = $this->select_model->selectwherejoinorderarrlimit("item.*,page.type","item",$wherearr,"page","item.page_id = page.id",8,0,"update_datetime,visited,frequency_update","desc");
		if($listitem->num_rows())
		{
			$data['item_row'] = $listitem->num_rows();
			$data['item'] = $listitem->result_array();
			foreach ($data['item'] as $key => $value) {
				$item_en = $this->getitem_en($value['id']);
				$data['item'][$key]['name_en'] = $item_en['item_en']['name_en'];
				//$data['item'][$key]['short_description_en'] = $item_en['item_en']['short_description_en'];
				$data['item'][$key]['description_en'] = $item_en['item_en']['description_en'];
				$item_picture = $this->getitem_picture($value['id']);
				$data['item'][$key]['picture_row'] = $item_picture['item_picture_row'];
				$data['item'][$key]['picture'] = $item_picture['item_picture'];
			}
		}
		else
		{
			$data['item_row'] = 0;
			$data['item'] = NULL;
		}

		$listnews = $this->select_model->selectwherejoinorderarrlimit("news.*,page.type","news",$wherearr2,"page","news.page_id = page.id",NULL,NULL,"update_datetime","desc");
		if($listnews->num_rows())
		{
			$data['news_row'] = $listnews->num_rows();
			$data['news'] = $listnews->result_array();
			foreach ($data['news'] as $key => $value) {
				$news_en = $this->getnews_en($value['id']);
				$data['news'][$key]['name_en'] = $news_en['news_en']['name_en'];
				//$data['news'][$key]['short_description_en'] = $news_en['news_en']['short_description_en'];
				$data['news'][$key]['description_en'] = $news_en['news_en']['description_en'];
				$news_picture = $this->getnews_picture($value['id']);
				$data['news'][$key]['picture_row'] = $news_picture['news_picture_row'];
				$data['news'][$key]['picture'] = $news_picture['news_picture'];
			}
		}
		else
		{
			$data['news_row'] = 0;
			$data['news'] = NULL;
		}

		$this->load->view('index/index',$data);
	}

	public function index2()
	{
		$wherearr = array("item.withdraw = 0","item.showhide = 0");

		$pageindex = $this->getpagebytype(1);
		$data['pageindex_row'] = $pageindex['page_row'];
		$data['pageindex'] = $pageindex['page'];

		$pageabout = $this->getpagebytype(10);
		$data['pageabout_row'] = $pageabout['page_row'];
		$data['pageabout'] = $pageabout['page'];

		$page1 = $this->getpagebytypewithpic(6);
		$data['page1_row'] = $page1['page_row'];
		$data['page1'] = $page1['page'];

		$page2 = $this->getpagebytypewithpic(2);
		$data['page2_row'] = $page2['page_row'];
		$data['page2'] = $page2['page'];

		$page3 = $this->getpagebytypewithpic(3);
		$data['page3_row'] = $page3['page_row'];
		$data['page3'] = $page3['page'];

		$page4 = $this->getpagebytypewithpic(7);
		$data['page4_row'] = $page4['page_row'];
		$data['page4'] = $page4['page'];

		$page5 = $this->getpagebytypewithpic(5);
		$data['page5_row'] = $page5['page_row'];
		$data['page5'] = $page5['page'];

		$page6 = $this->getpagebytypewithpic(8);
		$data['page6_row'] = $page6['page_row'];
		$data['page6'] = $page6['page'];

		$page7 = $this->getpagebytypewithpic(4);
		$data['page7_row'] = $page7['page_row'];
		$data['page7'] = $page7['page'];

		$listitem = $this->select_model->selectwherejoinorderarrlimit("item.*,page.type","item",$wherearr,"page","item.page_id = page.id",8,0,"update_datetime,visited,frequency_update","desc");
		if($listitem->num_rows())
		{
			$data['item_row'] = $listitem->num_rows();
			$data['item'] = $listitem->result_array();
			foreach ($data['item'] as $key => $value) {
				$item_en = $this->getitem_en($value['id']);
				$data['item'][$key]['name_en'] = $item_en['item_en']['name_en'];
				// /$data['item'][$key]['short_description_en'] = $item_en['item_en']['short_description_en'];
				$data['item'][$key]['description_en'] = $item_en['item_en']['description_en'];
				$item_picture = $this->getitem_picture($value['id']);
				$data['item'][$key]['picture_row'] = $item_picture['item_picture_row'];
				$data['item'][$key]['picture'] = $item_picture['item_picture'];
			}
		}
		else
		{
			$data['item_row'] = 0;
			$data['item'] = NULL;
		}

		$this->load->view('index/index_',$data);
	}


	public function download()
	{
		$this->load->view('index/download');
	}

	public function product()
	{
		$typeid = $this->input->get("commid");

		$wherearr = array("item.withdraw = 0","item.showhide = 0");

		$listitem = $this->select_model->selectwherejoinorderarr("item.*,page.type","item",$wherearr,"page","item.page_id = page.id","update_datetime,visited,frequency_update","desc");
		if($listitem->num_rows())
		{
			$data['item_row'] = $listitem->num_rows();
			$data['item'] = $listitem->result_array();
			foreach ($data['item'] as $key => $value) {
				$item_en = $this->getitem_en($value['id']);
				$data['item'][$key]['name_en'] = $item_en['item_en']['name_en'];
				//$data['item'][$key]['short_description_en'] = $item_en['item_en']['short_description_en'];
				$data['item'][$key]['description_en'] = $item_en['item_en']['description_en'];
				$item_picture = $this->getitem_picture($value['id']);
				$data['item'][$key]['picture_row'] = $item_picture['item_picture_row'];
				$data['item'][$key]['picture'] = $item_picture['item_picture'];
			}
		}
		else
		{
			$data['item_row'] = 0;
			$data['item'] = NULL;
		}

		$this->load->view('index/product',$data);

	}

  public function community()
  {
		$typeid = $this->input->get("commid");

		$data['commid'] = $typeid;

		if($typeid==NULL||$typeid==""||$typeid=="0"||($typeid<2||$typeid>8))
		{
			$this->load->view('errors/template/error_404');
		}
		else
		{
			$page = $this->getpagebytype($typeid);
			$data['page_row'] = $page['page_row'];
	  	$data['page'] = $page['page'];

	    if($data['page']['id']!=NULL&&$data['page']['id']!="")
	    {
				$page_en = $this->getpage_en($data['page']['id']);
				$data['page_en_row'] = $page_en['page_en_row'];
	      $data['page_en'] = $page_en['page_en'];
				$page_cn = $this->getpage_cn($data['page']['id']);
				$data['page_cn_row'] = $page_cn['page_cn_row'];
	      $data['page_cn'] = $page_cn['page_cn'];
				$slide = $this->getslide($data['page']['id']);
				$data['slide_row'] = $slide['slide_row'];
	      $data['slide'] = $slide['slide'];
				$picture = $this->getpicture($data['page']['id']);
				$data['picture_row'] = $picture['picture_row'];
	      $data['picture'] = $picture['picture'];
				$file = $this->getfile($data['page']['id']);
				$data['file_row'] = $file['file_row'];
				$data['file'] = $file['file'];
				$subpage = $this->getallsubpage($data['page']['id']);

				if($subpage['subpage_row']>0)
				{
					foreach ($subpage['subpage'] as $key1 => $value1)
					{
							$spicture = $this->getpicture($data['page']['id'],$value1['id']);
							$subpage['subpage'][$key1]['picture_row'] = $spicture['picture_row'];
							$subpage['subpage'][$key1]['picture'] = $spicture['picture'];
							if($value1['name']=="ตัวแทนชุมชน")
							{
								$data['agent']['content_row'] = $spicture['picture_row'];
								$data['agent']['content'] = $spicture['picture'];
							}
					}
				}

				$data['subpage_row'] = $subpage['subpage_row'];
				$data['subpage'] = $subpage['subpage'];

				$wherearr = array("withdraw = 0", "page_id = '".$data['page']['id']."'","item.showhide = 0");

	      $listitem = $this->select_model->select_where("*","item",$wherearr);
				if($listitem->num_rows())
				{
					$data['item_row'] = $listitem->num_rows();
					$data['item'] = $listitem->result_array();
					foreach ($data['item'] as $key => $value) {
						$item_en = $this->getitem_en($value['id']);
						$data['item'][$key]['name_en'] = $item_en['item_en']['name_en'];
						//$data['item'][$key]['short_description_en'] = $item_en['item_en']['short_description_en'];
						$data['item'][$key]['description_en'] = $item_en['item_en']['description_en'];
						$item_picture = $this->getitem_picture($value['id']);
						$data['item'][$key]['picture_row'] = $item_picture['item_picture_row'];
						$data['item'][$key]['picture'] = $item_picture['item_picture'];
					}
				}
				else
				{
					$data['item_row'] = 0;
					$data['item'] = NULL;
				}
				$this->load->view('index/community',$data);

	    }
	    else
	    {
				$this->load->view('errors/template/error_404');
	    }
		}
  }

	public function getmapcommunity()
	{
		$typeid = $this->input->get("commid");

		$page = $this->getpagebytype($typeid);
		$data['page_row'] = $page['page_row'];
		$data['page'] = $page['page'];

		$wherearr = array("withdraw = 0", "page_id = '".$data['page']['id']."'","item.showhide = 0");

		$listitem = $this->select_model->select_where("*","item",$wherearr);
		if($listitem->num_rows())
		{
			$data['item_row'] = $listitem->num_rows();
			$data['item'] = $listitem->result_array();
		}
		else
		{
			$data['item_row'] = 0;
			$data['item'] = NULL;
		}

		print_r(json_encode($data));
	}

	public function getmaponitemcommunity()
	{
		$itemid = $this->input->get("itemid");

		$wherearr = array("item.withdraw = 0", "item.id = '".$itemid."'","item.showhide = 0");

		$item = $this->select_model->selectwherejoin("item.*,page.type","item",$wherearr,array("page"),array("item.page_id = page.id"));
		if($item->num_rows())
		{
			$data['item_row'] = $item->num_rows();
			$data['item'] = $item->row_array();
			$item_picture = $item = $this->select_model->select_where("*","item_picture","item_id = '".$itemid."'");
			if($item_picture->num_rows())
			{
				$item_picture = $item_picture->row_array();
				$data['item']['picture'] = $item_picture['path'];
			}
			else
			{
				$data['item']['picture'] = NULL;
			}
		}
		else
		{
			$data['item_row'] = 0;
			$data['item'] = $this->emptyitem();
			$data['item']['picture'] = NULL;
		}

		$returndata = NULL;

		$returndata .= '<ul id="product-hilight" style="padding-left:0;">';
		$returndata .= '<li class="pd-th pd-space_around_box">';
		$returndata .= '<div class="pd-product_box setonmappdbox">';
		$returndata .= '<div class="pd-box_picture">';
		$returndata .= '<a data-toggle="modal" data-target="#Product-Modal'.$data['item']['id'].'">';
		if($data['item']['picture']!=NULL)
		{
			$returndata .= '<img style="display: block; max-width: 100%;  height: auto;" src="'.base_url().'assets/upload/item/'.$data['item']['picture'].'">';
		}
		else
		{
			if($data['item']['type']=="2")
			{
				$setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
			}
			else if($data['item']['type']=="3")
			{
				$setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
			}
			else if($data['item']['type']=="4")
			{
				$setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
			}
			else if($data['item']['type']=="5")
			{
				$setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
			}
			else if($data['item']['type']=="6")
			{
				$setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
			}
			else if($data['item']['type']=="7")
			{
				$setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
			}
			else if($data['item']['type']=="8")
			{
				$setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
			}
			else
			{
				$setnopic = base_url().'assets/images/nopic.jpg';
			}
			$returndata .= '<img style="display: block; max-width: 100%;  height: auto;" src="'.$setnopic.'">';
		}
		$returndata .= '</a>';
		$returndata .= '</div>';
		$returndata .= '<div class="pd-box_detail_product text-center">';
		$returndata .= '<div class="pd-caption">';
		$returndata .= '<h3>'.$data['item']['name'].'</h3>';
		$returndata .= '<p>';
		$returndata .=  iconv_substr(strip_tags($data['item']['description']),0,100, "UTF-8")."...";
		$returndata .= '</p>';
		$returndata .= '</div>';
		$returndata .= '</div>';
		$returndata .= '<h4 class="pd-box_btn_close"><a id="closebox_on_map">close</a></h4>';
		$returndata .= '<h4 class="pd-box_btn_moremaps"><a data-toggle="modal" data-target="#Product-Modal'.$data['item']['id'].'">more>></a></h4>';
		$returndata .= '</div>';
		$returndata .= '</li>';
		$returndata .= '</ul>';
		print_r(json_encode($returndata));
	}


	public function login_page()
	{
		if($this->session->userdata("Permission")!=""&&$this->session->userdata("Permission")!=NULL&&$this->session->userdata("User_ID")!=""&&$this->session->userdata("User_ID")!=NULL&&$this->session->userdata("Username")!=""&&$this->session->userdata("Username")!=NULL)
		{
			redirect("main/index");
		}
		else
		{
			$this->load->view('template/header');
			$this->load->view('template/navbar');
			$this->load->view('index/login');
			$this->load->view('template/footer');
		}
	}

	public function login()
	{
		$this->load->model('login_model');
		$this->form_validation->set_rules('username' ,'Username','required');
		$this->form_validation->set_rules('password' ,'Password','required|callback_verifyUser');

		if($this->form_validation->run() == false)
		{
			redirect('index/login_page?error=fail','refresh');
		}
		elseif($this->form_validation->run() == true && !$this->session->userdata("User_ID"))
		{
			redirect('index/login_page?error=inactive','refresh');
		}
		else
		{
			redirect('main/index','refresh');
		}

	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect("index");
	}

	public function verifyUser()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if($this->login_model->login($username ,$password))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('verifyUser','Incorrect Username or Password. Please try again');
			return false;
		}
	}

	public function forget()
	{
		$this->load->view('template/header');
		$this->load->view('index/forget');
		$this->load->view('template/footer');
	}

	public function sendemailforgetpassword()
	{
		$User_Email = $this->input->post("User_Email");

		$wherearr = array('User_Email = "'.$User_Email.'"');

    	$getemail = $this->select_model->select_where("*","user",$wherearr);
    	if($getemail->num_rows())
		{
			$getemail = $getemail->row_array();

			$email = $getemail['User_Email'];
			$userid = $getemail['User_ID'];
			$datetime = date("YmdHis");

			$genlink = base_url()."index/restartfromemailresetpassword?gen=".$email."-%%-".($userid+20)."-%%-".$datetime;

			$datetimeexpire = new DateTime('now');
			$datetimeexpire->modify('+1 day');

			$dataarr = array(
				"fgpw_Email" => $email,
				"fgpw_expire" => $datetimeexpire->format('Y-m-d H:i:s'),
				"fgpw_link" => $genlink
			);

			$updated = $this->update_model->insertforgetpassword($dataarr);
			if($updated == true)
			{
				$this->sendforgetpasswordtoemail($email,$genlink);
				$data['response'] = $getemail['User_Email'];
				$data['pass'] = true;
			}
			else
			{
				$data['response'] = "เกิดข้อผิดพลาดกับระบบ กรุณาติดต่อ xxx - xxxxxxx หรือ p.kittichet@gmail.com ";
				$data['pass'] = false;
			}
		}
		else
		{
			$data['response'] = "ไม่มีอีเมลล์นี้อยู่ในระบบ กรุณาติดต่อ xxx - xxxxxxx หรือ p.kittichet@gmail.com ";
			$data['pass'] = false;
		}

		$this->load->view('template/header');
		$this->load->view('index/forgetemailsended',$data);
		$this->load->view('template/footer');
	}

	function sendforgetpasswordtoemail($strTo,$link)
	{
		/*require_once "./assets/reCAPTCHA/autoload.php";
		$siteKey = '6LfyS_8SAAAAAMwYlai5PUIeHQfjZhEdNE2lEh0j';
		$secret = '6LfyS_8SAAAAAIwakpNXsikLiT6YGFlHtEHEi1kC';
		$lang = 'th';
		$recaptcha = new \ReCaptcha\ReCaptcha($secret);
		$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

		if ($resp->isSuccess())
		{

		}
		elseif($resp==NULL)
		{
			echo "กรุณาใส่ reCAPTCHA";
		}
		else
		{
			echo "กรุณาตรวจสอบ reCAPTCHA";
			//print_r($resp->getErrorCodes());
		}*/

		$bccmail = "p.kittichet@gmail.com";
		//$bccmail = "teatei3@hotmail.com";
		$strSubject = "Forget Password ".$this->sitename;
		$strHeader = "Content-type: text/html; charset=UTF-8\r\n"; // or UTF-8 //
		$strHeader .= "From: p.kittichet@gmail.com \r\n";
		$strHeader .= "Bcc: $bccmail\r\n";
		$strMessage = "<strong>Forget Password : ".$this->sitename."</strong> <br>";
		$strMessage .= "------------------------------------------------------------------------------ <br><br>";
		$strMessage .= "Link for Reset Password : <a href='$link'>" . $link . "</a><br>";
		$strMessage .= "------------------------------------------------------------------------------ <br>";

		//echo  $strHeader;
		//echo  $strMessage;
		//break;
		$flgSend = @mail($strTo,$strSubject,$strMessage,$strHeader);  // @ = No Show Error //
		if($flgSend)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public function changepassword()
	{
		$id = $this->input->post("userid");
		$newpass = $this->input->post("Password");

		$key='*sevencommunity*_';
		$password = md5($newpass.$key);
		$dataarr = array(
			"Password" => $password
		);

		$updated = $this->update_model->addedituser($dataarr, $id);

		$getgen = $this->input->post("gen");

		$fullgenlink = base_url()."index/restartfromemailresetpassword?gen=".$getgen;

		$wherearr = array('fgpw_link = "'.$fullgenlink.'"');

		$getforget = $this->select_model->select_where("*","forgetpassword",$wherearr);

		if($getforget->num_rows())
		{
			$getforget = $getforget->row_array();

			$logget = "fgpw_ID : ".$getforget['fgpw_ID']." fgpw_Email : ".$getforget['fgpw_Email']." fgpw_expire : ".$getforget['fgpw_expire']." fgpw_link : ".$getforget['fgpw_link'];

			$delarr = array('fgpw_ID = "'.$getforget['fgpw_ID'].'"');

			$delreturn = $this->delete_model->delete_data("forgetpassword",$delarr);
		}

		redirect("index/resetpasswordcomplete");
	}

	public function resetpasswordcomplete()
	{
		$this->load->view('template/header');
		$this->load->view('index/resetpasswordcomplete');
		$this->load->view('template/footer');
	}

	public function restartfromemailresetpassword()
	{
		$getgen = $this->input->get("gen");

		$fullgenlink = base_url()."index/restartfromemailresetpassword?gen=".$getgen;

		$genval = explode("-%%-", $getgen);
		$User_Email = $genval[0];
		$User_ID = $genval[1]-20;

		$wherearr = array('fgpw_Email = "'.$User_Email.'"', 'fgpw_link = "'.$fullgenlink.'"');

		$getforget = $this->select_model->select_where("*","forgetpassword",$wherearr);

		if($getforget->num_rows())
		{
			$getforget = $getforget->row_array();

			if($getforget['fgpw_expire']<=date("Y-m-d H:i:s"))
			{
				$logget = "fgpw_ID : ".$getforget['fgpw_ID']." fgpw_Email : ".$getforget['fgpw_Email']." fgpw_expire : ".$getforget['fgpw_expire']." fgpw_link : ".$getforget['fgpw_link'];

				$delarr = array('fgpw_ID = "'.$getforget['fgpw_ID'].'"');

				$delreturn = $this->delete_model->delete_data_log("forgetpassword",$delarr,$logget,"forgetpassword");

				if($delreturn==true)
				{
					$data['response'] = "ลิงก์ที่คุณใช้หมดอายุไปแล้ว กรุณาขอการเปลี่ยนรหัสผ่านใหม่จากทางหน้า Website และเปลี่ยนรหัสผ่านใน 24 ชั่วโมง";
					$data['canchange'] = false;
				}
				else
				{
					$data['response'] = "ไม่มีลิงก์ที่คุณร้องขอ";
					$data['canchange'] = false;
				}

			}
			else
			{

				$wherearr = array('User_Email = "'.$User_Email.'"', 'User_ID = "'.$User_ID.'"');

				$getuser = $this->select_model->select_where("*","user",$wherearr);
				if($getuser->num_rows())
				{
					$data['user'] = $getuser->row_array();
					$data['canchange'] = true;
				}
				else
				{
					echo "เกิดข้อผิดพลาด ข้อมูลที่เลือกอาจถูกลบไปเรียบร้อยแล้ว กรุณา Refresh Page";
					$data['canchange'] = false;
				}

			}

		}
		else
		{
			$data['response'] = "ไม่มีลิงก์ที่คุณร้องขอ";
			$data['canchange'] = false;
		}


		$this->load->view('template/header');
		$this->load->view('index/restartfromemailresetpassword',$data);
		$this->load->view('template/footer');
	}

	public function signup()
	{
		$this->load->view('template/header');
		$this->load->view('index/signup');
		$this->load->view('template/footer');
	}

	public function register_update_data()
	{

		$User_ID = $this->input->post('User_ID');
		$rePassword = $this->input->post('rePassword');
		$Username = $this->input->post('Username');
		$Password = $this->input->post('Password');
		$User_Firstname = $this->input->post('User_Firstname');
		$User_Lastname = $this->input->post('User_Lastname');
		$User_Phone_Number = $this->input->post('User_Phone_Number');
		$User_Email = $this->input->post('User_Email');
		$Permission = $this->input->post('Permission');
		$oldpic = $this->input->post('oldpic');

		/*$User_Birth_Date = str_replace("/","-",$User_Birth_Date);
		$date = explode("-",$User_Birth_Date);
		$User_Birth_Date = $date[2].'-'.$date[1].'-'.$date[0];*/

		if($_FILES['User_Display_Picture']['size']!=0&&($_FILES['User_Display_Picture']['name']!=NULL||$_FILES['User_Display_Picture']['name']!=''))
		{
			$statusupload = $this->user_pictureupload();
		}
		else
		{
			$statusupload = "noupload";
		}

		if($statusupload!="error")
		{
			if($User_ID==NULL||$User_ID=="")
			{
				$wherearr = array('User_Email = "'.$User_Email.'"','Username = "'.$Username.'"');
			}
			else
			{
				$wherearr = array('User_Email = "'.$User_Email.'"', 'Username = "'.$Username.'"', 'User_ID != "'.$User_ID.'"');
			}

	    	$dupuser = $this->select_model->select_where("*","user",$wherearr);
	    	if($dupuser->num_rows())
			{
					if($statusupload!="noupload")
					{
						if (file_exists(FCPATH."assets/upload/user_picture/".$statusupload)) {
					   		unlink(FCPATH."assets/upload/user_picture/".$statusupload);
						}
					}

					echo "มี E-mail นี้อยู่ในระบบอยู่แล้ว";
					exit;
			}
			else
			{
						$key='deathwing_tei';
						$password = md5($Password.$key);
						$dataarr = array(
							"Username" => strtolower($Username),
							"User_Firstname" => $User_Firstname,
							"User_Lastname" => $User_Lastname,
							"User_Phone_Number" => $User_Phone_Number,
							"User_Email" => $User_Email
						);

						if($statusupload!="noupload")
						{
							$dataarr = array_merge($dataarr, array( "User_Display_Picture" =>  $statusupload) );
						}

						if($User_ID==NULL||$User_ID=="")
						{
							$dataarr = array_merge($dataarr, array( "Password" =>  $password) );
							$dataarr = array_merge($dataarr, array( "Permission" => "User" ) );
							$updated = $this->update_model->addedituser($dataarr, NULL);
						}
						else
						{
							$dataarr = array_merge($dataarr, array( "Permission" => $Permission ) );
							$updated = $this->update_model->addedituser($dataarr, $User_ID);
						}


						if($updated == "พบข้อผิดพลาด กรุณาตรวจสอบ")
						{
							if($statusupload!="noupload")
							{
								if (file_exists(FCPATH."assets/upload/user_picture/".$statusupload)) {
							   		unlink(FCPATH."assets/upload/user_picture/".$statusupload);
								}
							}
						}
						else
						{
							if($oldpic!=NULL&&$oldpic!=""&&$statusupload!="noupload")
							{
								if (file_exists(FCPATH."assets/upload/user_picture/".$oldpic)) {
							   		unlink(FCPATH."assets/upload/user_picture/".$oldpic);
								}
							}
						}


					echo $updated;

					$this->registermail($User_Email);

			}
		}
		else
		{
			echo "กรุณาตรวจสอบขนาดไฟล์(สูงสุด 2 MB, 1024x768) และ นามสกุลไฟล์(.jpg, .jpeg, .png)";
		}

	}

	function registermail($strTo)
	{

		$bccmail = "p.kittichet@gmail.com";
		//$bccmail = "teatei3@hotmail.com";
		$strSubject = "Register ".$this->sitename;
		$strHeader = "Content-type: text/html; charset=UTF-8\r\n"; // or UTF-8 //
		$strHeader .= "From: p.kittichet@gmail.com \r\n";
		$strHeader .= "Bcc: $bccmail\r\n";
		$strMessage = "<strong>Register : ".$this->sitename."</strong> <br>";
		$strMessage .= "------------------------------------------------------------------------------ <br><br>";
		$strMessage .= "คุณได้สมัครสมาชิก ".$this->sitename."<br>กรุณารอ Email ยืนยันจากผู้ดูแลระบบเพื่อดำเนินการใช้งานต่อไป<br/>";
		$strMessage .= "------------------------------------------------------------------------------ <br>";

		//echo  $strHeader;
		//echo  $strMessage;
		//break;
		$flgSend = @mail($strTo,$strSubject,$strMessage,$strHeader);  // @ = No Show Error //
		if($flgSend)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	function user_pictureupload()
	{
		$newspic = str_replace(" ","_",$_FILES['User_Display_Picture']['name']);
		$config['upload_path'] = "./assets/upload/user_picture/";
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2048KB';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['file_name'] = $newspic;
		$config['overwrite'] = false;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('User_Display_Picture'))
		{
			$error = array('error' => $this->upload->display_errors());
			return "error";
		}
		else
		{
			$datauploaded  = array('upload_data' => $this->upload->data());
			$savepath = $datauploaded['upload_data']['file_name'];

			if($datauploaded['upload_data']['image_width'] > $datauploaded['upload_data']['image_height'])
			{
				$config['width'] = 1024;
				$config['height'] = 768;
			}
			elseif($datauploaded['upload_data']['image_width'] < $datauploaded['upload_data']['image_height'])
			{
				$config['width'] = 768;
				$config['height'] = 1024;
			}
			else
			{
				$config['width'] = 1024;
				$config['height'] = 1024;
			}

			$config['image_library'] = 'gd2';
			$config['source_image'] =  "./assets/upload/user_picture/".$savepath;
			$config['maintain_ratio'] = TRUE;
			$config['overwrite'] = true;

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();

			return $savepath;
		}
	}


	public function waitaccept()
	{
		$this->load->view('template/header');
		$this->load->view('index/waitaccept');
		$this->load->view('template/footer');
	}


		function getpagebytype($type)
		{
		  $retdata['page'] = $this->select_model->select_where("*","page",array("type = '".$type."'","withdraw = 0"));
		  if($retdata['page']->num_rows())
		  {
		    $retdata['page_row'] = $retdata['page']->num_rows();
		    $retdata['page'] = $retdata['page']->row_array();
		  }
		  else
		  {
		    $retdata['page_row'] = 0;
		    $retdata['page'] = $this->emptypage();
		    $retdata['page']['type'] = $type;
		  }
		  return $retdata;
		}

		function getpagebytypewithpic($type,$onpage = "main")
		{
			$retdata['page'] = $this->select_model->select_where("*","page",array("type = '".$type."'","withdraw = 0"));
			if($retdata['page']->num_rows())
			{
				$retdata['page_row'] = $retdata['page']->num_rows();
				$retdata['page'] = $retdata['page']->row_array();
				$arrsearch = array("page_id = '".$retdata['page']['id']."'");
				if($onpage=="main")
				{
					$arrsearch = array_merge($arrsearch, array( "sub_page_id IS NULL") );
				}
				$picture = $this->select_model->selectwhereorder("*","picture",$arrsearch,"picture_order","asc");
			  if($picture->num_rows())
			  {
			   $picture_row = $picture->num_rows();
			   $picture = $picture->row_array();
				 $retdata['page']['picture'] = $picture['path'];
			  }
			  else
			  {
					$retdata['page']['picture'] = NULL;
			  }
			}
			else
			{
				$retdata['page_row'] = 0;
				$retdata['page'] = $this->emptypage();
				$retdata['page']['type'] = $type;
				$retdata['page']['picture'] = NULL;
			}
			return $retdata;
		}

		function getpage_en($id)
		{
		  $retdata['page_en'] = $this->select_model->select_where("*","page_en","page_id = '".$id."'");
		  if($retdata['page_en']->num_rows())
		  {
		    $retdata['page_en_row'] = $retdata['page_en']->num_rows();
		    $retdata['page_en'] = $retdata['page_en']->row_array();
		  }
		  else
		  {
		    $retdata['page_en_row'] = 0;
		    $retdata['page_en'] = $this->emptypageen();
		  }
		  return $retdata;
		}

		function getpage_cn($id)
		{
		  $retdata['page_cn'] = $this->select_model->select_where("*","page_cn","page_id = '".$id."'");
		  if($retdata['page_cn']->num_rows())
		  {
		    $retdata['page_cn_row'] = $retdata['page_cn']->num_rows();
		    $retdata['page_cn'] = $retdata['page_cn']->row_array();
		  }
		  else
		  {
		    $retdata['page_cn_row'] = 0;
		    $retdata['page_cn'] = $this->emptypagecn();
		  }
		  return $retdata;
		}

		function getslide($id,$subid = NULL)
		{
			$arrsearch = array("page_id = '".$id."'");
			if($subid!=NULL&&$subid!=""&&$subid!="0")
			{
				$arrsearch = array_merge($arrsearch, array( "sub_page_id = '".$subid."'") );
			}
			else
			{
				$arrsearch = array_merge($arrsearch, array( "sub_page_id = ".NULL) );
			}

		  $retdata['slide']= $this->select_model->selectwhereorder("*","slide",$arrsearch,"slide_order","asc");
		  if($retdata['slide']->num_rows())
		  {
		    $retdata['slide_row'] = $retdata['slide']->num_rows();
		    $retdata['slide'] = $retdata['slide']->result_array();
		  }
		  else
		  {
		    $retdata['slide_row'] = 0;
		    $retdata['slide'] = $this->emptyslide();
		  }
		  return $retdata;
		}

		function getpicture($id,$subid = NULL)
		{
			$arrsearch = array("page_id = '".$id."'");
			if($subid!=NULL&&$subid!=""&&$subid!="0")
			{
				$arrsearch = array_merge($arrsearch, array( "sub_page_id = '".$subid."'") );
			}
			else
			{
				$arrsearch = array_merge($arrsearch, array( "sub_page_id = ".NULL) );
			}

		  $retdata['picture'] = $this->select_model->selectwhereorder("*","picture",$arrsearch,"picture_order","asc");
		  if($retdata['picture']->num_rows())
		  {
		   $retdata['picture_row'] = $retdata['picture']->num_rows();
		   $retdata['picture'] = $retdata['picture']->result_array();
		  }
		  else
		  {
		   $retdata['picture_row'] = 0;
		   $retdata['picture'] = $this->emptypicture();
		  }
		  return $retdata;
		}

		function getfile($id,$subid = NULL)
		{
			$arrsearch = array("page_id = '".$id."'");
			if($subid!=NULL&&$subid!=""&&$subid!="0")
			{
				$arrsearch = array_merge($arrsearch, array( "sub_page_id = '".$subid."'") );
			}
			else
			{
				$arrsearch = array_merge($arrsearch, array( "sub_page_id = ".NULL) );
			}

		  $retdata['file']  = $this->select_model->selectwhereorder("*","file",$arrsearch,"file_order","asc");
		  if($retdata['file']->num_rows())
		  {
		    $retdata['file_row'] = $retdata['file']->num_rows();
		    $retdata['file'] = $retdata['file']->result_array();
		  }
		  else
		  {
		    $retdata['file_row'] = 0;
		    $retdata['file'] = $this->emptyfile();
		  }
		  return $retdata;
		}

		function getallsubpage($id)
		{
			$getdata = $this->select_model->select_where("*","subpage",array("page_id = '".$id."'","withdraw = 0"));
			if($getdata->num_rows())
			{
				$setdata['subpage_row'] = $getdata->num_rows();
				$setdata['subpage'] = $getdata->result_array();
				return $setdata;
			}
			else
			{
				$setdata['subpage_row'] = 0;
				$setdata['subpage'] = $this->emptysubpage();
				return $setdata;
			}
		}

		function getsubpage($id,$subid)
		{
			$getdata = $this->select_model->select_where("*","subpage",array("page_id = '".$id."'","id = '".$subid."'","withdraw = 0"));
			if($getdata->num_rows())
			{
				$setdata['subpage_row'] = $getdata->num_rows();
				$setdata['subpage'] = $getdata->row_array();
				return $setdata;
			}
			else
			{
				$setdata['subpage_row'] = 0;
				$setdata['subpage'] = $this->emptysubpage();
				return $setdata;
			}
		}

		function getsubpage_en($id,$subid)
		{
			$getdata = $this->select_model->select_where("*","subpage_en",array("page_id = '".$id."'","subpage_id = '".$subid."'"));
			if($getdata->num_rows())
			{
				$setdata['subpage_en_row'] = $getdata->num_rows();
				$setdata['subpage_en'] = $getdata->row_array();
				return $setdata;
			}
			else
			{
				$setdata['subpage_en_row'] = 0;
				$setdata['subpage_en'] = $this->emptysubpage_en();
				return $setdata;
			}
		}

		function getsubpage_cn($id,$subid)
		{
			$getdata = $this->select_model->select_where("*","subpage_cn",array("page_id = '".$id."'","subpage_id = '".$subid."'"));
			if($getdata->num_rows())
			{
				$setdata['subpage_cn_row'] = $getdata->num_rows();
				$setdata['subpage_cn'] = $getdata->row_array();
				return $setdata;
			}
			else
			{
				$setdata['subpage_cn_row'] = 0;
				$setdata['subpage_cn'] = $this->emptysubpage_cn();
				return $setdata;
			}
		}

		function getitem($id)
		{
			$retdata['item'] = $this->select_model->select_where("*","item",array("id = '".$id."'","withdraw = 0","item.showhide = 0"));
			if($retdata['item']->num_rows())
			{
				$retdata['item_row'] = $retdata['item']->num_rows();
				$retdata['item'] = $retdata['item']->row_array();
			}
			else
			{
				$retdata['item_row'] = 0;
				$retdata['item'] = $this->emptyitem();
			}
			return $retdata;
		}

		function getitem_en($id)
		{
			$retdata['item_en'] = $this->select_model->select_where("*","item_en",array("item_id = '".$id."'"));
			if($retdata['item_en']->num_rows())
			{
				$retdata['item_en_row'] = $retdata['item_en']->num_rows();
				$retdata['item_en'] = $retdata['item_en']->row_array();
			}
			else
			{
				$retdata['item_en_row'] = 0;
				$retdata['item_en'] = $this->emptyitem_en();
			}
			return $retdata;
		}

		function getitem_picture($id)
		{
			$retdata['item_picture'] = $this->select_model->selectwhereorder("*","item_picture",array("item_id = '".$id."'"),"picture_order","asc");
			if($retdata['item_picture']->num_rows())
			{
				$retdata['item_picture_row'] = $retdata['item_picture']->num_rows();
				$retdata['item_picture'] = $retdata['item_picture']->result_array();
			}
			else
			{
				$retdata['item_picture_row'] = 0;
				$retdata['item_picture'] = NULL;
			}
			return $retdata;
		}

		function getnews($id)
		{
			$retdata['news'] = $this->select_model->select_where("*","news",array("id = '".$id."'","withdraw = 0"));
			if($retdata['news']->num_rows())
			{
				$retdata['news_row'] = $retdata['news']->num_rows();
				$retdata['news'] = $retdata['news']->row_array();
			}
			else
			{
				$retdata['news_row'] = 0;
				$retdata['news'] = $this->emptynews();
			}
			return $retdata;
		}

		function getnews_en($id)
		{
			$retdata['news_en'] = $this->select_model->select_where("*","news_en",array("news_id = '".$id."'"));
			if($retdata['news_en']->num_rows())
			{
				$retdata['news_en_row'] = $retdata['news_en']->num_rows();
				$retdata['news_en'] = $retdata['news_en']->row_array();
			}
			else
			{
				$retdata['news_en_row'] = 0;
				$retdata['news_en'] = $this->emptynews_en();
			}
			return $retdata;
		}

		function getnews_picture($id)
		{
			$retdata['news_picture'] = $this->select_model->selectwhereorder("*","news_picture",array("news_id = '".$id."'"),"picture_order","asc");
			if($retdata['news_picture']->num_rows())
			{
				$retdata['news_picture_row'] = $retdata['news_picture']->num_rows();
				$retdata['news_picture'] = $retdata['news_picture']->result_array();
			}
			else
			{
				$retdata['news_picture_row'] = 0;
				$retdata['news_picture'] = $this->emptynews_picture();
			}
			return $retdata;
		}

		private function emptyitem()
		{
			return array(
				"id" => NULL,
				"name" => NULL,
				"description" => NULL,
				"short_description" => NULL,
				"lat" => NULL,
				"lon" => NULL,
				"create_datetime" => NULL,
				"update_datetime" => NULL,
				"visited" => NULL,
				"frequency_update" => NULL,
				"page_id" => NULL
			);
		}

		private function emptyitem_en()
		{
			return array(
				"id" => NULL,
				"name_en" => NULL,
				"short_description_en" => NULL,
				"description_en" => NULL,
				"item_id" => NULL
			);
		}

		private function emptyitem_picture()
		{
			return array(
				"id" => NULL,
				"name" => NULL,
				"path" => NULL,
				"picture_order" => NULL,
				"item_id" => NULL
			);
		}


			private function emptynews()
			{
				return array(
					"id" => NULL,
					"name" => NULL,
					"description" => NULL,
					"create_datetime" => NULL,
					"update_datetime" => NULL,
					"visited" => NULL,
					"showhide" => NULL,
					"page_id" => NULL
				);
			}

			private function emptynews_en()
			{
				return array(
					"id" => NULL,
					"name_en" => NULL,
					"description_en" => NULL,
					"update_datetime" => NULL,
					"news_id" => NULL
				);
			}

			private function emptynews_picture()
			{
				return array(
					"id" => NULL,
					"name" => NULL,
					"path" => NULL,
					"picture_order" => NULL,
					"news_id" => NULL
				);
			}

	  private function emptypage()
	  {
	    return array(
	      "id" => NULL,
	      "name" => NULL,
	      "description" => NULL,
	      "link" => NULL,
	      "create_datetime" => NULL,
	      "type" => NULL
	    );
	  }

	  private function emptypageen()
	  {
	    return array(
	      "id" => NULL,
	      "name_en" => NULL,
	      "description_en" => NULL,
	      "link_en" => NULL,
	      "page_id" => NULL
	    );
	  }

	  private function emptypagecn()
	  {
	    return array(
	      "id" => NULL,
	      "name_cn" => NULL,
	      "description_cn" => NULL,
	      "link_cn" => NULL,
	      "page_id" => NULL
	    );
	  }

	  private function emptypicture()
	  {
	    return array(
	      "id" => NULL,
	      "name" => NULL,
	      "path" => NULL,
	      "picture_order" => NULL,
	      "page_id" => NULL
	    );
	  }

	  private function emptyslide()
	  {
	    return array(
	      "id" => NULL,
	      "name" => NULL,
	      "path" => NULL,
	      "slide_order" => NULL,
	      "page_id" => NULL
	    );
	  }

		private function emptyfile()
		{
			return array(
				"id" => NULL,
				"name" => NULL,
				"path" => NULL,
				"file_order" => NULL,
				"page_id" => NULL
			);
		}

		private function emptysubpage()
		{
			return array(
				"id" => NULL,
				"name" => NULL,
				"description" => NULL,
				"link" => NULL,
				"create_datetime" => NULL,
				"update_datetime" => NULL,
				"page_id" => NULL,
				"withdraw" => NULL
			);
		}

		private function emptysubpage_cn()
		{
			return array(
				"id" => NULL,
				"name_cn" => NULL,
				"description_cn" => NULL,
				"link_cn" => NULL,
				"update_datetime" => NULL,
				"page_id" => NULL,
				"subpage_id" => NULL
			);
		}

		private function emptysubpage_en()
		{
			return array(
				"id" => NULL,
				"name_en" => NULL,
				"description_en" => NULL,
				"link_en" => NULL,
				"update_datetime" => NULL,
				"page_id" => NULL,
				"subpage_id" => NULL
			);
		}

}
