<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {

	public function __construct()
  {
      parent::__construct();
      date_default_timezone_set('Asia/Bangkok');
      $this->load->model('select_model');
      $this->load->model('update_model');
      $this->load->model('login_model');
      $this->load->model('delete_model');

      $this->sitename = "Sevencommunity";
			$this->attruse = array('name' => true,'description' => true,'link' => true,'picture' => true,'slide' => true,'file' => true,'subpage' => true);

      if(!$this->session->userdata("Permission")||!$this->session->userdata("Username")||!$this->session->userdata("User_ID"))
      {
          redirect(base_url());
      }
  }

  function index()
  {
    $onactivemenu = "1";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
  	$data['page'] = $page['page'];

    if($data['page']['id']!=NULL&&$data['page']['id']!="")
    {
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
      $data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
      $data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
      $data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
      $data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
    }
    else
    {
			$data['attr'] = $this->attruse;

      $data['page_en_row'] = 0;
      $data['page_en'] = $this->emptypageen();
      $data['page_cn_row'] = 0;
      $data['page_cn'] = $this->emptypagecn();
      $data['slide_row'] = 0;
      $data['slide'] = $this->emptyslide();
      $data['picture_row'] = 0;
      $data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
      $data['file'] = $this->emptyfile();
    }

    $this->load->view('admin_template/header');
  	$this->load->view('admin_template/sidebar',$datamenu);
    $this->load->view('cms/default',$data);
    $this->load->view('admin_template/footer');
  }

	function com1()
  {
    $onactivemenu = "2";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
  	$data['page'] = $page['page'];

    if($data['page']['id']!=NULL&&$data['page']['id']!="")
    {
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
      $data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
      $data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
      $data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
      $data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
    }
    else
    {
			$data['attr'] = $this->attruse;

      $data['page_en_row'] = 0;
      $data['page_en'] = $this->emptypageen();
      $data['page_cn_row'] = 0;
      $data['page_cn'] = $this->emptypagecn();
      $data['slide_row'] = 0;
      $data['slide'] = $this->emptyslide();
      $data['picture_row'] = 0;
      $data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
      $data['file'] = $this->emptyfile();
    }

    $this->load->view('admin_template/header');
  	$this->load->view('admin_template/sidebar',$datamenu);
    $this->load->view('cms/default',$data);
    $this->load->view('admin_template/footer');
  }

	function com2()
  {
    $onactivemenu = "3";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
  	$data['page'] = $page['page'];

    if($data['page']['id']!=NULL&&$data['page']['id']!="")
    {
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
      $data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
      $data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
      $data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
      $data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
    }
    else
    {
			$data['attr'] = $this->attruse;

      $data['page_en_row'] = 0;
      $data['page_en'] = $this->emptypageen();
      $data['page_cn_row'] = 0;
      $data['page_cn'] = $this->emptypagecn();
      $data['slide_row'] = 0;
      $data['slide'] = $this->emptyslide();
      $data['picture_row'] = 0;
      $data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
      $data['file'] = $this->emptyfile();
    }

    $this->load->view('admin_template/header');
  	$this->load->view('admin_template/sidebar',$datamenu);
    $this->load->view('cms/default',$data);
    $this->load->view('admin_template/footer');
  }

	function com3()
	{
		$onactivemenu = "4";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
		$data['page'] = $page['page'];

		if($data['page']['id']!=NULL&&$data['page']['id']!="")
		{
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
			$data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
			$data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
			$data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
			$data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
		}
		else
		{
			$data['attr'] = $this->attruse;

			$data['page_en_row'] = 0;
			$data['page_en'] = $this->emptypageen();
			$data['page_cn_row'] = 0;
			$data['page_cn'] = $this->emptypagecn();
			$data['slide_row'] = 0;
			$data['slide'] = $this->emptyslide();
			$data['picture_row'] = 0;
			$data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
			$data['file'] = $this->emptyfile();
		}

		$this->load->view('admin_template/header');
		$this->load->view('admin_template/sidebar',$datamenu);
		$this->load->view('cms/default',$data);
		$this->load->view('admin_template/footer');
	}

	function com4()
	{
		$onactivemenu = "5";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
		$data['page'] = $page['page'];

		if($data['page']['id']!=NULL&&$data['page']['id']!="")
		{
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
			$data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
			$data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
			$data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
			$data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
		}
		else
		{
			$data['attr'] = $this->attruse;

			$data['page_en_row'] = 0;
			$data['page_en'] = $this->emptypageen();
			$data['page_cn_row'] = 0;
			$data['page_cn'] = $this->emptypagecn();
			$data['slide_row'] = 0;
			$data['slide'] = $this->emptyslide();
			$data['picture_row'] = 0;
			$data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
			$data['file'] = $this->emptyfile();
		}

		$this->load->view('admin_template/header');
		$this->load->view('admin_template/sidebar',$datamenu);
		$this->load->view('cms/default',$data);
		$this->load->view('admin_template/footer');
	}

	function com5()
	{
		$onactivemenu = "6";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
		$data['page'] = $page['page'];

		if($data['page']['id']!=NULL&&$data['page']['id']!="")
		{
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
			$data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
			$data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
			$data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
			$data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
		}
		else
		{
			$data['attr'] = $this->attruse;

			$data['page_en_row'] = 0;
			$data['page_en'] = $this->emptypageen();
			$data['page_cn_row'] = 0;
			$data['page_cn'] = $this->emptypagecn();
			$data['slide_row'] = 0;
			$data['slide'] = $this->emptyslide();
			$data['picture_row'] = 0;
			$data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
			$data['file'] = $this->emptyfile();
		}

		$this->load->view('admin_template/header');
		$this->load->view('admin_template/sidebar',$datamenu);
		$this->load->view('cms/default',$data);
		$this->load->view('admin_template/footer');
	}

	function com6()
	{
		$onactivemenu = "7";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
		$data['page'] = $page['page'];

		if($data['page']['id']!=NULL&&$data['page']['id']!="")
		{
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
			$data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
			$data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
			$data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
			$data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
		}
		else
		{
			$data['attr'] = $this->attruse;

			$data['page_en_row'] = 0;
			$data['page_en'] = $this->emptypageen();
			$data['page_cn_row'] = 0;
			$data['page_cn'] = $this->emptypagecn();
			$data['slide_row'] = 0;
			$data['slide'] = $this->emptyslide();
			$data['picture_row'] = 0;
			$data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
			$data['file'] = $this->emptyfile();
		}

		$this->load->view('admin_template/header');
		$this->load->view('admin_template/sidebar',$datamenu);
		$this->load->view('cms/default',$data);
		$this->load->view('admin_template/footer');
	}

	function com7()
	{
		$onactivemenu = "8";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
		$data['page'] = $page['page'];

		if($data['page']['id']!=NULL&&$data['page']['id']!="")
		{
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
			$data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
			$data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
			$data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
			$data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
		}
		else
		{
			$data['attr'] = $this->attruse;

			$data['page_en_row'] = 0;
			$data['page_en'] = $this->emptypageen();
			$data['page_cn_row'] = 0;
			$data['page_cn'] = $this->emptypagecn();
			$data['slide_row'] = 0;
			$data['slide'] = $this->emptyslide();
			$data['picture_row'] = 0;
			$data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
			$data['file'] = $this->emptyfile();
		}

		$this->load->view('admin_template/header');
		$this->load->view('admin_template/sidebar',$datamenu);
		$this->load->view('cms/default',$data);
		$this->load->view('admin_template/footer');
	}

	function contact()
	{
		$onactivemenu = "9";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
		$data['page'] = $page['page'];

		if($data['page']['id']!=NULL&&$data['page']['id']!="")
		{
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
			$data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
			$data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
			$data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
			$data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
		}
		else
		{
			$data['attr'] = $this->attruse;

			$data['page_en_row'] = 0;
			$data['page_en'] = $this->emptypageen();
			$data['page_cn_row'] = 0;
			$data['page_cn'] = $this->emptypagecn();
			$data['slide_row'] = 0;
			$data['slide'] = $this->emptyslide();
			$data['picture_row'] = 0;
			$data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
			$data['file'] = $this->emptyfile();
		}

		$this->load->view('admin_template/header');
		$this->load->view('admin_template/sidebar',$datamenu);
		$this->load->view('cms/default',$data);
		$this->load->view('admin_template/footer');
	}

	function about()
	{
		$onactivemenu = "10";
		$onactivesubmenu = NULL;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,NULL);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,NULL,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
		$data['page'] = $page['page'];

		if($data['page']['id']!=NULL&&$data['page']['id']!="")
		{
			$data['attr']['name'] = $data['page']['attr_name'];
			$data['attr']['description'] = $data['page']['attr_description'];
			$data['attr']['link'] = $data['page']['attr_link'];
			$data['attr']['picture'] = $data['page']['attr_picture'];
			$data['attr']['slide'] = $data['page']['attr_slide'];
			$data['attr']['file'] = $data['page']['attr_file'];
			$data['attr']['subpage'] = $data['page']['attr_subpage'];

			$page_en = $this->getpage_en($data['page']['id']);
			$data['page_en_row'] = $page_en['page_en_row'];
			$data['page_en'] = $page_en['page_en'];
			$page_cn = $this->getpage_cn($data['page']['id']);
			$data['page_cn_row'] = $page_cn['page_cn_row'];
			$data['page_cn'] = $page_cn['page_cn'];
			$slide = $this->getslide($data['page']['id']);
			$data['slide_row'] = $slide['slide_row'];
			$data['slide'] = $slide['slide'];
			$picture = $this->getpicture($data['page']['id']);
			$data['picture_row'] = $picture['picture_row'];
			$data['picture'] = $picture['picture'];
			$file = $this->getfile($data['page']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
			$subpage = $this->getallsubpage($data['page']['id']);
		}
		else
		{
			$data['attr'] = $this->attruse;

			$data['page_en_row'] = 0;
			$data['page_en'] = $this->emptypageen();
			$data['page_cn_row'] = 0;
			$data['page_cn'] = $this->emptypagecn();
			$data['slide_row'] = 0;
			$data['slide'] = $this->emptyslide();
			$data['picture_row'] = 0;
			$data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
			$data['file'] = $this->emptyfile();
		}

		$this->load->view('admin_template/header');
		$this->load->view('admin_template/sidebar',$datamenu);
		$this->load->view('cms/default',$data);
		$this->load->view('admin_template/footer');
	}

	function subpage()
	{
		$mainid = $this->input->get("main");
		$subid = $this->input->get("sub");
		$type = $this->input->get("type");
		$data['type'] = $type;
		$data['maxpic'] = 2;

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($type,$subid);
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($type,$subid,$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$subpagedata = $this->getsubpage($mainid,$subid);
		$data['subpage_row'] = $subpagedata['subpage_row'];
		$data['subpage'] = $subpagedata['subpage'];

		if($data['subpage']['id']!=NULL&&$data['subpage']['id']!="")
		{
			$data['attr']['name'] = $data['subpage']['attr_name'];
			$data['attr']['description'] = $data['subpage']['attr_description'];
			$data['attr']['link'] = $data['subpage']['attr_link'];
			$data['attr']['picture'] = $data['subpage']['attr_picture'];
			$data['attr']['slide'] = $data['subpage']['attr_slide'];
			$data['attr']['file'] = $data['subpage']['attr_file'];
			//$data['attr']['subpage'] = $data['subpage']['attr_subpage'];

			$subpagedata_en = $this->getsubpage_en($mainid,$subid);
			$data['subpage_en_row'] = $subpagedata_en['subpage_en_row'];
			$data['subpage_en'] = $subpagedata_en['subpage_en'];
			$subpagedata_cn = $this->getsubpage_cn($mainid,$subid);
			$data['subpage_cn_row'] = $subpagedata_cn['subpage_cn_row'];
			$data['subpage_cn'] = $subpagedata_cn['subpage_cn'];
			$slide = $this->getslide($mainid,$data['subpage']['id']);
			$data['slide_row'] = $slide['slide_row'];
			$data['slide'] = $slide['slide'];
			$picture = $this->getpicture($mainid,$data['subpage']['id']);
			$data['picture_row'] = $picture['picture_row'];
			$data['picture'] = $picture['picture'];
			$file = $this->getfile($mainid,$data['subpage']['id']);
			$data['file_row'] = $file['file_row'];
			$data['file'] = $file['file'];
		}
		else
		{
			$data['attr'] = $this->attruse;
			$data['subpage_en_row'] = 0;
			$data['subpage_en'] = $this->emptysubpage_en();
			$data['subpage_cn_row'] = 0;
			$data['subpage_cn'] = $this->emptysubpage_cn();
			$data['slide_row'] = 0;
			$data['slide'] = $this->emptyslide();
			$data['picture_row'] = 0;
			$data['picture'] = $this->emptypicture();
			$data['file_row'] = 0;
			$data['file'] = $this->emptyfile();
		}

		$this->load->view('admin_template/header');
		$this->load->view('admin_template/sidebar',$datamenu);
		$this->load->view('cms/default_sub',$data);
		$this->load->view('admin_template/footer');

	}

	function getpagebytype($type)
	{
	  $retdata['page'] = $this->select_model->select_where("*","page",array("type = '".$type."'","withdraw = 0"));
	  if($retdata['page']->num_rows())
	  {
	    $retdata['page_row'] = $retdata['page']->num_rows();
	    $retdata['page'] = $retdata['page']->row_array();
	  }
	  else
	  {
	    $retdata['page_row'] = 0;
	    $retdata['page'] = $this->emptypage();
	    $retdata['page']['type'] = $type;
	  }
	  return $retdata;
	}

	function getpage_en($id)
	{
	  $retdata['page_en'] = $this->select_model->select_where("*","page_en","page_id = '".$id."'");
	  if($retdata['page_en']->num_rows())
	  {
	    $retdata['page_en_row'] = $retdata['page_en']->num_rows();
	    $retdata['page_en'] = $retdata['page_en']->row_array();
	  }
	  else
	  {
	    $retdata['page_en_row'] = 0;
	    $retdata['page_en'] = $this->emptypageen();
	  }
	  return $retdata;
	}

	function getpage_cn($id)
	{
	  $retdata['page_cn'] = $this->select_model->select_where("*","page_cn","page_id = '".$id."'");
	  if($retdata['page_cn']->num_rows())
	  {
	    $retdata['page_cn_row'] = $retdata['page_cn']->num_rows();
	    $retdata['page_cn'] = $retdata['page_cn']->row_array();
	  }
	  else
	  {
	    $retdata['page_cn_row'] = 0;
	    $retdata['page_cn'] = $this->emptypagecn();
	  }
	  return $retdata;
	}

	function getslide($id,$subid = NULL)
	{
		$arrsearch = array("page_id = '".$id."'");
		if($subid!=NULL&&$subid!=""&&$subid!="0")
		{
			$arrsearch = array_merge($arrsearch, array( "sub_page_id = '".$subid."'") );
		}
		else
		{
			$arrsearch = array_merge($arrsearch, array( "sub_page_id = ".NULL) );
		}

	  $retdata['slide']= $this->select_model->selectwhereorder("*","slide",$arrsearch,"slide_order","asc");
	  if($retdata['slide']->num_rows())
	  {
	    $retdata['slide_row'] = $retdata['slide']->num_rows();
	    $retdata['slide'] = $retdata['slide']->result_array();
	  }
	  else
	  {
	    $retdata['slide_row'] = 0;
	    $retdata['slide'] = $this->emptyslide();
	  }
	  return $retdata;
	}

	function getpicture($id,$subid = NULL)
	{
		$arrsearch = array("page_id = '".$id."'");
		if($subid!=NULL&&$subid!=""&&$subid!="0")
		{
			$arrsearch = array_merge($arrsearch, array( "sub_page_id = '".$subid."'") );
		}
		else
		{
			$arrsearch = array_merge($arrsearch, array( "sub_page_id = ".NULL) );
		}

	  $retdata['picture'] = $this->select_model->selectwhereorder("*","picture",$arrsearch,"picture_order","asc");
	  if($retdata['picture']->num_rows())
	  {
	   $retdata['picture_row'] = $retdata['picture']->num_rows();
	   $retdata['picture'] = $retdata['picture']->result_array();
	  }
	  else
	  {
	   $retdata['picture_row'] = 0;
	   $retdata['picture'] = $this->emptypicture();
	  }
	  return $retdata;
	}

	function getfile($id,$subid = NULL)
	{
		$arrsearch = array("page_id = '".$id."'");
		if($subid!=NULL&&$subid!=""&&$subid!="0")
		{
			$arrsearch = array_merge($arrsearch, array( "sub_page_id = '".$subid."'") );
		}
		else
		{
			$arrsearch = array_merge($arrsearch, array( "sub_page_id = ".NULL) );
		}

	  $retdata['file']  = $this->select_model->selectwhereorder("*","file",$arrsearch,"file_order","asc");
	  if($retdata['file']->num_rows())
	  {
	    $retdata['file_row'] = $retdata['file']->num_rows();
	    $retdata['file'] = $retdata['file']->result_array();
	  }
	  else
	  {
	    $retdata['file_row'] = 0;
	    $retdata['file'] = $this->emptyfile();
	  }
	  return $retdata;
	}

	function getallsubpage($id)
	{
		$getdata = $this->select_model->select_where("*","subpage",array("page_id = '".$id."'","withdraw = 0"));
		if($getdata->num_rows())
		{
			$setdata['subpage_row'] = $getdata->num_rows();
			$setdata['subpage'] = $getdata->result_array();
			return $setdata;
		}
		else
		{
			$setdata['subpage_row'] = 0;
			$setdata['subpage'] = $this->emptysubpage();
			return $setdata;
		}
	}

	function getsubpage($id,$subid)
	{
		$getdata = $this->select_model->select_where("*","subpage",array("page_id = '".$id."'","id = '".$subid."'","withdraw = 0"));
		if($getdata->num_rows())
		{
			$setdata['subpage_row'] = $getdata->num_rows();
			$setdata['subpage'] = $getdata->row_array();
			return $setdata;
		}
		else
		{
			$setdata['subpage_row'] = 0;
			$setdata['subpage'] = $this->emptysubpage();
			return $setdata;
		}
	}

	function getsubpage_en($id,$subid)
	{
		$getdata = $this->select_model->select_where("*","subpage_en",array("page_id = '".$id."'","subpage_id = '".$subid."'"));
		if($getdata->num_rows())
		{
			$setdata['subpage_en_row'] = $getdata->num_rows();
			$setdata['subpage_en'] = $getdata->row_array();
			return $setdata;
		}
		else
		{
			$setdata['subpage_en_row'] = 0;
			$setdata['subpage_en'] = $this->emptysubpage_en();
			return $setdata;
		}
	}

	function getsubpage_cn($id,$subid)
	{
		$getdata = $this->select_model->select_where("*","subpage_cn",array("page_id = '".$id."'","subpage_id = '".$subid."'"));
		if($getdata->num_rows())
		{
			$setdata['subpage_cn_row'] = $getdata->num_rows();
			$setdata['subpage_cn'] = $getdata->row_array();
			return $setdata;
		}
		else
		{
			$setdata['subpage_cn_row'] = 0;
			$setdata['subpage_cn'] = $this->emptysubpage_cn();
			return $setdata;
		}
	}

	function updatecmsdata()
	{
		$type = $this->input->post('type');
		$mainid = $this->input->post('mainid');

		$page_id = $this->input->post('page_id');
		$name = $this->input->post('name');
		$description = $this->input->post('description');
		$link = $this->input->post('link');

		$page_id_en = $this->input->post('page_id_en');
		$name_en = $this->input->post('name_en');
		$description_en = $this->input->post('description_en');
		$link_en = $this->input->post('link_en');

		$page_id_cn = $this->input->post('page_id_cn');
		$name_cn = $this->input->post('name_cn');
		$description_cn = $this->input->post('description_cn');
		$link_cn = $this->input->post('link_cn');

		$updatedata = array(
			"name" => $name,
			"description" => $description,
			"link" => $link,
			"type" => $type
		);

		if($page_id!=NULL&&$page_id!=""&&$page_id!="0")
		{
			$updated = $this->update_model->updated_data($updatedata,"page",$page_id,"id");
		}
		else
		{
			$updatedata = array_merge($updatedata, array( "create_datetime" =>  DATE("Y-m-d H:i:s")) );
			$updated = $this->update_model->updated_data($updatedata,"page",NULL,NULL);
			$page_id = $this->db->insert_id();
			$mainid = $this->db->insert_id();
		}

		$updatedataen = array(
			"name_en" => $name_en,
			"description_en" => $description_en,
			"link_en" => $link_en,
			"page_id" => $mainid
		);

		if($page_id_en!=NULL&&$page_id_en!=""&&$page_id_en!="0")
		{
			$updated2 = $this->update_model->updated_data($updatedataen,"page_en",$page_id_en,"id");
		}
		else
		{
			$updated2 = $this->update_model->updated_data($updatedataen,"page_en",NULL,NULL);
			$page_id_en = $this->db->insert_id();
		}

		$updatedatacn = array(
			"name_cn" => $name_cn,
			"description_cn" => $description_cn,
			"link_cn" => $link_cn,
			"page_id" => $mainid
		);

		if($page_id_cn!=NULL&&$page_id_cn!=""&&$page_id_cn!="0")
		{
			$updated3 = $this->update_model->updated_data($updatedatacn,"page_cn",$page_id_cn,"id");
		}
		else
		{
			$updated3 = $this->update_model->updated_data($updatedatacn,"page_cn",NULL,NULL);
			$page_id_cn = $this->db->insert_id();
		}
	 	echo $updated;
	}

	function updatecmsattr()
	{
		$mainid = $this->input->post('mainidattr');
		$subid = $this->input->post('subidattr');
		$attr_name = $this->input->post('attr_name');
		$attr_description = $this->input->post('attr_description');
		$attr_link = $this->input->post('attr_link');
		$attr_picture = $this->input->post('attr_picture');
		$attr_slide = $this->input->post('attr_slide');
		$attr_file = $this->input->post('attr_file');
		$attr_subpage = $this->input->post('attr_subpage');

		$updatedata = array(
			"attr_name" => $attr_name,
			"attr_description" => $attr_description,
			"attr_link" => $attr_link,
			"attr_picture" => $attr_picture,
			"attr_slide" => $attr_slide,
			"attr_file" => $attr_file
		);

		if($subid!=NULL&&$subid!=""&&$subid!="0"&&$mainid!=NULL&&$mainid!=""&&$mainid!="0")
		{
			$updated = $this->update_model->updated_data($updatedata,"subpage",$subid,"id");
		}
		if($mainid!=NULL&&$mainid!=""&&$mainid!="0")
		{
			$updatedata = array_merge($updatedata, array( "attr_subpage" => $attr_subpage ));
			$updated = $this->update_model->updated_data($updatedata,"page",$mainid,"id");
		}
		else
		{
			$updated = "Check Page Data";
		}

		echo $updated;
	}

	function updatecmsdata_sub()
	{
		$type = $this->input->post('type');
		$mainid = $this->input->post('mainid');
		$subid = $this->input->post('subid');

		$subpage_id = $this->input->post('subpage_id');
		$name = $this->input->post('name');
		$description = $this->input->post('description');
		$link = $this->input->post('link');

		$subpage_id_en = $this->input->post('subpage_id_en');
		$name_en = $this->input->post('name_en');
		$description_en = $this->input->post('description_en');
		$link_en = $this->input->post('link_en');

		$subpage_id_cn = $this->input->post('subpage_id_cn');
		$name_cn = $this->input->post('name_cn');
		$description_cn = $this->input->post('description_cn');
		$link_cn = $this->input->post('link_cn');

		$updatedata = array(
			"name" => $name,
			"description" => $description,
			"link" => $link,
			"page_id" => $mainid
		);

		if($subpage_id!=NULL&&$subpage_id!=""&&$subpage_id!="0")
		{
			$updated = $this->update_model->updated_data($updatedata,"subpage",$subpage_id,"id");
		}
		else
		{
			$updatedata = array_merge($updatedata, array( "create_datetime" =>  DATE("Y-m-d H:i:s")) );
			$updated = $this->update_model->updated_data($updatedata,"subpage",NULL,NULL);
			$subpage_id = $this->db->insert_id();
		}

		$updatedataen = array(
			"name_en" => $name_en,
			"description_en" => $description_en,
			"link_en" => $link_en,
			"page_id" => $mainid,
			"subpage_id" => $subpage_id
		);

		if($subpage_id_en!=NULL&&$subpage_id_en!=""&&$subpage_id_en!="0")
		{
			$updated2 = $this->update_model->updated_data($updatedataen,"subpage_en",$subpage_id_en,"id");
		}
		else
		{
			$updated2 = $this->update_model->updated_data($updatedataen,"subpage_en",NULL,NULL);
			$subpage_id_en = $this->db->insert_id();
		}

		$updatedatacn = array(
			"name_cn" => $name_cn,
			"description_cn" => $description_cn,
			"link_cn" => $link_cn,
			"page_id" => $mainid,
			"subpage_id" => $subpage_id
		);

		if($subpage_id_cn!=NULL&&$subpage_id_cn!=""&&$subpage_id_cn!="0")
		{
			$updated3 = $this->update_model->updated_data($updatedatacn,"subpage_cn",$subpage_id_cn,"id");
		}
		else
		{
			$updated3 = $this->update_model->updated_data($updatedatacn,"subpage_cn",NULL,NULL);
			$subpage_id_cn = $this->db->insert_id();
		}
	 	echo $updated;
	}

	function updatecmspicture()
	{
		$name = $this->input->post("picturename");
		$picturedescription = $this->input->post("picturedescription");
		$mainid = $this->input->post("mainidpic");
		$subid = $this->input->post("subidpic");
		$id = $this->input->post("pictureid");
		$oldpic = $this->input->post("oldpic");

		if($_FILES['uploadpicture']['size']!=0&&($_FILES['uploadpicture']['name']!=NULL||$_FILES['uploadpicture']['name']!=''))
		{
				$statusupload = $this->fileupload("picture",'picture_'. DATE("Ymdhis"),"uploadpicture","picture");
		}
		else
		{
				$statusupload = "noupload";
		}

		if($statusupload!="error"&&$statusupload!="noupload"||($oldpic!=NULL&&$oldpic!=""))
		{
			if($name==NULL||$name=="")
			{
				$name = $statusupload;
			}

			$dataarr = array(
				"name" => $name,
				"description" => $picturedescription,
				"page_id" => $mainid
			);

			if($subid!=NULL&&$subid!=""&&$subid!="0")
			{
				$dataarr = array_merge($dataarr, array( "sub_page_id" =>  $subid) );
			}

			if($statusupload!="noupload")
			{
				$dataarr = array_merge($dataarr, array( "path" =>  $statusupload) );
			}

			if($id==NULL||$id=="")
			{
				$updated = $this->update_model->updated_data($dataarr,"picture",NULL,NULL);
			}
			else
			{
				$updated = $this->update_model->updated_data($dataarr,"picture",$id,"id");
			}


			if($updated == "พบข้อผิดพลาด กรุณาตรวจสอบ")
			{
					if($statusupload!="noupload")
					{
							if (file_exists(FCPATH."assets/upload/picture/".$statusupload)) {
									unlink(FCPATH."assets/upload/picture/".$statusupload);
							}
					}
			}
			else
			{
					if($oldpic!=NULL&&$oldpic!=""&&$statusupload!="noupload")
					{
							if (file_exists(FCPATH."assets/upload/picture/".$oldpic)) {
									unlink(FCPATH."assets/upload/picture/".$oldpic);
							}
					}
			}


			echo $updated;
		}
		else
		{
				echo "กรุณาตรวจสอบขนาดไฟล์(สูงสุด 4 MB) และ นามสกุลไฟล์(.jpg, .jpeg, .png)";
		}
		//$uploadedfile = $this->fileupload("picture",'picture_'. DATE("Ymdhis"),"uploadpicture","picture");
	}

	function updatecmsslide()
	{
		$name = $this->input->post("slidename");
		$mainid = $this->input->post("mainidslide");
		$subid = $this->input->post("subidslide");
		$id = $this->input->post("slideid");
		$oldpic = $this->input->post("oldslide");

		if($_FILES['uploadslide']['size']!=0&&($_FILES['uploadslide']['name']!=NULL||$_FILES['uploadslide']['name']!=''))
		{
				$statusupload = $this->fileupload("slide",'slide_'. DATE("Ymdhis"),"uploadslide","slide");
		}
		else
		{
				$statusupload = "noupload";
		}

		if($statusupload!="error"&&$statusupload!="noupload"||($oldpic!=NULL&&$oldpic!=""))
		{
			if($name==NULL||$name=="")
			{
				$name = $statusupload;
			}

			$dataarr = array(
				"name" => $name,
				"page_id" => $mainid
			);

			if($subid!=NULL&&$subid!=""&&$subid!="0")
			{
				$dataarr = array_merge($dataarr, array( "sub_page_id" =>  $subid) );
			}

			if($statusupload!="noupload")
			{
				$dataarr = array_merge($dataarr, array( "path" =>  $statusupload) );
			}

			if($id==NULL||$id=="")
			{
					$updated = $this->update_model->updated_data($dataarr,"slide",NULL,NULL);
			}
			else
			{
					$updated = $this->update_model->updated_data($dataarr,"slide",$id,"id");
			}


			if($updated == "พบข้อผิดพลาด กรุณาตรวจสอบ")
			{
					if($statusupload!="noupload")
					{
							if (file_exists(FCPATH."assets/upload/slide/".$statusupload)) {
									unlink(FCPATH."assets/upload/slide/".$statusupload);
							}
					}
			}
			else
			{
					if($oldpic!=NULL&&$oldpic!=""&&$statusupload!="noupload")
					{
							if (file_exists(FCPATH."assets/upload/slide/".$oldpic)) {
									unlink(FCPATH."assets/upload/slide/".$oldpic);
							}
					}
			}
			echo $updated;
		}
		else
		{
				echo "กรุณาตรวจสอบขนาดไฟล์(สูงสุด 4 MB) และ นามสกุลไฟล์(.jpg, .jpeg, .png)";
		}
		//$uploadedfile = $this->fileupload("slide",'slide_'. DATE("Ymdhis"),"uploadslide","slide");
	}

	function updatecmsfile()
	{
		$name = $this->input->post("filename");
		$mainid = $this->input->post("mainidfile");
		$subid = $this->input->post("subidfile");
		$id = $this->input->post("fileid");
		$oldpic = $this->input->post("oldfile");
		if($_FILES['uploadfile']['size']!=0&&($_FILES['uploadfile']['name']!=NULL||$_FILES['uploadfile']['name']!=''))
		{
				$statusupload = $this->fileupload("file",'file_'. DATE("Ymdhis"),"uploadfile","file");
		}
		else
		{
				$statusupload = "noupload";
		}

		if($statusupload!="error"&&$statusupload!="noupload"||($oldpic!=NULL&&$oldpic!=""))
		{

			if($name==NULL||$name=="")
			{
				$name = $statusupload;
			}

			$dataarr = array(
				"name" => $name,
				"page_id" => $mainid
			);

			if($subid!=NULL&&$subid!=""&&$subid!="0")
			{
				$dataarr = array_merge($dataarr, array( "sub_page_id" =>  $subid) );
			}

			if($statusupload!="noupload")
			{
					$dataarr = array_merge($dataarr, array( "path" =>  $statusupload) );
			}

			if($id==NULL||$id=="")
			{
					$updated = $this->update_model->updated_data($dataarr,"file",NULL,NULL);
			}
			else
			{
					$updated = $this->update_model->updated_data($dataarr,"file",$id,"id");
			}


			if($updated == "พบข้อผิดพลาด กรุณาตรวจสอบ")
			{
					if($statusupload!="noupload")
					{
							if (file_exists(FCPATH."assets/upload/file/".$statusupload)) {
									unlink(FCPATH."assets/upload/file/".$statusupload);
							}
					}
			}
			else
			{
					if($oldpic!=NULL&&$oldpic!=""&&$statusupload!="noupload")
					{
							if (file_exists(FCPATH."assets/upload/file/".$oldpic)) {
									unlink(FCPATH."assets/upload/file/".$oldpic);
							}
					}
			}

			echo $updated;
		}
		else
		{
				echo "กรุณาตรวจสอบขนาดไฟล์(สูงสุด 4 MB) และ นามสกุลไฟล์(.jpg, .jpeg, .png)";
		}
		//$uploadedfile = $this->fileupload("file",'file_'. DATE("Ymdhis"),"uploadfile","file");
	}

	function addsubpage()
	{
		$mainid = $this->input->post('mainid_sub');
		$name = $this->input->post('subpage_name');

		$dataarr = array(
			"name" => $name,
			"page_id" => $mainid,
			"create_datetime" => DATE("Y-m-d H:i:s")
		);

		$dataarr2 = array(
			"page_id" => $mainid,
		);

		if($mainid!=NULL&&$mainid!="")
		{
			$updated = $this->update_model->updated_data($dataarr,"subpage",NULL,NULL);
			$subpage_id = $this->db->insert_id();
			$dataarr2 = array_merge($dataarr2, array( "subpage_id" => $subpage_id) );
			$updated = $this->update_model->updated_data($dataarr2,"subpage_en",NULL,NULL);
			$updated = $this->update_model->updated_data($dataarr2,"subpage_cn",NULL,NULL);
			echo $updated;
		}
		else
		{
			echo "กรุณาตรวจสอบ page หลักก่อน";
		}

	}

	function deletefile()
	{
		$iddel = $this->input->get("iddel");
		$type = $this->input->get("type");

		$pathdata = $this->select_model->select_where("*",$type,"id = '".$iddel."'");
		if($pathdata->num_rows())
		{
			$pathdata = $pathdata->row_array();
		}
		else
		{
			if($type="picture")
			{
				$pathdata = $this->emptypicture();
			}
			elseif($type="slide")
			{
				$pathdata = $this->emptyslide();
			}
			else
			{
				$pathdata = $this->emptyfile();
			}
		}

		if (file_exists(FCPATH."assets/upload/".$type."/".$pathdata['path'])) {
				unlink(FCPATH."assets/upload/".$type."/".$pathdata['path']);
			}

		$delmodreturn = $this->delete_model->delete_data($type,"id = '".$iddel."'");
		if($delmodreturn==true)
		{
			echo "Delete Complete";
		}
		else
		{
			echo "Delete Error";
		}
	}

	function editfile()
	{
		$idedt = $this->input->get("idedt");
		$type = $this->input->get("type");

		$pathdata = $this->select_model->select_where("*",$type,"id = '".$idedt."'");
		if($pathdata->num_rows())
		{
			$pathdata = $pathdata->row_array();
		}
		else
		{
			if($type="picture")
			{
				$pathdata = $this->emptypicture();
			}
			elseif($type="slide")
			{
				$pathdata = $this->emptyslide();
			}
			else
			{
				$pathdata = $this->emptyfile();
			}
		}

		print_r(json_encode($pathdata));

	}

	function getfilecontent()
	{
		$pageid = $this->input->get('pageid');
		$subid = $this->input->get('subid');
		$type = $this->input->get('type');
		$retdata = NULL;

		$arrsearch = array("page_id = '".$pageid."'");
		if($subid!=NULL&&$subid!=""&&$subid!="0")
		{
			$arrsearch = array_merge($arrsearch, array( "sub_page_id = '".$subid."'") );
		}
		else
		{
			$arrsearch = array_merge($arrsearch, array( "sub_page_id = ".NULL) );
		}

		$alldata = $this->select_model->selectwhereorder("*",$type,$arrsearch,$type."_order","asc");
		if($alldata->num_rows())
		{
			$alldata_row = $alldata->num_rows();
			$alldata = $alldata->result_array();

			$retdata .= '<center><h4>Review '.ucfirst($type).'</h4></center>';
			$retdata .= '<center><a href="javascript:void(0);" class="btn outlined mleft_no reorder_link'.$type.'" id="save_reorder'.$type.'">Reorder '.$type.'</a></center>';
			$retdata .= '<div id="reorder-helper'.$type.'" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click "Save Reordering" when finished.</div>';
			$retdata .= '<div class="gallery">';
			$retdata .= '<ul class="reorder_ul reorder-'.$type.'-list">';
			foreach ($alldata as $keyp => $valuep)
			{

				if($type=="picture")
				{
					$prefix = "pic";
					$prefixs = "p";
					$pof = "'".base_url()."assets/upload/".$type."/".$valuep['path']."' alt ='".$valuep['name']."'";
				}
				else if($type=="slide")
				{
					$prefix = "slide";
					$prefixs = "s";
					$pof = "'".base_url()."assets/upload/".$type."/".$valuep['path']."' alt ='".$valuep['name']."'";
				}
				else
				{
					$prefix = "file";
					$prefixs = "f";
					$pof = "'".base_url()."assets/images/nopic.jpg' alt ='".$valuep['name']."'";
				}

				$retdata .= '<li id="'.$type.'_li_'.$valuep['id'].'" class="ui-sortable-handle">';
				$retdata .= '<a href="javascript:void(0);" style="float:none;" class="image_link'.$type.'"><img src='.$pof.'></a>';
				$retdata .= '<center><h4>'.$valuep['name'].'</h4></center>';
				$retdata .= '<center><a class="edt'.$prefix.'" style="cursor:pointer" id="'.$prefixs.'edt_'.$valuep['id'].'" >Edit</a>';
				$retdata .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				$retdata .= '<a class="delthis'.$prefix.'" style="cursor:pointer" id="'.$prefixs.'del_'.$valuep['id'].'" >Delete</a></center>';
				$retdata .= '</li>';
			}
			$retdata .= '</ul>';
			$retdata .= '</div>';
			$retdata .= '<script src="'.base_url().'assets/js/cms/defaultrefreshdel'.$type.'.js"></script>';
		}
		else
		{
			$alldata_row = 0;
			$alldata = NULL;
			$retdata = NULL;
		}

		$data['alldata_row'] = $alldata_row;
		//$data['alldata'] = $alldata;
		$data['retdata'] = $retdata;

		print_r(json_encode($data));
	}

	function reorderbytype()
	{

		$type = $this->input->post('type');
		$orderiddata = $this->input->post('order');
		$mainid = $this->input->post('mainid');
		$subid = $this->input->post('subid');

		$settyprordername = $type."_order";

		if(($subid==""||$subid==NULL||$subid=="0"))
		{
			$page = $this->select_model->select_where("*","page","id = '".$mainid."'");
			if($page->num_rows())
			{
				$page = $page->row_array();
				$page_id = $page['id'];

				foreach ($orderiddata as $key => $value) {
					$dataarr = array(
											$settyprordername => $key+1,
											"page_id" => $page_id
										);
					$updated = $this->update_model->updated_data($dataarr,$type,$value,"id");
				}

				echo "complete";
			}
			else
			{
				echo "dont have page to update";
				exit;
			}
		}
		else
		{
			$page = $this->select_model->select_where("*","subpage",array("page_id = '".$mainid."'","id = '".$subid."'"));
			if($page->num_rows())
			{
				$page = $page->row_array();
				$page_id = $page['page_id'];
				$sub_page_id = $page['id'];

				foreach ($orderiddata as $key => $value) {
					$dataarr = array(
											$settyprordername => $key+1,
											"page_id" => $page_id,
											"sub_page_id" => $sub_page_id,
										);
					$updated = $this->update_model->updated_data($dataarr,$type,$value,"id");
				}

				echo "complete";
			}
			else
			{
				echo "dont have page to update";
				exit;
			}
		}

	}

  private function emptypage()
  {
    return array(
      "id" => NULL,
      "name" => NULL,
      "description" => NULL,
      "link" => NULL,
      "create_datetime" => NULL,
      "type" => NULL
    );
  }

  private function emptypageen()
  {
    return array(
      "id" => NULL,
      "name_en" => NULL,
      "description_en" => NULL,
      "link_en" => NULL,
      "page_id" => NULL
    );
  }

  private function emptypagecn()
  {
    return array(
      "id" => NULL,
      "name_cn" => NULL,
      "description_cn" => NULL,
      "link_cn" => NULL,
      "page_id" => NULL
    );
  }

  private function emptypicture()
  {
    return array(
      "id" => NULL,
      "name" => NULL,
      "path" => NULL,
      "picture_order" => NULL,
      "page_id" => NULL
    );
  }

  private function emptyslide()
  {
    return array(
      "id" => NULL,
      "name" => NULL,
      "path" => NULL,
      "slide_order" => NULL,
      "page_id" => NULL
    );
  }

	private function emptyfile()
	{
		return array(
			"id" => NULL,
			"name" => NULL,
			"path" => NULL,
			"file_order" => NULL,
			"page_id" => NULL
		);
	}

	private function emptysubpage()
	{
		return array(
			"id" => NULL,
			"name" => NULL,
			"description" => NULL,
			"link" => NULL,
			"create_datetime" => NULL,
			"update_datetime" => NULL,
			"page_id" => NULL,
			"withdraw" => NULL
		);
	}

	private function emptysubpage_cn()
	{
		return array(
			"id" => NULL,
			"name_cn" => NULL,
			"description_cn" => NULL,
			"link_cn" => NULL,
			"update_datetime" => NULL,
			"page_id" => NULL,
			"subpage_id" => NULL
		);
	}

	private function emptysubpage_en()
	{
		return array(
			"id" => NULL,
			"name_en" => NULL,
			"description_en" => NULL,
			"link_en" => NULL,
			"update_datetime" => NULL,
			"page_id" => NULL,
			"subpage_id" => NULL
		);
	}

	private function upload_multiple_files($ldestpath, $title, $files, $type, $setname)
	{
		$config['upload_path'] = "./assets/upload/".$ldestpath."/";

		if($type=="picture"||$type=="slide")
		{
				$config['allowed_types'] = 'jpg|png|gif';
		}
		elseif($type=="pdf")
		{
				$config['allowed_types'] = 'pdf';
		}
		elseif($type=="other"||$type=="unitymodel"||$type=="file")
		{
				$config['allowed_types'] = '*';
		}

		$config['overwrite'] = true;

		$this->load->library('upload', $config);

		$images = array();

		foreach ($files['name'] as $key => $image)
		{
			if($files['size'][$key]>0)
			{
				if($image!=NULL&&$image!="")
				{
						$_FILES['images[]']['name']= $files['name'][$key];
						$_FILES['images[]']['type']= $files['type'][$key];
						$_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images[]']['error']= $files['error'][$key];
						$_FILES['images[]']['size']= $files['size'][$key];

						$ext = pathinfo($image, PATHINFO_EXTENSION);

						$fileName = $title .'_'. DATE("Ymdhis").'_'.$key.".".$ext;

					/*	if($setname==NULL||$setname=="")
						{
								$showname = explode('.', $fileName);
						}
						else
						{
								$showname = explode('.', $setname[$key]);
						}

						$showname[0] = str_replace(' ', '_', $showname[0]);

						$showname[0] = str_replace('.', '_', $showname[0]);

						if($type=="unitymodel"){
							$fileName = $showname[0];
						}
						else {
							$fileName = $showname[0].'.'.$ext;
						}

						$images[] = $fileName;
						*/

						$config['file_name'] = $fileName;

						$this->upload->initialize($config);

						if (!$this->upload->do_upload('images[]')) {
								$error = array('error' => $this->upload->display_errors());

								foreach ($images as $key2 => $value2) {
										if (file_exists(FCPATH."assets/upload/".$ldestpath."/".$value2)) {
												unlink(FCPATH."assets/upload/".$ldestpath."/".$value2);
										}
								}
								return "error";
						} else {
								$datauploaded  = array('upload_data' => $this->upload->data());
								$savepath[] = $datauploaded['upload_data']['file_name'];
						}
				}
				else
				{
					$savepath[] = NULL;
				}
			}
			else {
				$savepath[] = NULL;
			}
		}

		return $savepath;
	}

	function fileupload($ldestpath,$newfilename,$filename,$typeoffile)
	{
		$ext = pathinfo($_FILES[$filename]['name'], PATHINFO_EXTENSION);
		$config['upload_path'] = "./assets/upload/".$ldestpath."/";
		if($typeoffile=="picture"||$typeoffile=="slide")
		{
				$config['allowed_types'] = 'jpg|png|gif';
		}
		elseif($typeoffile=="pdf")
		{
				$config['allowed_types'] = 'pdf';
		}
		elseif($typeoffile=="other"||$typeoffile=="unitymodel"||$typeoffile=="file")
		{
				$config['allowed_types'] = '*';
		}
		$config['max_size'] = '40960KB';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$newfilename = explode('.', $newfilename);
		$config['file_name'] = $newfilename[0].'.'.$ext;
		$config['overwrite'] = false;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload($filename))
		{
				$error = array('error' => $this->upload->display_errors());
				//print_r($error);
				return "error";
		}
		else
		{
				$datauploaded  = array('upload_data' => $this->upload->data());
				$savepath = $datauploaded['upload_data']['file_name'];

				return $savepath;
		}
	}
}
?>
