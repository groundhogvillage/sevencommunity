<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
  {
      parent::__construct();
      date_default_timezone_set('Asia/Bangkok');
      $this->load->model('select_model');
      $this->load->model('update_model');
      $this->load->model('login_model');
      $this->load->model('delete_model');

      $this->sitename = "Sevencommunity";

      if(!$this->session->userdata("Permission")||!$this->session->userdata("Username")||!$this->session->userdata("User_ID"))
      {
          redirect(base_url());
      }

  }

    function index()
    {
			if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin("index");
			}
			else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuagent("index",NULL,$this->session->userdata("Community"));
			}
			else
			{
				$datamenu['datamenu'] = NULL;
			}


      $this->load->view('admin_template/header');
      $this->load->view('admin_template/sidebar',$datamenu);
      $this->load->view('main/index');
      $this->load->view('admin_template/footer');
    }

		function userlist()
		{
			if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin("user");
			}
			else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuagent("user",NULL,$this->session->userdata("Community"));
			}
			else
			{
				$datamenu['datamenu'] = NULL;
			}

			$wherearr = array("User_ID != '".$this->session->userdata("User_ID")."'","User_Deleted = 0");

			$userdata = $this->select_model->select_where("*","user",$wherearr);
			if($userdata->num_rows())
			{
				/*$data['user_row'] = $userdata->num_rows();
				$data['user'] = $userdata->result_array();*/

				$data['user_row'] = $userdata->num_rows();
				$config['base_url'] = base_url().'main/userlist';
				if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
				$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
				$config['total_rows'] = $data['user_row'];
				$config['per_page'] = 20;
				$data['user'] = $this->select_model->selectwherejoinlimit("*","user",$wherearr, NULL, NULL,$config['per_page'],$this->uri->segment(3),"User_ID","desc");
				$data['user'] = $data['user']->result_array();

				$this->pagination->initialize($config);
			}
			else
			{
				$data['user_row'] = 0;
				$data['user'] = NULL;
			}
			$this->load->view('admin_template/header');
			$this->load->view('admin_template/sidebar',$datamenu);
			$this->load->view('main/userlist',$data);
			$this->load->view('admin_template/footer');
		}

		function edituser()
		{
			if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin("user");
			}
			else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuagent("user",NULL,$this->session->userdata("Community"));
			}
			else
			{
				$datamenu['datamenu'] = NULL;
			}

			$User_ID = $this->input->get('User_ID');

			$data['userdata'] = $this->select_model->select_where("*","user","User_ID = '".$User_ID."'");
			if($data['userdata']->num_rows())
			{
				$data['userdata'] = $data['userdata']->row_array();
			}
			else
			{
				$data['userdata'] = $this->setemptyuser();
			}

			$this->load->view('admin_template/header');
      $this->load->view('admin_template/sidebar',$datamenu);
      $this->load->view('main/edituser',$data);
      $this->load->view('admin_template/footer');
		}

		function edituserdata()
		{
				$User_ID = $this->input->post("User_ID");
				$Username = $this->input->post('Username');
				$Password = $this->input->post('Password');
				$User_Firstname = $this->input->post('User_Firstname');
				$User_Lastname = $this->input->post('User_Lastname');
				$User_Phone_Number = $this->input->post('User_Phone_Number');
				$User_Email = $this->input->post('User_Email');
			  $Permission = $this->input->post('Permission');
				$Community = $this->input->post('Community');
				$oldpic = $this->input->post('oldpic');

				/*$User_Birth_Date = str_replace("/","-",$User_Birth_Date);
				$date = explode("-",$User_Birth_Date);
				$User_Birth_Date = $date[2].'-'.$date[1].'-'.$date[0];*/

				if($_FILES['User_Display_Picture']['size']!=0&&($_FILES['User_Display_Picture']['name']!=NULL||$_FILES['User_Display_Picture']['name']!=''))
				{
						$statusupload = $this->user_pictureupload();
				}
				else
				{
						$statusupload = "noupload";
				}

				if($statusupload!="error")
				{
						if($User_ID==NULL||$User_ID=="")
						{
								$wherearr = array('Username = "'.$Username.'"');
						}
						else
						{
								$wherearr = array('Username = "'.$Username.'"', 'User_ID != "'.$User_ID.'"');
						}

						$dupuser = $this->select_model->select_where("*","user",$wherearr);

						if($User_ID==NULL||$User_ID=="")
						{
								$wherearr = array('User_Email = "'.$User_Email.'"');
						}
						else
						{
								$wherearr = array('User_Email = "'.$User_Email.'"','User_ID != "'.$User_ID.'"');
						}

						$dupemail = $this->select_model->select_where("*","user",$wherearr);

						if($dupemail->num_rows())
						{
										if($statusupload!="noupload")
										{
												if (file_exists(FCPATH."assets/upload/user_picture/".$statusupload)) {
														unlink(FCPATH."assets/upload/user_picture/".$statusupload);
												}
										}

										echo "มี E-mail นี้อยู่ในระบบอยู่แล้ว";
										exit;
						}
						else if($dupuser->num_rows())
						{
										if($statusupload!="noupload")
										{
												if (file_exists(FCPATH."assets/upload/user_picture/".$statusupload)) {
														unlink(FCPATH."assets/upload/user_picture/".$statusupload);
												}
										}

										echo "มี Username นี้อยู่ในระบบอยู่แล้ว";
										exit;
						}
						else
						{

										$dataarr = array(
												"Username" => strtolower($Username),
												"User_Firstname" => $User_Firstname,
												"User_Lastname" => $User_Lastname,
												"User_Phone_Number" => $User_Phone_Number,
											  "Permission" => $Permission,
												"Community" => $Community,
												"User_Email" => $User_Email
										);

										if($Password!=NULL&&$Password!="")
										{
											$key='*sevencommunity*_';
											$password = md5($Password.$key);
											$dataarr = array_merge($dataarr, array( "Password" => $password) );
										}

										if($statusupload!="noupload")
										{
												$dataarr = array_merge($dataarr, array( "User_Display_Picture" =>  $statusupload) );
										}

										if($User_ID==NULL||$User_ID=="")
										{
												$updated = $this->update_model->addedituser($dataarr, NULL);
										}
										else
										{
												$updated = $this->update_model->addedituser($dataarr, $User_ID);
										}


										if($updated == "พบข้อผิดพลาด กรุณาตรวจสอบ")
										{
												if($statusupload!="noupload")
												{
														if (file_exists(FCPATH."assets/upload/user_picture/".$statusupload)) {
																unlink(FCPATH."assets/upload/user_picture/".$statusupload);
														}
												}
										}
										else
										{
												if($oldpic!=NULL&&$oldpic!=""&&$statusupload!="noupload")
												{
														if (file_exists(FCPATH."assets/upload/user_picture/".$oldpic)) {
																unlink(FCPATH."assets/upload/user_picture/".$oldpic);
														}
												}
										}


										echo $updated;

						}
				}
				else
				{
						echo "กรุณาตรวจสอบขนาดไฟล์(สูงสุด 2 MB, 1024x768) และ นามสกุลไฟล์(.jpg, .jpeg, .png)";
				}
		}

		function editinfo()
		{
			if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin("user");
			}
			else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuagent("user",NULL,$this->session->userdata("Community"));
			}
			else
			{
				$datamenu['datamenu'] = NULL;
			}

			$data['userdata'] = $this->select_model->select_where("*","user","User_ID = '".$this->session->userdata['User_ID']."'");
			if($data['userdata']->num_rows())
			{
			    $data['userdata'] = $data['userdata']->row_array();

					$this->load->view('admin_template/header');
					$this->load->view('admin_template/sidebar',$datamenu);
					$this->load->view('main/editinfo',$data);
					$this->load->view('admin_template/footer');

			}
			else
			{
			    redirect(base_url());
			}
    }

    function changepassword()
    {
        $oldpassword = $this->input->post('oldpass');
        $newpassword = $this->input->post('reinputPassword');

        $checkpass = $this->select_model->select_where("*","user","User_ID = '".$this->session->userdata('User_ID')."'");

        if($checkpass->num_rows())
        {
            $checkpass = $checkpass->row_array();

            $key='*sevencommunity*_';
            $mdpass = md5($oldpassword.$key);

            if($mdpass==$checkpass['Password'])
            {
                $key='*sevencommunity*_';
                $password = md5($newpassword.$key);
                $dataarr = array(
                    "Password" => $password
                );
                if($this->session->userdata('User_ID')==NULL||$this->session->userdata('User_ID')=="")
                {
                     //redirect(base_url());
										 echo "No User ID";
                }
                else
                {
                    $updated = $this->update_model->addedituser($dataarr, $this->session->userdata('User_ID'));
                    echo $updated;
                }
            }
            else
            {
                echo "Please Check your old password";
            }
        }
        else
        {
             echo "No User ID";
        }
    }

    function editinfodata()
    {
        $User_ID = $this->session->userdata("User_ID");
        $Username = $this->input->post('Username');
        $User_Firstname = $this->input->post('User_Firstname');
        $User_Lastname = $this->input->post('User_Lastname');
        $User_Phone_Number = $this->input->post('User_Phone_Number');
        $User_Email = $this->input->post('User_Email');
      //  $Permission = $this->input->post('Permission');
        $oldpic = $this->input->post('oldpic');

        /*$User_Birth_Date = str_replace("/","-",$User_Birth_Date);
        $date = explode("-",$User_Birth_Date);
        $User_Birth_Date = $date[2].'-'.$date[1].'-'.$date[0];*/

        if($_FILES['User_Display_Picture']['size']!=0&&($_FILES['User_Display_Picture']['name']!=NULL||$_FILES['User_Display_Picture']['name']!=''))
        {
            $statusupload = $this->user_pictureupload();
        }
        else
        {
            $statusupload = "noupload";
        }

        if($statusupload!="error")
        {
            if($User_ID==NULL||$User_ID=="")
            {
                $wherearr = array('User_Email = "'.$User_Email.'"','Username = "'.$Username.'"');
            }
            else
            {
                $wherearr = array('User_Email = "'.$User_Email.'"', 'Username = "'.$Username.'"', 'User_ID != "'.$User_ID.'"');
            }

            $dupuser = $this->select_model->select_where("*","user",$wherearr);
            if($dupuser->num_rows())
            {
                    if($statusupload!="noupload")
                    {
                        if (file_exists(FCPATH."assets/upload/user_picture/".$statusupload)) {
                            unlink(FCPATH."assets/upload/user_picture/".$statusupload);
                        }
                    }

                    echo "มี E-mail นี้อยู่ในระบบอยู่แล้ว";
                    exit;
            }
            else
            {
                    /*if($Permission==""||$Permission==NULL)
                    {
                        $Permission = "user";
                    }*/

                    $dataarr = array(
                        "Username" => strtolower($Username),
                        "User_Firstname" => $User_Firstname,
                        "User_Lastname" => $User_Lastname,
                        "User_Phone_Number" => $User_Phone_Number,
                      //  "Permission" => $Permission,
                        "User_Email" => $User_Email
                    );

                    if($statusupload!="noupload")
                    {
                        $dataarr = array_merge($dataarr, array( "User_Display_Picture" =>  $statusupload) );
                    }

                    if($User_ID==NULL||$User_ID=="")
                    {
                        redirect(base_url());
                    }
                    else
                    {
                        $updated = $this->update_model->addedituser($dataarr, $User_ID);
                    }


                    if($updated == "พบข้อผิดพลาด กรุณาตรวจสอบ")
                    {
                        if($statusupload!="noupload")
                        {
                            if (file_exists(FCPATH."assets/upload/user_picture/".$statusupload)) {
                                unlink(FCPATH."assets/upload/user_picture/".$statusupload);
                            }
                        }
                    }
                    else
                    {
                        if($oldpic!=NULL&&$oldpic!=""&&$statusupload!="noupload")
                        {
                            if (file_exists(FCPATH."assets/upload/user_picture/".$oldpic)) {
                                unlink(FCPATH."assets/upload/user_picture/".$oldpic);
                            }
                        }
                    }


                    echo $updated;

            }
        }
        else
        {
            echo "กรุณาตรวจสอบขนาดไฟล์(สูงสุด 2 MB, 1024x768) และ นามสกุลไฟล์(.jpg, .jpeg, .png)";
        }
    }

    function user_pictureupload()
    {
        $newspic = str_replace(" ","_",$_FILES['User_Display_Picture']['name']);
        $config['upload_path'] = "./assets/upload/user_picture/";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048KB';
        $config['max_width']  = '0';
        $config['max_height']  = '0';
        $config['file_name'] = $newspic;
        $config['overwrite'] = false;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('User_Display_Picture'))
        {
            $error = array('error' => $this->upload->display_errors());
            return "error";
        }
        else
        {
            $datauploaded  = array('upload_data' => $this->upload->data());
            $savepath = $datauploaded['upload_data']['file_name'];

            if($datauploaded['upload_data']['image_width'] > $datauploaded['upload_data']['image_height'])
            {
                $config['width'] = 1024;
                $config['height'] = 768;
            }
            elseif($datauploaded['upload_data']['image_width'] < $datauploaded['upload_data']['image_height'])
            {
                $config['width'] = 768;
                $config['height'] = 1024;
            }
            else
            {
                $config['width'] = 1024;
                $config['height'] = 1024;
            }

            $config['image_library'] = 'gd2';
            $config['source_image'] =  "./assets/upload/user_picture/".$savepath;
            $config['maintain_ratio'] = TRUE;
            $config['overwrite'] = true;

            $this->load->library('image_lib', $config);

            $this->image_lib->resize();

            return $savepath;
        }
    }

		private function setemptyuser()
		{
			return array(
				"User_ID" => NULL,
				"User_Email" => NULL,
				"Username" => NULL,
				"Password" => NULL,
				"User_Firstname" => NULL,
				"User_Lastname" => NULL,
				"Permission" => NULL,
				"User_Phone_Number" => NULL,
				"User_Display_Picture" => NULL,
				"Community" => NULL
			);
		}

		function getpagebytype($type)
		{
			$retdata['page'] = $this->select_model->select_where("*","page",array("type = '".$type."'","withdraw = 0"));
			if($retdata['page']->num_rows())
			{
				$retdata['page_row'] = $retdata['page']->num_rows();
				$retdata['page'] = $retdata['page']->row_array();
			}
			else
			{
				$retdata['page_row'] = 0;
				$retdata['page'] = $this->emptypage();
				$retdata['page']['type'] = $type;
			}
			return $retdata;
		}

		function getallsubpage($id)
		{
			$getdata = $this->select_model->select_where("*","subpage",array("page_id = '".$id."'","withdraw = 0"));
			if($getdata->num_rows())
			{
				$setdata['subpage_row'] = $getdata->num_rows();
				$setdata['subpage'] = $getdata->result_array();
				return $setdata;
			}
			else
			{
				$setdata['subpage_row'] = 0;
				$setdata['subpage'] = $this->emptysubpage();
				return $setdata;
			}
		}

		private function emptypage()
		{
			return array(
				"id" => NULL,
				"name" => NULL,
				"description" => NULL,
				"link" => NULL,
				"create_datetime" => NULL,
				"type" => NULL
			);
		}

		private function emptysubpage()
		{
			return array(
				"id" => NULL,
				"name" => NULL,
				"description" => NULL,
				"link" => NULL,
				"create_datetime" => NULL,
				"update_datetime" => NULL,
				"page_id" => NULL,
				"withdraw" => NULL
			);
		}

		public function updateview()
		{
			$id = $this->input->get("itemid");

			/*$getoldvisited = $this->select_model->select_where("*","item","id = '".$id."'");
			if($getoldvisited->num_rows())
			{
				$getoldvisited = $getoldvisited->row_array();
				$oldvisited = $getoldvisited['visited'];
			}

			$dataarr = array(
					"visited" => intval($oldvisited)+1
			);

			if($id!=NULL&&$id!=""&&$id!="0")
			{
				$updated = $this->update_model->updated_data($dataarr,"item",$id,"id");
			}*/

			$dataarr = array(
					"view_datetime" => DATE("Y-m-d H:i:s"),
					"item_id" => $id
			);

			if($id!=NULL&&$id!=""&&$id!="0")
			{
				$updated = $this->update_model->updated_data($dataarr,"item_view",NULL,NULL);
			}
			else {
				$updated = "No Update";
			}

			$getallview = $this->select_model->select_where("*","item_view","item_id = '".$id."'");
			if($getallview->num_rows())
			{
				$allview = $getallview->num_rows();
			}

			$dataarr = array(
					"visited" => $allview
			);

			if($id!=NULL&&$id!=""&&$id!="0")
			{
				$updated2 = $this->update_model->updated_data($dataarr,"item",$id,"id");
			}

			echo $updated;

		}

		public function deleteuser()
		{
			if($this->session->userdata("Permission")=="creator"||$this->session->userdata("Permission")=="admin")
			{
				$User_ID = $this->input->get("User_ID");

				$dataarr = array(
						"User_Deleted" => 1
				);

				if($User_ID!=NULL&&$User_ID!=""&&$User_ID!="0")
				{
					$updated = $this->update_model->updated_data($dataarr,"user",$User_ID,"User_ID");
				}
				redirect(base_url()."main/userlist");
			}
			else
			{
				echo "Don't have permission";
				echo "<a href='".base_url()."'>back</a>";
				exit;
			}

		}
}
?>
