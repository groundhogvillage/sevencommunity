<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Community extends CI_Controller {

	public function __construct()
  {
      parent::__construct();
      date_default_timezone_set('Asia/Bangkok');
      $this->load->model('select_model');
      $this->load->model('update_model');
      $this->load->model('login_model');
      $this->load->model('delete_model');

      $this->sitename = "Sevencommunity";
			$this->attruse = array('name' => true,'description' => true,'link' => true,'picture' => true,'slide' => true,'file' => true,'subpage' => true);

      if(!$this->session->userdata("Permission")||!$this->session->userdata("Username")||!$this->session->userdata("User_ID"))
      {
          redirect(base_url());
      }
  }

  function itemcommlist()
  {
    $onactivemenu = $this->input->get("type");
    $onactivesubmenu = NULL;

    $page = $this->getpagebytype($onactivemenu);
    $data['page_row'] = $page['page_row'];
    $data['page'] = $page['page'];

    if($page['page_row']>0)
    {
      $data['page_id'] = $data['page']['id'];
      $data['type'] = $onactivemenu;
			if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,"itemadd");
			}
			else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,"itemadd",$this->session->userdata("Community"));
			}
			else
			{
				$datamenu['datamenu'] = NULL;
			}


      $wherearr = array("withdraw = 0", "page_id = '".$data['page']['id']."'");

      $list = $this->select_model->select_where("*","item",$wherearr);

      if($list->num_rows())
      {
        $data['list_row'] = $list->num_rows();
        $config['base_url'] = base_url().'community/itemcommlist';
        if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        $config['total_rows'] = $data['list_row'];
        $config['per_page'] = 20;
        $data['list'] = $this->select_model->selectwherejoinlimit("*","item",$wherearr, NULL, NULL,$config['per_page'],$this->uri->segment(3),"id","desc");
        $data['list'] = $data['list']->result_array();

				foreach ($data['list'] as $keyl => $valuel) {
					$wherearrx = array("item_id = '".$valuel['id']."'");
					$getlastvisited = $this->select_model->selectwhereorder("*","item_view",$wherearrx,"id","desc");
					if($getlastvisited->num_rows())
					{
						$data['list'][$keyl]["visited"] = $getlastvisited->num_rows();
						$getlv = $getlastvisited->row_array();
						$data['list'][$keyl]["lastview"] = $getlv['view_datetime'];
					}
					else
					{
						$data['list'][$keyl]["visited"] = 0;
						$data['list'][$keyl]["lastview"] = "-";
					}
				}

        $this->pagination->initialize($config);
      }
      else
      {
        $data['list_row'] = 0;
        $data['list'] = NULL;
      }
    }
    else
    {
      echo "<center>add main data first</center>";
      echo "<br/>";
      echo "<a href='".base_url()."cms/".$onactivemenu."'>back</a>";
    }

    $this->load->view('admin_template/header');
    $this->load->view('admin_template/sidebar',$datamenu);
    $this->load->view('community/itemcommlist',$data);
    $this->load->view('admin_template/footer');
  }

	function getviewdata()
	{
		$onactivemenu = $this->input->get("type");
    $onactivesubmenu = NULL;
    $id = $this->input->get("id");
		$mainid = $this->input->get("main");

		$page = $this->getpagebytype($onactivemenu);
		$data['page_row'] = $page['page_row'];
		$data['page'] = $page['page'];

		if($page['page_row']>0)
		{
			$data['page_id'] = $data['page']['id'];
			$data['type'] = $onactivemenu;
			if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,"itemadd");
			}
			else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,"itemadd",$this->session->userdata("Community"));
			}
			else
			{
				$datamenu['datamenu'] = NULL;
			}

			$item = $this->getitem($id);
			$data['item_row'] = $item['item_row'];
			$data['item'] = $item['item'];

			$wherearr = array("item_id = '".$id."'");

			$list = $this->select_model->select_where("*","item_view",$wherearr);

			if($list->num_rows())
			{
				$data['list_row'] = $list->num_rows();
				$data['list'] = $list->result_array();
			}
			else
			{
				$data['list_row'] = 0;
				$data['list'] = NULL;
			}
		}
		else
		{
			echo "<center>add main data first</center>";
			echo "<br/>";
			echo "<a href='".base_url()."cms/".$onactivemenu."'>back</a>";
		}

		$this->load->view('admin_template/header');
		$this->load->view('admin_template/sidebar',$datamenu);
		$this->load->view('community/getviewdata',$data);
		$this->load->view('admin_template/footer');
	}


  function additemcomm()
  {
    $onactivemenu = $this->input->get("type");
    $onactivesubmenu = NULL;
    $id = $this->input->get("id");
		$mainid = $this->input->get("main");

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,"itemadd");
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,"itemadd",$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$data['type'] = $onactivemenu;
		$data['pageid'] = $mainid;

    $item = $this->getitem($id);
    $data['item_row'] = $item['item_row'];
    $data['item'] = $item['item'];

		$item_en = $this->getitem_en($id);
		$data['item_en_row'] = $item_en['item_en_row'];
    $data['item_en'] = $item_en['item_en'];

		$item_picture = $this->getitem_picture($id);
		$data['item_picture_row'] = $item_picture['item_picture_row'];
    $data['item_picture'] = $item_picture['item_picture'];

    $this->load->view('admin_template/header');
    $this->load->view('admin_template/sidebar',$datamenu);
    $this->load->view('community/additemcomm',$data);
    $this->load->view('admin_template/footer');
  }

	function updateitemcomm()
	{
		$type = $this->input->post('type');
		$page_id = $this->input->post('page_id');

		$item_id = $this->input->post('item_id');
		$name = $this->input->post('name');
		$description = $this->input->post('description');
		//$short_description = $this->input->post('short_description');

		$item_id_en = $this->input->post('item_id_en');
		$name_en = $this->input->post('name_en');
		$description_en = $this->input->post('description_en');
		//$short_description_en = $this->input->post('short_description_en');

		$frequency_update = $this->input->post('frequency_update');
		$lat = $this->input->post('lat');
		$lon = $this->input->post('lon');

		$showhide = $this->input->post('showhide');

		$updatedata = array(
			"name" => $name,
			"description" => $description,
			//"short_description" => $short_description,
			"page_id" => $page_id,
			"lat" => $lat,
			"lon" => $lon,
			"update_datetime" => DATE("Y-m-d H:i:s"),
			"showhide" => $showhide
		);

		if($item_id!=NULL&&$item_id!=""&&$item_id!="0")
		{
			$frequency_update += 1;
			$updatedata = array_merge($updatedata, array( "frequency_update" =>  $frequency_update) );
			$updated = $this->update_model->updated_data($updatedata,"item",$item_id,"id");
		}
		else
		{
			$updatedata = array_merge($updatedata, array( "create_datetime" =>  DATE("Y-m-d H:i:s")) );
			$updated = $this->update_model->updated_data($updatedata,"item",NULL,NULL);
			$item_id = $this->db->insert_id();
		}

		$updatedataen = array(
			"name_en" => $name_en,
			"description_en" => $description_en,
			//"short_description_en" => $short_description_en,
			"item_id" => $item_id
		);

		if($item_id_en!=NULL&&$item_id_en!=""&&$item_id_en!="0")
		{
			$updated2 = $this->update_model->updated_data($updatedataen,"item_en",$item_id_en,"id");
		}
		else
		{
			$updated2 = $this->update_model->updated_data($updatedataen,"item_en",NULL,NULL);
			$item_id_en = $this->db->insert_id();
		}

		if($_FILES['fileToUpload']['tmp_name'][0])
		{
			$this->uploaditempicture($item_id,$_FILES);
			$getallitempicture = $this->select_model->selectwhereorder("*","item_picture","item_id = '".$item_id."'","id","desc");
			if($getallitempicture->num_rows())
			{
				$reorderpic = $getallitempicture->result_array();
				foreach ($reorderpic as $keyre => $valuere) {
					$updatedreorder = array(
						"picture_order" => $keyre
					);

					if($valuere['id']!=NULL&&$valuere['id']!=""&&$valuere['id']!="0")
					{
						$updated2 = $this->update_model->updated_data($updatedreorder,"item_picture",$valuere['id'],"id");
					}
				}
			}
		}

	 	echo $updated;
	}

	function deleteitemcomm()
	{
		$id = $this->input->get("id");
		$main = $this->input->get("main");
		$type = $this->input->get("type");

		$updatedata = array(
			"withdraw" => 1
		);

		$updated = $this->update_model->updated_data($updatedata,"item",$id,"id");

		redirect(base_url()."community/itemcommlist?main=".$main."&type=".$type);
	}

	function newscommlist()
  {
    $onactivemenu = $this->input->get("type");
    $onactivesubmenu = NULL;

    $page = $this->getpagebytype($onactivemenu);
    $data['page_row'] = $page['page_row'];
    $data['page'] = $page['page'];

    if($page['page_row']>0)
    {
      $data['page_id'] = $data['page']['id'];
      $data['type'] = $onactivemenu;
			if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,"newsadd");
			}
			else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
			{
				$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,"newsadd",$this->session->userdata("Community"));
			}
			else
			{
				$datamenu['datamenu'] = NULL;
			}


      $wherearr = array("withdraw = 0", "page_id = '".$data['page']['id']."'");

      $list = $this->select_model->select_where("*","news",$wherearr);

      if($list->num_rows())
      {
        $data['list_row'] = $list->num_rows();
        $config['base_url'] = base_url().'community/newscommlist';
        if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        $config['total_rows'] = $data['list_row'];
        $config['per_page'] = 20;
        $data['list'] = $this->select_model->selectwherejoinlimit("*","news",$wherearr, NULL, NULL,$config['per_page'],$this->uri->segment(3),"id","desc");
        $data['list'] = $data['list']->result_array();

        $this->pagination->initialize($config);
      }
      else
      {
        $data['list_row'] = 0;
        $data['list'] = NULL;
      }
    }
    else
    {
      echo "<center>add main data first</center>";
      echo "<br/>";
      echo "<a href='".base_url()."cms/".$onactivemenu."'>back</a>";
    }

    $this->load->view('admin_template/header');
    $this->load->view('admin_template/sidebar',$datamenu);
    $this->load->view('community/newscommlist',$data);
    $this->load->view('admin_template/footer');
  }

  function addnewscomm()
  {
    $onactivemenu = $this->input->get("type");
    $onactivesubmenu = NULL;
    $id = $this->input->get("id");
		$mainid = $this->input->get("main");

		if($this->session->userdata("Permission")=="admin"||$this->session->userdata("Permission")=="creator")
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuadmin($onactivemenu,"newsadd");
		}
		else if($this->session->userdata("Permission")=="agent"&&($this->session->userdata("Community")!=NULL&&$this->session->userdata("Community")!=""&&$this->session->userdata("Community")!="0"))
		{
			$datamenu['datamenu'] = $this->buildmenu->buildmenuagent($onactivemenu,"newsadd",$this->session->userdata("Community"));
		}
		else
		{
			$datamenu['datamenu'] = NULL;
		}

		$data['type'] = $onactivemenu;
		$data['pageid'] = $mainid;

    $news = $this->getnews($id);
    $data['news_row'] = $news['news_row'];
    $data['news'] = $news['news'];

		$news_en = $this->getnews_en($id);
		$data['news_en_row'] = $news_en['news_en_row'];
    $data['news_en'] = $news_en['news_en'];

		$news_picture = $this->getnews_picture($id);
		$data['news_picture_row'] = $news_picture['news_picture_row'];
    $data['news_picture'] = $news_picture['news_picture'];

    $this->load->view('admin_template/header');
    $this->load->view('admin_template/sidebar',$datamenu);
    $this->load->view('community/addnewscomm',$data);
    $this->load->view('admin_template/footer');
  }

	function updatenewscomm()
	{
		$type = $this->input->post('type');
		$page_id = $this->input->post('page_id');

		$news_id = $this->input->post('news_id');
		$name = $this->input->post('name');
		$description = $this->input->post('description');
		//$short_description = $this->input->post('short_description');

		$news_id_en = $this->input->post('news_id_en');
		$name_en = $this->input->post('name_en');
		$description_en = $this->input->post('description_en');
		//$short_description_en = $this->input->post('short_description_en');

		/*$frequency_update = $this->input->post('frequency_update');
		$lat = $this->input->post('lat');
		$lon = $this->input->post('lon');*/

		$showhide = $this->input->post('showhide');

		$updatedata = array(
			"name" => $name,
			"description" => $description,
			"page_id" => $page_id,
			"update_datetime" => DATE("Y-m-d H:i:s"),
			"showhide" => $showhide
		);

		if($news_id!=NULL&&$news_id!=""&&$news_id!="0")
		{
			$updated = $this->update_model->updated_data($updatedata,"news",$news_id,"id");
		}
		else
		{
			$updatedata = array_merge($updatedata, array( "create_datetime" =>  DATE("Y-m-d H:i:s")) );
			$updated = $this->update_model->updated_data($updatedata,"news",NULL,NULL);
			$news_id = $this->db->insert_id();
		}

		$updatedataen = array(
			"name_en" => $name_en,
			"description_en" => $description_en,
			//"short_description_en" => $short_description_en,
			"news_id" => $news_id
		);

		if($news_id_en!=NULL&&$news_id_en!=""&&$news_id_en!="0")
		{
			$updated2 = $this->update_model->updated_data($updatedataen,"news_en",$news_id_en,"id");
		}
		else
		{
			$updated2 = $this->update_model->updated_data($updatedataen,"news_en",NULL,NULL);
			$news_id_en = $this->db->insert_id();
		}

		if($_FILES['fileToUpload']['tmp_name'][0])
		{
			$this->uploadnewspicture($news_id,$_FILES);
			$getallnewspicture = $this->select_model->selectwhereorder("*","news_picture","news_id = '".$news_id."'","id","desc");
			if($getallnewspicture->num_rows())
			{
				$reorderpic = $getallnewspicture->result_array();
				foreach ($reorderpic as $keyre => $valuere) {
					$updatedreorder = array(
						"picture_order" => $keyre
					);

					if($valuere['id']!=NULL&&$valuere['id']!=""&&$valuere['id']!="0")
					{
						$updated2 = $this->update_model->updated_data($updatedreorder,"news_picture",$valuere['id'],"id");
					}
				}
			}
		}

	 	echo $updated;
	}

	function deletenewscomm()
	{
		$id = $this->input->get("id");
		$main = $this->input->get("main");
		$type = $this->input->get("type");

		$updatedata = array(
			"withdraw" => 1
		);

		$updated = $this->update_model->updated_data($updatedata,"news",$id,"id");

		redirect(base_url()."community/newscommlist?main=".$main."&type=".$type);
	}

	private function emptypage()
	{
		return array(
			"id" => NULL,
			"name" => NULL,
			"description" => NULL,
			"link" => NULL,
			"create_datetime" => NULL,
			"type" => NULL
		);
	}

  private function emptyitem()
  {
    return array(
      "id" => NULL,
      "name" => NULL,
      "description" => NULL,
      "short_description" => NULL,
      "lat" => NULL,
      "lon" => NULL,
      "create_datetime" => NULL,
      "update_datetime" => NULL,
      "visited" => NULL,
			"showhide" => NULL,
      "frequency_update" => NULL,
      "page_id" => NULL
    );
  }

  private function emptyitem_en()
  {
    return array(
      "id" => NULL,
      "name_en" => NULL,
      "short_description_en" => NULL,
      "description_en" => NULL,
      "item_id" => NULL
    );
  }

  private function emptyitem_picture()
  {
    return array(
      "id" => NULL,
      "name" => NULL,
      "path" => NULL,
      "picture_order" => NULL,
      "item_id" => NULL
    );
  }

	private function emptynews()
	{
		return array(
			"id" => NULL,
			"name" => NULL,
			"description" => NULL,
			"create_datetime" => NULL,
			"update_datetime" => NULL,
			"visited" => NULL,
			"showhide" => NULL,
			"page_id" => NULL
		);
	}

	private function emptynews_en()
	{
		return array(
			"id" => NULL,
			"name_en" => NULL,
			"description_en" => NULL,
			"update_datetime" => NULL,
			"news_id" => NULL
		);
	}

	private function emptynews_picture()
	{
		return array(
			"id" => NULL,
			"name" => NULL,
			"path" => NULL,
			"picture_order" => NULL,
			"news_id" => NULL
		);
	}

  function getpagebytype($type)
  {
    $retdata['page'] = $this->select_model->select_where("*","page",array("type = '".$type."'","withdraw = 0"));
    if($retdata['page']->num_rows())
    {
      $retdata['page_row'] = $retdata['page']->num_rows();
      $retdata['page'] = $retdata['page']->row_array();
    }
    else
    {
      $retdata['page_row'] = 0;
      $retdata['page'] = $this->emptypage();
      $retdata['page']['type'] = $type;
    }
    return $retdata;
  }

  function getitem($id)
  {
    $retdata['item'] = $this->select_model->select_where("*","item",array("id = '".$id."'","withdraw = 0"));
    if($retdata['item']->num_rows())
    {
      $retdata['item_row'] = $retdata['item']->num_rows();
      $retdata['item'] = $retdata['item']->row_array();
    }
    else
    {
      $retdata['item_row'] = 0;
      $retdata['item'] = $this->emptyitem();
    }
    return $retdata;
  }

	function getitem_en($id)
	{
		$retdata['item_en'] = $this->select_model->select_where("*","item_en",array("item_id = '".$id."'"));
		if($retdata['item_en']->num_rows())
		{
			$retdata['item_en_row'] = $retdata['item_en']->num_rows();
			$retdata['item_en'] = $retdata['item_en']->row_array();
		}
		else
		{
			$retdata['item_en_row'] = 0;
			$retdata['item_en'] = $this->emptyitem_en();
		}
		return $retdata;
	}

	function getitem_picture($id)
	{
		$retdata['item_picture'] = $this->select_model->selectwhereorder("*","item_picture",array("item_id = '".$id."'"),"picture_order","asc");
		if($retdata['item_picture']->num_rows())
		{
			$retdata['item_picture_row'] = $retdata['item_picture']->num_rows();
			$retdata['item_picture'] = $retdata['item_picture']->result_array();
		}
		else
		{
			$retdata['item_picture_row'] = 0;
			$retdata['item_picture'] = $this->emptyitem_picture();
		}
		return $retdata;
	}

	function getnews($id)
	{
		$retdata['news'] = $this->select_model->select_where("*","news",array("id = '".$id."'","withdraw = 0"));
		if($retdata['news']->num_rows())
		{
			$retdata['news_row'] = $retdata['news']->num_rows();
			$retdata['news'] = $retdata['news']->row_array();
		}
		else
		{
			$retdata['news_row'] = 0;
			$retdata['news'] = $this->emptynews();
		}
		return $retdata;
	}

	function getnews_en($id)
	{
		$retdata['news_en'] = $this->select_model->select_where("*","news_en",array("news_id = '".$id."'"));
		if($retdata['news_en']->num_rows())
		{
			$retdata['news_en_row'] = $retdata['news_en']->num_rows();
			$retdata['news_en'] = $retdata['news_en']->row_array();
		}
		else
		{
			$retdata['news_en_row'] = 0;
			$retdata['news_en'] = $this->emptynews_en();
		}
		return $retdata;
	}

	function getnews_picture($id)
	{
		$retdata['news_picture'] = $this->select_model->selectwhereorder("*","news_picture",array("news_id = '".$id."'"),"picture_order","asc");
		if($retdata['news_picture']->num_rows())
		{
			$retdata['news_picture_row'] = $retdata['news_picture']->num_rows();
			$retdata['news_picture'] = $retdata['news_picture']->result_array();
		}
		else
		{
			$retdata['news_picture_row'] = 0;
			$retdata['news_picture'] = $this->emptynews_picture();
		}
		return $retdata;
	}

	private function uploaditempicture($item_ID,$getallfile)
	{
		if($getallfile['fileToUpload']['tmp_name'][0])
		{
				// retrieve the number of images uploaded;
				$number_of_files = sizeof($getallfile['fileToUpload']['tmp_name']);
				// considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
				$files = $getallfile['fileToUpload'];
				$errors = array();

				// first make sure that there is no error in uploading the files
				for($i=0;$i<$number_of_files;$i++)
				{
					if($getallfile['fileToUpload']['error'][$i] != 0){ $errors[$i][] = 'Couldn\'t upload file '.$getallfile['fileToUpload']['name'][$i]; }
				}
				if(sizeof($errors)==0)
				{
					// now, taking into account that there can be more than one file, for each file we will have to do the upload
					// we first load the upload library
					$this->load->library('upload');
					// next we pass the upload path for the images
					$picname = "item_".Date("dmYHis");
					$config['upload_path'] = "./assets/upload/item/";
					$config['file_name'] = $picname;
					// also, we make sure we allow only certain type of images
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '4096KB';
					$config['max_width']  = '0';
					$config['max_height']  = '0';

				for ($i = 0; $i < $number_of_files; $i++) {
					if(($files['size'][$i]/1024)>4096)
					{
						echo "มีขนาดรูปที่ใหญ่กว่าที่เว็บไซต์กำหนด กรุณาตรวจสอบ (Maximun 4096KB)";
						//echo "\n<a href='".base_url()."/admin/store/item_list'>back</a>";
						exit;
					}
				}

					for ($i = 0; $i < $number_of_files; $i++) {
						$_FILES['uploadedimage']['name'] = str_replace(" ","_",$files['name'][$i]);
						$_FILES['uploadedimage']['type'] = $files['type'][$i];
						$_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
						$_FILES['uploadedimage']['error'] = $files['error'][$i];
						$_FILES['uploadedimage']['size'] = $files['size'][$i];
						//now we initialize the upload library
						$this->upload->initialize($config);
						// we retrieve the number of files that were uploaded
						if (!$this->upload->do_upload('uploadedimage'))
						{
							$error = $this->upload->display_errors();
							$error = strip_tags($error);
							print_r($error);
							exit;
						}
						else
						{

							$dataupload = $this->upload->data();

							$getrunningnum = $this->select_model->select_where("*", "item_picture", "item_id = '".$item_ID."'");

							if($getrunningnum->num_rows())
							{
								$runningnum_rows = $getrunningnum->num_rows();
								$getrunningnum = $getrunningnum->result_array();
								$needarrnum = $runningnum_rows - 1;
							}
							else
							{
								$runningnum_rows = 0;
							}

							$path[] = $dataupload['full_path'];
							$widthfn[] = $dataupload['image_width'];
							$heightfn[] =  $dataupload['image_height'];
							$newpicturename[] = $dataupload['file_name'];

							$picarr = array(
								"name" => $dataupload['file_name'],
								"path" => $dataupload['file_name'],
								"item_id" => $item_ID,
							);

							$uploadpicture = $this->update_model->updated_data($picarr,'item_picture',NULL,NULL);
						}

					}
				}
				else
				{
					$error = strip_tags($error);
					print_r($errors);
					exit;
				}
		}

	}

	private function uploadnewspicture($news_ID,$getallfile)
	{
		if($getallfile['fileToUpload']['tmp_name'][0])
		{
				// retrieve the number of images uploaded;
				$number_of_files = sizeof($getallfile['fileToUpload']['tmp_name']);
				// considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
				$files = $getallfile['fileToUpload'];
				$errors = array();

				// first make sure that there is no error in uploading the files
				for($i=0;$i<$number_of_files;$i++)
				{
					if($getallfile['fileToUpload']['error'][$i] != 0){ $errors[$i][] = 'Couldn\'t upload file '.$getallfile['fileToUpload']['name'][$i]; }
				}
				if(sizeof($errors)==0)
				{
					// now, taking into account that there can be more than one file, for each file we will have to do the upload
					// we first load the upload library
					$this->load->library('upload');
					// next we pass the upload path for the images
					$picname = "news_".Date("dmYHis");
					$config['upload_path'] = "./assets/upload/news/";
					$config['file_name'] = $picname;
					// also, we make sure we allow only certain type of images
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '4096KB';
					$config['max_width']  = '0';
					$config['max_height']  = '0';

				for ($i = 0; $i < $number_of_files; $i++) {
					if(($files['size'][$i]/1024)>4096)
					{
						echo "มีขนาดรูปที่ใหญ่กว่าที่เว็บไซต์กำหนด กรุณาตรวจสอบ (Maximun 4096KB)";
						//echo "\n<a href='".base_url()."/admin/store/news_list'>back</a>";
						exit;
					}
				}

					for ($i = 0; $i < $number_of_files; $i++) {
						$_FILES['uploadedimage']['name'] = str_replace(" ","_",$files['name'][$i]);
						$_FILES['uploadedimage']['type'] = $files['type'][$i];
						$_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
						$_FILES['uploadedimage']['error'] = $files['error'][$i];
						$_FILES['uploadedimage']['size'] = $files['size'][$i];
						//now we initialize the upload library
						$this->upload->initialize($config);
						// we retrieve the number of files that were uploaded
						if (!$this->upload->do_upload('uploadedimage'))
						{
							$error = $this->upload->display_errors();
							$error = strip_tags($error);
							print_r($error);
							exit;
						}
						else
						{

							$dataupload = $this->upload->data();

							$getrunningnum = $this->select_model->select_where("*", "news_picture", "news_id = '".$news_ID."'");

							if($getrunningnum->num_rows())
							{
								$runningnum_rows = $getrunningnum->num_rows();
								$getrunningnum = $getrunningnum->result_array();
								$needarrnum = $runningnum_rows - 1;
							}
							else
							{
								$runningnum_rows = 0;
							}

							$path[] = $dataupload['full_path'];
							$widthfn[] = $dataupload['image_width'];
							$heightfn[] =  $dataupload['image_height'];
							$newpicturename[] = $dataupload['file_name'];

							$picarr = array(
								"name" => $dataupload['file_name'],
								"path" => $dataupload['file_name'],
								"news_id" => $news_ID,
							);

							$uploadpicture = $this->update_model->updated_data($picarr,'news_picture',NULL,NULL);
						}

					}
				}
				else
				{
					$error = strip_tags($error);
					print_r($errors);
					exit;
				}
		}

	}

	public function reorderitempicture()
	{
		$type = $this->input->post('type');
		$orderiddata = $this->input->post('order');
		$item_id = $this->input->post('item_id');

		$settyprordername = "picture_order";

		$item = $this->select_model->select_where("*","item","id = '".$item_id."'");
		if($item->num_rows())
		{
			$item = $item->row_array();
			$item_id = $item['id'];

			foreach ($orderiddata as $key => $value) {
				$dataarr = array(
										$settyprordername => $key+1,
										"item_id" => $item_id
									);
				$updated = $this->update_model->updated_data($dataarr,"item_picture",$value,"id");
			}

			echo "complete";
		}
		else
		{
			echo "dont have page to update";
			exit;
		}

	}

	public function getitempictureorder()
	{
		$item_id = $this->input->get('item_id');
		$retdata = NULL;

		$arrsearch = array("item_id = '".$item_id."'");

		$alldata = $this->select_model->selectwhereorder("*","item_picture",$arrsearch,"picture_order","asc");
		if($alldata->num_rows())
		{
			$alldata_row = $alldata->num_rows();
			$alldata = $alldata->result_array();

			$retdata .= '<center><h4>Review Picture</h4></center>';
			$retdata .= '<center><a href="javascript:void(0);" class="btn outlined mleft_no reorder_linkpicture" id="save_reorderpicture">Reorder Picture</a></center>';
			$retdata .= '<div id="reorder-helperpicture" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click "Save Reordering" when finished.</div>';
			$retdata .= '<div class="gallery">';
			$retdata .= '<ul class="reorder_ul reorder-picture-list">';
			foreach ($alldata as $keyp => $valuep)
			{
				$prefix = "pic";
				$prefixs = "p";
				$pof = "'".base_url()."assets/upload/item/".$valuep['path']."' alt ='".$valuep['name']."'";
				$retdata .= '<li id="picture_li_'.$valuep['id'].'" class="ui-sortable-handle">';
				$retdata .= '<a href="javascript:void(0);" style="float:none;" class="image_linkpicture"><img src='.$pof.'></a>';
				$retdata .= '<center><h4>'.$valuep['name'].'</h4></center>';
				$retdata .= '<center><a class="delthis'.$prefix.'" style="cursor:pointer" id="'.$prefixs.'del_'.$valuep['id'].'" >Delete</a></center>';
				$retdata .= '</li>';
			}
			$retdata .= '</ul>';
			$retdata .= '</div>';
			$retdata .= '<script src="'.base_url().'assets/js/community/defaultrefreshdelpic.js"></script>';
		}
		else
		{
			$alldata_row = 0;
			$alldata = NULL;
			$retdata = NULL;
		}

		$data['alldata_row'] = $alldata_row;
		//$data['alldata'] = $alldata;
		$data['retdata'] = $retdata;

		print_r(json_encode($data));
	}

	public function delitempicture()
	{
		$iddel = $this->input->get("iddel");

		$pathdata = $this->select_model->select_where("*","item_picture","id = '".$iddel."'");
		if($pathdata->num_rows())
		{
			$pathdata = $pathdata->row_array();
		}
		else
		{
				$pathdata = $this->emptyitem_picture();
		}

		if (file_exists(FCPATH."assets/upload/item/".$pathdata['path'])) {
				unlink(FCPATH."assets/upload/item/".$pathdata['path']);
			}

		$delmodreturn = $this->delete_model->delete_data("item_picture","id = '".$iddel."'");
		if($delmodreturn==true)
		{
			echo "Delete Complete";
		}
		else
		{
			echo "Delete Error";
		}
	}

	public function reordernewspicture()
	{
		$type = $this->input->post('type');
		$orderiddata = $this->input->post('order');
		$news_id = $this->input->post('news_id');

		$settyprordername = "picture_order";

		$news = $this->select_model->select_where("*","news","id = '".$news_id."'");
		if($news->num_rows())
		{
			$news = $news->row_array();
			$news_id = $news['id'];

			foreach ($orderiddata as $key => $value) {
				$dataarr = array(
										$settyprordername => $key+1,
										"news_id" => $news_id
									);
				$updated = $this->update_model->updated_data($dataarr,"news_picture",$value,"id");
			}

			echo "complete";
		}
		else
		{
			echo "dont have page to update";
			exit;
		}

	}

	public function getnewspictureorder()
	{
		$news_id = $this->input->get('news_id');
		$retdata = NULL;

		$arrsearch = array("news_id = '".$news_id."'");

		$alldata = $this->select_model->selectwhereorder("*","news_picture",$arrsearch,"picture_order","asc");
		if($alldata->num_rows())
		{
			$alldata_row = $alldata->num_rows();
			$alldata = $alldata->result_array();

			$retdata .= '<center><h4>Review Picture</h4></center>';
			$retdata .= '<center><a href="javascript:void(0);" class="btn outlined mleft_no reorder_linkpicture" id="save_reorderpicture">Reorder Picture</a></center>';
			$retdata .= '<div id="reorder-helperpicture" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click "Save Reordering" when finished.</div>';
			$retdata .= '<div class="gallery">';
			$retdata .= '<ul class="reorder_ul reorder-picture-list">';
			foreach ($alldata as $keyp => $valuep)
			{
				$prefix = "pic";
				$prefixs = "p";
				$pof = "'".base_url()."assets/upload/news/".$valuep['path']."' alt ='".$valuep['name']."'";
				$retdata .= '<li id="picture_li_'.$valuep['id'].'" class="ui-sortable-handle">';
				$retdata .= '<a href="javascript:void(0);" style="float:none;" class="image_linkpicture"><img src='.$pof.'></a>';
				$retdata .= '<center><h4>'.$valuep['name'].'</h4></center>';
				$retdata .= '<center><a class="delthis'.$prefix.'" style="cursor:pointer" id="'.$prefixs.'del_'.$valuep['id'].'" >Delete</a></center>';
				$retdata .= '</li>';
			}
			$retdata .= '</ul>';
			$retdata .= '</div>';
			$retdata .= '<script src="'.base_url().'assets/js/community/defaultrefreshdelnewspic.js"></script>';
		}
		else
		{
			$alldata_row = 0;
			$alldata = NULL;
			$retdata = NULL;
		}

		$data['alldata_row'] = $alldata_row;
		//$data['alldata'] = $alldata;
		$data['retdata'] = $retdata;

		print_r(json_encode($data));
	}

	public function delnewspicture()
	{
		$iddel = $this->input->get("iddel");

		$pathdata = $this->select_model->select_where("*","news_picture","id = '".$iddel."'");
		if($pathdata->num_rows())
		{
			$pathdata = $pathdata->row_array();
		}
		else
		{
				$pathdata = $this->emptynews_picture();
		}

		if (file_exists(FCPATH."assets/upload/news/".$pathdata['path'])) {
				unlink(FCPATH."assets/upload/news/".$pathdata['path']);
			}

		$delmodreturn = $this->delete_model->delete_data("news_picture","id = '".$iddel."'");
		if($delmodreturn==true)
		{
			echo "Delete Complete";
		}
		else
		{
			echo "Delete Error";
		}
	}

}
?>
