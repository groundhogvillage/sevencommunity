
<link href="<?=base_url("assets/css/popupbox.css");?>" rel="stylesheet">

<main>
	<section>
		<div class="row" style="padding-top:20px;">
			<div class="col-md-12" style="text-align:center;">
				<h3>Edit Profile</h3>
				<h5> กรุณาใส่ข้อมูลใน <font color="red">*</font> ให้ครบถ้วน </h5>
				<?php if(isset($_GET['process'])&&$_GET['process']=="passchange"){?><font color="green">เปลี่ยน Password แล้ว</font><?php } ?>
				<?php if(isset($_GET['process'])&&$_GET['process']=="updated"){?><font color="green">แก้ไขข้อมูลแล้ว</font><?php } ?>
			</div>
		</div>
	</section>
	<br/>
		<?php if($userdata['User_Display_Picture']!=""&&$userdata['User_Display_Picture']!=NULL){?>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6"><img class="img-responsive" src="<?=base_url();?>assets/upload/user_picture/<?=$userdata['User_Display_Picture'];?>" ></div>
				<div class="col-sm-3"></div>
			</div>
		<?php } ?>
	<br/>
	<article>
	<form id="editinfo" enctype="multipart/form-data" method="post" action="<?=base_url();?>index/register_update_data" class="form-horizontal">
		<div class="form-group">
		    <label for="inputFirstname" class="col-sm-2 control-label"> <font color="red">*</font> First Name : </label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="inputFirstname" name="User_Firstname" value="<?=$userdata['User_Firstname'];?>" placeholder="Firstname" required>
		    </div>

		    <label for="inputLastname" class="col-sm-2 control-label"> <font color="red">*</font> Last Name : </label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="inputLastname" name="User_Lastname" value="<?=$userdata['User_Lastname'];?>" placeholder="Lastname" required>
		    </div>
		</div>

		<div class="form-group">
		    <label for="Username" class="col-sm-2 control-label"> <font color="red">*</font> Username : </label>
		    <div class="col-sm-6">
		      <input type="text" class="form-control" id="Username" name="Username" value="<?=$userdata['Username'];?>" placeholder="Username." required readonly>
		    </div>
			<div class="col-sm-4">
				 <center><a style="text-decoration: underline;" id="popupopen" href="#popup_a">Change Password / เปลี่ยนรหัสผ่าน</a></center>
			</div>
		</div>

		<div class="form-group">
		    <label for="inputPhone_Number" class="col-sm-2 control-label"> Phone No. : </label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="inputPhone_Number" name="User_Phone_Number" value="<?=$userdata['User_Phone_Number'];?>" placeholder="Phone Number">
		    </div>

		    <label for="inputEmail" class="col-sm-2 control-label"> <font color="red">*</font> Email : </label>
		    <div class="col-sm-4">
		      <input type="email" class="form-control" id="inputEmail" name="User_Email" placeholder="User Email" value="<?=$userdata['User_Email'];?>" required>
		    </div>
		</div>

		<div class="form-group">
		    <label for="inputDisplay_Picture" class="col-sm-2 control-label">Upload รูปภาพ (.jpg/.png) : </label>
		    <div class="col-sm-10">
		      <input type="file" class="form-control" style="border:none;"  id="inputDisplay_Picture" name="User_Display_Picture" >
		      <input type="hidden" id="oldpic" name="oldpic" value="<?=$userdata['User_Display_Picture'];?>">
		    </div>
		</div>

		<br/>

		<div class="row" style="text-align:center">
			<div class="col-md-4" style="margin-top:10px;">
				<a style="text-decoration:none;" href="<?=base_url();?>"><input type="button" value="Back" class="btn btn-danger btn-lg btn-block"></a>
			</div>
			<div class="col-md-4" style="margin-top:10px;">
				<input type="submit" value="Submit" id="submitbutton" class="btn btn-success btn-lg btn-block">
			</div>
			<div class="col-md-4" style="margin-top:10px;">
				<input type="button" onclick="resetform()" value="Clear all" class="btn btn-info btn-lg btn-block">
			</div>
		</div>
	</form>
	</article>
</main>

<br/>

<div id="popup_a" class="overlay">
	<div class="popup_a">
		<h4>Change Password</h4>
		<br/>
		<a class="close" id="xpopupbutton" href="#">×</a>
		<div class="content">
		<form id="changepass" method="post">
		<div class="row_on_popup_set" style="margin-top:20px;">
			<div class="title_on_popup_set"><label for="oldpass">Old Password / รหัสผ่านปัจจุบัน : </label></div>
			<input type="password" id="oldpass" class="input_on_popup_set" name="oldpass" value="" placeholder="Old Password" >
		</div>
		<div class="row_on_popup_set">
			<div class="title_on_popup_set"><label  for="newpass">New Password / รหัสผ่านใหม่ : </label></div>
			<input type="password" id="inputPassword" class="input_on_popup_set" name="inputPassword" value="" placeholder="New Password" >
		</div>
		<div class="row_on_popup_set">
			<div class="title_on_popup_set"><label for="retypepass">Retype Password / รหัสผ่านใหม่อีกครั้ง : </label></div>
			<input type="password" id="reinputPassword" class="input_on_popup_set" name="reinputPassword" value="" placeholder="Retype Password" >
		</div>
			<!--<div id="pswd_info"  style="margin-top:30px;">
			   	<h5>Password must meet the following requirements:</h5>
		        <div class="invalid" id="passletter">At least <strong>one letter</strong></div>
		        <div class="invalid" id="passlettercap">At least <strong>one capital letter</strong></div>
		        <div class="invalid" id="passnumber">At least <strong>one number</strong></div>
		        <div class="invalid" id="passchar" >Be at least <strong>6 characters</strong> and maximum <strong>14 characters</strong></div>
		        <div class="invalid" id="passmatch"><strong>Match Password</strong></div>
			</div>-->
		<br/>
		<div class="row_on_popup_set" style="text-align:center;">
		<a href="#" style="text-decoration:none; margin-right:30px;"><input type="Submit" id="submitpopupbutton" value="Save" class="btn btn-success btn-lg"></a>
		</form>
		<a href="#" id="cancelpopupbutton"  style="text-decoration:none; margin-left:30px;"><input type="button" value="Cancel" class="btn btn-danger btn-lg"></a>
		</div>
	</div>
</div>

<br/>

</div><!--End Container Fluid-->
<script type="text/javascript">
       var url = "<?=base_url('assets/js/editinfo.js');?>";
        $.getScript(url);
</script>
