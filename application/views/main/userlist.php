
<link href="<?=base_url("assets/css/popupbox.css");?>" rel="stylesheet">
<br/>
<br/>
<main>
	<section>
		<div class="row" style="padding-top:20px;">
			<div class="col-md-12" style="text-align:center;">
				<h3>User List</h3>
				<br/>
				<center>
					<a style="cursor:pointer;" href="<?=base_url();?>main/edituser"><button class="btn btn-primary">
						<span class="glyphicon glyphicon-plus"></span>Add New User</button>
					</a>
				</center>
				<br/>
			</div>
		</div>
	</section>
	<br/>
	<article>
		<?php if($user_row>0){ ?>
			<div class="table-responsive">
				<table id="" class="table table-striped">
					<thead class=" ">
						<tr class=" header1">
							<th>No.</th>
							<th>Email</th>
							<th>Username</th>
							<th>Name</th>
              <th>Permission</th>
              <th>Phone Number</th>
							<th>Edit / Withhold</th>
						</tr>
					</thead>
					<tbody class="">
            <?php
						$next = $this->uri->segment(3);
						foreach ($user as $key => $value) {
						?>
						<tr class="" id="">
							<td><?=$key+$next+1;?></td>
							<td><?=$value['User_Email'];?></td>
							<td><?=$value['Username'];?></td>
							<td><?=$value['User_Firstname']." ".$value['User_Lastname'];?></td>
              <td><?=$value['Permission'];?></td>
              <td><?=$value['User_Phone_Number'];?></td>
							<td><a href="<?=base_url();?>main/edituser?User_ID=<?=$value['User_ID'];?>">Edit</a> / <a class="confirmdelete" href="<?=base_url();?>main/deleteuser?User_ID=<?=$value['User_ID'];?>">Delete</a> </td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			</div>
		<?php } ?>
		<div id="showpagination"><?php echo $this->pagination->create_links(); ?></div>
	</article>
</main>

<br/>
<br/>
