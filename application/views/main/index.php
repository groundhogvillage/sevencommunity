
<div class="row">
  <div class="col-md-12">
    <!--<div class="content-box-header">
      <div class="panel-title">Welcome to Seven Community Website</div>
    </div>-->
    <div class="content-box-large box-with-header">
        <h3>ยินดีต้อนรับ ผู้ประสานงานหรือตัวแทนชุมชน</h3><br>
        <p style="font-size:18px;">ตัวแทนชุมชน สามารถเข้าใช้งานได้ 3 เมนูที่อยู่ทางด้านซ้าย<br>
        เมนูที่ 1 "เพิ่มสถานที่" สําหรับการเพิ่ม/แก้ไขข้อมูล รายการสถานที่หรือผลิตภัณฑ์ในชุมชน <br>
        เมนูที่ 2 "เพิ่มข่าวสาร" สําหรับการเพิ่ม/แก้ไขข้อมูล รายการข่าวสารในชุมชน<br>
        เมนูที่ 3 "ตัวแทนชุมชน" สําหรับ เพิ่ม/แก้ไข ชื่อผู้ประสานงานของชุมชนเพิ่มเติม<br>
        ต้องการอ่านรายละเอียดการใช้งาน <a href="<?=base_url();?>assets/pdf/new-manual.pdf">คลิก</a></p>
    <br /><br />
  </div>
  </div>
</div>
