
<link href="<?=base_url("assets/css/popupbox.css");?>" rel="stylesheet">

<main>
	<section>
		<div class="row" style="padding-top:20px;">
			<div class="col-md-12" style="text-align:center;">
				<h3>Edit User Data</h3>
				<h5> กรุณาใส่ข้อมูลใน <font color="red">*</font> ให้ครบถ้วน </h5>
				<?php if(isset($_GET['process'])&&$_GET['process']=="updated"){?><font color="green">แก้ไขข้อมูลแล้ว</font><?php } ?>
			</div>
		</div>
	</section>
	<br/>
		<?php if($userdata['User_Display_Picture']!=""&&$userdata['User_Display_Picture']!=NULL){?>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6"><img class="img-responsive" src="<?=base_url();?>assets/upload/user_picture/<?=$userdata['User_Display_Picture'];?>" ></div>
				<div class="col-sm-3"></div>
			</div>
		<?php } ?>
	<br/>
	<article>
	<form id="editinfo" enctype="multipart/form-data" method="post" action="" class="form-horizontal">
		<input type="hidden" name="User_ID" id="User_ID" value="<?=$userdata['User_ID'];?>">
		<div class="form-group">
		    <label for="inputFirstname" class="col-sm-2 control-label"> <font color="red">*</font> First Name : </label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="inputFirstname" name="User_Firstname" value="<?=$userdata['User_Firstname'];?>" placeholder="Firstname" required>
		    </div>

		    <label for="inputLastname" class="col-sm-2 control-label"> <font color="red">*</font> Last Name : </label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="inputLastname" name="User_Lastname" value="<?=$userdata['User_Lastname'];?>" placeholder="Lastname" required>
		    </div>
		</div>

		<div class="form-group">
		  <label for="Username" class="col-sm-2 control-label"> <font color="red">*</font> Username : </label>
		  <div class="col-sm-4">
		    <input type="text" class="form-control" id="Username" name="Username" value="<?=$userdata['Username'];?>" placeholder="Username." required <?php if($userdata['Username']!=NULL&&$userdata['Username']!=""){ ?>readonly<?php } ?>>
	    </div>
			<label for="Password" class="col-sm-2 control-label"> Password : </label>
			<div class="col-sm-4">
				 <input type="password" class="form-control" id="Password" name="Password" value="" placeholder="Password">
			</div>
		</div>

		<div class="form-group">
		    <label for="inputPhone_Number" class="col-sm-2 control-label"> Phone No. : </label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="inputPhone_Number" name="User_Phone_Number" value="<?=$userdata['User_Phone_Number'];?>" placeholder="Phone Number">
		    </div>

		    <label for="inputEmail" class="col-sm-2 control-label"> <font color="red">*</font> Email : </label>
		    <div class="col-sm-4">
		      <input type="email" class="form-control" id="inputEmail" name="User_Email" placeholder="User Email" value="<?=$userdata['User_Email'];?>" required>
		    </div>
		</div>

		<div class="form-group">
				<label for="Permission" class="col-sm-2 control-label"> <font color="red">*</font> Permission. : </label>
				<div class="col-sm-4">
					<select name="Permission" id="Permission" class="form-control">
						<!--<option <?php if($userdata['Permission']=="user"){ echo "selected"; } ?> value="user">User</option>-->
						<option <?php if($userdata['Permission']=="agent"){ echo "selected"; } ?> value="agent">ตัวแทนชุมชน</option>
						<option <?php if($userdata['Permission']=="admin"){ echo "selected"; } ?> value="admin">Admin</option>
					</select>
				</div>
				<label for="Community" class="col-sm-2 control-label"> <font color="red">*</font> ชุมชนที่อยู่ : </label>
				<div class="col-sm-4">
					<select name="Community" id="Community" class="form-control">
					<?php $datatacom = array(NULL,"ป่าคลอก","บ้านแขนน","เกาะโหลน","เชิงทะเล","ท่าฉัตรไชย","เมืองเก่า","กมลา"); ?>
					<?php for ($i=1; $i < 8; $i++) { ?>
						<option <?php if($userdata['Community']==$i){ echo "selected"; } ?> value="<?=$i;?>"><?=$datatacom[$i];?></option>
					<?php } ?>
					</select>
				</div>
		</div>



		<div class="form-group">
		    <label for="inputDisplay_Picture" class="col-sm-4 control-label">Upload รูปภาพ (.jpg/.png) : </label>
		    <div class="col-sm-8">
		      <input type="file" class="form-control" style="border:none;"  id="inputDisplay_Picture" name="User_Display_Picture" >
		      <input type="hidden" id="oldpic" name="oldpic" value="<?=$userdata['User_Display_Picture'];?>">
		    </div>
		</div>

		<br/>

		<div class="row" style="text-align:center">
			<div class="col-md-4" style="margin-top:10px;">
				<a style="text-decoration:none;" href="<?=base_url();?>main/userlist"><input type="button" value="Back" class="btn btn-danger btn-lg btn-block"></a>
			</div>
			<div class="col-md-4" style="margin-top:10px;">
				<input type="submit" value="Submit" id="submitbutton" class="btn btn-success btn-lg btn-block">
			</div>
			<div class="col-md-4" style="margin-top:10px;">
				<input type="button" onclick="resetform()" value="Clear all" class="btn btn-info btn-lg btn-block">
			</div>
		</div>
	</form>
	</article>
</main>

<br/>

</div>

<script type="text/javascript">
       var url = "<?=base_url('assets/js/edituser.js');?>";
        $.getScript(url);
</script>
