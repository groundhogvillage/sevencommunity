</div>
</div>
</div>
    <footer>
        <div class="modalload"><!-- Place at bottom of page --></div>
         <div class="container">

            <div class="copy text-center">
              ©2017 <a href="https://phuketcommunitytourism.com">7COMMUNITY BASED TOURISM ROUTES AT PHUKET</a>
            </div>

            <div class="logout text-center">
              <a href="<?=base_url();?>index/logout">Logout</a>
            </div>
         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>assets/bootstrap/js/custom.js"></script>
  </body>
</html>
