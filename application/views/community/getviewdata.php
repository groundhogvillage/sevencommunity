<main>
	<section>
		<div class="row" style="padding-top:20px;">
			<div class="col-md-12" style="text-align:center;">
				<h3>รายการสถานที่หรือผลิตภัณฑ์ในชุมชน : <?=$item['name'];?></h3>
				<br/>
			</div>
		</div>
	</section>
	<br/>
	<article>
		<?php if($list_row>0){ ?>
			<div class="table-responsive">
				<table id="" class="table table-striped">
					<thead class=" ">
						<tr class=" header1">
							<th>No.</th>
							<th>Visited Date Time</th>
						</tr>
					</thead>
					<tbody class="">
            <?php
						$next = $this->uri->segment(3);
						foreach ($list as $key => $value) {
						?>
						<tr class="" id="">
							<td><?=$key+$next+1;?></td>
							<td><?=$value['view_datetime'];?></td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			</div>
		<?php } ?>
	</article>
	<div class="col-md-4" style="margin-top:10px;">
		<a style="text-decoration:none;" href="<?=base_url();?>community/itemcommlist?main=<?=$page_id;?>&type=<?=$type;?>"><input type="button" value="Back" class="btn btn-danger btn-lg btn-block"></a>
	</div>
</main>

<br/>
<br/>
