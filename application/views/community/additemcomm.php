<?php
//$setshow = array("TH","EN");
//$setprefix = array("","_en");
$setshow = array("TH");
$setprefix = array("");
?>
<div class="col-sm-10">
  <div class="row">
    <center id="updateddatacomplete" style="display:none;"><h3 style="color:green">เพิ่มข้อมูลเรียบร้อยแล้ว</h3></center>
    <br/>
    <div class="col-md-12">
      <form class="form-horizontal" id="defform" name="defform" method="post" action="<?=base_url();?>cms/updateitemcomm" enctype="multipart/form-data">
      <input type="hidden" id="type" name="type" value="<?=$type;?>">
      <input type="hidden" id="page_id" name="page_id" value="<?=$pageid;?>">
      <input type="hidden" id="item_picture_row" name="item_picture_row" value="<?=$item_picture_row;?>">
      <input type="hidden" id="frequency_update" name="frequency_update" value="<?=$item['frequency_update'];?>">
      <!--TH-->
      <?php foreach ($setprefix as $key => $value):
        if($key==0)
        {
          $itemid = $item['id'];
          $itemname = $item['name'.$value];
          $itemdescription = $item['description'.$value];
        //  $itemshortdescription = $item['short_description'.$value];
        }
        else if($key==1)
        {
          $itemid = $item_en['id'];
          $itemname = $item_en['name'.$value];
          $itemdescription = $item_en['description'.$value];
          //$itemshortdescription = $item_en['short_description'.$value];
        }
        else
        {
          $itemid = $item['id'];
          $itemname = $item['name'.$value];
          $itemdescription = $item['description'.$value];
          //$itemshortdescription = $item['short_description'.$value];
        }
      ?>
      <div class="col-md-12">
        <input type="hidden" id="item_id<?=$value;?>" name="item_id<?=$value;?>" value="<?=$itemid;?>">
        <div class="form-group">
          <label for="name" class="control-label col-md-2">ชื่อสถานที่ <!--<?=$setshow[$key];?>--> : </label>
          <div class="col-md-10">
            <input type="text" id="name<?=$value;?>" name="name<?=$value;?>" value="<?=$itemname;?>" class="form-control" <?php if($value==""){ echo "required"; }?> >
          </div>
        </div>
        <!--<div class="form-group">
          <label for="short_description" class="control-label col-md-2">Short Description <?=$setshow[$key];?> : </label>
          <div class="col-md-10">
            <input type="text" id="short_description<?=$value;?>" name="short_description<?=$value;?>" value="<?=$itemshortdescription;?>" class="form-control">
          </div>
        </div>-->
        <div class="form-group">
          <label for="description" class="control-label col-md-2">รายละเอียดสถานที่ <!--<?=$setshow[$key];?>--> : </label>
          <div class="col-md-10">
            <textarea id="description<?=$value;?>" name="description<?=$value;?>" class="form-control"><?=$itemdescription;?></textarea>
          </div>
        </div>
        </div>
      <?php endforeach; ?>

      <div class="form-group">
        <label for="fileToUpload" class="control-label col-md-2">อัพโหลดรูปภาพ : </label>
        <div class="col-md-10">
          <input type="file" name="fileToUpload[]" id="fileToUpload"  multiple="">
        </div>
      </div>
<!--
      <div class="form-group">
        <label for="showhide" class="control-label col-md-2">Show/Hide : </label>
        <div class="col-md-10">
          <select name="showhide" id="showhide" class="form-control">
            <option <?php if($item['showhide']=="0"){ echo "selected"; } ?> value="0">Show</option>
            <option <?php if($item['showhide']=="1"){ echo "selected"; } ?> value="1">Hide</option>
          </select>
        </div>
      </div>
-->
      <div class="form-group">
        <label for="lat" class="control-label col-md-2">ละติจูด : </label>
        <div class="col-md-3">
          <input type="number" step="any" id="lat" name="lat" value="<?=$item['lat'];?>" class="form-control">
        </div>
        <label for="lon" class="control-label col-md-2">ลองติจูด : </label>
        <div class="col-md-3">
          <input type="number" step="any" id="lon" name="lon" value="<?=$item['lon'];?>" class="form-control">
        </div>
        <input type="hidden" name="setzoom" id="setzoom" value="15">
        <div class="col-md-2">
          <input type="button" id="updatelocation" name="updatelocation" value='ดูสถานที่'  class="btn btn-info">
        </div>
      </div>

      <div class="form-group spacer_30">
				<div class="col-sm-1"></div>
				<div class="col-sm-10">
					<center>
					    <div id="map-canvas" style="width:100%;height:400px;">
					    </div>
					</center>
				</div>
				<div class="col-sm-1"></div>
			</div>

      <div class="form-group" id="submitdata">
        <br/>
        <div class="col-md-4">
          <input type="submit" id="submit" name="submit" value='เพิ่มข้อมูลสถานที่' class="btn btn-success btn-block btn-lg">
        </div>
        <div class="col-md-4">
          <a id="back" name="back" href="<?=base_url();?>community/itemcommlist?main=<?=$pageid;?>&type=<?=$type;?>" class="btn btn-warning btn-block btn-lg">กลับไปหน้ารายการ</a>
        </div>
        <div class="col-md-4">
          <input type="button" id="cancel" name="cancel" value='ยกเลิกการเพิ่มข้อมูล' onclick="resetform()" class="btn btn-danger btn-block btn-lg">
        </div>
      </div>

      </form>
    </div>
  </div>
  <?php if($item['id']!=""&&$item['id']!=NULL&&$item['id']!="0"){
    if($item_picture_row>0){
  ?>
  <div class="row">
    <div class="col-md-12">
    <div id="reviewpicture">
    <center><h4>รูปภาพที่อัพโหลดเข้ามา</h4></center>
      <center><a href="javascript:void(0);" class="btn outlined mleft_no reorder_linkpicture" id="save_reorderpicture">Reorder Photos</a></center>
      <div id="reorder-helperpicture" class="light_box" style="display:none;">1. กดที่รูปภาพค้างไว้แล้วพร้อมขยับเม้าเพื่อเลื่อนจัดลำดับรูปภาพตามที่ต้องการ.<br>2. คลิก "ยืนยันการเรียงลำดับ" เมื่อเรียงลำดับตามที่ต้องการเสร็จสิ้น</div>
      <div class="gallery">
          <ul class="reorder_ul reorder-picture-list">
          <?php
                  foreach ($item_picture as $keyp => $valuep){
          ?>
              <li id="picture_li_<?php echo $valuep['id']; ?>" class="ui-sortable-handle">
                <a href="javascript:void(0);" style="float:none;" class="image_linkpicture"><img src="<?=base_url();?>assets/upload/item/<?=$valuep['path'];?>" alt="<?php echo $valuep['name']; ?>"></a>
                <center><h4><?php echo $valuep['name']; ?></h4></center>
                <center><a class="delthispic" style="cursor:pointer" id="pdel_<?=$valuep['id'];?>" >ลบรูปภาพ</a></center>
              </li>
          <?php } } ?>
          </ul>
      </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>

</div><!--End Container Fluid-->

<script src="<?=base_url()?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="<?=base_url()?>assets/js/settinymce.js"></script>

<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3_8iL9KVkIwGPPH3_QD39wrAGPUBWSIs"  type="text/javascript"></script>
<script src="<?=base_url('assets/js/community/additem.js');?>"></script>
