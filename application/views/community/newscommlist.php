
<main>
	<section>
		<div class="row" style="padding-top:20px;">
			<div class="col-md-12" style="text-align:center;">
				<h3>รายการข่าวสารในชุมชน</h3>
				<br/>
				<center>
					<a style="cursor:pointer;" href="<?=base_url();?>community/addnewscomm?main=<?=$page_id;?>&type=<?=$type;?>"><button class="btn btn-primary">
						<span class="glyphicon glyphicon-plus"></span>เพิ่มข่าวสาร</button>
					</a>
				</center>
				<br/>
			</div>
		</div>
	</section>
	<br/>
	<article>
		<?php if($list_row>0){ ?>
			<div class="table-responsive">
				<table id="" class="table table-striped">
					<thead class=" ">
						<tr class=" header1">
							<th>No.</th>
							<th>Name</th>
							<th>Description</th>
							<th>Update Datetime</th>
							<th>Edit / Withhold</th>
						</tr>
					</thead>
					<tbody class="">
            <?php
						$next = $this->uri->segment(3);
						foreach ($list as $key => $value) {
						?>
						<tr class="" id="">
							<td><?=$key+$next+1;?></td>
							<td><?=$value['name'];?></td>
							<td><?=iconv_substr(strip_tags($value['description']),0,100, "UTF-8")."...";?></td>
							<td><?=$value['update_datetime'];?></td>
							<td>
                <a href="<?=base_url();?>community/addnewscomm?main=<?=$page_id;?>&type=<?=$type;?>&id=<?=$value['id'];?>">Edit</a>
                 / <a class="confirmdelete" href="<?=base_url();?>community/deletenewscomm?main=<?=$page_id;?>&type=<?=$type;?>&id=<?=$value['id'];?>">Delete</a>
               </td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			</div>
		<?php } ?>
		<div id="showpagination"><?php echo $this->pagination->create_links(); ?></div>
	</article>
</main>

<br/>
<br/>
