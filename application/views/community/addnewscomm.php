<?php
//$setshow = array("TH","EN");
//$setprefix = array("","_en");
$setshow = array("TH");
$setprefix = array("");
?>
<div class="col-sm-10">
  <div class="row">
    <center id="updateddatacomplete" style="display:none;"><h3 style="color:green">เพิ่มข้อมูลเรียบร้อยแล้ว</h3></center>
    <br/>
    <div class="col-md-12">
      <form class="form-horizontal" id="defform" name="defform" method="post" action="<?=base_url();?>cms/updatenewscomm" enctype="multipart/form-data">
      <input type="hidden" id="type" name="type" value="<?=$type;?>">
      <input type="hidden" id="page_id" name="page_id" value="<?=$pageid;?>">
      <input type="hidden" id="news_picture_row" name="news_picture_row" value="<?=$news_picture_row;?>">
      <!--TH-->
      <?php foreach ($setprefix as $key => $value):
        if($key==0)
        {
          $newsid = $news['id'];
          $newsname = $news['name'.$value];
          $newsdescription = $news['description'.$value];
        }
        else if($key==1)
        {
          $newsid = $news_en['id'];
          $newsname = $news_en['name'.$value];
          $newsdescription = $news_en['description'.$value];
        }
        else
        {
          $newsid = $news['id'];
          $newsname = $news['name'.$value];
          $newsdescription = $news['description'.$value];
        }
      ?>
      <div class="col-md-12">
        <input type="hidden" id="news_id<?=$value;?>" name="news_id<?=$value;?>" value="<?=$newsid;?>">
        <div class="form-group">
          <label for="name" class="control-label col-md-2">หัวข้อข่าวสาร <!--<?=$setshow[$key];?>--> : </label>
          <div class="col-md-10">
            <input type="text" id="name<?=$value;?>" name="name<?=$value;?>" value="<?=$newsname;?>" class="form-control" <?php if($value==""){ echo "required"; }?> >
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="control-label col-md-2">รายละเอียดข่าวสาร <!--<?=$setshow[$key];?>--> : </label>
          <div class="col-md-10">
            <textarea id="description<?=$value;?>" name="description<?=$value;?>" class="form-control"><?=$newsdescription;?></textarea>
          </div>
        </div>
        </div>
      <?php endforeach; ?>

      <div class="form-group">
        <label for="fileToUpload" class="control-label col-md-2">อัพโหลดรูปภาพ : </label>
        <div class="col-md-10">
          <input type="file" name="fileToUpload[]" id="fileToUpload"  multiple="">
        </div>
      </div>
<!--
      <div class="form-group">
        <label for="showhide" class="control-label col-md-2">Show/Hide : </label>
        <div class="col-md-10">
          <select name="showhide" id="showhide" class="form-control">
            <option <?php if($news['showhide']=="0"){ echo "selected"; } ?> value="0">Show</option>
            <option <?php if($news['showhide']=="1"){ echo "selected"; } ?> value="1">Hide</option>
          </select>
        </div>
      </div>
-->
      <div class="form-group" id="submitdata">
        <br/>
        <div class="col-md-4">
          <input type="submit" id="submit" name="submit" value='เพิ่มข้อมูลข่าวสาร' class="btn btn-success btn-block btn-lg">
        </div>
        <div class="col-md-4">
          <a id="back" name="back" href="<?=base_url();?>community/newscommlist?main=<?=$pageid;?>&type=<?=$type;?>" class="btn btn-warning btn-block btn-lg">กลับไปหน้ารายการ</a>
        </div>
        <div class="col-md-4">
          <input type="button" id="cancel" name="cancel" value='ยกเลิกการเพิ่มข้อมูล' onclick="resetform()" class="btn btn-danger btn-block btn-lg">
        </div>
      </div>

      </form>
    </div>
  </div>
  <?php if($news['id']!=""&&$news['id']!=NULL&&$news['id']!="0"){
    if($news_picture_row>0){
  ?>
  <div class="row">
    <div class="col-md-12">
    <div id="reviewpicture">
    <center><h4>รูปภาพที่อัพโหลดเข้ามา</h4></center>
      <center><a href="javascript:void(0);" class="btn outlined mleft_no reorder_linkpicture" id="save_reorderpicture">REORDERPICTURE</a></center>
      <div id="reorder-helperpicture" class="light_box" style="display:none;">1. กดที่รูปภาพค้างไว้แล้วพร้อมขยับเม้าเพื่อเลื่อนจัดลำดับรูปภาพตามที่ต้องการ.<br>2. คลิก "ยืนยันการเรียงลำดับ" เมื่อเรียงลำดับตามที่ต้องการเสร็จสิ้น</div>
      <div class="gallery">
          <ul class="reorder_ul reorder-picture-list">
          <?php
                  foreach ($news_picture as $keyp => $valuep){
          ?>
              <li id="picture_li_<?php echo $valuep['id']; ?>" class="ui-sortable-handle">
                <a href="javascript:void(0);" style="float:none;" class="image_linkpicture"><img src="<?=base_url();?>assets/upload/news/<?=$valuep['path'];?>" alt="<?php echo $valuep['name']; ?>"></a>
                <center><h4><?php echo $valuep['name']; ?></h4></center>
                <center><a class="delthispic" style="cursor:pointer" id="pdel_<?=$valuep['id'];?>" >ลบรูปภาพ</a></center>
              </li>
          <?php } } ?>
          </ul>
      </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>

</div><!--End Container Fluid-->

<script src="<?=base_url()?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="<?=base_url()?>assets/js/settinymce.js"></script>
<script src="<?=base_url('assets/js/community/addnews.js');?>"></script>
