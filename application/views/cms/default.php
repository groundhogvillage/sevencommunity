<?php
//$setshow = array("TH","EN");
//$setprefix = array("","_en");
$setshow = array("TH");
$setprefix = array("");
?>
<div class="col-sm-10">
  <?php if($page['id']!=NULL&&$page['id']!=""&&$attr['subpage']!=""&&$attr['subpage']!=NULL&&$attr['subpage']!="0"){ ?>
  <div class="row">
    <div class="col-md-12">
      <a class="btn btn-info" id="addsubpage" style="margin-top:30px;">Add Sub Page</a>
    </div>
  </div>
  <?php } ?>
  <div class="row">
    <center id="updateddatacomplete" style="display:none;"><h3 style="color:green">Update Complete</h3></center>
    <center id="deleteddatacomplete" style="display:none;"><h3 style="color:red">Delete Complete</h3></center>
    <br/>
    <div class="col-md-12">
      <ul class="tab">
        <?php foreach ($setshow as $key1 => $value1): ?>
        <li class="ontab"><a href="javascript:void(0)" <?php if($key1==0){ echo "id='defaultOpen'"; } ?> class="tablinks" onclick="openlang(event, '<?=$value1;?>')"><?=$value1;?></a></li>
        <?php endforeach; ?>
        <?php if($page['id']!=NULL&&$page['id']!=""&&$page['id']!="0"){ ?>
        <?php if($attr['picture']==true){ ?><li class="ontab"><a href="javascript:void(0)" class="tablinks" onclick="openlang(event, 'picture')">Picture</a></li><?php } ?>
        <?php if($attr['slide']==true){ ?><li class="ontab"><a href="javascript:void(0)" class="tablinks" onclick="openlang(event, 'slide')">Slide</a></li><?php } ?>
        <?php if($attr['file']==true){ ?><li class="ontab"><a href="javascript:void(0)" class="tablinks" onclick="openlang(event, 'file')">File</a></li><?php } ?>
        <?php if($this->session->userdata("Permission")=="creator"){ ?><li class="ontab"><a href="javascript:void(0)" class="tablinks" onclick="openlang(event, 'attrsetting')">Attribute</a></li><?php } ?>
        <?php } ?>
      </ul>
      <form class="form-horizontal" id="defform" name="defform" method="post" action="<?=base_url();?>cms/updatecmsdata" enctype="multipart/form-data">
      <input type="hidden" id="type" name="type" value="<?=$page['type'];?>">
      <input type="hidden" id="mainid" name="mainid" value="<?=$page['id'];?>">
      <input type="hidden" id="subid" name="subid" value="">
      <!--TH-->
      <?php foreach ($setprefix as $key => $value):
        if($key==0)
        {
          $pageid = $page['id'];
          $pagename = $page['name'.$value];
          $pagedescription = $page['description'.$value];
          $pagelink = $page['link'.$value];
        }
        else if($key==1)
        {
          $pageid = $page_en['id'];
          $pagename = $page_en['name'.$value];
          $pagedescription = $page_en['description'.$value];
          $pagelink = $page_en['link'.$value];
        }
        else
        {
          $pageid = $page_cn['id'];
          $pagename = $page_cn['name'.$value];
          $pagedescription = $page_cn['description'.$value];
          $pagelink= $page_cn['link'.$value];
        }
      ?>
        <input type="hidden" id="page_id<?=$value;?>" name="page_id<?=$value;?>" value="<?=$pageid;?>">
        <div id="<?=$setshow[$key];?>" class="tabcontent">
        <?php if($attr['name']==true){ ?>
        <div class="form-group">
          <label for="name" class="control-label col-md-2">Name <?=$setshow[$key];?> : </label>
          <div class="col-md-10">
            <input type="text" id="name" name="name<?=$value;?>" value="<?=$pagename;?>" class="form-control" <?php if($value==""){ echo "required"; }?> >
          </div>
        </div>
        <?php } ?>
        <?php if($attr['description']==true){ ?>
        <div class="form-group">
          <label for="description" class="control-label col-md-2">Description <?=$setshow[$key];?> : </label>
          <div class="col-md-10">
            <textarea id="description" name="description<?=$value;?>" class="form-control"><?=$pagedescription;?></textarea>
          </div>
        </div>
        <?php } ?>
        <?php if($attr['link']==true){ ?>
        <div class="form-group">
          <label for="link" class="control-label col-md-2">Link <?=$setshow[$key];?> : </label>
          <div class="col-md-10">
            <input type="text" id="link" name="link<?=$value;?>" value="<?=$pagelink;?>" class="form-control">
          </div>
        </div>
        <?php } ?>
      </div>
      <?php endforeach; ?>

      <div class="form-group" id="submitfortxtdata">
        <?php if($attr['name']==true||$attr['description']==true||$attr['link']==true){ ?>
        <br/>
        <div class="col-md-6">
          <input type="submit" id="submit" name="submit" value='submit' class="btn btn-success btn-block btn-lg">
        </div>
        <div class="col-md-6">
          <input type="button" id="cancel" name="cancel" value='cancel' onclick="resetform()" class="btn btn-danger btn-block btn-lg">
        </div>
        <?php } ?>
      </div>

      </form>
    </div>
  </div>

  <?php if($attr['picture']==true&&$page['id']!=NULL&&$page['id']!=""&&$page['id']!="0"){ ?>
    <div class="row ">
      <div class="col-md-12">
        <div id="picture" class="tabcontent">
        <form class="form-horizontal" id="defformpic" name="defformpic" method="post" action="<?=base_url();?>cms/updatecmspicture" enctype="multipart/form-data">
        <input type="hidden" id="mainidpic" name="mainidpic" value="<?=$page['id'];?>">
        <input type="hidden" id="allpic" name="allpic" value="">
        <input type="hidden" id="maxpic" name="maxpic" value="">
        <input type="hidden" id="pictureid" name="pictureid" value="">
        <input type="hidden" id="oldpic" name="oldpic" value="">
        <div class="form-group">
          <label for="picturename" class="control-label col-md-2">Name : </label>
          <div class="col-md-10">
            <input type="text" id="picturename" name="picturename" class="form-control">
          </div>
        </div>

        <div class="form-group">
          <label for="uploadpicture" class="control-label col-md-2">Upload : </label>
          <div class="col-md-10">
            <input type="file" id="uploadpicture" name="uploadpicture" class="form-control">
          </div>
        </div>

        <div class="form-group" id="submitforpicdata">
          <br/>
          <div class="col-md-6">
            <input type="submit" id="submit" name="submit" value='submit' class="btn btn-success btn-block btn-lg">
          </div>
          <div class="col-md-6">
            <input type="button" id="cancel" name="cancel" value='cancel' onclick="resetformpicture()" class="btn btn-danger btn-block btn-lg">
          </div>
        </div>

        </form>
        <br/>
      <div id="reviewpicture">
      <?php if($picture_row>0&&$page['id']!=NULL&&$page['id']!=""&&$page['id']!="0"){ ?>
          <center><h4>Review Picture</h4></center>
          <center><a href="javascript:void(0);" class="btn outlined mleft_no reorder_linkpicture" id="save_reorderpicture">Reorder Photos</a></center>
          <div id="reorder-helperpicture" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
          <div class="gallery">
              <ul class="reorder_ul reorder-picture-list">
              <?php
                      foreach ($picture as $keyp => $valuep){
              ?>
                  <li id="picture_li_<?php echo $valuep['id']; ?>" class="ui-sortable-handle">
                    <a href="javascript:void(0);" style="float:none;" class="image_linkpicture"><img src="<?=base_url();?>assets/upload/picture/<?=$valuep['path'];?>" alt="<?php echo $valuep['name']; ?>"></a>
                    <center><h4><?php echo $valuep['name']; ?></h4></center>
                    <center><a class="edtpic" style="cursor:pointer" id="pedt_<?=$valuep['id'];?>" >Edit</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="delthispic" style="cursor:pointer" id="pdel_<?=$valuep['id'];?>" >Delete</a></center>
                  </li>
              <?php } ?>
              </ul>
          </div>
      <?php } ?>
        </div>
      </div>
    </div>
  </div>
    <?php } ?>

  <?php if($attr['slide']==true&&$page['id']!=NULL&&$page['id']!=""&&$page['id']!="0"){ ?>
  <div class="row ">
    <div class="col-md-12">
      <div id="slide" class="tabcontent">
        <form class="form-horizontal" id="defformslide" name="defformslide" method="post" action="<?=base_url();?>cms/updatecmsslide" enctype="multipart/form-data">
        <input type="hidden" id="mainidslide" name="mainidslide" value="<?=$page['id'];?>">
        <input type="hidden" id="slideid" name="slideid" value="">
        <input type="hidden" id="allslide" name="allslide" value="">
        <input type="hidden" id="maxslide" name="maxslide" value="">
        <input type="hidden" id="oldslide" name="oldslide" value="">
        <div class="form-group">
          <label for="slidename" class="control-label col-md-2">Name : </label>
          <div class="col-md-10">
            <input type="text" id="slidename" name="slidename" class="form-control">
          </div>
        </div>
        <div class="form-group">
          <label for="uploadslide" class="control-label col-md-2">Upload Slide : </label>
          <div class="col-md-10">
            <input type="file" id="uploadslide" name="uploadslide" class="form-control">
          </div>
        </div>

        <div class="form-group" id="submitforslidedata">
          <br/>
          <div class="col-md-6">
            <input type="submit" id="submit" name="submit" value='submit' class="btn btn-success btn-block btn-lg">
          </div>
          <div class="col-md-6">
            <input type="button" id="cancel" name="cancel" value='cancel' onclick="resetformslide()" class="btn btn-danger btn-block btn-lg">
          </div>
        </div>
        </form>
        <br/>
        <div id="reviewslide">
        <?php if($slide_row>0){ ?>
          <center><h4>Review Slide</h4></center>
          <center><a href="javascript:void(0);" class="btn outlined mleft_no reorder_linkslide" id="save_reorderslide">Reorder Slide</a></center>
          <div id="reorder-helperslide" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
          <div class="gallery">
              <ul class="reorder_ul reorder-slide-list">
              <?php
                      foreach ($slide as $keys => $values){
              ?>
                  <li id="slide_li_<?php echo $values['id']; ?>" class="ui-sortable-handle">
                    <a href="javascript:void(0);" style="float:none;" class="image_linkslide"><img src="<?=base_url();?>assets/upload/slide/<?=$values['path'];?>" alt="<?php echo $values['name']; ?>"></a>
                      <center><h4><?php echo $values['name']; ?></h4></center>
                      <center><a class="edtslide" style="cursor:pointer" id="sedt_<?=$values['id'];?>" >Edit</a>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <a class="delthisslide" style="cursor:pointer" id="sdel_<?=$values['id'];?>" >Delete</a></center>

                  </li>
              <?php } ?>
              </ul>
          </div>
      <?php } ?>
      </div>
      </div>
    </div>
  </div>
  <?php } ?>

  <?php if($attr['file']==true&&$page['id']!=NULL&&$page['id']!=""&&$page['id']!="0"){ ?>
    <div class="row ">
      <div class="col-md-12">
        <div id="file" class="tabcontent">
          <form class="form-horizontal" id="defformfile" name="defformfile" method="post" action="<?=base_url();?>cms/updatecmsfile" enctype="multipart/form-data">
          <input type="hidden" id="mainidfile" name="mainidfile" value="<?=$page['id'];?>">
          <input type="hidden" id="allfile" name="allfile" value="">
          <input type="hidden" id="maxfile" name="maxfile" value="2">
          <input type="hidden" id="fileid" name="fileid" value="">
          <input type="hidden" id="oldfile" name="oldfile" value="">
          <div class="form-group">
            <label for="filename" class="control-label col-md-2">Name : </label>
            <div class="col-md-10">
              <input type="text" id="filename" name="filename" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label for="uploadfile" class="control-label col-md-2">Upload Files (PDF,DOC,Other) : </label>
            <div class="col-md-10">
              <input type="file" id="uploadfile" name="uploadfile" class="form-control">
            </div>
          </div>

          <div class="form-group" id="submitforfilrdata">
            <br/>
            <div class="col-md-6">
              <input type="submit" id="submit" name="submit" value='submit' class="btn btn-success btn-block btn-lg">
            </div>
            <div class="col-md-6">
              <input type="button" id="cancel" name="cancel" onclick="resetformfile()" value='cancel' class="btn btn-danger btn-block btn-lg">
            </div>
          </div>
          </form>
          <br/>
          <div id="reviewfile">
          <?php if($file_row>0){ ?>
            <center><h4>Review File</h4></center>
            <center><a href="javascript:void(0);" class="btn outlined mleft_no reorder_linkfile" id="save_reorderfile">Reorder Files</a></center>
            <div id="reorder-helperfile" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
            <div class="gallery">
                <ul class="reorder_ul reorder-file-list">
                <?php
                        foreach ($file as $keyf => $valuef){
                ?>
                    <li id="file_li_<?php echo $valuef['id']; ?>" class="ui-sortable-handle">
                      <a href="javascript:void(0);" style="float:none;" class="image_linkfile"><img src="<?=base_url();?>assets/images/nopic.jpg" alt="<?php echo $valuef['name']; ?>"></a>
                        <center><h4><?php echo $valuef['name']; ?></h4></center>
                        <center><a class="edtfile" style="cursor:pointer" id="fedt_<?=$valuef['id'];?>" >Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a class="delthisfile" style="cursor:pointer" id="fdel_<?=$valuef['id'];?>" >Delete</a></center>

                    </li>
                <?php } ?>
                </ul>
            </div>
        <?php } ?>
        </div>
        </div>
      </div>
    </div>
  <?php } ?>

  <?php if($page['id']!=NULL&&$page['id']!=""&&$page['id']!="0"){ ?>
  <div class="row ">
    <div class="col-md-12">
      <div id="attrsetting" class="tabcontent">
        <form class="form-horizontal" id="defformattr" name="defformattr" method="post" action="<?=base_url();?>cms/updatecmsattr" enctype="multipart/form-data">
        <input type="hidden" id="mainidattr" name="mainidattr" value="<?=$page['id'];?>">

        <div class="form-group">
          <div class="checkbox col-md-12">
            <label><input name="attr_name" id="attr_name" type="checkbox" value="1" <?php if($attr['name']==true){ echo "checked"; } ?> >Name</label>
          </div>
        </div>

        <div class="form-group">
          <div class="checkbox col-md-12">
            <label><input name="attr_description" id="attr_description" type="checkbox" value="1" <?php if($attr['description']==true){ echo "checked"; } ?> >Description</label>
          </div>
        </div>

        <div class="form-group">
          <div class="checkbox col-md-12">
            <label><input name="attr_link" id="attr_link" type="checkbox" value="1" <?php if($attr['link']==true){ echo "checked"; } ?> >Link</label>
          </div>
        </div>

        <div class="form-group">
          <div class="checkbox col-md-12">
            <label><input name="attr_picture" id="attr_picture" type="checkbox" value="1" <?php if($attr['picture']==true){ echo "checked"; } ?> >Picture</label>
          </div>
        </div>

        <div class="form-group">
          <div class="checkbox col-md-12">
            <label><input name="attr_slide" id="attr_slide" type="checkbox" value="1" <?php if($attr['slide']==true){ echo "checked"; } ?> >Slide</label>
          </div>
        </div>

        <div class="form-group">
          <div class="checkbox col-md-12">
            <label><input name="attr_file" id="attr_file" type="checkbox" value="1" <?php if($attr['file']==true){ echo "checked"; } ?> >File</label>
          </div>
        </div>

        <div class="form-group">
          <div class="checkbox col-md-12">
            <label><input name="attr_subpage" id="attr_subpage" type="checkbox" value="1" <?php if($attr['subpage']==true){ echo "checked"; } ?> >Sub Page</label>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-6">
            <input type="submit" id="submit" name="submit" value='submit' class="btn btn-success btn-block btn-lg">
          </div>
          <div class="col-md-6">
            <input type="button" id="cancel" name="cancel" onclick="resetformattr()" value='cancel' class="btn btn-danger btn-block btn-lg">
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
  <?php } ?>

  <br/>

</div>

</div><!--End Container Fluid-->

<div class="popup" id="popup2">
  <form class="form-horizontal" id="addsubform" name="addsubform" method="post" action="<?=base_url();?>cms/addsubpage" enctype="multipart/form-data">
    <input type="hidden" id="mainid_sub" name="mainid_sub" value="<?=$page['id'];?>">
    <label for="subpage_name">Subpage Name : </label>
    <br/>
    <input type="text" id="subpage_name" name="subpage_name" class="form-control" required>
    <br/>
    <input type="submit" class="btn btn-success" name="submit" value="Submit">
    <a class="btn btn-danger" id="closepopup">Close!</a>
  </form>
</div>

<script src="<?=base_url()?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="<?=base_url()?>assets/js/settinymce.js"></script>


<script type="text/javascript">
       var url = "<?=base_url('assets/js/cms/default.js');?>";
       var url2 = "<?=base_url('assets/js/cms/defaultrefreshdelpicture.js');?>";
       var url3 = "<?=base_url('assets/js/cms/defaultrefreshdelslide.js');?>";
       var url4 = "<?=base_url('assets/js/cms/defaultrefreshdelfile.js');?>";
       $.getScript(url);
       $.getScript(url2);
       $.getScript(url3);
       $.getScript(url4);
</script>
