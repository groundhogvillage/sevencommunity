<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
  	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <meta name="description" content="เว็บไซต์7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต|7 Community Based Tourism Routes at Phuket">

    <title>
      7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต|7 Community Based Tourism Routes @ Phuket
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/frontend/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?=base_url();?>assets/frontend/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don"t actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?=base_url();?>assets/frontend/js/ie-emulation-modes-warning.js"></script>

    <!-- CSS Add more -->
    <link href="<?=base_url();?>assets/frontend/css/style.css" rel="stylesheet">

    <!-- FontFace -->
    <link href="<?=base_url();?>assets/frontend/css/fontface/fontface.css" rel="stylesheet">

    <!-- ICON Font Awesome -->
    <link href="<?=base_url();?>assets/frontend/css/font-awesome.min.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="<?=base_url();?>assets/frontend/img/image_section/favicon7.ico">

  </head>

  <body>

    <!-- NAVBAR -->

    <nav class="navbar fixed-navbar navbar-download">
      <div class="container">
        <div class="navbar-header page-scroll" role="navigation">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Fixed-navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="hidden-lg hidden-md hidden-sm navbar-brand" href="<?=base_url();?>index">
            <img class="img-responsive logo" style="margin-top: -15px;" src="<?=base_url();?>assets/frontend/img/image_section/logo_webmobile-sm.png">
          </a>
        </div>
        <div class="collapse navbar-collapse page-scroll navbar-download" id="Fixed-navbar">
          <ul class="nav navbar-nav inside-navbar">
            <li>
              <a class="page-scroll" href="<?=base_url();?>index">หน้าแรก</a>
            </li>
            <!--<li>
              <a class="page-scroll" href="/#about">About</a>
            </li>-->
            <li>
              <a class="page-scroll" href="<?=base_url();?>index/product">สถานที่ท่องเที่ยวโดยชุมชน</a>
            </li>
            <li>
              <a class="page-scroll" href="<?=base_url();?>index/community?commid=6">เจ็ดชุมชนท่องเที่ยว</a>
            </li>
            <li class="active">
              <a class="page-scroll" href="<?=base_url();?>index/download">ดาวน์โหลดแผนที่ท่องเที่ยว</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Before-fixed navbar on top -->
    <div id="before-fixed-navbar">
      <!-- Navigation For Download Page -->
      <nav class="navbar navbar-download" role="navigation">
        <div class="container">
          <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#download-navbar">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="hidden-lg hidden-md hidden-sm navbar-brand" href="<?=base_url();?>index">
              <img class="img-responsive logo" style="margin-top: -15px;" src="<?=base_url();?>assets/frontend/img/image_section/logo_webmobile-sm.png">
            </a>
          </div>
          <div class="collapse navbar-collapse page-scroll navbar-download" id="download-navbar">
            <ul class="nav navbar-nav inside-navbar">
              <li class="hidden">
                <a class="page-scroll" href="#page-top"></a>
              </li>
              <li>
              <a class="page-scroll" href="<?=base_url();?>index">หน้าแรก</a>
            </li>
            <!--<li>
              <a class="page-scroll" href="<?=base_url();?>index">About</a>
            </li>-->
            <li>
              <a class="page-scroll" href="<?=base_url();?>index/product">สถานที่ท่องเที่ยวโดยชุมชน</a>
            </li>
            <li>
              <a class="page-scroll" href="<?=base_url();?>index/community?commid=6">เจ็ดชุมชนท่องเที่ยว</a>
            </li>
            <li class="active">
              <a class="page-scroll" href="<?=base_url();?>index/download">ดาวน์โหลดแผนที่ท่องเที่ยว</a>
            </li>
            </ul>
          </div>    <!-- END Navbar-Collapse -->
        </div>    <!-- END Container -->
      </nav>    <!-- END Navigation For Download Page -->
    </div>    <!-- END Before-fixed navbar on top -->

    <!-- END NAVBAR -->

    <!-- CONTENT -->

    <!-- Community PDF Download : Big Map -->
    <section class="download-section" id="download">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=base_url();?>assets/frontend/img/map_download/pdf/01_map_large_web.pdf" title="7เส้นทางหลัก ท่องเที่ยวโดยชุมชน จังหวัดภูเก็ต">
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-pdf.png">
              </div>
              <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/map_download/mapp_large_web-01.jpg" title="7เส้นทางหลัก ท่องเที่ยวโดยชุมชน จังหวัดภูเก็ต" alt="7เส้นทางหลัก ท่องเที่ยวโดยชุมชน จังหวัดภูเก็ต">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>

    <!-- Community PDF Download : Community 1 -->
    <section class="download-section">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=base_url();?>assets/frontend/img/map_download/pdf/map_thachatchai.pdf" title="ชุมชนบ้านท่าฉัตรไชย Baan Tha Chatchai Community">
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-pdf.png">
              </div>
              <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/map-design/map-design1.jpg" title="ชุมชนบ้านท่าฉัตรไชย Baan Tha Chatchai Community" alt="ชุมชนบ้านท่าฉัตรไชย Baan Tha Chatchai Community">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>

    <!-- Community PDF Download : Community 2 -->
    <section class="download-section">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=base_url();?>assets/frontend/img/map_download/pdf/map_paklog.pdf" title="ชุมชนตำบลป่าคลอก Tambol Pa Klog Community">
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-pdf.png">
              </div>
              <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/map-design/map-design2.jpg" title="ชุมชนตำบลป่าคลอก Tambol Pa Klog Community" alt="ชุมชนตำบลป่าคลอก Tambol Pa Klog Community">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>

    <!-- Community PDF Download : Community 3 -->
    <section class="download-section">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=base_url();?>assets/frontend/img/map_download/pdf/map_kanan.pdf" title="ชุมชนบ้านแขนน(หมู่บ้านวัฒนธรรมถลาง) Baan Kanan Community(Thalang Cultural and Traditional village)">
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-pdf.png">
              </div>
              <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/map-design/map-design3.jpg" title="ชุมชนบ้านแขนน(หมู่บ้านวัฒนธรรมถลาง) Baan Kanan Community(Thalang Cultural and Traditional village)" alt="ชุมชนบ้านแขนน(หมู่บ้านวัฒนธรรมถลาง) Baan Kanan Community(Thalang Cultural and Traditional village)">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>

    <!-- Community PDF Download : Community 4 -->
    <section class="download-section">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=base_url();?>assets/frontend/img/map_download/pdf/map_oldphuket.pdf" title="ชุมชนย่านเมืองเก่าภูเก็ต The Old Phuket Town Community">
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-pdf.png">
              </div>
              <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/map-design/map-design4.jpg" title="ชุมชนย่านเมืองเก่าภูเก็ต The Old Phuket Town Community" alt="ชุมชนย่านเมืองเก่าภูเก็ต The Old Phuket Town Community">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>

    <!-- Community PDF Download : Community 5 -->
    <section class="download-section">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=base_url();?>assets/frontend/img/map_download/pdf/map_bangtao_cherng_talay.pdf" title="ชุมชนบ้านบางเทา-เชิงทะเล Baan Bangtao-Cherng Talay Community">
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-pdf.png">
              </div>
              <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/map-design/map-design5.jpg" title="ชุมชนบ้านบางเทา-เชิงทะเล Baan Bangtao-Cherng Talay Community" alt="ชุมชนบ้านบางเทา-เชิงทะเล Baan Bangtao-Cherng Talay Community">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>

    <!-- Community PDF Download : Community 6 -->
    <section class="download-section">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=base_url();?>assets/frontend/img/map_download/pdf/map_kamala.pdf" title="ชุมชนตำบลกมลา Tambol Kamala Community">
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-pdf.png">
              </div>
              <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/map-design/map-design6.jpg" title="ชุมชนตำบลกมลา Tambol Kamala Community" alt="ชุมชนตำบลกมลา Tambol Kamala Community">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>

    <!-- Community PDF Download : Community 7 -->
    <section class="download-section">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=base_url();?>assets/frontend/img/map_download/pdf/map_lone.pdf" title="ชุมชนตำบลราไวย์(เกาะโหลน) Tambol Rawai of Lone Island Community">
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-pdf.png">
              </div>
              <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/map-design/map-design7.jpg" title="ชุมชนบ้านท่าฉัตรไชย Baan Tha Chatchai Community" alt="ชุมชนบ้านท่าฉัตรไชย Baan Tha Chatchai Community">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>

    <!-- END CONTENT -->

    <!-- Footer -->
    <footer class="margin-top-on-footer">
      <div class="footer-bottom">
        <div class="container">
          <div class="mobile-center pull-left">
            ©2017 <a href="https://phuketcommunitytourism.com">7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต</a>
          </div>
          <div class="pull-right">
            <ul class="mobile-center social">
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn1.png"> </a> </li>
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn2.png"> </a> </li>
              <li> <a href="<?=base_url()?>index/login_page"> <img src="<?=base_url();?>assets/frontend/img/image_section/login-btn.png"> </a> </li>
            </ul>
          </div>
        </div>
      </div>    <!-- FOOTER-BOTTOM-->
    </footer>
    <!-- END FOOTER -->

    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url();?>assets/frontend/js/jquery.min.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/bootstrap.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?=base_url();?>assets/frontend/js/ie10-viewport-bug-workaround.js"></script>
    <!-- Smooth Scrolling Page
    <script src="<?=base_url();?>assets/frontend/js/jQuery.scrollSpeed.js"></script>
    -->
    <!-- Scrolling Nav Easing JavaScript -->
    <script src="<?=base_url();?>assets/frontend/js/jquery.easing.min.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/community.js"></script>

  </body>
</html>
