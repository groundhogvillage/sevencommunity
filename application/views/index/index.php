<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
  	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <meta name="description" content="เว็บไซต์7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต|7 Community Based Tourism Routes at Phuket">

    <title>
      7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต|7 Community Based Tourism Routes @ Phuket
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/frontend/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?=base_url();?>assets/frontend/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don"t actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?=base_url();?>assets/frontend/js/ie-emulation-modes-warning.js"></script>

    <!-- CSS Add more -->
    <link href="<?=base_url();?>assets/frontend/css/styleforindex.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/frontend/css/indexpage2.css" rel="stylesheet">

    <!-- FontFace -->
    <link href="<?=base_url();?>assets/frontend/css/fontface/fontface.css" rel="stylesheet">

    <!-- ICON Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="all">

    <!-- Animate -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" media="all">

    <!-- Favicon -->
    <link rel="icon" href="<?=base_url();?>assets/frontend/img/image_section/favicon7.ico">

    <link href="<?=base_url("assets/frontend/owlslide/assets/owl.carousel.css");?>" rel="stylesheet">

  </head>

  <body>

    <!-- NAVBAR -->

    <!-- Fixed Navbar on top -->
    <nav class="navbar fixed-navbar navbar-index">
      <div class="container">
        <div class="navbar-header page-scroll" role="navigation">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Fixed-navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="hidden-lg hidden-md hidden-sm navbar-brand" href="/#home">
            <img class="img-responsive logo" src="<?=base_url();?>assets/frontend/img/image_section/logo_webmobile-sm.png">
          </a>
        </div>
        <div class="collapse navbar-collapse page-scroll navbar-index" id="Fixed-navbar">
          <ul class="nav navbar-nav inside-navbar">
            <!--<li>
              <a class="page-scroll" href="#parahd2">Home</a>
            </li>-->
            <li class="active">
              <a class="page-scroll" href="#about">เกี่ยวกับโครงการ</a>
            </li>
            <li>
              <a class="page-scroll" href="#news">ข่าวสาร</a>
            </li>
            <li>
              <a class="page-scroll" href="#product">สถานที่ท่องเที่ยวโดยชุมชน</a>
            </li>
            <li>
              <a class="page-scroll" href="#community">เจ็ดชุมชนท่องเที่ยว</a>
            </li>
            <li>
              <a class="page-scroll" href="#download">ดาวน์โหลดแผนที่ท่องเที่ยว</a>
            </li>
          </ul>
        </div>    <!-- END Navbar-Collapse -->
      </div>    <!-- END Container -->
    </nav>    <!-- END Fixed Navbar on top -->

    <!-- Before-fixed navbar on top -->
    <div id="before-fixed-navbar">

      <div id="parahd2">
        <div class='layerhd-bgx layerparahd2' data-depth='0.15' data-type='parallax'></div>
        <div class='layerhd-cloudx layerparahd2' data-depth='0.05' data-type='parallax'></div>
        <div class='layerhd-mount1x layerparahd2' data-depth='0.21' data-type='parallax'></div>
        <div class='layerhd-mount2x layerparahd2' data-depth='0.22' data-type='parallax'></div>
        <div class='layerhd-mount3x layerparahd2' data-depth='0.23' data-type='parallax'></div>
        <div class='layerhd-logox layerparahd2' data-depth='0.10' data-type='parallax'></div>
        <div class='layerhd-boatx layerparahd2' data-depth='0.20' data-type='parallax'></div>
        <div class='layerhd-cocorx layerparahd2' data-depth='0.95' data-type='parallax'></div>
        <div class='layerhd-cocolx layerparahd2' data-depth='1.00' data-type='parallax'></div>
      </div>
      <div id='parafhd'>
        <!-- background -->
        <div class='layerfhd-bgx layerparafhd' data-depth='0.15' data-type='parallax'></div>
        <div class='layerfhd-cloudx layerparafhd' data-depth='0.05' data-type='parallax'></div>
        <div class='layerfhd-mount1x layerparafhd' data-depth='0.21' data-type='parallax'></div>
        <div class='layerfhd-mount2x layerparafhd' data-depth='0.22' data-type='parallax'></div>
        <div class='layerfhd-mount3x layerparafhd' data-depth='0.23' data-type='parallax'></div>
        <div class='layerfhd-logox layerparafhd' data-depth='0.10' data-type='parallax'></div>
        <div class='layerfhd-boatx layerparafhd' data-depth='0.20' data-type='parallax'></div>
        <div class='layerfhd-cocorx layerparafhd' data-depth='0.95' data-type='parallax'></div>
        <div class='layerfhd-cocolx layerparafhd' data-depth='1.00' data-type='parallax'></div>
      </div>
      <div id='parahd'>
        <div class='layerhd-bgx layerparahd' data-depth='0.15' data-type='parallax'></div>
        <div class='layerhd-cloudx layerparahd' data-depth='0.05' data-type='parallax'></div>
        <div class='layerhd-mount1x layerparahd' data-depth='0.21' data-type='parallax'></div>
        <div class='layerhd-mount2x layerparahd' data-depth='0.22' data-type='parallax'></div>
        <div class='layerhd-mount3x layerparahd' data-depth='0.23' data-type='parallax'></div>
        <div class='layerhd-logox layerparahd' data-depth='0.10' data-type='parallax'></div>
        <div class='layerhd-boatx layerparahd' data-depth='0.20' data-type='parallax'></div>
        <div class='layerhd-cocorx layerparahd' data-depth='0.95' data-type='parallax'></div>
        <div class='layerhd-cocolx layerparahd' data-depth='1.00' data-type='parallax'></div>
      </div>
      <div id="normalbg">
        <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/header_parallax/hero.jpg" style="width:1920px;">
      </div>

      <div id="mobilebg">
        <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/header_parallax/mobilehero.jpg">
      </div>
      <!--<div id="hero-mobile"></div>-->

      <!-- Navigation For Download Page -->
      <nav class="navbar navbar-index" style="padding-top:15px;" role="navigation">
        <div class="container">
          <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#download-navbar">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="hidden-lg hidden-md hidden-sm navbar-brand" href="/#home">
              <img class="img-responsive logo" src="<?=base_url();?>assets/frontend/img/image_section/logo_webmobile-sm.png">
            </a>
          </div>
          <div class="collapse navbar-collapse page-scroll navbar-index" id="download-navbar">
            <ul class="nav navbar-nav inside-navbar">
              <li class="hidden">
                <a class="page-scroll" href="#page-top"></a>
              </li>
              <!--<li class="active">
                <a class="page-scroll" href="#parahd2">Home</a>
              </li>-->
              <li>
                <a class="page-scroll" href="#about">เกี่ยวกับโครงการ</a>
              </li>
              <li>
                <a class="page-scroll" href="#news">ข่าวสาร</a>
              </li>
              <li>
                <a class="page-scroll" href="#product">สถานที่ท่องเที่ยวโดยชุมชน</a>
              </li>
              <li>
                <a class="page-scroll" href="#community">เจ็ดชุมชนท่องเที่ยว</a>
              </li>
              <li>
                <a class="page-scroll" href="#download">ดาวน์โหลดแผนที่ท่องเที่ยว</a>
              </li>
            </ul>
          </div>    <!-- END Navbar-Collapse -->
        </div>    <!-- END Container -->
      </nav>    <!-- END Navigation For Download Page -->
    </div>    <!-- END Before-fixed navbar on top -->

    <!-- END NAVBAR -->

    <!-- CONTENT -->

    <!-- About-Section ================================== -->
    <section class="section-about" style="margin-bottom: -1px;"  id="about">

      <!-- Content in About-Section -->
      <div class="container text-center">
        <div class="col-lg-12"  id="icocom">
          <ul class="inline-ul">
            <li class="inline-li">
              <a href="<?=base_url();?>index/community?commid=4">
              <img class="img-responsive img-inline" src="<?=base_url();?>assets/frontend/img/image_section/about-community1.png">
              </a>
            </li>
            <li class="inline-li">
              <a href="<?=base_url();?>index/community?commid=3">
              <img class="img-responsive img-inline" src="<?=base_url();?>assets/frontend/img/image_section/about-community2.png">
              </a>
            </li>
            <li class="inline-li">
              <a href="<?=base_url();?>index/community?commid=8">
              <img class="img-responsive img-inline" src="<?=base_url();?>assets/frontend/img/image_section/about-community3.png">
              </a>
            </li>
            <li class="inline-li">
              <a href="<?=base_url();?>index/community?commid=6">
              <img class="img-responsive img-inline" src="<?=base_url();?>assets/frontend/img/image_section/about-community4.png">
              </a>
            </li>
            <li class="inline-li">
              <a href="<?=base_url();?>index/community?commid=5">
              <img class="img-responsive img-inline" src="<?=base_url();?>assets/frontend/img/image_section/about-community5.png">
              </a>
            </li>
            <li class="inline-li">
              <a href="<?=base_url();?>index/community?commid=2">
              <img class="img-responsive img-inline" src="<?=base_url();?>assets/frontend/img/image_section/about-community6.png">
              </a>
            </li>
            <li class="inline-li">
              <a href="<?=base_url();?>index/community?commid=7">
              <img class="img-responsive img-inline" src="<?=base_url();?>assets/frontend/img/image_section/about-community7.png">
              </a>
            </li>
          </ul>
        </div>

        <div class="col-md-8 col-md-offset-2">
          <p>
            <?=$pageindex['description'];?>
          </p>
        </div>

        <!-- MORE>> Button in About-Section -->
        <div class="col-md-8 col-md-offset-2 card-header text-right" role="tab" id="headingTwo">
          <h5>
            <a style="color:#FFFBF6; font-family:'THSarabunNewRegular'; font-size:23px; font-weight:100;" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
              อ่านผู้ประสานงานเพิ่มเติม>>
            </a>
          </h5>
          <!--<h5>
            <a style="color:#FFFBF6; font-family:'THSarabunNewRegular'; font-size:25px; font-weight:100;" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              อ่านผู้สนับสนุนหลักเพิ่มเติม>>
            </a>
          </h5>-->
        </div>
      </div>
    </section>
    <div class="bg-card-block">
      <div class="container">
        <div class="row">
          <!-- MORE>> Button in About-Section -->
          <div class="col-md-8 col-md-offset-2 text-center">
            <div id="accordion" role="tablist" aria-multiselectable="true">
              <div class="card">
                <!-- Content of MORE>> Button in About-Section -->
                <div id="collapseOne" class="collapse introwho" role="tabpanel" aria-labelledby="headingTwo">

                  <?=$pageabout['description'];?>

                  <div class="col-lg-12">
                    <ul class="inline-support">
                      <li class="inline-li">
                        <a href="http://www.phuket.go.th/index.php">
                        <img class="img-responsive img-inline-news" src="<?=base_url();?>assets/frontend/img/image_section/support-logo1-1.png">
                        </a>
                      </li>
                      <li class="inline-li">
                        <a href="http://www.mots.go.th/phuket/main.php?filename=index">
                        <img class="img-responsive img-inline-news" src="<?=base_url();?>assets/frontend/img/image_section/support-logo2-1.png">
                        </a>
                      </li>
                      <li class="inline-li">
                        <a href="http://www.phuket.psu.ac.th/index1.php">
                        <img class="img-responsive img-inline-news" src="<?=base_url();?>assets/frontend/img/image_section/support-logo3-1.png">
                        </a>
                      </li>
                    </ul>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </div>   <!-- END Content in More-Section -->
      </div>   <!-- END BG-Color in More-Section -->
    </div>  <!-- END CARD-BLOCK 1 -->
    <!--<div class="bg-card-block">
      <div class="container">
        <div class="row">
          <!-- MORE>> Button in About-Section
          <div class="col-md-8 col-md-offset-2 text-center">
            <div id="accordion" role="tablist" aria-multiselectable="true">
              <div class="card">
                <!-- Content of MORE>> Button in About-Section
                <div id="collapseTwo" class="collapse introwho" role="tabpanel" aria-labelledby="headingTwo">

                  <div class="col-lg-12">
                    <ul class="inline-support">
                      <li class="inline-li">
                        <a href="http://www.phuket.go.th/index.php">
                        <img class="img-responsive img-inline-news" src="<?=base_url();?>assets/frontend/img/image_section/support-logo1-1.png">
                        </a>
                      </li>
                      <li class="inline-li">
                        <a href="http://www.mots.go.th/phuket/main.php?filename=index">
                        <img class="img-responsive img-inline-news" src="<?=base_url();?>assets/frontend/img/image_section/support-logo2-1.png">
                        </a>
                      </li>
                      <li class="inline-li">
                        <a href="http://www.phuket.psu.ac.th/index1.php">
                        <img class="img-responsive img-inline-news" src="<?=base_url();?>assets/frontend/img/image_section/support-logo3-1.png">
                        </a>
                      </li>
                    </ul>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>   <!-- END Content in More-Section
      </div>   <!-- END BG-Color in More-Section
    </div>  <!-- END CARD-BLOCK 2 -->
    <!-- END About-Section -->

    <!-- NEWS-Section -->
    <!-- Header with background -->
    <section class="section-news-head" style="margin-bottom: -1px;" id="news">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <img class="img-responsive img-news" src="<?=base_url();?>assets/frontend/img/image_section/news-line.png">
          </div>
        </div>
      </div>
    </section>
    <!-- Content only -->
    <section class="section-news-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <?php if($news_row>0){ ?>
            <div id="news-hilight" class="owl-carousel owl-theme setedtslide">
              <?php foreach ($news as $keynews => $valuenews) {
                if($valuenews['type']=="2")
                {
                  $setcommname = "ชุมชนป่าคลอก";
                }
                else if($valuenews['type']=="3")
                {
                  $setcommname = "ชุมชนบ้านแขนน";
                }
                else if($valuenews['type']=="4")
                {
                  $setcommname = "ชุมชนเกาะโหลน";
                }
                else if($valuenews['type']=="5")
                {
                  $setcommname = "ชุมชนเชิงทะเล";
                }
                else if($valuenews['type']=="6")
                {
                  $setcommname = "ชุมชนท่าฉัตรไชย";
                }
                else if($valuenews['type']=="7")
                {
                  $setcommname = "ชุมชนเมืองเก่าภูเก็ต";
                }
                else if($valuenews['type']=="8")
                {
                  $setcommname = "ชุมชนกมลา";
                }
                else
                {
                  $setcommname = "ชุมชน...";
                }
              ?>
              <div class="item  pd-th pd-space_around_box">
                <div class="pd-product_box_rate">
                  <div class="pd-box-news_picture">
                    <a data-toggle="modal" class="updateview" data-id="" data-target="#News-Modal<?=$valuenews['id'];?>">
                      <?php if($valuenews['picture_row']>0){ ?>
                        <img class="img-box_picture img-box img-center" src="<?=base_url();?>assets/upload/news/<?=$valuenews['picture'][0]['path'];?>">
                      <?php }else{
                        if($valuenews['type']=="2")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
                        }
                        else if($valuenews['type']=="3")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
                        }
                        else if($valuenews['type']=="4")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
                        }
                        else if($valuenews['type']=="5")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
                        }
                        else if($valuenews['type']=="6")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
                        }
                        else if($valuenews['type']=="7")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
                        }
                        else if($valuenews['type']=="8")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
                        }
                        else
                        {
                          $setnopic = base_url().'assets/images/nopic.jpg';
                        }
                        ?>
                        <img class="img-box_picture img-box img-center" src="<?=$setnopic;?>">
                      <?php } ?>
                      <!--<img style="display: block; max-width: 100%;  height: auto;" src="assets/images/nopic.jpg">-->
                    </a>
                  </div>
                  <div class="pd-box-news_detail text-center">
                    <div class="pd-caption">
                      <h3><?=$setcommname;?></h3>
                      <h3><?=$valuenews['name'];?></h3>
                      <hr class="line-pd-caption">
                      <p>
                        <?php
                        if(strlen($valuenews['description'])>0)
                        {
                          echo iconv_substr(strip_tags($valuenews['description']),0,60, "UTF-8")."...";
                        }
                        else
                        {
                          echo "";
                        }
                      ?>
                      </p>
                    </div>
                  </div>
                  <div class="pd-box_btn_more">
                    <h4><a data-toggle="modal" style="font-size:22px; cursor:pointer; color:#281b1c;" class="" data-id="" data-target="#News-Modal<?=$valuenews['id'];?>">อ่านต่อ>></a></h4>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>

          <?php } ?>
          </div>
        </div>
      </div>
    </section>
    <!-- END NEWS-Section -->

    <?php if($news_row>0){ ?>
      <?php foreach ($news as $keynews => $valuenews) {
        if($valuenews['type']=="2")
        {
          $setcommname = "ชุมชนป่าคลอก";
        }
        else if($valuenews['type']=="3")
        {
          $setcommname = "ชุมชนบ้านแขนน";
        }
        else if($valuenews['type']=="4")
        {
          $setcommname = "ชุมชนเกาะโหลน";
        }
        else if($valuenews['type']=="5")
        {
          $setcommname = "ชุมชนเชิงทะเล";
        }
        else if($valuenews['type']=="6")
        {
          $setcommname = "ชุมชนท่าฉัตรไชย";
        }
        else if($valuenews['type']=="7")
        {
          $setcommname = "ชุมชนเมืองเก่าภูเก็ต";
        }
        else if($valuenews['type']=="8")
        {
          $setcommname = "ชุมชนกมลา";
        }
        else
        {
          $setcommname = "ชุมชน...";
        }
      ?>
    <!-- Modal News-Hilight-->
    <div class="modal fade" id="News-Modal<?=$valuenews['id'];?>" tabindex="-1" role="dialog" aria-labelledby="News-Modal<?=$valuenews['id'];?>" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- SLIDE IN MODAL -->
          <div class="modal-header">

            <div id="myCarouselnews<?=$valuenews['id'];?>" class="carousel slide carousel-fit" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php
                if($valuenews['picture_row']>0)
                {
                  foreach ($valuenews['picture'] as $keynews2 => $valuenews2) {
                ?>
                <li data-target="#myCarouselnews<?=$valuenews['id'];?>" data-slide-to="<?=$keynews2;?>" <?php if($keynews2==0){echo 'class="active"'; }?>></li>
                <?php
                  }
                }
                else
                {
                ?>
                  <li data-target="#myCarouselnews<?=$valuenews['id'];?>" data-slide-to="0" class="active"></li>
                <?php
                }
                ?>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <?php
                if($valuenews['picture_row']>0)
                {
                  foreach ($valuenews['picture'] as $keynews2 => $valuenews2) {
                    if($keynews2==0){ $activethis = "active"; }else{ $activethis = ""; }
                ?>
                <div class="item <?=$activethis;?>">
                  <img class="img-responsive img-center" src="<?=base_url();?>assets/upload/news/<?=$valuenews2['path'];?>" alt="">
                </div>
                <?php
                  }
                }
                else
                {
                  if($valuenews['type']=="2")
                  {
                    $setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
                  }
                  else if($valuenews['type']=="3")
                  {
                    $setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
                  }
                  else if($valuenews['type']=="4")
                  {
                    $setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
                  }
                  else if($valuenews['type']=="5")
                  {
                    $setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
                  }
                  else if($valuenews['type']=="6")
                  {
                    $setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
                  }
                  else if($valuenews['type']=="7")
                  {
                    $setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
                  }
                  else if($valuenews['type']=="8")
                  {
                    $setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
                  }
                  else
                  {
                    $setnopic = base_url().'assets/images/nopic.jpg';
                  }
                  ?>
                  <div class="item active">
                    <img class="img-responsive img-center" src="<?=$setnopic;?>" alt="">
                  </div>
                  <?php
                }
                ?>
              </div>

            </div> <!--end mycarusel-->
            <div class="pd-tab-header-carousel">
              <?=$setcommname;?> <br> <hr style="margin:0;"> <?=$valuenews['name'];?>
            </div>
          </div>

          <!-- CONTENT -->
          <div class="modal-body">

            <p class="text-left">
              <?=$valuenews['description'];?>
            </p>
          </div>
          <div class="modal-footer">
            <center>
              <button type="button" class="btn btn-default" style="font-size:25px;" data-dismiss="modal">ปิด</button>
            </center>
          </div>
        </div>    <!-- END MODAL CONTENT -->
      </div>    <!-- END MODAL DIALOG -->
    </div>    <!-- END MODAL -->

    <?php } } ?>

    <!-- Product-Section -->
    <!-- Header with background -->
    <section class="section-product-head" id="product">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <!--<h1 class="font-product"><strong>PRODUCT</strong></h1>
            <h2 class="font-product">HIGHLIGHTS</h2>-->
            <img class="img-responsive img-product" src="<?=base_url();?>assets/frontend/img/image_section/product-line1.png">
          </div>
        </div>
      </div>
    </section>
    <!-- Content only -->
    <section class="section-product-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">

            <ul id="product-hilight" style="padding-left:0;">
              <?php
              if($item_row>0)
              {
                foreach ($item as $key => $value) {

                    if($value['type']=="2")
                    {
                      $setcommname = "ชุมชนป่าคลอก";
                    }
                    else if($value['type']=="3")
                    {
                      $setcommname = "ชุมชนบ้านแขนน";
                    }
                    else if($value['type']=="4")
                    {
                      $setcommname = "ชุมชนเกาะโหลน";
                    }
                    else if($value['type']=="5")
                    {
                      $setcommname = "ชุมชนเชิงทะเล";
                    }
                    else if($value['type']=="6")
                    {
                      $setcommname = "ชุมชนท่าฉัตรไชย";
                    }
                    else if($value['type']=="7")
                    {
                      $setcommname = "ชุมชนเมืองเก่าภูเก็ต";
                    }
                    else if($value['type']=="8")
                    {
                      $setcommname = "ชุมชนกมลา";
                    }
                    else
                    {
                      $setcommname = "ชุมชน...";
                    }

              ?>
              <li class="pd-th pd-space_around_box">
                <div class="pd-product_box_rate">
                  <div class="pd-box_picture">
                    <a data-toggle="modal" class="updateview" data-id="<?=$value['id'];?>" data-target="#Product-Modal<?=$value['id'];?>">
                      <?php if($value['picture_row']>0){ ?>
                        <img class="img-box_picture img-box img-center" src="<?=base_url();?>assets/upload/item/<?=$value['picture'][0]['path'];?>">
                      <?php }else{
                        if($value['type']=="2")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
                        }
                        else if($value['type']=="3")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
                        }
                        else if($value['type']=="4")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
                        }
                        else if($value['type']=="5")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
                        }
                        else if($value['type']=="6")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
                        }
                        else if($value['type']=="7")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
                        }
                        else if($value['type']=="8")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
                        }
                        else
                        {
                          $setnopic = base_url().'assets/images/nopic.jpg';
                        }
                        ?>
                        <img class="img-box_picture img-box img-center" src="<?=$setnopic;?>">
                      <?php } ?>
                    </a>
                  </div>
                  <div class="pd-box_detail_product text-center">
                    <div class="pd-caption">
                      <h3><?=$setcommname;?></h3>
                      <h3><?=$value['name'];?></h3>
                      <hr class="line-pd-caption">
                      <p>
                      <?
                        if(strlen($value['description'])>0)
                        {
                          echo iconv_substr(strip_tags($value['description']),0,100, "UTF-8")."...";
                        }
                        else
                        {
                          echo "";
                        }
                      ?>
                      </p>
                    </div>
                  </div>
                  <div class="pd-box_btn_more">
                    <h4><a data-toggle="modal" class="updateview" data-id="<?=$value['id'];?>" style="font-size:22px; cursor:pointer; color:#281b1c;" data-target="#Product-Modal<?=$value['id'];?>">อ่านต่อ>></a></h4>
                  </div>
                    <?php
                      $updatedatetime = explode(" ",$value['update_datetime']);
                      $updatedate = explode("-",$updatedatetime[0]);
                    ?>
                    <div style="margin-left:0px;display:inline-block">View : <?=$value['visited'];?></div>
                    <!--<div style="margin-right:0px;display:inline-block">Updated : <?=$updatedate[2]."/".$updatedate[1]."/".$updatedate[0];?></div>-->
                </div>
              </li>
              <!-- Modal Product-Hilight-->
              <div class="modal fade" id="Product-Modal<?=$value['id'];?>" tabindex="-1" role="dialog" aria-labelledby="Product-Modal<?=$value['id'];?>" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- SLIDE IN MODAL -->
                    <div class="modal-header">

                      <div id="myCarousel<?=$value['id'];?>" class="carousel slide carousel-fit" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <?php
                          if($value['picture_row']>0)
                          {
                            foreach ($value['picture'] as $key2 => $value2) {
                          ?>
                          <li data-target="#myCarousel<?=$value['id'];?>" data-slide-to="<?=$key2;?>" <?php if($key2==0){echo 'class="active"'; }?>></li>
                          <?php
                            }
                          }
                          else
                          {
                          ?>
                            <li data-target="#myCarousel<?=$value['id'];?>" data-slide-to="0" class="active"></li>
                          <?php
                          }
                          ?>
                        </ol>


                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                          <?php
                          if($value['picture_row']>0)
                          {
                            foreach ($value['picture'] as $key2 => $value2) {
                              if($key2==0){ $activethis = "active"; }else{ $activethis = ""; }
                          ?>
                          <div class="item <?=$activethis;?>">
                            <img class="img-responsive img-center" src="<?=base_url();?>assets/upload/item/<?=$value2['path'];?>" alt="">
                          </div>
                          <?php
                            }
                          }
                          else
                          {
                            if($value['type']=="2")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
                            }
                            else if($value['type']=="3")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
                            }
                            else if($value['type']=="4")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
                            }
                            else if($value['type']=="5")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
                            }
                            else if($value['type']=="6")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
                            }
                            else if($value['type']=="7")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
                            }
                            else if($value['type']=="8")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
                            }
                            else
                            {
                              $setnopic = base_url().'assets/images/nopic.jpg';
                            }
                            ?>
                            <div class="item active">
                              <img class="img-responsive img-center" src="<?=$setnopic;?>" alt="">
                            </div>
                            <?php
                          }
                          ?>
                        </div>

                      </div> <!--end mycarusel-->

                      <div class="pd-tab-header-carousel">
                        <?=$setcommname;?>
                        <br> <hr style="margin: 0;">
                        <?=$value['name'];?>
                      </div>
                    </div>

                    <!-- CONTENT -->
                    <div class="modal-body">

                      <p class="text-left">
                        <?=$value['description'];?>
                      </p>
                    </div>
                    <div class="modal-footer">
                      <center>
                        <button type="button" class="btn btn-default" style="font-size:25px;" data-dismiss="modal">ปิด</button>
                      </center>
                    </div>
                  </div>    <!-- END MODAL CONTENT -->
                </div>    <!-- END MODAL DIALOG -->
              </div>    <!-- END MODAL -->
              <?php
                }
              }
              ?>


            </ul>

            <!-- MORE>> Button in Product-Section font-old#210403-->
            <div class="col-lg-12 text-right">
              <h3 style="font-size: 25px !important;" class="font-product">
                <a style="color:#281b1c;" href="<?=base_url();?>index/product">
                  อ่านจุดเด่นของชุมชนเพิ่มเติม>>
                </a>
              </h3>
            </div>

          </div>
        </div>
      </div>

    </section>
    <!-- END Product-Section -->

    <!-- Comunity-Section -->
    <!-- Section Comunity HEADER -->
    <section class="section-community" id="community">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <img class="img-responsive img-head-community" src="<?=base_url();?>assets/frontend/img/image_section/community-line1.png">
          </div>

          <!-- Nav Indicator -->
          <div class="navbar-collapse nav-slide-collapse">
            <ol class="nav navbar-nav nav-indicators nav-slide-collapse">
              <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active">
                <a href="#">ชุมชนบ้านท่าฉัตรไชย</a>
              </li>
              <li data-target="#bootstrap-touch-slider" data-slide-to="1">
                <a href="#">ชุมชนตำบลป่าคลอก</a>
              </li>
              <li data-target="#bootstrap-touch-slider" data-slide-to="2">
                <a href="#">ชุมชนบ้านแขนน</a>
              </li>
              <li data-target="#bootstrap-touch-slider" data-slide-to="3">
                <a href="#">ชุมชนย่านเมืองเก่าภูเก็ต</a>
              </li>
              <li data-target="#bootstrap-touch-slider" data-slide-to="4">
                <a href="#">ชุมชนบ้านบางเทา-เชิงทะเล</a>
              </li>
              <li data-target="#bootstrap-touch-slider" data-slide-to="5">
                <a href="#">ชุมชนตำบลกมลา</a>
              </li>
              <li data-target="#bootstrap-touch-slider" data-slide-to="6">
                <a href="#">ชุมชนราไวย์-เกาะโหลน</a>
              </li>
            </ol>
          </div>

        </div>

        <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="4000" >

          <!-- Wrapper For Slides -->
          <div class="carousel-inner" role="listbox">

            <!--<div class="item">
                <img src="<?=base_url();?>assets/upload/picture/<?=$page5['picture'];?>" alt="Bootstrap Touch Slider"  class="slide-image"/>
                <div class="bs-slider-overlay"></div>
                <div class="slide-text slide_style_right">
                    <h2><?=$page5['name'];?></h2>
                    <p class="foncommindex"><?=iconv_substr(strip_tags($page5['description']),0,500, "UTF-8")."...";?></p>
                    <p class="sub-text"><a style="color:white;" href="<?=base_url();?>index/community?commid=<?=$page5['type'];?>">Read more</a></p>
                </div>
            </div>-->

            <!-- One Slide -->
            <div class="item active">
              <!-- Slide Background -->
              <img src="<?=base_url();?>assets/upload/picture/<?=$page1['picture'];?>">
              <div class="bs-slider-overlay"></div>   <!-- Overlay -->
              <div class="container">     <!-- Slide in Overlay-->
                <div class="row">
                  <!-- Slide Text Layer -->
                  <div class="slide-text slide_style_right">
                    <hr>
                    <h1 data-animation="animated fadeInLeft">ชุมชนท่องเที่ยว</h1>
                    <br>
                    <h1 data-animation="animated zoomInRight">บ้านท่าฉัตรไชย<span class="hidmob">-<br/>ไม้ขาว</span></h1>
                    <hr >
                    <p class="foncommindex" data-animation="animated fadeInLeft">
                      <?=iconv_substr(strip_tags($page1['description']),0,100, "UTF-8")."...";?>
                    </p>
                    <a href="<?=base_url();?>index/community?commid=<?=$page1['type'];?>">อ่านต่อ>></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End of Slide -->

            <!-- Two Slide -->
            <div class="item ">
              <!-- Slide Background -->
              <img src="<?=base_url();?>assets/upload/picture/<?=$page2['picture'];?>">
              <div class="bs-slider-overlay"></div>   <!-- Overlay -->
              <div class="container">     <!-- Slide in Overlay-->
                <div class="row">
                  <!-- Slide Text Layer -->
                  <div class="slide-text slide_style_right">
                    <hr>
                    <h1 data-animation="animated fadeInLeft">ชุมชนท่องเที่ยว</h1>
                    <br>
                    <h1 data-animation="animated zoomInRight">บ้านบางโรง<span class="hidmob">-<br/>ป่าคลอก</span></h1>
                    <hr >
                    <p class="foncommindex" data-animation="animated fadeInLeft">
                      <?=iconv_substr(strip_tags($page2['description']),0,100, "UTF-8")."...";?>
                    </p>
                    <a href="<?=base_url();?>index/community?commid=<?=$page2['type'];?>">อ่านต่อ>></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End of Slide -->

            <!-- Three Slide -->
            <div class="item ">
              <!-- Slide Background -->
              <img src="<?=base_url();?>assets/upload/picture/<?=$page3['picture'];?>">
              <div class="bs-slider-overlay"></div>   <!-- Overlay -->
              <div class="container">     <!-- Slide in Overlay-->
                <div class="row">
                  <!-- Slide Text Layer -->
                  <div class="slide-text slide_style_right">
                    <hr>
                    <h1 data-animation="animated fadeInLeft">ชุมชนท่องเที่ยว</h1>
                    <br>
                    <h1 data-animation="animated zoomInRight">บ้านแขนน<span class="hidmob">-<br/>เทพกระษัตรี</span></h1>
                    <hr>
                    <p class="foncommindex" data-animation="animated fadeInLeft">
                      <?=iconv_substr(strip_tags($page3['description']),0,100, "UTF-8")."...";?>
                    </p>
                    <a href="<?=base_url();?>index/community?commid=<?=$page3['type'];?>">อ่านต่อ>></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End of Slide -->

            <!-- Four Slide -->
            <div class="item ">
              <!-- Slide Background -->
              <img src="<?=base_url();?>assets/upload/picture/<?=$page4['picture'];?>">
              <div class="bs-slider-overlay"></div>   <!-- Overlay -->
              <div class="container">     <!-- Slide in Overlay-->
                <div class="row">
                  <!-- Slide Text Layer -->
                  <div class="slide-text slide_style_right">
                    <hr>
                    <h1 data-animation="animated fadeInLeft">ชุมชนท่องเที่ยว</h1>
                    <br>
                    <h1 data-animation="animated zoomInRight">ย่านเมืองภูเก็ต</h1>
                    <hr >
                    <p class="foncommindex" data-animation="animated fadeInLeft">
                      <?=iconv_substr(strip_tags($page4['description']),0,100, "UTF-8")."...";?>
                    </p>
                    <a href="<?=base_url();?>index/community?commid=<?=$page4['type'];?>">อ่านต่อ>></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End of Slide -->

            <!-- Five Slide -->
            <div class="item ">
              <!-- Slide Background -->
              <img src="<?=base_url();?>assets/upload/picture/<?=$page5['picture'];?>">
              <div class="bs-slider-overlay"></div>   <!-- Overlay -->
              <div class="container">     <!-- Slide in Overlay-->
                <div class="row">
                  <!-- Slide Text Layer -->
                  <div class="slide-text slide_style_right">
                    <hr>
                    <h1 data-animation="animated fadeInLeft">ชุมชนท่องเที่ยว</h1>
                    <br>
                    <h1 data-animation="animated zoomInRight">บ้านบางเทา<span class="hidmob">-<br/>เชิงทะเล</span></h1>
                    <hr >
                    <p class="foncommindex" data-animation="animated fadeInLeft">
                      <?=iconv_substr(strip_tags($page5['description']),0,100, "UTF-8")."...";?>
                    </p>
                    <a href="<?=base_url();?>index/community?commid=<?=$page5['type'];?>">อ่านต่อ>></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End of Slide -->

            <!-- Six Slide -->
            <div class="item ">
              <!-- Slide Background -->
              <img src="<?=base_url();?>assets/upload/picture/<?=$page6['picture'];?>">
              <div class="bs-slider-overlay"></div>   <!-- Overlay -->
              <div class="container">     <!-- Slide in Overlay-->
                <div class="row">
                  <!-- Slide Text Layer -->
                  <div class="slide-text slide_style_right">
                    <hr>
                    <h1 data-animation="animated fadeInLeft">ชุมชนท่องเที่ยว</h1>
                    <br>
                    <h1 data-animation="animated zoomInRight">ตำบลกมลา</h1>
                    <hr >
                    <p class="foncommindex" data-animation="animated fadeInLeft">
                      <?=iconv_substr(strip_tags($page6['description']),0,100, "UTF-8")."...";?>
                    </p>
                    <a href="<?=base_url();?>index/community?commid=<?=$page6['type'];?>">อ่านต่อ>></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End of Slide -->

            <!-- Seven Slide -->
            <div class="item">
              <!-- Slide Background -->
              <img src="<?=base_url();?>assets/upload/picture/<?=$page7['picture'];?>">
              <div class="bs-slider-overlay"></div>   <!-- Overlay -->
              <div class="container">     <!-- Slide in Overlay-->
                <div class="row">
                  <!-- Slide Text Layer -->
                  <div class="slide-text slide_style_right">
                    <hr>
                    <h1 data-animation="animated fadeInLeft">ชุมชนท่องเที่ยว</h1>
                    <br>
                    <h1 data-animation="animated zoomInRight">บ้านเกาะโหลน<span class="hidmob">-<br/>ราไวย์</span></h1>
                    <hr >
                    <p class="foncommindex" data-animation="animated fadeInLeft">
                      <?=iconv_substr(strip_tags($page7['description']),0,100, "UTF-8")."...";?>
                    </p>
                    <a href="<?=base_url();?>index/community?commid=<?=$page7['type'];?>">อ่านต่อ>></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End of Slide -->


            </div><!-- End of Wrapper For Slides -->

            <!-- Left Control -->
            <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
                <span class="fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <!-- Right Control -->
            <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
                <span class="fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div> <!-- End  bootstrap-touch-slider Slider -->

        <div class="row img-head-download-row">
          <div class="col-lg-12">
            <img class="img-responsive img-head-download" src="<?=base_url();?>assets/frontend/img/image_section/download-line1.png">
          </div>
        </div>
      </div>
    </section>

    <div class="safari_only"> </div>

    <!-- Community Image Download : MAP-DESIGN -->
    <section class="section-download" id="download" style="background-color:#f4efe4; padding-top: 0 !important;" id="download">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=base_url();?>index/download">
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-hover1.png">
              </div>
              <img class="img-responsive img-download-width" src="<?=base_url();?>assets/frontend/img/image_section/download-map-design.jpg">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>
    <!-- END Community Image Download -->

    <!-- END CONTENT -->

    <!-- Footer -->
    <footer class="margin-top-on-footer">
      <div class="footer-bottom">
        <div class="container">
          <div class="mobile-center pull-left">
            ©2017 <a href="https://phuketcommunitytourism.com">7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต</a>
          </div>
          <div class="pull-right">
            <ul class="mobile-center social">
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn1.png"> </a> </li>
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn2.png"> </a> </li>
              <li> <a href="<?=base_url()?>index/login_page"> <img src="<?=base_url();?>assets/frontend/img/image_section/login-btn.png"> </a> </li>
            </ul>
          </div>
        </div>
      </div>    <!-- FOOTER-BOTTOM-->
    </footer>
    <!-- END FOOTER -->

    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url();?>assets/frontend/js/jquery.min.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/bootstrap.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?=base_url();?>assets/frontend/js/ie10-viewport-bug-workaround.js"></script>

    <!-- Parallax -->
    <script src="<?=base_url();?>assets/frontend/js/parallax.js"></script>

    <!-- Fixed navbar on top -->
    <script type="text/javascript">
      $(function(){
        var height = $("#before-fixed-navbar").height();
        if ($(window).scrollTop() > height-1) {
          $(".fixed-navbar").css("display","block");
        }
        else  {
          $(".fixed-navbar").css("display","none");
        }
      });

      $( window ).scroll(function() {
        var height = $("#before-fixed-navbar").height();
        if ($(window).scrollTop() > height-1) {
          $(".fixed-navbar").css("display","block");
        }
        else  {
          $(".fixed-navbar").css("display","none");
        }
      });

      $(window).resize(function(){
        var height = $("#before-fixed-navbar").height();
        if ($(window).scrollTop() > height-1) {
          $(".fixed-navbar").css("display","block");
        }
        else  {
          $(".fixed-navbar").css("display","none");
        }
      });
    </script>

    <!-- Smooth Scrolling Page
    <script src="js/jQuery.scrollSpeed.js"></script>
    <script>
      $(function() {
        jQuery.scrollSpeed(100, 800);
      });
    </script>
    -->

    <!-- Scrolling Nav Easing JavaScript -->
    <script src="<?=base_url();?>assets/frontend/js/jquery.easing.min.js"></script>
    <script type="text/javascript">
      //jQuery to collapse the navbar on scroll
      $(window).scroll(function() {
          if ($(".navbar").offset().top > 50) {
              $(".fixed-navbar").addClass("top-nav-collapse");
          } else {
              $(".fixed-navbar").removeClass("top-nav-collapse");
          }
      });

      //jQuery for page scrolling feature - requires jQuery Easing plugin
      $(function() {
          $(document).on('click', 'a.page-scroll', function(event) {
              var $anchor = $(this);
              $('html, body').stop().animate({
                  scrollTop: $($anchor.attr('href')).offset().top
              }, 1500, 'easeInOutExpo');
              event.preventDefault();
          });
      });
    </script>

    <!-- Home Page : Bootstrap Carousel in Comunity-Section -->
    <script type="text/javascript">
      jQuery(document).ready(function($) {

        $('#myCarousel').carousel({
                interval: 5000
        });

        //Handles the carousel thumbnails
       $('[id^=carousel-selector-]').click( function(){
            var id = this.id.substr(this.id.lastIndexOf("-") + 1);
            var id = parseInt(id);
            $('#myCarousel').carousel(id);
        });

        // Text
        $('#carousel-text').html($('#slide-content-0').html());

        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
                 var id = $('.item.active').data('slide-number');
                $('#carousel-text').html($('#slide-content-'+id).html());
        });
      });
    </script>

    <!-- Product Page : Product Filter -->
    <script type="text/javascript">
      $(function() {
        var selectedClass = "";
          $(".fil-cat").click(function(){
            selectedClass = $(this).attr("data-rel");

            $("#product-hilight").fadeTo(200, 0.1);

            $("#product-hilight li").not("."+selectedClass).fadeOut().removeClass('scale-anm');

            setTimeout(function() {

              $("."+selectedClass).fadeIn().addClass('scale-anm');

              $("#product-hilight").fadeTo(400, 1);
            }, 400);

          });
      });
    </script>

    <script src="<?=base_url();?>assets/js/updateview.js"></script>

    <script src="<?=base_url();?>assets/frontend/owlslide/owl.carousel.js"></script>

    <script>
      var owl = $('.owl-carousel');
      owl.owlCarousel({
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                navText: ["<",">"],
                loop:true
            },
            600:{
                items:2,
                navText: ["<",">"],
                loop:true
            },
            800:{
                items:3,
                navText: ["<",">"],
                loop:true
            },
            1280:{
                items:4,
                navText: ["<",">"],
                loop:true
            }
        },
        autoplay:true,
        autoWidth:false,
        autoHeight:false,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        nav: true
      });

      owl.trigger('refresh.owl.carousel');
    </script>
  </body>
</html>
