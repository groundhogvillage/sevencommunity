<br/><br/>
<div class="container">
	<?php if($canchange==true) { ?>
	<form name="resetpassword" id="resetpassword" method="post" action="<?=base_url();?>index/changepassword">
		<div class="row">
			<div class="col-md-12">
				<h2>Reset Your Password / รีเซ็ตรหัสผ่าน</h2>
				<hr/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="margin-top:30px;margin-bottom:20px">
				<div class="form-group">
				    <label for="inputPassword" class="col-sm-2 control-label"> <font color="red">*</font>New Password : </label>
				    <div class="col-sm-10">
				      <input type="password" class="form-control" id="inputPassword" name="Password" placeholder="Password" required>
				    </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				    <label for="reinputPassword" class="col-sm-2 control-label"> <font color="red">*</font>Confirm Password : </label>
				    <div class="col-sm-10">
				      <input type="password" class="form-control" id="reinputPassword" name="rePassword" placeholder="rePassword" required>
				    </div>
				</div>
			</div>
		</div>

		<!--<div id="pswd_info" class="row" style="margin-top:30px;">
			<div class="col-sm-12">
		   		<h5>Password must meet the following requirements:</h5>
			</div>
	        <div class="col-sm-3 invalid" id="passletter">At least <strong>one letter</strong></div>
	        <div class="col-sm-3 invalid" id="passlettercap">At least <strong>one capital letter</strong></div>
	        <div class="col-sm-3 invalid" id="passnumber">At least <strong>one number</strong></div>
	        <div class="col-sm-3 invalid" id="passchar" >Be at least <strong>6 characters</strong> and maximum <strong>14 characters</strong></div>
	        <div class="col-sm-6 invalid" id="passspecial">At least <strong>one special letter (as if @ # ! %)</strong></div>
	        <div class="col-sm-6 invalid" id="passmatch">Match Password</strong></div>
		</div>-->

		<div class="row">
			<div class="col-md-12">
				<input type="hidden" name="userid" value="<?=$user['User_ID'];?>">
				<input type="hidden" name="gen" value="<?=$_GET['gen'];?>">
			</div>
		</div>
		<br/><br/>
		<div class="row">
			<div class="col-sm-6">
				<input type="Submit" value="Reset" name="Reset" class="btn btn-success btn-lg btn-block">
			</div>
			<div class="col-sm-6">
				<input type="button" value="Cancel" onclick="resetform()" class="btn btn-danger btn-lg btn-block">
			</div>
		</div>
	</form>
	<br/><br/>
	<!--<div class="row">
			<div class="col-md-12">
				<h4>ข้อกำหนดในการตั้งรหัสผ่าน</h4>
			</div>
			<div class="col-md-12">
				- รหัสผ่านความยาวอย่างน้อย 6 ตัวอักษร แต่ไม่เกิน 14 ตัวอักษร
			</div>
			<div class="col-md-12">
				- ประกอบไปด้วยอัคระ A-Z, a-z, 0-9, และอัครพิเศษอื่นๆ เช่น @ # ! % เป็นต้น
			</div>
			<div class="col-md-12">
				- ตัวอักษรพิมพ์ใหญ่และพิมพ์เล็กแตกต่างกัน
			</div>
	</div>-->

	<?php }else { ?>
		<center><h4><?=$response;?></h4></center>
	<?php } ?>
</div>

<script type="text/javascript">
       var url = "<?=base_url('assets/js/resetpasswordfromemail.js');?>";
        $.getScript(url);
</script>
