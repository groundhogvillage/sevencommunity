<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="เว็บไซต์7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต|7 Community Based Tourism Routes at Phuket">

    <title>
      7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต|7 Community Based Tourism Routes @ Phuket
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/frontend/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?=base_url();?>assets/frontend/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don"t actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?=base_url();?>assets/frontend/js/ie-emulation-modes-warning.js"></script>

    <!-- CSS Add more -->
    <link href="<?=base_url();?>assets/frontend/css/style.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/frontend/css/community.css" rel="stylesheet">

    <!-- FontFace -->
    <link href="<?=base_url();?>assets/frontend/css/fontface/fontface.css" rel="stylesheet">

    <!-- ICON Font Awesome -->
    <link href="<?=base_url();?>assets/frontend/css/font-awesome.min.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="<?=base_url();?>assets/frontend/img/image_section/favicon7.ico">

    <link href="<?=base_url("assets/owlslide/assets/owl.carousel.css");?>" rel="stylesheet">

  </head>

  <body>
    <!-- Fixed Navbar on top -->
    <nav class="navbar fixed-navbar navbar-community">
      <div class="container">
        <div class="navbar-header page-scroll" role="navigation">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Fixed-navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="hidden-lg navbar-brand" href="<?=base_url();?>index">
            <img class="img-responsive logo" src="<?=base_url();?>assets/frontend/img/image_section/logo_webmobile-sm.png">
          </a>
        </div>
        <div class="collapse navbar-collapse page-scroll navbar-community" id="Fixed-navbar">
          <ul class="nav navbar-nav inside-navbar">
            <li style="padding-right: 20px;">
              <a class="page-scroll hidden-md hidden-sm hidden-xs" href="<?=base_url();?>index">
                <img src="<?=base_url();?>assets/frontend/img/image_section/home-btn.png">
              </a>
            </li>
            <li style="padding-right: 30px;" <?php if($_GET['commid']=="6"){ echo 'class="active"'; } ?>>
              <a class="page-scroll" href="<?=base_url();?>index/community?commid=6">ชุมชนบ้าน<br/>ท่าฉัตรไชย</a>
            </li>
            <li  style="padding-right: 30px;" <?php if($_GET['commid']=="2"){ echo 'class="active"'; } ?>>
              <a class="page-scroll" href="<?=base_url();?>index/community?commid=2">ชุมชน<br/>ตำบลป่าคลอก</a>
            </li >
            <li style="padding-right: 30px;" <?php if($_GET['commid']=="3"){ echo 'class="active"'; } ?>>
              <a class="page-scroll" href="<?=base_url();?>index/community?commid=3">ชุมชนบ้านแขนน<br/>(หมู่บ้านวัฒนธรรมถลาง)</a>
            </li >
            <li  style="padding-right: 30px;" <?php if($_GET['commid']=="7"){ echo 'class="active"'; } ?>>
              <a class="page-scroll" href="<?=base_url();?>index/community?commid=7">ชุมชน<br/>ย่านเมืองภูเก็ต</a>
            </li>
            <li style="padding-right: 30px;" <?php if($_GET['commid']=="5"){ echo 'class="active"'; } ?>>
              <a class="page-scroll" href="<?=base_url();?>index/community?commid=5">ชุมชน<br/>บ้านบางเทา-เชิงทะเล</a>
            </li>
            <li style="padding-right: 30px;" <?php if($_GET['commid']=="8"){ echo 'class="active"'; } ?>>
              <a class="page-scroll" href="<?=base_url();?>index/community?commid=8">ชุมชน<br/>ตำบลกมลา</a>
            </li >
            <li  style="padding-right: 30px;" <?php if($_GET['commid']=="4"){ echo 'class="active"'; } ?>>
              <a class="page-scroll" href="<?=base_url();?>index/community?commid=4">ชุมชนตำบลราไวย์<br/>(เกาะโหลน)</a>
            </li>
          </ul>
        </div>    <!-- END Navbar-Collapse -->
      </div>    <!-- END Container -->
    </nav>    <!-- END Fixed Navbar on top -->
    <!-- NAVBAR -->
    <input type="hidden" name="commid" id="commid" value="<?=$commid;?>">
    <!-- Before-fixed navbar on top -->
    <div id="before-fixed-navbar">
      <!-- Navigation For Download Page -->
      <nav class="navbar navbar-community" role="navigation">
        <div class="container">
          <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#community-navbar">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="hidden-lg navbar-brand" href="<?=base_url();?>index">
              <img class="img-responsive logo" style="margin-top: -15px;" src="<?=base_url();?>assets/frontend/img/image_section/logo_webmobile-sm.png">
            </a>
          </div>
          <div class="collapse navbar-collapse page-scroll navbar-community" id="community-navbar">
            <ul class="nav navbar-nav inside-navbar">
              <li  style="padding-right: 20px;">
                <a class="page-scroll hidden-md hidden-sm hidden-xs" href="<?=base_url();?>index">
                  <img src="<?=base_url();?>assets/frontend/img/image_section/home-btn.png">
                </a>
              </li>
              <li  style="padding-right: 30px;" <?php if($_GET['commid']=="6"){ echo 'class="active"'; } ?>>
                <a class="page-scroll" href="<?=base_url();?>index/community?commid=6">ชุมชนบ้าน<br/>ท่าฉัตรไชย</a>
              </li>
              <li  style="padding-right: 30px;" <?php if($_GET['commid']=="2"){ echo 'class="active"'; } ?>>
                <a class="page-scroll" href="<?=base_url();?>index/community?commid=2">ชุมชน<br/>ตำบลป่าคลอก</a>
              </li >
              <li style="padding-right: 30px;" <?php if($_GET['commid']=="3"){ echo 'class="active"'; } ?>>
                <a class="page-scroll" href="<?=base_url();?>index/community?commid=3">ชุมชนบ้านแขนน<br/>(หมู่บ้านวัฒนธรรมถลาง)</a>
              </li >
              <li  style="padding-right: 30px;" <?php if($_GET['commid']=="7"){ echo 'class="active"'; } ?>>
                <a class="page-scroll" href="<?=base_url();?>index/community?commid=7">ชุมชน<br/>ย่านเมืองภูเก็ต</a>
              </li>
              <li style="padding-right: 30px;" <?php if($_GET['commid']=="5"){ echo 'class="active"'; } ?>>
                <a class="page-scroll" href="<?=base_url();?>index/community?commid=5">ชุมชน<br/>บ้านบางเทา-เชิงทะเล</a>
              </li>
              <li style="padding-right: 30px;" <?php if($_GET['commid']=="8"){ echo 'class="active"'; } ?>>
                <a class="page-scroll" href="<?=base_url();?>index/community?commid=8">ชุมชน<br/>ตำบลกมลา</a>
              </li >
              <li  style="padding-right: 30px;" <?php if($_GET['commid']=="4"){ echo 'class="active"'; } ?>>
                <a class="page-scroll" href="<?=base_url();?>index/community?commid=4">ชุมชนตำบลราไวย์<br/>(เกาะโหลน)</a>
              </li>
            </ul>
          </div>    <!-- END Navbar-Collapse -->
        </div>    <!-- END Container -->
      </nav>    <!-- END Navigation For Download Page -->
    </div>    <!-- END Before-fixed navbar on top -->

    <!-- END NAVBAR -->

    <!-- CONTENT -->

    <!-- About Community -->
    <section class="container-fluid community_cover" id="community_cover<?=$_GET['commid'];?>">
      <span class="text_hoverlay">
        <h2 class="section-heading">ชุมชนท่องเที่ยว</h2>

        <h2 class="section-heading text-left"><?=$page['name'];?></h2>
        <hr/>
          <p>
            <div id="onmore600">
              <?=iconv_substr(strip_tags($page['description']),0,155, "UTF-8")."...";?>
            </div>
            <div id="onmore300">
              <?=iconv_substr(strip_tags($page['description']),0,0, "UTF-8")."...";?>
            </div>
            <div id="onless300">

            </div>
          </p>
          <div class="card-header" role="tab" id="headingTwo">
            <h5 class="section-heading">
              <a style="color:white;text-align:right !important;" id="shmaincontent" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                MORE>>
              </a>
            </h5>
          </div>
      </span>

      <!-- Full image with text -->
    </section>
    <!-- More-Section : Card-Block -->
      <div class="bg-card-block">
        <div class="container">
          <div class="row">
            <!-- MORE>> Button in About-Section -->
            <div class="col-lg-8 col-lg-offset-2 text-center">
              <div id="accordion" role="tablist" aria-multiselectable="true">
                <div class="card">
                  <!-- Content of MORE>> Button in About-Section -->
                  <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="card-block padding-card-block">
                      <?=$page['description'];?>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>   <!-- END Content in More-Section -->
        </div>   <!-- END BG-Color in More-Section -->
      </div>
    <!-- END ABOUT COMMUNITY -->

    <!-- Google Map -->
    <section style="padding-top: 0 !important;">
      <div class="container-fluid" style="padding-left: 0 !important;padding-right: 0 !important;">

        <div id="map" style="width:100%;height:450px;"></div>

        <div id="box_on_map" style="">

          <ul id="product-hilight" style="padding-left:0;display:none;">
            <li class="pd-th pd-space_around_box">
              <div class="pd-product_box setonmappdbox">
                <div class="pd-box_picture">
                  <a data-toggle="modal" class="updateview" data-id="<?=$item[0]['id'];?>"  data-target="#Product-Modal<?=$item[0]['id'];?>">
                    <?php if($item[0]['picture_row']>0){ ?>
                      <img style="display: block; max-width: 100%;  height: auto;" src="<?=base_url();?>assets/upload/item/<?=$item[0]['picture'][0]['path'];?>">
                    <?php }else{
                      if($page['type']=="2")
                      {
                        $setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
                      }
                      else if($page['type']=="3")
                      {
                        $setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
                      }
                      else if($page['type']=="4")
                      {
                        $setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
                      }
                      else if($page['type']=="5")
                      {
                        $setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
                      }
                      else if($page['type']=="6")
                      {
                        $setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
                      }
                      else if($page['type']=="7")
                      {
                        $setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
                      }
                      else if($page['type']=="8")
                      {
                        $setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
                      }
                      else
                      {
                        $setnopic = base_url().'assets/images/nopic.jpg';
                      }
                      ?>
                      <img style="display: block; max-width: 100%;  height: auto;" src="<?=$setnopic;?>">
                    <?php } ?>
                  </a>
                </div>
                <div class="pd-box_detail_product text-center">
                  <div class="pd-caption">
                    <h3><?=$item[0]['name'];?></h3>
                    <hr style="margin-top: 5px;margin-bottom: 5px;">
                    <p>
                      <?
                        if(strlen($item[0]['description'])>0)
                        {
                          echo iconv_substr(strip_tags($item[0]['description']),0,100, "UTF-8")."...";
                        }
                        else
                        {
                          echo "";
                        }
                      ?>
                    </p>
                  </div>
                </div>
                  <h4 class="pd-box_btn_close"><a id="closebox_on_map">close</a></h4>
                  <h4 class="pd-box_btn_moremaps"><a data-toggle="modal" class="updateview" data-id="<?=$item[0]['id'];?>"  data-target="#Product-Modal<?=$item[0]['id'];?>">more>></a></h4>
              </div>
            </li>
          </ul>

        </div>

      </div>
    </section>
    <!-- END GOOGLE MAP -->


    <!-- Product Hilight -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">

            <ul id="product-hilight" style="padding-left:0;">
              <?php
              if($item_row>0)
              {
                foreach ($item as $key => $value) {
              ?>
              <li class="pd-th pd-space_around_box">
                <div class="pd-product_box_rate">
                  <div class="pd-box_picture">
                    <a data-toggle="modal" class="updateview" data-id="<?=$value['id'];?>" data-target="#Product-Modal<?=$value['id'];?>">
                      <?php if($value['picture_row']>0){ ?>
                        <img style="display: block; max-width: 100%;  height: auto;" src="<?=base_url();?>assets/upload/item/<?=$value['picture'][0]['path'];?>">
                        <?php }else{
                          if($page['type']=="2")
                          {
                            $setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
                          }
                          else if($page['type']=="3")
                          {
                            $setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
                          }
                          else if($page['type']=="4")
                          {
                            $setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
                          }
                          else if($page['type']=="5")
                          {
                            $setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
                          }
                          else if($page['type']=="6")
                          {
                            $setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
                          }
                          else if($page['type']=="7")
                          {
                            $setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
                          }
                          else if($page['type']=="8")
                          {
                            $setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
                          }
                          else
                          {
                            $setnopic = base_url().'assets/images/nopic.jpg';
                          }
                          ?>
                        <img style="display: block; max-width: 100%;  height: auto;" src="<?=$setnopic;?>">
                      <?php } ?>
                    </a>
                  </div>
                  <div class="pd-box_detail_product text-center">
                    <div class="pd-caption">
                    <h3><?=$value['name'];?></h3>
                    <hr style="margin-top: 5px;margin-bottom: 5px;">
                    <p>
                      <?
                        if(strlen($value['description'])>0)
                        {
                          echo iconv_substr(strip_tags($value['description']),0,100, "UTF-8")."...";
                        }
                        else
                        {
                          echo "";
                        }
                      ?>
                    </p>
                    </div>
                  </div>
                  <div class="pd-box_btn_more">
                    <h4><a data-toggle="modal" class="updateview" data-id="<?=$value['id'];?>" data-target="#Product-Modal<?=$value['id'];?>">more>></a></h4>
                  </div>
                  <?php
                    $updatedatetime = explode(" ",$value['update_datetime']);
                    $updatedate = explode("-",$updatedatetime[0]);
                  ?>
                  <!--<div style="margin-left:0px;display:inline-block">View : <?=number_format($value['visited'],0,".",",");?></div>
                  <div style="margin-right:0px;display:inline-block">Updated : <?=$updatedate[2]."/".$updatedate[1]."/".$updatedate[0];?></div>-->

                  <!--<div class="pd-tab-rate">
                    <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                    <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                    <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                    <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                    <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                  </div>-->
                </div>
              </li>
              <!-- Modal Product-Hilight-->
              <div class="modal fade" id="Product-Modal<?=$value['id'];?>" tabindex="-1" role="dialog" aria-labelledby="Product-Modal<?=$value['id'];?>" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- SLIDE IN MODAL -->
                    <div class="modal-header">

                      <div id="myCarousel<?=$value['id'];?>" class="carousel slide carousel-fit" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <?php
                          if($value['picture_row']>0)
                          {
                            foreach ($value['picture'] as $key2 => $value2) {
                          ?>
                          <li data-target="#myCarousel<?=$value['id'];?>" data-slide-to="<?=$key2;?>" <?php if($key2==0){echo 'class="active"'; }?>></li>
                          <?php
                            }
                          }
                          else
                          {
                          ?>
                            <li data-target="#myCarousel<?=$value['id'];?>" data-slide-to="0" class="active"></li>
                          <?php
                          }
                          ?>
                        </ol>


                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                          <?php
                          if($value['picture_row']>0)
                          {
                            foreach ($value['picture'] as $key2 => $value2) {
                              if($key2==0){ $activethis = "active"; }else{ $activethis = ""; }
                          ?>
                          <div class="item <?=$activethis;?>">
                            <img class="img-responsive" src="<?=base_url();?>assets/upload/item/<?=$value2['path'];?>" alt="">
                            <div class="carousel-caption">
                              <!--<h3><?=$value['name'];?></h3>
                              <p><?=$value['description'];?></p>-->
                            </div>
                          </div>
                          <?php
                            }
                          }else{
                              if($page['type']=="2")
                              {
                                $setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
                              }
                              else if($page['type']=="3")
                              {
                                $setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
                              }
                              else if($page['type']=="4")
                              {
                                $setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
                              }
                              else if($page['type']=="5")
                              {
                                $setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
                              }
                              else if($page['type']=="6")
                              {
                                $setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
                              }
                              else if($page['type']=="7")
                              {
                                $setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
                              }
                              else if($page['type']=="8")
                              {
                                $setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
                              }
                              else
                              {
                                $setnopic = base_url().'assets/images/nopic.jpg';
                              }
                            ?>
                            <div class="item active">
                              <img class="img-responsive" src="<?=$setnopic;?>" alt="">
                              <div class="carousel-caption">
                                <!--<h3>First slide label</h3>
                                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>-->
                              </div>
                            </div>
                            <?php
                          }
                          ?>
                        </div>

                      </div> <!--end mycarusel-->

                      <div class="pd-tab-header-carousel"><?=$value['name'];?></div>
                    </div>

                    <!-- CONTENT -->
                    <div class="modal-body">

                      <p class="text-left">
                        <?=$value['description'];?>
                      </p>
                    </div>
                    <div class="modal-footer">
                      <center>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </center>
                    </div>
                  </div>    <!-- END MODAL CONTENT -->
                </div>    <!-- END MODAL DIALOG -->
              </div>    <!-- END MODAL -->
              <?php
                }
              }
              ?>


            </ul>

            <!-- MORE>> Button in Product-Section
            <div class="col-lg-12 text-right">
              <h3>
                <a style="color:#210403;" href="#">
                  More>>
                </a>
              </h3>
            </div>  -->

          </div>
        </div>
      </div>
    </section>
    <!-- END PRODUCT HILIGHT -->
    <?php
    if($page['type']=="2")
    {
      $setmappic = base_url().'assets/frontend/img/map_download/06_map_paklog_web-01.jpg';
      $setpdf = base_url().'assets/frontend/img/map_download/pdf/map_paklog.pdf';
      $ttd = "ชุมชนตำบลป่าคลอก Tambol Pa Klog Community";
    }
    else if($page['type']=="3")
    {
      $setmappic = base_url().'assets/frontend/img/map_download/02_map_kanan_web-01.jpg';
      $setpdf = base_url().'assets/frontend/img/map_download/pdf/map_kanan.pdf';
      $ttd = "ชุมชนบ้านแขนน(หมู่บ้านวัฒนธรรมถลาง) Baan Kanan Community(Thalang Cultural and Traditional village)";
    }
    else if($page['type']=="4")
    {
      $setmappic = base_url().'assets/frontend/img/map_download/01_map_lone_web-01.jpg';
      $setpdf = base_url().'assets/frontend/img/map_download/pdf/map_lone.pdf';
      $ttd = "ชุมชนบ้านแขนน(หมู่บ้านวัฒนธรรมถลาง) Baan Kanan Community(Thalang Cultural and Traditional village)";
    }
    else if($page['type']=="5")
    {
      $setmappic = base_url().'assets/frontend/img/map_download/03_map_cherngtalay_web-01.jpg';
      $setpdf = base_url().'assets/frontend/img/map_download/pdf/map_bangtao_cherng_talay.pdf';
      $ttd = "ชุมชนบ้านบางเทา-เชิงทะเล Baan Bangtao-Cherng Talay Community";
    }
    else if($page['type']=="6")
    {
      $setmappic = base_url().'assets/frontend/img/map_download/04_map_thachatchai_web-01.jpg';
      $setpdf = base_url().'assets/frontend/img/map_download/pdf/map_thachatchai.pdf';
      $ttd = "ชุมชนบ้านท่าฉัตรไชย Baan Tha Chatchai Community";
    }
    else if($page['type']=="7")
    {
      $setmappic = base_url().'assets/frontend/img/map_download/05_map_oldphuket_web-01.jpg';
      $setpdf = base_url().'assets/frontend/img/map_download/pdf/map_oldphuket.pdf';
      $ttd = "ชุมชนย่านเมืองเก่าภูเก็ต The Old Phuket Town Community";
    }
    else if($page['type']=="8")
    {
      $setmappic = base_url().'assets/frontend/img/map_download/07_map_kamala_web-01.jpg';
      $setpdf = base_url().'assets/frontend/img/map_download/pdf/map_kamala.pdf';
      $ttd = "ชุมชนตำบลกมลา Tambol Kamala Community";
    }
    else
    {
      $setmappic = base_url().'assets/images/nopic.jpg';
      $setpdf = base_url().'assets/images/nopic.jpg';
      $ttd = "";
    }
    ?>
    <!-- Map Design -->
    <section class="padd-top-section">
      <div class="container">
        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="<?=$setpdf;?>" title="<?=$ttd;?>">
              <div class="pdf-thumb-box-overlay">
                <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/image_section/download-pdf.png">
              </div>
              <img class="img-responsive" src="<?=$setmappic;?>" title="<?=$ttd;?>" alt="<?=$ttd;?>">
            </a>
          </div>
          </center>
          <hr class="community">
        </div>
      </div>
    </section>
    <!-- END MAP-DESIGN -->

    <!-- Community Contact -->
    <secton>
      <div class="container">
        <div class="row">
        <!-- Header -->
          <div class="col-lg-12">
            <h2 class="section-heading text-left" style="text-align: left;">ติดต่อชุมชน</h2>
            <br/><br/>
            <!--<h2 class="section-heading text-left">Community Contact</h2>-->
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <?php if($agent['content_row']>0){
                  foreach ($agent['content'] as $key => $value) {
           ?>
          <div class="col-xs-12 col-sm-6">
            <div class="team-member">
              <div class="col-xs-12 col-sm-6 ">
                <?php if($value['path']==NULL||$value['path']==""){ ?>
                <img class="img-responsive img-circle" src="<?=base_url();?>assets/frontend/img/image_section/community-contact.png">
                <?php }else{ ?>
                <img class="img-responsive img-circle" src="<?=base_url();?>assets/upload/picture/<?=$value['path'];?>">
                <?php } ?>
              </div>
              <div class="col-xs-12 col-sm-6 text-left">
                <h4><?=$value['name'];?></h4>
                <?=$value['description'];?>
              </div>
            </div>
          </div>    <!-- END CommunityContact 1 -->
          <?php } } ?>
        </div>
      </div>

    </secton>
    <!-- END COMMUNITY CONTACT -->

	  <!-- END CONTENT -->
    <input type="hidden" name="location" value="">
    <!-- Footer -->
    <footer class="margin-top-on-footer">
      <div class="footer-bottom">
        <div class="container">
          <p class="mobile-center pull-left">
            ©2017 <a href="https://phuketcommunitytourism.com">7COMMUNITY BASED TOURISM ROUTES AT PHUKET</a>
          </p>
          <div class="pull-right">
            <ul class="mobile-center social">
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn1.png"> </a> </li>
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn2.png"> </a> </li>
              <li> <a href="<?=base_url()?>index/login_page"> <img src="<?=base_url();?>assets/frontend/img/image_section/login-btn.png"> </a> </li>
            </ul>
          </div>
        </div>
      </div>    <!-- FOOTER-BOTTOM-->
    </footer>
    <!-- END FOOTER -->

    <!-- Bootstrap core JavaScript -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <script src="<?=base_url();?>assets/frontend/js/jquery.min.js"></script>
    <!--  <script>window.jQuery || document.write("<script src="js/jquery.min.js"><\/script>")</script> -->
    <script src="<?=base_url();?>assets/frontend/js/bootstrap.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?=base_url();?>assets/frontend/js/ie10-viewport-bug-workaround.js"></script>
    <!-- Smooth Scrolling Page
    <script src="<?=base_url();?>assets/frontend/js/jQuery.scrollSpeed.js"></script> -->
    <!-- Scrolling Nav Easing JavaScript -->
    <script src="<?=base_url();?>assets/frontend/js/jquery.easing.min.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/community.js"></script>
    <!-- Community Page : Google Map -->
    <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6I2hekRwWIiD6O0WN3UUFU_p2kAY0pPQ"  type="text/javascript"></script>
    <script src="<?=base_url();?>assets/frontend/js/googlemaps_point.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/navbartoogle.js"></script>
    <script src="<?=base_url();?>assets/js/updateview.js"></script>


    <!-- TEST JS Product Highlights -->
    <script>
  /*  (function(){
      $('.carousel-showmanymoveone .item').each(function(){
        var itemToClone = $(this);

        for (var i=1;i<4;i++) {
          itemToClone = itemToClone.next();

          // wrap around if at end of item collection
          if (!itemToClone.length) {
            itemToClone = $(this).siblings(':first');
          }

          // grab item, clone, add marker class, add to collection
          itemToClone.children(':first-child').clone()
            .addClass("cloneditem-"+(i))
            .appendTo($(this));
        }
      });
    }());*/
    </script>
    <!-- END TEST JS Product Highlights -->

    <script src="<?=base_url();?>assets/owlslide/owl.carousel.js"></script>

    <script>
    var owl = $('.owl-carousel');
    owl.owlCarousel({
      loop:true,
      margin:10,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:3,
              nav:false
          },
          1280:{
              items:4,
              nav:true,
              loop:false
          }
      },
      autoplay:true,
      autoWidth:false,
      autoHeight:false,
      autoplayTimeout:5000,
      autoplayHoverPause:true
    });

    owl.trigger('refresh.owl.carousel');
    </script>


  </body>
</html>
