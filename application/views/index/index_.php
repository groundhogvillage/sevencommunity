<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="เว็บไซต์7เส้นทางหลัก ท่องเที่ยวโดยชุมชนจังหวัดภูเก็ต|7 Community Based Tourism Routes at Phuket">

    <title>
      7เส้นทางหลัก ท่องเที่ยวโดยชุมชนจังหวัดภูเก็ต|7 Community Based Tourism Routes @ Phuket
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/frontend/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?=base_url();?>assets/frontend/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don"t actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?=base_url();?>assets/frontend/js/ie-emulation-modes-warning.js"></script>

    <!-- CSS Add more -->
    <link href="<?=base_url();?>assets/frontend/css/style.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/frontend/css/indexpage.css" rel="stylesheet">

    <!-- FontFace -->
    <link href="<?=base_url();?>assets/frontend/css/fontface/fontface.css" rel="stylesheet">

    <!-- ICON Font Awesome -->
    <link href="<?=base_url();?>assets/frontend/css/font-awesome.min.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="<?=base_url();?>assets/frontend/img/image_section/favicon7.ico">

  </head>

  <body>

    <!-- NAVBAR -->

    <!-- Before-fixed navbar on top -->
    <div id="before-fixed-navbar">

      <div id="hero-parallax">
        <!-- background -->
        <div class="layer-bg layer" data-depth="0.15" data-type="parallax"></div>
        <!-- sky -->
        <div class="layer-sky-center layer" data-depth="0.05" data-type="parallax"></div>
        <div class="layer-sky3-right layer" data-depth="0.09" data-type="parallax"></div>
        <div class="layer-sky3-left layer" data-depth="0.07" data-type="parallax"></div>
        <div class="layer-sky2-right layer" data-depth="0.19" data-type="parallax"></div>
        <div class="layer-sky2-left layer" data-depth="0.17" data-type="parallax"></div>
        <div class="layer-sky1-right layer" data-depth="0.24" data-type="parallax"></div>
        <div class="layer-sky1-left layer" data-depth="0.22" data-type="parallax"></div>
        <!-- mountain -->
        <div class="layer-mountain3-right layer" data-depth="0.24" data-type="parallax"></div>
        <div class="layer-mountain3-left layer" data-depth="0.23" data-type="parallax"></div>
        <div class="layer-mountain2-right layer" data-depth="0.22" data-type="parallax"></div>
        <div class="layer-mountain2-left layer" data-depth="0.21" data-type="parallax"></div>
        <div class="layer-mountain1-right layer" data-depth="0.20" data-type="parallax"></div>
        <div class="layer-mountain1-left layer" data-depth="0.19" data-type="parallax"></div>
        <!-- sea -->
        <div class="layer-sea layer" data-depth="0.20" data-type="parallax"></div>
        <!-- logo -->
        <div class="layer-logo layer" data-depth="0.10" data-type="parallax"></div>
        <!-- coconut -->
        <div class="layer-coconut-right layer" data-depth="0.80" data-type="parallax"></div>
        <div class="layer-coconut-left layer" data-depth="0.85" data-type="parallax"></div>
      </div>
      <div id="normalbg">
        <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/header_parallax/hero.jpg" style="width:1920px;">
      </div>
      <!--<div id="hero-mobile"></div>-->

            <!-- Navigation For Download Page -->
            <nav class="navbar navbar-index" role="navigation">
              <div class="container">
                <div class="navbar-header page-scroll">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#download-navbar">
                  <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="hidden-lg hidden-md hidden-sm navbar-brand" href="<?=base_url();?>index">
                    <img class="img-responsive logo" src="<?=base_url();?>assets/frontend/img/image_section/logo_webmobile-sm.png">
                  </a>
                </div>
                <div class="collapse navbar-collapse page-scroll navbar-index" id="download-navbar">
                  <ul class="nav navbar-nav inside-navbar">
                    <li class="hidden">
                      <a class="page-scroll" href="#page-top"></a>
                    </li>
                    <li class="active">
                      <a class="page-scroll" href="<?=base_url();?>index">Home</a>
                    </li>
                    <!--<li>
                      <a class="page-scroll" href="/#about">About</a>
                    </li>-->
                    <li>
                      <a class="page-scroll" href="<?=base_url();?>index/product">Product</a>
                    </li>
                    <li>
                      <a class="page-scroll" href="<?=base_url();?>index/community?commid=6">Community</a>
                    </li>
                    <li>
                      <a class="page-scroll" href="<?=base_url();?>index/download">Download</a>
                    </li>
                  </ul>
                </div>    <!-- END Navbar-Collapse -->
              </div>    <!-- END Container -->
            </nav>    <!-- END Navigation For Download Page -->
          </div>    <!-- END Before-fixed navbar on top -->

          <!-- END NAVBAR -->

    <!-- CONTENT -->

    <!-- About-Section ================================== -->
    <section class="section-about" style="margin-bottom: -1px;">

      <!-- Content in About-Section -->
      <div class="container text-center">

        <div class="col-md-8 col-md-offset-2">
          <p>
            <!--จังหวัดภูเก็ต เป็นจังหวัดที่มีเอกลักษณ์และมีวัฒนธรรมอันโดดเด่น มีแหล่งท่องเที่ยวสำคัญระดับโลก รวมทั้งความหลากหลายทางวัฒนธรรม ซึ่งสิ่งที่กล่าวนี้เป็นทรัพยากรการท่องเที่ยวที่สำคัญของจังหวัดภูเก็ต ปัจจุบันการท่องเที่ยวโดยชุมชน (Community Based Tourism: CBT) เป็นการบริหารจัดการและดำเนินการท่องเที่ยวโดยชุมชน เป็นการท่องเที่ยวประเภทหนึ่งที่นักท่องเที่ยวให้ความสนใจเพิ่มมากขึ้น ซึ่งการท่องเที่ยวชุมชนมีความเป็นเอกลักษณ์ และมีความน่าสนใจในการสะท้อนให้เห็นถึงวิถีชีวิต ศิลปวัฒนธรรม ประเพณีของชุมชน การท่องเที่ยวโดยชุมชน เป็นหนึ่งในการท่องเที่ยวอย่างยั่งยืน และการประชาสัมพันธ์คือสิ่งสำคัญที่เป็นสื่อกลางในการเผยแพร่ข้อมูลข่าวสารไปสู่สาธารณะชนให้นักท่องเที่ยวทั้งชาวไทยและชาวต่างประเทศเทศรับทราบข้อมูลกิจกรรมด้านการท่องเที่ยวของจังหวัดอย่างต่อเนื่อง ทันต่อสถานการณ์ และเป็นการกระตุ้นเศรษฐกิจในภาพรวมชุมชนจังหวัดภูเก็ต ดังนั้น เพื่อเป็นการส่งเสริมการท่องเที่ยวโดยชุมชน สำนักงานท่องเที่ยวและกีฬาจังหวัดภูเก็ต ได้เห็นถึงความสำคัญของการท่องเที่ยวโดยชุมชนในจังหวัดภูเก็ต ซึ่งเป็นอีกหนึ่งพื้นที่ที่ควรจะส่งเสริมให้เกิดการท่องเที่ยวโดยชุมชน การเข้าถึงแหล่งท่องเที่ยวต่าง ๆ จำเป็นต้องอาศัยแผนที่และสื่อในการดึงดูดความสนใจ โดยแสดงผลตำแหน่งที่ตั้งแหล่งท่องเที่ยวในระบบแผนที่ ร่วมกับการจัดทำเว็บไซต์ แอพพลิเคชั่นเพื่อแสดงผลแผนที่แหล่งท่องเที่ยวผ่านเครือข่ายอินเตอร์เน็ต เพื่อให้นักท่องเที่ยวสามารถเข้าถึงแหล่งท่องเที่ยวได้ง่ายขึ้น และสร้างช่องทางการกระจายรายได้สู่ชุมชนอย่างเป็นรูปธรรมมากขึ้น .....-->
            <?=$pageindex['description'];?>
          </p>
        </div>

        <!-- MORE>> Button in About-Section -->
        <div class="col-md-8 col-md-offset-2 card-header text-right" role="tab" id="headingTwo">
          <h5>
            <a style="color:#FFFBF6;" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              MORE>>
            </a>
          </h5>
        </div>
      </div>
    </section>
    <div class="bg-card-block" style="padding-bottom: 25px;">
      <div class="container">
        <div class="row">
          <!-- MORE>> Button in About-Section -->
          <div class="col-lg-8 col-lg-offset-2 text-center">
            <div id="accordion" role="tablist" aria-multiselectable="true">
              <div class="card">
                <!-- Content of MORE>> Button in About-Section -->
                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                  <?=$pageabout['description'];?>
                  <!--
                  <div class="card-block ">
                    <div class="card-block">
                    สมาชิก
                  </div>
                  <div class="card-block">
                    - นายสมพร แทนสกุล ( ชุมชนบ้านท่าฉัตรไชย )
                  </div>
                  <div class="card-block">
                    - นายประเสริฐ ฤิทธิ์รักษา ( ชุมชนตำบลป่าคลอก )
                  </div>
                  <div class="card-block">
                    - นางธัญลักษณ์ จริยะเลอพงษ์ ( ชุมชนบ้านแขนน(หมู่บ้านวัฒนธรรมถลาง) )
                  </div>
                  <div class="card-block">
                    - นายสมยศ ปาทาน ( ชุมชนย่านเมืองเก่าภูเก็ต )
                  </div>
                  <div class="card-block">
                    - นายสนธยา คงทิพย์ ( ชุมชนบ้านบางเทา-เชิงทะเล )
                  </div>
                  <div class="card-block">
                    - นายสยามพงษ์ คงราช ( ชุมชนตำบลกมลา )
                  </div>
                  <div class="card-block">
                    - นายทรงสิทธิ์ บุญผล ( ชุมชนราไวย์-เกาะโหลน )
                  </div>
                  <div class="card-block">
                    - ดร.อภิรมย์ พรหมจรรยา( ที่ปรึกษาโครงการ )
                  </div>
                  </div>
                -->
                </div>
              </div>
            </div>
          </div>

        </div>   <!-- END Content in More-Section -->
      </div>   <!-- END BG-Color in More-Section -->
    </div>
    <!-- END About-Section -->


      <!-- Product Hilight -->
      <section class="block3">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">

                  <div class="row">
                    <div class="col-lg-12">
                      <h2 class="font-product">PRODUCT HILIGHT</h2>
                      <img class="img-responsive img-product" src="<?=base_url();?>assets/frontend/img/image_section/product-line.png">
                    </div>
                  </div>

              <ul id="product-hilight" style="padding-left:0;">
                <?php
                if($item_row>0)
                {
                  foreach ($item as $key => $value) {
                ?>
                <li class="pd-th pd-space_around_box">
                  <div class="pd-product_box_rate">
                    <div class="pd-box_picture">
                      <a data-toggle="modal" data-target="#Product-Modal<?=$value['id'];?>">
                        <?php if($value['picture_row']>0){ ?>
                          <img style="display: block; max-width: 100%;  height: auto;" src="<?=base_url();?>assets/upload/item/<?=$value['picture'][0]['path'];?>">
                        <?php }else{ ?>
                          <img style="display: block; max-width: 100%;  height: auto;" src="<?=base_url();?>assets/images/nopic.jpg">
                        <?php } ?>
                      </a>
                    </div>
                    <div class="pd-box_detail_product text-center">
                      <div class="pd-caption">
                        <h3><?=$value['name'];?></h3>
                        <p><?=$value['description'];?></p>
                      </div>
                    </div>
                    <div class="pd-box_btn_more">
                      <h4><a data-toggle="modal" style="cursor:pointer;" data-target="#Product-Modal<?=$value['id'];?>">more>></a></h4>
                    </div>
                    <!--<div class="pd-tab-rate">
                      <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                      <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                      <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                      <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                      <img src="<?=base_url();?>assets/frontend/img/image_section/product-rate.png">
                    </div>-->
                  </div>
                </li>
                <!-- Modal Product-Hilight-->
                <div class="modal fade" id="Product-Modal<?=$value['id'];?>" tabindex="-1" role="dialog" aria-labelledby="Product-Modal<?=$value['id'];?>" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">

                      <!-- SLIDE IN MODAL -->
                      <div class="modal-header">

                        <div id="myCarousel<?=$value['id'];?>" class="carousel slide carousel-fit" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <?php
                            if($value['picture_row']>0)
                            {
                              foreach ($value['picture'] as $key2 => $value2) {
                            ?>
                            <li data-target="#myCarousel<?=$value['id'];?>" data-slide-to="<?=$key2;?>" <?php if($key2==0){echo 'class="active"'; }?>></li>
                            <?php
                              }
                            }
                            else
                            {
                            ?>
                              <li data-target="#myCarousel<?=$value['id'];?>" data-slide-to="0" class="active"></li>
                            <?php
                            }
                            ?>
                          </ol>


                          <!-- Wrapper for slides -->
                          <div class="carousel-inner">
                            <?php
                            if($value['picture_row']>0)
                            {
                              foreach ($value['picture'] as $key2 => $value2) {
                                if($key2==0){ $activethis = "active"; }else{ $activethis = ""; }
                            ?>
                            <div class="item <?=$activethis;?>">
                              <img class="img-responsive" src="<?=base_url();?>assets/upload/item/<?=$value2['path'];?>" alt="">
                            </div>
                            <?php
                              }
                            }
                            else
                            {
                              ?>
                              <div class="item active">
                                <img class="img-responsive" src="<?=base_url();?>assets/images/nopic.jpg" alt="">
                              </div>
                              <?php
                            }
                            ?>
                          </div>

                        </div> <!--end mycarusel-->

                        <div class="pd-tab-header-carousel"><?=$value['name'];?></div>
                      </div>

                      <!-- CONTENT -->
                      <div class="modal-body">

                        <p class="text-left">
                          <?=$value['description'];?>
                        </p>
                      </div>
                      <div class="modal-footer">
                        <center>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </center>
                      </div>
                    </div>    <!-- END MODAL CONTENT -->
                  </div>    <!-- END MODAL DIALOG -->
                </div>    <!-- END MODAL -->
                <?php
                  }
                }
                ?>


              </ul>

              <!-- MORE>> Button in Product-Section
              <div class="col-lg-12 text-right">
                <h3>
                  <a style="color:#210403;" href="#">
                    More>>
                  </a>
                </h3>
              </div>  -->

            </div>
          </div>
        </div>
      </section>
      <!-- END PRODUCT HILIGHT -->

    <!-- Comunity-Section -->
    <section class="section-community" id="community">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="font-head-community">COMMUNITY</h2>
            <img class="img-responsive img-head-community" src="<?=base_url();?>assets/frontend/img/image_section/community-line.png">
          </div>
        </div>

        <div class="container">
          <!-- Bootstrap Carousel -->
          <div class="row">
            <div class="col-xs-12" id="slider">
              <!-- Top part of the slider -->
              <div class="row carousel-fade">
                <!-- Image Box -->
                <div class="col-sm-12 col-md-8" id="carousel-img-box">
                  <div class="carousel slide" id="myCarousel">
                    <!-- Carousel inner -->
                    <div class="carousel-inner">

                      <!-- Img1 -->
                      <div class="item comm active" data-slide-number="0">
                        <img src="<?=base_url();?>assets/upload/picture/<?=$page1['picture'];?>">
                      </div>   <!-- END Img1 -->

                      <!-- Img2 -->
                      <div class="item comm " data-slide-number="1">
                        <img src="<?=base_url();?>assets/upload/picture/<?=$page2['picture'];?>">
                      </div>   <!-- END Img2 -->

                      <!-- Img3 -->
                      <div class="item comm " data-slide-number="2">
                        <img src="<?=base_url();?>assets/upload/picture/<?=$page3['picture'];?>">
                      </div>   <!-- END Img3 -->

                      <!-- Img4 -->
                      <div class="item comm " data-slide-number="3">
                        <img src="<?=base_url();?>assets/upload/picture/<?=$page4['picture'];?>">
                      </div>   <!-- END Img4 -->

                      <!-- Img5 -->
                      <div class="item comm " data-slide-number="4">
                        <img src="<?=base_url();?>assets/upload/picture/<?=$page5['picture'];?>">
                      </div>   <!-- END Img5 -->

                      <!-- Img6 -->
                      <div class="item comm " data-slide-number="5">
                        <img src="<?=base_url();?>assets/upload/picture/<?=$page6['picture'];?>">
                      </div>   <!-- END Img6 -->

                      <!-- Img7 -->
                      <div class="item comm " data-slide-number="6">
                        <img src="<?=base_url();?>assets/upload/picture/<?=$page7['picture'];?>">
                      </div>   <!-- END Img7 -->

                    </div><!-- END Carousel inner -->

                  </div>
                </div>   <!-- END Image Box -->

                <div class="col-sm-12 col-md-4" id="carousel-text">

                </div>   <!-- Auto Text Box -->

                <!-- Text in Auto Text Box -->
                <div id="slide-content" style="display: none;">
                  <div id="slide-content-0">
                    <h2><?=$page1['name'];?></h2>
                    <p class="foncommindex"><?=iconv_substr(strip_tags($page1['description']),0,500, "UTF-8")."...";?></p>
                    <p class="sub-text"><a href="<?=base_url();?>index/community?commid=<?=$page1['type'];?>">Read more</a></p>
                  </div>   <!-- END Text1 -->
                  <div id="slide-content-1">
                    <h2><?=$page2['name'];?></h2>
                    <p class="foncommindex"><?=iconv_substr(strip_tags($page2['description']),0,500, "UTF-8")."...";?></p>
                    <p class="sub-text"><a href="<?=base_url();?>index/community?commid=<?=$page2['type'];?>">Read more</a></p>
                  </div>   <!-- END Text1 -->
                  <div id="slide-content-2">
                    <h2><?=$page3['name'];?></h2>
                    <p class="foncommindex"><?=iconv_substr(strip_tags($page3['description']),0,500, "UTF-8")."...";?></p>
                    <p class="sub-text"><a href="<?=base_url();?>index/community?commid=<?=$page3['type'];?>">Read more</a></p>
                  </div>   <!-- END Text1 -->
                  <div id="slide-content-3">
                    <h2><?=$page4['name'];?></h2>
                    <p class="foncommindex"><?=iconv_substr(strip_tags($page4['description']),0,500, "UTF-8")."...";?></p>
                    <p class="sub-text"><a href="<?=base_url();?>index/community?commid=<?=$page4['type'];?>">Read more</a></p>
                  </div>   <!-- END Text1 -->
                  <div id="slide-content-4">
                    <h2><?=$page5['name'];?></h2>
                    <p class="foncommindex"><?=iconv_substr(strip_tags($page5['description']),0,500, "UTF-8")."...";?></p>
                    <p class="sub-text"><a href="<?=base_url();?>index/community?commid=<?=$page5['type'];?>">Read more</a></p>
                  </div>   <!-- END Text1 -->
                  <div id="slide-content-5">
                    <h2><?=$page6['name'];?></h2>
                    <p class="foncommindex"><?=iconv_substr(strip_tags($page6['description']),0,500, "UTF-8")."...";?></p>
                    <p class="sub-text"><a href="<?=base_url();?>index/community?commid=<?=$page6['type'];?>">Read more</a></p>
                  </div>   <!-- END Text1 -->
                  <div id="slide-content-6">
                    <h2><?=$page7['name'];?></h2>
                    <p class="foncommindex"><?=iconv_substr(strip_tags($page7['description']),0,500, "UTF-8")."...";?></p>
                    <p class="sub-text"><a href="<?=base_url();?>index/community?commid=<?=$page7['type'];?>">Read more</a></p>
                  </div>   <!-- END Text1 -->

                </div>   <!-- END Txt in Auto Text Box -->

                <a class="left carousel-control" style="background:none;width:auto;top:45%;" href="#myCarousel" role="button" data-slide="prev">
                  <img style="" src="<?=base_url();?>assets/frontend/img/image_section/carousel-right-btn.png">
                </a>
                <a class="right carousel-control" style="background:none;width:auto;top:45%;" href="#myCarousel" role="button" data-slide="next">
                  <img  src="<?=base_url();?>assets/frontend/img/image_section/carousel-left-btn.png">
                </a>

              </div>
            </div>
          </div>   <!-- END Bootstrap Carousel -->
        </div>
      </div>
    </section>
    <!-- END Comunity-Section -->


    <!-- Community Image Download : MAP-DESIGN -->
    <section class="section-download" id="download" style="background-color:#f4efe4;">
      <div class="container">

        <div class="row">
          <div class="col-lg-12">
            <h2 class="font-download">Download</h2>
            <img class="img-responsive img-download" src="<?=base_url();?>assets/frontend/img/image_section/download-line.png">
          </div>
        </div>

        <div class="row">
          <center>
          <div class="pdf-thumb-box">
            <a href="img/image_section/download-map-design.jpg" download>
              <div class="pdf-thumb-box-overlay">
                <img src="<?=base_url();?>assets/frontend/img/image_section/download-hover1.png">
              </div>
              <img class="img-responsive" src="<?=base_url();?>assets/frontend/img/image_section/download-map-design.jpg">
            </a>
          </div>
          </center>
        </div>
      </div>
    </section>
    <!-- END Community Image Download -->

    <!-- END CONTENT -->

    <!-- Footer -->
    <footer class="margin-top-on-footer">
      <div class="footer-bottom">
        <div class="container">
          <p class="mobile-center pull-left">
            ©2017 <a href="https://phuketcommunitytourism.com">7COMMUNITY BASED TOURISM ROUTES AT PHUKET</a>
          </p>
          <div class="pull-right">
            <ul class="mobile-center social">
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn1.png"> </a> </li>
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn2.png"> </a> </li>
            </ul>
          </div>
        </div>
      </div>    <!-- FOOTER-BOTTOM-->
    </footer>
    <!-- END FOOTER -->

    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url();?>assets/frontend/js/jquery.min.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/bootstrap.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?=base_url();?>assets/frontend/js/ie10-viewport-bug-workaround.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/indexpage.js"></script>
    <!-- Parallax -->
    <script src="<?=base_url();?>assets/frontend/js/parallax.js"></script>


    <!-- Smooth Scrolling Page
    <script src="js/jQuery.scrollSpeed.js"></script>
    <script>
      $(function() {
        jQuery.scrollSpeed(100, 800);
      });
    </script>
    -->

    <!-- Scrolling Nav Easing JavaScript -->
    <script src="<?=base_url();?>assets/frontend/js/jquery.easing.min.js"></script>


  </body>
</html>
