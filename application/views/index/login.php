
<div class="container">
  <?php if(isset($_GET['error'])&&$_GET['error']=="fail"){ ?>
    <div class="row">
      <div class="col-md-12" style="text-align:center;">
          <font color="red"><h4>Incorrect Username or Password</h4></font>
      </div>
    </div>
  <?php }elseif(isset($_GET['error'])&&$_GET['error']=="nologin"){ ?>
    <div class="row">
      <div class="col-md-12" style="text-align:center;">
          <font color="red"><h4>Please Login</h4></font>
      </div>
    </div>
  <?php }elseif(isset($_GET['error'])&&$_GET['error']=="inactive"){ ?>
    <div class="row">
      <div class="col-md-12" style="text-align:center;">
          <font color="red"><h4>Inactive user. Please wait admin active this account</h4></font>
      </div>
    </div>
  <?php } ?>
  <div class="row">
    <div class="col-md-12" style="text-align:center;">
        <h2>Login - Phuket Community Tourism</h2>
    </div>
  </div>
  <form name="login" action="<?=base_url();?>index/login" method="post">
  <div class="row">
    <div class="col-md-3"></div>
    <div class="form-group col-md-6">
      <label for="inputusername">Username</label>
      <input type="text" class="form-control" id="inputusername" placeholder="Username" name="username">
    </div>
    <div class="col-md-3"></div>
  </div>
  <div class="row">
    <div class="col-md-3"></div>
    <div class="form-group col-md-6">
      <label for="inputpassword">Password</label>
      <input type="password" class="form-control" id="inputpassword" placeholder="Password" name="password">
    </div>
   </div>
    <div class="col-md-3"></div>
  <div class="row" style="padding-top:20px;">
  <div class="col-md-3"></div>
    <div class="col-md-6" style="text-align:center;">
        <input type="submit" value="Login" class="btn btn-primary btn-lg btn-block">
        <br/>
        <!--<a style="text-decoration:none;" href="<?=base_url();?>index/signup"><input type="button" value="Sign Up" class="btn btn-info btn-lg btn-block"></a>
        <br/>-->
        <a style="text-decoration:none;" href="<?=base_url();?>index/forget"><input type="button" value="Forget Password?" class="btn btn-danger btn-lg btn-block"></a>
    </div>
  </div>

</form>
  </div>
