<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
  	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <meta name="description" content="เว็บไซต์7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต|7 Community Based Tourism Routes at Phuket">

    <title>
      7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต|7 Community Based Tourism Routes @ Phuket
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/frontend/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?=base_url();?>assets/frontend/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don"t actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?=base_url();?>assets/frontend/js/ie-emulation-modes-warning.js"></script>

    <!-- CSS Add more -->
    <link href="<?=base_url();?>assets/frontend/css/style.css" rel="stylesheet">

    <!-- FontFace -->
    <link href="<?=base_url();?>assets/frontend/css/fontface/fontface.css" rel="stylesheet">

    <!-- ICON Font Awesome -->
    <link href="<?=base_url();?>assets/frontend/css/font-awesome.min.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="<?=base_url();?>assets/frontend/img/image_section/favicon7.ico">

  </head>

  <body>

    <!-- NAVBAR -->

    <!-- Fixed Navbar on top -->
    <nav class="navbar fixed-navbar navbar-download">
      <div class="container">
        <div class="navbar-header page-scroll" role="navigation">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Fixed-navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="hidden-lg hidden-md hidden-sm navbar-brand" href="<?=base_url();?>index">
            <img class="img-responsive logo" src="<?=base_url();?>assets/frontend/img/image_section/logo_webmobile-sm.png">
          </a>
        </div>
        <div class="collapse navbar-collapse page-scroll navbar-download" id="Fixed-navbar">
          <ul class="nav navbar-nav inside-navbar">
            <li>
              <a class="" href="<?=base_url();?>index">หน้าแรก</a>
            </li>
            <!--<li>
              <a class="page-scroll" href="/#about">About</a>
            </li>-->
            <li class="active">
              <a class="page-scroll dropdown-toggle" href="#" data-toggle="dropdown">
                สถานที่ท่องเที่ยวโดยชุมชน <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li class="btn-product fil-cat active" href="" data-rel="all">รวมทั้ง เจ็ดชุมชนท่องเที่ยว</li>
                <li class="btn-product fil-cat" href="" data-rel="community6">ชุมชนบ้านท่าฉัตรไชย</li>
                <li class="btn-product fil-cat" href="" data-rel="community2">ชุมชนตำบลป่าคลอก</li>
                <li class="btn-product fil-cat" href="" data-rel="community3">ชุมชนบ้านแขนน-เทพกระษัตรี</li>
                <li class="btn-product fil-cat" href="" data-rel="community7">ชุมชนย่านเมืองภูเก็ต</li>
                <li class="btn-product fil-cat" href="" data-rel="community5">ชุมชนบ้านบางเทา-เชิงทะเล</li>
                <li class="btn-product fil-cat" href="" data-rel="community8">ชุมชนตำบลกมลา</li>
                <li class="btn-product fil-cat" href="" data-rel="community4">ชุมชนตำบลราไวย์(เกาะโหลน)</li>
              </ul>
            </li>
            <li>
              <a class="" href="<?=base_url();?>index/community?commid=6">เจ็ดชุมชนท่องเที่ยว</a>
            </li>
            <li>
              <a class="" href="<?=base_url();?>index/download">ดาวน์โหลดแผนที่ท่องเที่ยว</a>
            </li>
          </ul>
        </div>    <!-- END Navbar-Collapse -->
      </div>    <!-- END Container -->
    </nav>    <!-- END Fixed Navbar on top -->

    <!-- Before-fixed navbar on top -->
    <div id="before-fixed-navbar">
      <!-- Navigation For Download Page -->
      <nav class="navbar navbar-download" role="navigation">
        <div class="container">
          <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#download-navbar">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="hidden-lg hidden-md hidden-sm navbar-brand" href="<?=base_url();?>index">
              <img class="img-responsive logo" src="<?=base_url();?>assets/frontend/img/image_section/logo_webmobile-sm.png">
            </a>
          </div>
          <div class="collapse navbar-collapse page-scroll navbar-download" id="download-navbar">
            <ul class="nav navbar-nav inside-navbar">
              <li class="hidden">
                <a class="page-scroll" href="#page-top"></a>
              </li>
              <li>
              <a class="" href="<?=base_url();?>index">หน้าแรก</a>
            </li>
            <!--<li>
              <a class="page-scroll" href="/#about">About</a>
            </li>-->
            <li class="active">
              <a class="page-scroll dropdown-toggle" href="#" data-toggle="dropdown">
                สถานที่ท่องเที่ยวโดยชุมชน <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li class="btn-product fil-cat active" href="" data-rel="all">รวมทั้งเจ็ดชุมชนท่องเที่ยว</li>
                <li class="btn-product fil-cat" href="" data-rel="community6">ชุมชนบ้านท่าฉัตรไชย</li>
                <li class="btn-product fil-cat" href="" data-rel="community2">ชุมชนตำบลป่าคลอก</li>
                <li class="btn-product fil-cat" href="" data-rel="community3">ชุมชนบ้านแขนน-เทพกระษัตรี</li>
                <li class="btn-product fil-cat" href="" data-rel="community7">ชุมชนย่านเมืองภูเก็ต</li>
                <li class="btn-product fil-cat" href="" data-rel="community5">ชุมชนบ้านบางเทา-เชิงทะเล</li>
                <li class="btn-product fil-cat" href="" data-rel="community8">ชุมชนตำบลกมลา</li>
                <li class="btn-product fil-cat" href="" data-rel="community4">ชุมชนตำบลราไวย์(เกาะโหลน)</li>
              </ul>
            </li>
            <li>
              <a class="" href="<?=base_url();?>index/community?commid=6">เจ็ดชุมชนท่องเที่ยว</a>
            </li>
            <li>
              <a class="" href="<?=base_url();?>index/download">ดาวน์โหลดแผนที่ท่องเที่ยว</a>
            </li>
            </ul>
          </div>    <!-- END Navbar-Collapse -->
        </div>    <!-- END Container -->
      </nav>    <!-- END Navigation For Download Page -->
    </div>    <!-- END Before-fixed navbar on top -->

    <!-- END NAVBAR -->


    <!-- CONTENT -->

    <section class="product-section" id="product">
      <div class="container">

        <div class="row">
          <div class="col-lg-12">

            <ul id="product-hilight" style="padding-left:0;">

              <?php foreach ($item as $key => $value) { ?>
              <?php
                if($value['type']=="2")
                {
                  $setcommname = "ชุมชนป่าคลอก";
                }
                else if($value['type']=="3")
                {
                  $setcommname = "ชุมชนบ้านแขนน";
                }
                else if($value['type']=="4")
                {
                  $setcommname = "ชุมชนเกาะโหลน";
                }
                else if($value['type']=="5")
                {
                  $setcommname = "ชุมชนเชิงทะเล";
                }
                else if($value['type']=="6")
                {
                  $setcommname = "ชุมชนท่าฉัตรไชย";
                }
                else if($value['type']=="7")
                {
                  $setcommname = "ชุมชนเมืองเก่าภูเก็ต";
                }
                else if($value['type']=="8")
                {
                  $setcommname = "ชุมชนกมลา";
                }
                else
                {
                  $setcommname = "ชุมชน...";
                }
              ?>
              <li class="pd-th pd-space_around_box tile scale-anm community<?=$value['type'];?> all">
                <div class="pd-product_box">
                  <div class="pd-box_picture">
                    <a data-toggle="modal" data-target="#Product-Modal<?=$value['id'];?>" class="updateview" data-id="<?=$value['id'];?>">
                      <?php if($value['picture_row']>0){ ?>
                        <img class="img-box_picture img-box img-center" src="<?=base_url();?>assets/upload/item/<?=$value['picture'][0]['path'];?>">
                      <?php }else{
                        if($value['type']=="2")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
                        }
                        else if($value['type']=="3")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
                        }
                        else if($value['type']=="4")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
                        }
                        else if($value['type']=="5")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
                        }
                        else if($value['type']=="6")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
                        }
                        else if($value['type']=="7")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
                        }
                        else if($value['type']=="8")
                        {
                          $setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
                        }
                        else
                        {
                          $setnopic = base_url().'assets/images/nopic.jpg';
                        }
                        ?>
                        <img class="img-box_picture img-box img-center" src="<?=$setnopic;?>">
                      <?php } ?>
                    </a>
                  </div>
                  <div class="pd-box_detail_product text-center">
                    <div class="pd-caption">
                      <h3><?=$setcommname;?></h3>
                      <h3><?=$value['name'];?></h3>
                      <hr class="line-pd-caption">
                      <p>
                        <?
                          if(strlen($value['description'])>0)
                          {
                            echo iconv_substr(strip_tags($value['description']),0,100, "UTF-8")."...";
                          }
                          else
                          {
                            echo "";
                          }
                        ?>
                      </p>
                    </div>
                  </div>
                  <div class="pd-box_btn_more">
                    <h4><a data-toggle="modal" style="font-size:22px; cursor:pointer; color:#281b1c;" class="updateview" data-id="<?=$value['id'];?>" data-target="#Product-Modal<?=$value['id'];?>">อ่านต่อ>></a></h4>
                  </div>
                  <div style="margin-left:0px;display:inline-block">View : <?=$value['visited'];?></div>
                </div>
              </li>
              <!-- Modal Product-Hilight-->
              <div class="modal fade" id="Product-Modal<?=$value['id'];?>" tabindex="-1" role="dialog" aria-labelledby="Product-Modal<?=$value['id'];?>" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- SLIDE IN MODAL -->
                    <div class="modal-header">

                      <div id="myCarousel<?=$value['id'];?>" class="carousel slide carousel-fit" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <?php
                          if($value['picture_row']>0)
                          {
                            foreach ($value['picture'] as $key2 => $value2) {
                          ?>
                          <li data-target="#myCarousel<?=$value['id'];?>" data-slide-to="<?=$key2;?>" <?php if($key2==0){echo 'class="active"'; }?>></li>
                          <?php
                            }
                          }
                          else
                          {
                          ?>
                            <li data-target="#myCarousel<?=$value['id'];?>" data-slide-to="0" class="active"></li>
                          <?php
                          }
                          ?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                          <?php
                          if($value['picture_row']>0)
                          {
                            foreach ($value['picture'] as $key2 => $value2) {
                              if($key2==0){ $activethis = "active"; }else{ $activethis = ""; }
                          ?>
                          <div class="item <?=$activethis;?>">
                            <img class="img-responsive" src="<?=base_url();?>assets/upload/item/<?=$value2['path'];?>" alt="">
                            <div class="carousel-caption">
                              <!--<h3><?=$value['name'];?></h3>
                              <p><?=$value['description'];?></p>-->
                            </div>
                          </div>
                          <?php
                            }
                          }
                          else
                          {
                            if($value['type']=="2")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm2.jpg';
                            }
                            else if($value['type']=="3")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm3.jpg';
                            }
                            else if($value['type']=="4")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm4.jpg';
                            }
                            else if($value['type']=="5")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm5.jpg';
                            }
                            else if($value['type']=="6")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm6.jpg';
                            }
                            else if($value['type']=="7")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm7.jpg';
                            }
                            else if($value['type']=="8")
                            {
                              $setnopic = base_url().'assets/frontend/img/community_image/comm8.jpg';
                            }
                            else
                            {
                              $setnopic = base_url().'assets/images/nopic.jpg';
                            }
                            ?>
                            <div class="item active">
                              <img class="img-responsive" src="<?=$setnopic;?>" alt="">
                              <div class="carousel-caption">
                                <!--<h3>First slide label</h3>
                                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>-->
                              </div>
                            </div>
                            <?php
                          }
                          ?>
                        </div>
                      </div>

                      <div class="pd-tab-header-carousel">
                        <?=$setcommname;?> 
                        <br> <hr style="margin: 0;">
                        <?=$value['name'];?>
                      </div>

                    </div>

                    <!-- CONTENT -->
                    <div class="modal-body">
                      
                      <p class="text-left">
                      <?=$value['description'];?>
                      </p>
                    </div>
                    <div class="modal-footer">
                      <center>
                        <button type="button" class="btn btn-default" style="font-size:25px;" data-dismiss="modal">ปิด</button>
                      </center>
                    </div>
                  </div>    <!-- END MODAL CONTENT -->
                </div>    <!-- END MODAL DIALOG -->
              </div>    <!-- END MODAL -->
              <?php } ?>
            </ul>

            <ul style="clear:both;"></ul>

          </div>
        </div>

      </div>
    </section>

    <!-- END CONTENT -->

    <!-- Footer -->
    <footer class="margin-top-on-footer">
      <div class="footer-bottom">
        <div class="container">
          <div class="mobile-center pull-left">
            ©2017 <a href="https://phuketcommunitytourism.com">7เส้นทางหลัก ชุมชนท่องเที่ยวจังหวัดภูเก็ต</a>
          </div>
          <div class="pull-right">
            <ul class="mobile-center social">
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn1.png"> </a> </li>
              <li> <a href="#"> <img src="<?=base_url();?>assets/frontend/img/image_section/social-btn2.png"> </a> </li>
              <li> <a href="<?=base_url()?>index/login_page"> <img src="<?=base_url();?>assets/frontend/img/image_section/login-btn.png"> </a> </li>
            </ul>
          </div>
        </div>
      </div>    <!-- FOOTER-BOTTOM-->
    </footer>
    <!-- END FOOTER -->

    <script src="<?=base_url();?>assets/frontend/js/jquery.min.js"></script>
    <!--  <script>window.jQuery || document.write("<script src="js/jquery.min.js"><\/script>")</script> -->
    <script src="<?=base_url();?>assets/frontend/js/bootstrap.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?=base_url();?>assets/frontend/js/ie10-viewport-bug-workaround.js"></script>
    <!-- Parallax -->
    <!-- Smooth Scrolling Page
    <script src="<?=base_url();?>assets/frontend/js/jQuery.scrollSpeed.js"></script> -->
    <!-- Scrolling Nav Easing JavaScript -->
    <script src="<?=base_url();?>assets/frontend/js/jquery.easing.min.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/productpage.js"></script>
    <script src="<?=base_url();?>assets/js/updateview.js"></script>

  </body>
</html>
