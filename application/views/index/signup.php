<div class="container">
<main>
	<section>
		<div class="row" style="padding-top:20px;">
			<div class="col-md-12" style="text-align:center;">
				<h3>Registeration</h3>
				<h5> กรุณาใส่ข้อมูลใน <font color="red">*</font> ให้ครบถ้วน </h5>
			</div>
		</div>
	</section>
	<br/>
	<article>

	<form id="signup" enctype="multipart/form-data" method="post" action="<?=base_url();?>index/register_update_data" class="form-horizontal">

		<div class="form-group">
		    <label for="inputFirstname" class="col-sm-2 control-label"> <font color="red">*</font> First Name : </label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="inputFirstname" name="User_Firstname" placeholder="Firstname" required>
		    </div>

		    <label for="inputLastname" class="col-sm-2 control-label"> <font color="red">*</font> Last Name : </label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="inputLastname" name="User_Lastname" placeholder="Lastname" required>
		    </div> 
		</div>

		<div class="form-group">
		    <label for="inputID_Passport_Card" class="col-sm-2 control-label"> <font color="red">*</font> Username : </label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="Username" name="Username" placeholder="Username." required>
		    </div> 
		</div>

		<div class="form-group">
		    <label for="inputPassword" class="col-sm-2 control-label"> <font color="red">*</font> Password : </label>
		    <div class="col-sm-4">
		      <input type="password" class="form-control" id="inputPassword" name="Password" placeholder="Password" required>
		    </div>

		    <label for="reinputPassword" class="col-sm-2 control-label"> <font color="red">*</font> Retype Password : </label>
		    <div class="col-sm-4">
		      <input type="password" class="form-control" id="reinputPassword" name="rePassword" placeholder="rePassword" required>
		    </div> 
		</div>

		<!--<div class="form-group">
		    <label for="inputBirthDate" class="col-sm-2 control-label"> <font color="red">*</font> Birth Date / วันเกิด : </label>
		    <div class="col-sm-4">
		   	 	<div class='input-group date'>
		        <input type="text" class="form-control datepicker_bd" id="inputBirthDate" name="User_Birth_Date" placeholder="BirthDate" readonly="" style="background-color:white;" required>
		        <span class="input-group-addon clickshowcalendar">
               		<span class="glyphicon glyphicon-calendar"></span>
                </span>
		    	</div>
		    </div>

		    <label for="inputGender" class="col-sm-2 control-label"> <font color="red">*</font> Gender / เพศ : </label>
		    <div class="col-sm-4">
		      	<select name="User_Gender" class="form-control" id="inputGender" required>
					<option value="Female">หญิง</option>
					<option value="Male">ชาย</option>
				</select>
		    </div> 
		</div>-->

		<div class="form-group">
		    <label for="inputPhone_Number" class="col-sm-2 control-label"> Phone No. : </label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" id="inputPhone_Number" name="User_Phone_Number" placeholder="Phone Number">
		    </div>

		    <label for="inputEmail" class="col-sm-2 control-label"> <font color="red">*</font> Email : </label>
		    <div class="col-sm-4">
		      <input type="email" class="form-control" id="inputEmail" name="User_Email" placeholder="User Email" required>
		    </div> 
		</div>


		<div class="form-group">
		    <label for="inputDisplay_Picture" class="col-sm-4 control-label">Upload รูปภาพ (.jpg/.png) : </label>
		    <div class="col-sm-8">
		      <input type="file" class="form-control" style="border:none;"  id="inputDisplay_Picture" name="User_Display_Picture" >
		    </div> 
		</div>

		<br/>

		<div class="row" style="text-align:center">
			<div class="col-md-4" style="margin-top:10px;">
				<a style="text-decoration:none;" href="<?=base_url();?>"><input type="button" value="Back" class="btn btn-danger btn-lg btn-block"></a>
			</div>
			<div class="col-md-4" style="margin-top:10px;">
				<input type="submit" value="Submit" id="submitbutton" class="btn btn-primary btn-lg btn-block">
			</div>
			<div class="col-md-4" style="margin-top:10px;">
				<input type="button" onclick="resetform()" value="Clear all" class="btn btn-info btn-lg btn-block">
			</div>
		</div>
	</form>
	</article>
</main>
</div>
<br/>
<div id="showerr"></div>
<script type="text/javascript">
       var url = "<?=base_url('assets/js/signup.js');?>";
        $.getScript(url);
</script>