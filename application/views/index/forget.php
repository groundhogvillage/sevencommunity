<br/>
<div class="container">
    <div class="row">
      <div class="col-md-12" style="text-align:center;">
      		<h2>ค้นหาบัญชีผู้ใช้ของคุณ</h2>
      </div>
    </div>
    <br/>
    <form action="<?=base_url();?>index/sendemailforgetpassword" method="POST">
	<div class="form-group">
	    <label for="mailforget" class="col-sm-2 control-label"> <font color="red">*</font> E-mail / อีเมล์ : </label>
	    <div class="col-sm-10">
	        <input type="email" class="form-control" id="mailforget" name="User_Email" placeholder="E-mail" required>
	    </div>
	</div>
	<br/><br/>
	<div class="col-md-6" style="margin-top:10px;">
		<input type="submit" value="Submit" id="submitbutton" class="btn btn-primary btn-lg btn-block">
	</div>

	<div class="col-md-6" style="margin-top:10px;">
		<a style="text-decoration:none;" href="<?=base_url();?>"><input type="button" value="Back" class="btn btn-danger btn-lg btn-block"></a>
	</div>

</div>