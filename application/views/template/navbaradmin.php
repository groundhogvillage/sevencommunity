<?php
	$mainseg = $this->uri->segment(1);
	$subseg = $this->uri->segment(2);
?>
<ul class="nav nav-pills nav-justified">
  <li role="presentation" <?php if(($mainseg=="main"&&($subseg=="index"||$subseg==""))){ echo 'class="active"'; } ?> ><a href="<?=base_url();?>main/index">Index</a></li>
	<li role="presentation" <?php if(($mainseg=="cms")){ echo 'class="active"'; } ?> ><a href="<?=base_url();?>cms/index">Content Management</a></li>
	<?php if($this->session->userdata('Permission')=="admin" ){ ?>
		<li role="presentation" <?php if(($mainseg=="main"&&($subseg=="userlist"||$subseg=="edituser"))){ echo 'class="active"'; } ?> ><a href="<?=base_url();?>main/userlist">Edit User</a></li>
	<?php } ?>
  <li role="presentation" <?php if(($mainseg=="main"&&$subseg=="editinfo")){ echo 'class="active"'; } ?> ><a href="<?=base_url();?>main/editinfo">Edit <?=$this->session->userdata['Username'];?> Profile</a></li>
	<li role="presentation"><a href="<?=base_url();?>">Visit Frontend</a></li>
  <li role="presentation"><a href="<?=base_url();?>index/logout">Logout</a></li>
</ul>
