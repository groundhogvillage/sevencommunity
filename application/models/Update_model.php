<?php
class Update_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Bangkok');
	}

	function update_password($newpassword)
	{

		$key='jd-+*pools';
		$password = md5($newpassword.$key);

		$dataarr = array(
			'Password' => $password
		);

		$this->db->where('Admin_ID =', $this->session->userdata('Admin_ID'));
		if( $this->db->update('admininfo', $dataarr) )
		{
			return "เปลี่ยนรหัสผ่านเรียบร้อยแล้ว";
		}
		else
		{
			return "พบข้อผิดพลาด กรุณาตรวจสอบ";
		}
	}


	function addedituser($dataarr,$id)
	{
		if($id!=NULL)
		{
			$this->db->where('User_ID =', $id);
			if( $this->db->update('user', $dataarr) )
			{
				return "ข้อมูลได้ทำการแก้ไขแล้ว";
			}
			else
			{
				return "พบข้อผิดพลาด กรุณาตรวจสอบ";
			}
		}
		else
		{
			if( $this->db->insert('user', $dataarr) )
			{
				return "ข้อมูลได้ทำการเพิ่มแล้ว";
			}
			else
			{
				return "พบข้อผิดพลาด กรุณาตรวจสอบ";
			}
		}
	}

	function insertforgetpassword($dataarr)
	{
		if( $this->db->insert('forgetpassword',$dataarr) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function updated_data($dataarr,$table,$id,$idname)
	{
		if($id!=NULL&&$idname!=NULL)
		{
			$this->db->where( $idname.' =', $id );
			if( $this->db->update($table, $dataarr) )
			{
				return "ข้อมูลได้ทำการแก้ไขแล้ว";
			}
			else
			{
				return "พบข้อผิดพลาด กรุณาตรวจสอบ";
			}
		}
		else
		{
			if( $this->db->insert($table, $dataarr) )
			{
				return "ข้อมูลได้ทำการเพิ่มแล้ว";
			}
			else
			{
				return "พบข้อผิดพลาด กรุณาตรวจสอบ";
			}
		}
	}

	function addeditfranchise($dataarr,$id)
	{

		if($id!=NULL)
		{
			$this->db->where('Branch_ID =', $id);
			if( $this->db->update('branch', $dataarr) )
			{
				return "ข้อมูลได้ทำการแก้ไขแล้ว";
			}
			else
			{
				return "พบข้อผิดพลาด กรุณาตรวจสอบ";
			}
		}
		else
		{
			if( $this->db->insert('branch', $dataarr) )
			{
				return "ข้อมูลได้ทำการเพิ่มแล้ว";
			}
			else
			{
				return "พบข้อผิดพลาด กรุณาตรวจสอบ";
			}
		}
	}

}
