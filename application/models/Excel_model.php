<?php
class Excel_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Bangkok');
	}


	function update_import_product_excel($arrayimport)
	{
		$this->db->select("Country_ID, Country_Name");
		$this->db->from("country");
		$ret = $this->db->get();
		$country_row = $ret->num_rows();
		$country = $ret->result_array();

		$this->db->select("Province_ID, Province_Name, Province_Name_ENG" );
		$this->db->from("province");
		$ret = $this->db->get();
		$province_row = $ret->num_rows();
		$province = $ret->result_array();

		$this->db->select("District_ID, District_Name, District_Name_ENG");
		$this->db->from("district");
		$ret = $this->db->get();
		$district_row = $ret->num_rows();
		$district = $ret->result_array();

		$this->db->select("Sub_District_ID, Sub_District_Name");
		$this->db->from("sub_district");
		$ret = $this->db->get();
		$sub_district_row = $ret->num_rows();
		$sub_district = $ret->result_array();

		$this->db->select("Supplier_Product_Type_ID, Supplier_Product_Type_Name");
		$this->db->from("supplier_product_type");
		$ret = $this->db->get();
		$supplier_product_type_row = $ret->num_rows();
		$supplier_product_type = $ret->result_array();

		$this->db->select("Supplier_ID, Supplier_Name");
		$this->db->from("supplier");
		$ret = $this->db->get();
		$supplier_row = $ret->num_rows();
		$supplier = $ret->result_array();

		$this->db->select("*");
		$this->db->from("supplier_product");
		$ret = $this->db->get();
		$product_row = $ret->num_rows();
		$product_res = $ret->result_array();

		$this->db->select("*");
		$this->db->from("product_unit");
		$ret = $this->db->get();
		$product_unit_row = $ret->num_rows();
		$product_unit_res = $ret->result_array();

		$supplier_product_type_arr = array();
		$supplier_arr = array();
		$country_arr = array();
		$province_arr = array();
		$district_arr = array();
		$product_unit_arr = array();
		$sub_district_arr = array();
		$productname = array();

		foreach ($product_res as $keypro => $valuepro) {
			 array_push($productname, $valuepro['Supplier_Product_Name']);
		}

		array_unshift($productname, null);
		unset($productname[0]);

		foreach ($supplier_product_type as $keysp => $valuesp) {
			$supplier_product_type_id = $valuesp['Supplier_Product_Type_ID'];
			$supplier_product_type_arr[$supplier_product_type_id] = $valuesp['Supplier_Product_Type_Name'];
		}

		foreach ($product_unit_res as $keyu => $valueu) {
			$product_unit_id = $valueu['Product_Unit_ID'];
			$product_unit_arr[$product_unit_id] = $valueu['Product_Unit_Name'];
		}

		foreach ($supplier as $keys => $values) {
			$supplierid = $values['Supplier_ID'];
			$supplier_arr[$supplierid] = $values['Supplier_Name'];
		}

		foreach ($country as $keyc => $valuec) {
			$country_id = $valuec['Country_ID'];
			$country_arr[$country_id] = $valuec['Country_Name'];
		}
		
		foreach ($province as $keypv => $valuepv) {
			$provinceid = $valuepv['Province_ID'];
			$province_arr[$provinceid] = $valuepv['Province_Name'];
			$province_arr_eng[$provinceid] = $valuepv['Province_Name_ENG'];
		}

		foreach ($district as $keydis => $valuedis) {
			$district_id = $valuedis['District_ID'];
			$district_arr[$district_id] = $valuedis['District_Name'];
			$district_arr_eng[$district_id] = $valuedis['District_Name_ENG'];
		}
		
		foreach ($sub_district as $keysubdis => $valuesubdis) {
			$sub_districtid = $valuesubdis['Sub_District_ID'];
			$sub_district_arr[$sub_districtid] = $valuesubdis['Sub_District_Name'];
		}

		extract($arrayimport);
		foreach ($Supplier_Product_Name as $key => $value) {

			//Update data country
			if(preg_replace('/\s+/', '', $Country_Name[$key])==NULL||preg_replace('/\s+/', '', $Country_Name[$key])=="")
			{
				$Country_ID[$key] = "";
				$thiscountryid =  0;
			}
			elseif($Country_Name[$key]=="ไทย"||$Country_Name[$key]=="ประเทศไทย"||$Country_Name[$key]=="thai"||$Country_Name[$key]=="Thai"||$Country_Name[$key]=="THAI"||$Country_Name[$key]=="THAILAND"||$Country_Name[$key]=="thailand")
			{
				$Country_ID[$key] = 216;
				$thiscountryid = $Country_ID[$key];
			}
			elseif(array_search($Country_Name[$key],$country_arr))
			{
				$Country_ID[$key] = array_search($Country_Name[$key],$country_arr);
				$thiscountryid = $Country_ID[$key];
			}
			else
			{
				$country_row++;
				$insertcountry = array(
					'Country_Name' => $Country_Name[$key],
					'Country_code' => strtoupper(substr($Country_Name[$key], 0, 2))
				);
				$this->db->insert('country',$insertcountry);
				$newcountryid = $this->db->insert_id();
				$country_arr[$newcountryid] = $Country_Name[$key];
				$Country_ID[$key] = $newcountryid;
				$thiscountryid = $newcountryid;
			}

			//Update data province
			if(preg_replace('/\s+/', '', $Province_Name[$key])==NULL||preg_replace('/\s+/', '', $Province_Name[$key])=="")
			{
				$Province_ID[$key] = "";
				$thisprovinceid =  0;
			}
			elseif(array_search($Province_Name[$key],$province_arr))
			{
				$Province_ID[$key] = array_search($Province_Name[$key],$province_arr);
				$thisprovinceid = $Province_ID[$key];
			}
			elseif(array_search($Province_Name[$key],$province_arr_eng))
			{
				$Province_ID[$key] = array_search($Province_Name[$key],$province_arr_eng);
				$thisprovinceid = $Province_ID[$key];
			}
			else
			{
				$province_row++;
				$insertprovince = array(
					'Province_Name' => $Province_Name[$key],
					'Province_Name_ENG' => $Province_Name[$key],
					'Country_ID' => $Country_ID[$key]
				);
				$this->db->insert('province',$insertprovince);
				$newprovinceid = $this->db->insert_id();
				$province_arr[$newprovinceid] = $Province_Name[$key];
				$Province_ID[$key] = $newprovinceid;
				$thisprovinceid = $newprovinceid;
			}

			//Update data district
			if(preg_replace('/\s+/', '', $District_Name[$key])==NULL||preg_replace('/\s+/', '', $District_Name[$key])=="")
			{
				$District_ID[$key] = "";
				$thisdistrictid =  0;
			}
			elseif(array_search($District_Name[$key],$district_arr))
			{
				$District_ID[$key] = array_search($District_Name[$key],$district_arr);
				$thisdistrictid = $District_ID[$key];
			}
			elseif(array_search($District_Name[$key],$district_arr_eng))
			{
				$District_ID[$key] = array_search($District_Name[$key],$district_arr_eng);
				$thisdistrictid = $District_ID[$key];
			}
			else
			{
				$district_row++;
				$insertdistrict = array(
					'District_Name' => $District_Name[$key],
					'District_Name_ENG' => $District_Name[$key],
					'Province_ID' => $Province_ID[$key]
				);
				$this->db->insert('district',$insertdistrict);
				$newdistrictid = $this->db->insert_id();
				$district_arr[$newdistrictid] = $District_Name[$key];
				$District_ID[$key] = $newdistrictid;
				$thisdistrictid = $newdistrictid;
			}

			//Update data subdistrict
			if(preg_replace('/\s+/', '', $Sub_District_Name[$key])==NULL||preg_replace('/\s+/', '', $Sub_District_Name[$key])=="")
			{
				$Sub_District_ID[$key] = "";
				$thissubdistrictid =  0;
			}
			elseif(array_search($Sub_District_Name[$key],$sub_district_arr))
			{
				$Sub_District_ID[$key] = array_search($Sub_District_Name[$key],$sub_district_arr);
				$thissubdistrictid = $Sub_District_ID[$key];
			}
			else
			{
				$sub_district_row++;
				$insertsubdistrict = array(
					'Sub_District_Name' => $Sub_District_Name[$key],
					'District_ID' => $District_ID[$key],
					'Province_ID' => $Province_ID[$key]
				);
				$this->db->insert('sub_district',$insertsubdistrict);
				$newsubdistrictid = $this->db->insert_id();
				$sub_district_arr[$newsubdistrictid] = $Sub_District_Name[$key];
				$Sub_District_ID[$key] = $newsubdistrictid;
				$thissubdistrictid = $newsubdistrictid;
			}

			//Update data supplier
			if(preg_replace('/\s+/', '', $Supplier_Name[$key])==NULL||preg_replace('/\s+/', '', $Supplier_Name[$key])=="")
			{
				$Supplier_ID[$key] = "";
				$thissupid =  0;
			}
			elseif(array_search($Supplier_Name[$key],$supplier_arr))
			{
				$Supplier_ID[$key] = array_search($Supplier_Name[$key],$supplier_arr);
				$thissupid = $Supplier_ID[$key];
			}
			else
			{
				$supplier_row++;
				$insertsupplier = array(
					'Supplier_Name' => $Supplier_Name[$key],
					'Supplier_Tel' => $Supplier_Tel[$key],
					'Supplier_Fax' => $Supplier_Fax[$key],
					'Supplier_Address' => $Supplier_Address[$key],
					'Country_ID' => $Country_ID[$key],
					'Province_ID' => $Province_ID[$key],
					'District_ID' => $District_ID[$key],
					'Sub_District_ID' => $Sub_District_ID[$key],
					'Supplier_Postal_Code' => $Supplier_Postal_Code[$key]
				);
				$this->db->insert('supplier',$insertsupplier);
				$newsupplierid = $this->db->insert_id();
				$supplier_arr[$newsupplierid] = $Supplier_Name[$key];
				$Supplier_ID[$key] = $newsupplierid;
				$thissupid = $newsupplierid;
			}

			//Update data product unit name
			if(preg_replace('/\s+/', '', $Product_Unit_Name[$key])==NULL||preg_replace('/\s+/', '', $Product_Unit_Name[$key])=="")
			{
				$Product_Unit_ID[$key] = "";
				$thisprodunitid =  0;
			}
			elseif(array_search($Product_Unit_Name[$key],$product_unit_arr))
			{
				$Product_Unit_ID[$key] = array_search($Product_Unit_Name[$key],$product_unit_arr);
				$thisprodunitid = $Product_Unit_ID[$key];
			}
			else
			{
				$product_unit_row++;
				$insertprodunit = array(
					'Product_Unit_Name' => $Product_Unit_Name[$key]
				);
				$this->db->insert('product_unit',$insertprodunit);
				$newprodunitid = $this->db->insert_id();
				$product_unit_arr[$newprodunitid] = $Product_Unit_Name[$key];
				$Product_Unit_ID[$key] = $newprodunitid;
				$thisprodunitid = $newprodunitid;
			}

			//Update data supplier product type
			if(preg_replace('/\s+/', '', $Supplier_Product_Type_Name[$key])==NULL||preg_replace('/\s+/', '', $Supplier_Product_Type_Name[$key])=="")
			{
				$Supplier_Product_Type_ID[$key] = "";
				$thissupprodtypeid =  0;
			}
			elseif(array_search($Supplier_Product_Type_Name[$key],$supplier_product_type_arr))
			{
				$Supplier_Product_Type_ID[$key] = array_search($Supplier_Product_Type_Name[$key],$supplier_product_type_arr);
				$thissupprodtypeid = $Supplier_Product_Type_ID[$key];
			}
			else
			{
				$supplier_product_type_row++;
				$insertsupplierprodtype = array(
					'Supplier_Product_Type_Name' => $Supplier_Product_Type_Name[$key]
				);
				$this->db->insert('supplier_product_type',$insertsupplierprodtype);
				$newsupprodtypeid = $this->db->insert_id();
				$supplier_product_type_arr[$newsupprodtypeid] = $Supplier_Product_Type_Name[$key];
				$Supplier_Product_Type_ID[$key] = $newsupprodtypeid;
				$thissupprodtypeid = $newsupprodtypeid;
			}

			if($Supplier_Product_Picture[$key]!=NULL&&$Supplier_Product_Picture[$key]!=""&&$Supplier_Product_Picture[$key]!=" ")
			{
				$Supplier_Product_Picture[$key] = $Supplier_Product_Picture[$key].".jpg";
			}

			if(array_search($Supplier_Product_Name[$key], $productname)!=""||array_search($Supplier_Product_Name[$key], $productname)!=NULL)
			{
				//update product data
				$product_row++;

				$dataupdate = array(
					'Supplier_Product_Name' => $Supplier_Product_Name[$key],
					'Supplier_Product_Name_ENG' => $Supplier_Product_Name_ENG[$key],
					'Supplier_Product_Code' => $Supplier_Product_Code[$key],
					'Product_Unit_ID' => $Product_Unit_ID[$key],
					'Supplier_Product_Price' => $Supplier_Product_Price[$key],
					'Supplier_Product_Picture' => $Supplier_Product_Picture[$key],
					'Supplier_Product_Description' => $Supplier_Product_Description[$key],
					'Supplier_Product_Description2' => $Supplier_Product_Description2[$key],
					'Supplier_Product_Warranty' => $Supplier_Product_Warranty[$key],
					'Supplier_Product_Warranty_Term' => $Supplier_Product_Warranty_Term[$key],
					'Supplier_Product_Type_ID' => $Supplier_Product_Type_ID[$key],
					'Supplier_ID' => $Supplier_ID[$key]
				);

				$this->db->where('Supplier_Product_Name =', $Supplier_Product_Name[$key]);
				$this->db->update('supplier_product', $dataupdate);

				$this->db->select("*");
				$this->db->from("supplier_product");
				$this->db->where('Supplier_Product_Name =', $Supplier_Product_Name[$key]);
				$ret = $this->db->get();
				$thisres = $ret->row_array();
				$productid = $thisres['Product_ID'];
			}
			else
			{
				$product_row++;

				$datainsert = array(
					'Supplier_Product_Name' => $Supplier_Product_Name[$key],
					'Supplier_Product_Name_ENG' => $Supplier_Product_Name_ENG[$key],
					'Supplier_Product_Code' => $Supplier_Product_Code[$key],
					'Product_Unit_ID' => $Product_Unit_ID[$key],
					'Supplier_Product_Price' => $Supplier_Product_Price[$key],
					'Supplier_Product_Picture' => $Supplier_Product_Picture[$key],
					'Supplier_Product_Description' => $Supplier_Product_Description[$key],
					'Supplier_Product_Description2' => $Supplier_Product_Description2[$key],
					'Supplier_Product_Warranty' => $Supplier_Product_Warranty[$key],
					'Supplier_Product_Warranty_Term' => $Supplier_Product_Warranty_Term[$key],
					'Supplier_Product_Type_ID' => $Supplier_Product_Type_ID[$key],
					'Supplier_ID' => $Supplier_ID[$key]
				);

				$this->db->insert('supplier_product', $datainsert);
				$productid = $this->db->insert_id();
			}

		}

		if($show!=NULL&&$show!="")
		{
			array_unshift($show, null);
			array_unshift($show, null);
			array_unshift($show, null);
			unset($show[0]);
			unset($show[1]);
			unset($show[2]);
		}

		return "done";
	}
}