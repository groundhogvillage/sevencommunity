<?php
class Select_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Bangkok');
	}

	function  select($s,$f)
	{
		$this->db->select($s);
		$this->db->from($f);
		return $ret = $this->db->get();
	}

	function  selectorder($s,$f,$order,$adesc)
	{
		$this->db->select($s);
		$this->db->from($f);
		$this->db->order_by($order,$adesc);
		return $ret = $this->db->get();
	}

	function  selectlimit($s,$f,$l,$start)
	{
		$this->db->select($s);
		$this->db->from($f);
		$this->db->limit($l,$start);
		return $ret = $this->db->get();
	}

	function  select_where($s,$f,$w = NULL)
	{
		$arr =is_array($w);
		$this->db->select($s);
		$this->db->from($f);
		if($w!=NULL)
		{
			if($arr==TRUE)
			{
				foreach ($w as $key => $value) {
					$this->db->where($value);
				}

			}
			else
			{
				$this->db->where($w);
			}
		}
		return $ret = $this->db->get();
	}

	function  selectwhereorder($s,$f,$w,$order,$adesc)
	{
		$arr =is_array($w);
		$this->db->select($s);
		$this->db->from($f);
		if($w!=NULL)
		{
			if($arr==TRUE)
			{
				foreach ($w as $key => $value) {
					$this->db->where($value);
				}

			}
			else
			{
				$this->db->where($w);
			}
		}

		if($order!=NULL&&$adesc!=NULL)
		{
			$this->db->order_by($order,$adesc);
		}


		return $ret = $this->db->get();
	}

	function select_orwhere($s,$f,$w)
	{
		$arr =is_array($w);
		$this->db->select($s);
		$this->db->from($f);
		if($w!=NULL)
		{
			if($arr==TRUE)
			{
				foreach ($w as $key => $value) {
					if($key==0)
					{
						$this->db->where($value);
					}
					else
					{
						$this->db->or_where($value);
					}
				}
			}
			else
			{
				$this->db->where($w);
			}
		}

		return $ret = $this->db->get();
	}

	function  selectorwherejoin($s,$f,$w,$j,$jc,$order,$adesc)
	{
		$arrw =is_array($w);
		$this->db->select($s);
		$this->db->from($f);
		$arr =is_array($j);
		$arr2 =is_array($jc);

		if(($arr==TRUE)&&($arr2==TRUE)&&(sizeof($j)==sizeof($jc)))
		{
			foreach ($j as $key => $value) {
				$this->db->join($j[$key],$jc[$key],"left");
			}
		}
		elseif(($arr==false)&&($arr2==false)&&$j!=NULL&&$jc!=NULL)
		{
			$this->db->join($j,$jc);
		}
		else
		{

		}
		if($w != NULL)
		{
			if($arrw==TRUE)
			{
				foreach ($w as $key => $value) {
					if($key==0)
					{
						$this->db->where($value);
					}
					else
					{
						$this->db->or_where($value);
					}
				}
			}
			else
			{
				$this->db->where($w);
			}

		}
		if($order!=NULL&&$adesc!=NULL)
		{
			$this->db->order_by($order,$adesc);
		}

		return $ret = $this->db->get();
	}


	function  selectwherejoin($s,$f,$w,$j=NULL,$jc=NULL,$order=NULL,$adesc=NULL)
	{
		$arrw =is_array($w);
		$this->db->select($s);
		$this->db->from($f);
		$arr =is_array($j);
		$arr2 =is_array($jc);

		if(($arr==TRUE)&&($arr2==TRUE)&&(sizeof($j)==sizeof($jc)))
		{
			foreach ($j as $key => $value) {
				$this->db->join($j[$key],$jc[$key],"left");
			}
		}
		elseif(($arr==false)&&($arr2==false)&&$j!=NULL&&$jc!=NULL)
		{
			$this->db->join($j,$jc);
		}
		else
		{

		}
		if($w != NULL)
		{
			if($arrw==TRUE)
			{
				foreach ($w as $key => $value) {
					$this->db->where($value);

				}
			}
			else
			{
				$this->db->where($w);
			}

		}
		if($order!=NULL&&$adesc!=NULL)
		{
			$this->db->order_by($order,$adesc);
		}

		return $ret = $this->db->get();
	}

	function  selectwherejoinorderarr($s,$f,$w,$j,$jc,$order,$adesc)
	{
		$arrw =is_array($w);
		$this->db->select($s);
		$this->db->from($f);
		$arr =is_array($j);
		$arr2 =is_array($jc);
		$arr2 =is_array($jc);
		$arr3 =is_array($order);
		$arr4 =is_array($adesc);


		if(($arr==TRUE)&&($arr2==TRUE)&&(sizeof($j)==sizeof($jc)))
		{
			foreach ($j as $key => $value) {
				$this->db->join($j[$key],$jc[$key],"left");
			}
		}
		elseif(($arr==false)&&($arr2==false)&&$j!=NULL&&$jc!=NULL)
		{
			$this->db->join($j,$jc);
		}
		else
		{

		}
		if($w != NULL)
		{
			if($arrw==TRUE)
			{
				foreach ($w as $key => $value) {
					$this->db->where($value);

				}
			}
			else
			{
				$this->db->where($w);
			}

		}
		if($order!=NULL&&$adesc!=NULL)
		{
			if(($arr3==false)&&($arr4==false)&&$order!=NULL&&$adesc!=NULL)
			{
				$this->db->order_by($order,$adesc);
			}
			elseif(($arr3==TRUE)&&($arr4==TRUE)&&(sizeof($order)==sizeof($adesc)))
			{
				foreach ($order as $key => $value) {
					$this->db->order_by($order[$key],$adesc[$key]);
				}
			}
			else
			{

			}
		}

		return $ret = $this->db->get();
	}

	function  selectwherejoinorderarrlimit($s,$f,$w,$j,$jc,$l,$start,$order,$adesc)
	{
		$arrw =is_array($w);
		$this->db->select($s);
		$this->db->from($f);
		$arr =is_array($j);
		$arr2 =is_array($jc);
		$arr2 =is_array($jc);
		$arr3 =is_array($order);
		$arr4 =is_array($adesc);

		if(($arr==TRUE)&&($arr2==TRUE)&&(sizeof($j)==sizeof($jc)))
		{
			foreach ($j as $key => $value) {
				$this->db->join($j[$key],$jc[$key],"left");
			}
		}
		elseif(($arr==false)&&($arr2==false)&&$j!=NULL&&$jc!=NULL)
		{
			$this->db->join($j,$jc);
		}
		else
		{

		}
		if($w != NULL)
		{
			if($arrw==TRUE)
			{
				foreach ($w as $key => $value) {
					$this->db->where($value);

				}
			}
			else
			{
				$this->db->where($w);
			}

		}

		$this->db->limit($l,$start);

		if($order!=NULL&&$adesc!=NULL)
		{
			if(($arr3==false)&&($arr4==false)&&$order!=NULL&&$adesc!=NULL)
			{
				$this->db->order_by($order,$adesc);
			}
			elseif(($arr3==TRUE)&&($arr4==TRUE)&&(sizeof($order)==sizeof($adesc)))
			{
				foreach ($order as $key => $value) {
					$this->db->order_by($order[$key],$adesc[$key]);
				}
			}
			else
			{

			}
		}

		return $ret = $this->db->get();
	}

	function  selectwherejoin_distinct($s,$f,$w,$j,$jc,$order,$adesc,$groupby)
	{
		$arrw =is_array($w);
		$this->db->distinct();
		$this->db->select($s);
		$this->db->from($f);
		$arr =is_array($j);
		$arr2 =is_array($jc);

		if(($arr==TRUE)&&($arr2==TRUE)&&(sizeof($j)==sizeof($jc)))
		{
			foreach ($j as $key => $value) {
				$this->db->join($j[$key],$jc[$key],"left");
			}
		}
		elseif(($arr==false)&&($arr2==false)&&$j!=NULL&&$jc!=NULL)
		{
			$this->db->join($j,$jc);
		}
		else
		{

		}
		if($w != NULL)
		{
			if($arrw==TRUE)
			{
				foreach ($w as $key => $value) {
					$this->db->where($value);

				}
			}
			else
			{
				$this->db->where($w);
			}

		}

		if($groupby!=NULL)
		{
			$this->db->group_by($groupby);
		}

		if($order!=NULL&&$adesc!=NULL)
		{
			$this->db->order_by($order,$adesc);
		}

		return $ret = $this->db->get();
	}

	function  selectwherejoinlimit($s,$f,$w,$j,$jc,$l,$start,$order,$adesc)
	{
		$arrw =is_array($w);
		$this->db->select($s);
		$this->db->from($f);
		$arr =is_array($j);
		$arr2 =is_array($jc);

		if(($arr==TRUE)&&($arr2==TRUE)&&(sizeof($j)==sizeof($jc)))
		{
			foreach ($j as $key => $value) {
				$this->db->join($j[$key],$jc[$key],"left");
			}
		}
		elseif(($arr==false)&&($arr2==false)&&$j!=NULL&&$jc!=NULL)
		{
			$this->db->join($j,$jc);
		}
		else
		{

		}
		if($w != NULL)
		{
			if($arrw==TRUE)
			{
				foreach ($w as $key => $value) {
					$this->db->where($value);

				}
			}
			else
			{
				$this->db->where($w);
			}

		}

		$this->db->limit($l,$start);

		if($order!=NULL&&$adesc!=NULL)
		{
			$this->db->order_by($order,$adesc);
		}


		return $ret = $this->db->get();
	}

	function  selectwherejoinlimitorderarr($s,$f,$w,$j,$jc,$l,$start,$order,$adesc)
	{
		$arrw =is_array($w);
		$this->db->select($s);
		$this->db->from($f);
		$arr =is_array($j);
		$arr2 =is_array($jc);
		$arr3 =is_array($order);
		$arr4 =is_array($adesc);

		if(($arr==TRUE)&&($arr2==TRUE)&&(sizeof($j)==sizeof($jc)))
		{
			foreach ($j as $key => $value) {
				$this->db->join($j[$key],$jc[$key],"left");
			}
		}
		elseif(($arr==false)&&($arr2==false)&&$j!=NULL&&$jc!=NULL)
		{
			$this->db->join($j,$jc);
		}
		else
		{

		}
		if($w != NULL)
		{
			if($arrw==TRUE)
			{
				foreach ($w as $key => $value) {
					$this->db->where($value);

				}
			}
			else
			{
				$this->db->where($w);
			}

		}

		$this->db->limit($l,$start);

		if($order!=NULL&&$adesc!=NULL)
		{
			if(($arr3==false)&&($arr4==false)&&$order!=NULL&&$adesc!=NULL)
			{
				$this->db->order_by($order,$adesc);
			}
			elseif(($arr3==TRUE)&&($arr4==TRUE)&&(sizeof($order)==sizeof($adesc)))
			{
				foreach ($order as $key => $value) {
					$this->db->order_by($order[$key],$adesc[$key]);
				}
			}
			else
			{

			}
		}


		return $ret = $this->db->get();
	}

}
