<?php
class Delete_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Bangkok');
	}

	function delete_data_log($table, $wherearr, $logget, $filelogname)
	{
		$arr =is_array($wherearr);

		if($arr==TRUE)
		{
			foreach ($wherearr as $key => $value) {
				$this->db->where($value);
			}
		}
		else
		{
			$this->db->where($wherearr);
		}

  		if($this->db->delete($table))
  		{
  			//create log file

  			$thisdate = date("Ym");
			$myfile = fopen(FCPATH . "assets/logfile/log_".$filelogname."_".$thisdate.".txt", "a") or die("Unable to open file!");
  			$txt = $logget." File Delete at : ".date("Y-m-d H:i:s");
			fwrite($myfile, "\r\n".$txt);
			fclose($myfile);

			return true;
		}
		else
		{
			return false;
		}
	}

	function delete_data($table, $wherearr)
	{
		$arr =is_array($wherearr);

		if($arr==TRUE)
		{
			foreach ($wherearr as $key => $value) {
				$this->db->where($value);
			}
		}
		else
		{
			$this->db->where($wherearr);
		}

  		if($this->db->delete($table))
  		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
