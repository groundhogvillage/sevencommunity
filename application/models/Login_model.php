<?php
class Login_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Bangkok');
	}

	function login($username, $password)
	{
		$this->session->unset_userdata("session");
		$key='*sevencommunity*_';
		$mdpass = md5($password.$key);

		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('Username',strtolower($username));
		$this->db->where('Password',$mdpass);
		$this->db->where('User_Deleted','0');
		$ret = $this->db->get();

		if($ret->num_rows())
		{
			$data_ret = $ret->result();
			$this->session->set_userdata('User_ID',$data_ret[0]->User_ID);
			$this->session->set_userdata('Username',$data_ret[0]->Username);
			$this->session->set_userdata('Permission',$data_ret[0]->Permission);
			$this->session->set_userdata('Community',$data_ret[0]->Community);

			$this->db->select('*');
			$this->db->from('web_login');
			$this->db->where('User_ID',$data_ret[0]->User_ID);
			$retlogin = $this->db->get();

			if($retlogin->num_rows())
			{
				$dataarr = array(
					'Web_Login_Time' => Date("Y-m-d H:i:s")
				);

				$this->db->where( 'User_ID =', $data_ret[0]->User_ID );
				$this->db->update("web_login", $dataarr);
			}
			else
			{
				$dataarr = array(
					'Web_Login_Time' => Date("Y-m-d H:i:s"),
					'User_ID' => $data_ret[0]->User_ID
				);
				$this->db->insert("web_login", $dataarr);
			}
			return true;
		}
		else
		{
				$this->session->sess_destroy();
				return false;
		}
	}

}
?>
