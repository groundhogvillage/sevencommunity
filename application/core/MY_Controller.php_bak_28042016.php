<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Bangkok');
        $this->load->model('select_model');
        $this->load->model('update_model');
        $this->load->model('login_model');
        $this->load->model('delete_model');

        /*if(!$this->session->userdata("Permission")||!$this->session->userdata("Username")||!$this->session->userdata("User_ID"))
		{
			redirect(base_url());
		}*/
		$this->load->library('pdf'); //
		//$this->pdf->fontpath = 'font/';
		$this->pdf->AddFont('angsa','','angsa.php');
		$this->pdf->AddFont('angsa','B','angsab.php');
		$this->pdf->AddFont('angsa','I','angsai.php');
		$this->pdf->AddFont('angsa','U','angsaz.php');
		$widths;
		$aligns;
		//$this->pdf->fontpath = 'fonts/'; // Create folder fonts at Codeigniter
    }   

	function SetWidths($w)
	{
	    //Set the array of column widths
	    $this->pdf->widths=$w;
	}

	function SetAligns($a)
	{
	    //Set the array of column alignments
	    $this->pdf->aligns=$a;
	}

	function Row($data)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<count($data);$i++)
	        $nb=max($nb,$this->pdf->NbLines($this->pdf->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->pdf->CheckPageBreak($h);
	    //Draw the cells of the row
	    for($i=0;$i<count($data);$i++)
	    {
	        $w=$this->pdf->widths[$i];
	        $a=isset($this->pdf->aligns[$i]) ? $this->pdf->aligns[$i] : 'L';
	        //Save the current position
	        $x=$this->pdf->GetX();
	        $y=$this->pdf->GetY();
	        //Draw the border
	        $this->pdf->Rect($x,$y,$w,$h);
	        //Print the text
	        $this->pdf->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->pdf->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->pdf->Ln($h);
	}

	function CheckPageBreak($h)
	{
	    //If the height h would cause an overflow, add a new page immediately
	    if($this->pdf->GetY()+$h>$this->pdf->PageBreakTrigger)
	        $this->pdf->AddPage($this->pdf->CurOrientation);
	}

	function NbLines($w,$txt)
	{
	    //Computes the number of lines a MultiCell of width w will take
	    $cw=&$this->pdf->CurrentFont['cw'];
	    if($w==0)
	        $w=$this->pdf->w-$this->pdf->rMargin-$this->pdf->x;
	    $wmax=($w-2*$this->pdf->cMargin)*1000/$this->pdf->FontSize;
	    $s=str_replace("\r",'',$txt);
	    $nb=strlen($s);
	    if($nb>0 and $s[$nb-1]=="\n")
	        $nb--;
	    $sep=-1;
	    $i=0;
	    $j=0;
	    $l=0;
	    $nl=1;
	    while($i<$nb)
	    {
	        $c=$s[$i];
	        if($c=="\n")
	        {
	            $i++;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	            continue;
	        }
	        if($c==' ')
	            $sep=$i;
	        $l+=$cw[$c];
	        if($l>$wmax)
	        {
	            if($sep==-1)
	            {
	                if($i==$j)
	                    $i++;
	            }
	            else
	                $i=$sep+1;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	        }
	        else
	            $i++;
	    }
	    return $nl;
	}
}

class MY_Calculate extends MY_Controller {
	
	public function __construct()
    {
        parent::__construct();

       /* date_default_timezone_set('Asia/Bangkok');
        $this->load->model('select_model');
        $this->load->model('update_model');
        $this->load->model('login_model');
        $this->load->model('delete_model');*/
    } 


    function getproj($Project_ID)
    {
    	$wherearr = array('Project_ID = "'.$Project_ID.'"');
		$jarr = array('country', 'province', 'district', 'sub_district');
		$jcarr = array(
			'project.Country_ID = country.Country_ID',
			'project.Province_ID = province.Province_ID',
			'project.District_ID = district.District_ID',
			'project.Sub_District_ID = sub_district.Sub_District_ID');

    	$editproject = $this->select_model->selectwherejoin("*","project",$wherearr,$jarr,$jcarr,NULL,NULL);
    	if($editproject->num_rows())
    	{
    		$editproject = $editproject->row_array();
    	}
    	else
    	{
    		$editproject = NULL;
    	}
		
		return $editproject;
    }

    function getcustomer($Customer_ID)
    {
    	$wherearr = array('Customer_ID = "'.$Customer_ID.'"');
		$jarr = array('country', 'province', 'district', 'sub_district');
		$jcarr = array(
			'customer.Customer_Country_ID = country.Country_ID',
			'customer.Customer_Province_ID = province.Province_ID',
			'customer.Customer_District_ID = district.District_ID',
			'customer.Customer_Sub_District_ID = sub_district.Sub_District_ID');

		$editcustomer = $this->select_model->selectwherejoin("*","customer",$wherearr,$jarr,$jcarr,NULL,NULL);
    	//$editcustomer = $this->select_model->select_where("*","customer","Customer_ID = '".$Customer_ID."'");
    	if($editcustomer->num_rows())
    	{
    		$editcustomer = $editcustomer->row_array();
    	}
    	else
    	{
    		$editcustomer = NULL;
    	}
		
		return $editcustomer;
    }

    function getcustomer_contact($Project_ID,$Customer_ID)
   	{
    	$wherearr = array('Customer_ID = "'.$Customer_ID.'"',"Project_ID = ".$Project_ID);
		$jarr = array('country', 'province', 'district', 'sub_district');
		$jcarr = array(
			'customer_contact.Customer_Contact_Country_ID = country.Country_ID',
			'customer_contact.Customer_Contact_Province_ID = province.Province_ID',
			'customer_contact.Customer_Contact_District_ID = district.District_ID',
			'customer_contact.Customer_Contact_Sub_District_ID = sub_district.Sub_District_ID');

		$editcustomer = $this->select_model->selectwherejoin("*","customer_contact",$wherearr,$jarr,$jcarr,NULL,NULL);
    	//$editcustomer = $this->select_model->select_where("*","customer","Customer_ID = '".$Customer_ID."'");
    	if($editcustomer->num_rows())
    	{
    		$editcustomer = $editcustomer->result_array();
    	}
    	else
    	{
    		$editcustomer = NULL;
    	}
		
		return $editcustomer;
    }

    function getipanel($Project_ID)
    {
    	$editipanel = $this->select_model->select_where("*","ipanel_pools","Project_ID = '".$Project_ID."'");
    	if($editipanel->num_rows())
    	{
    		$editipanel = $editipanel->row_array();
    	}
		else
		{
			$editipanel = NULL;
		}

		return $editipanel;
    }

    function getcomposite($Project_ID)
    {
    	$editcomposite = $this->select_model->select_where("*","composite_pools","Project_ID = '".$Project_ID."'");
    	if($editcomposite->num_rows())
    	{
    		$editcomposite = $editcomposite->row_array();
    	}
		else
		{
			$editcomposite = NULL;
		}

		return $editcomposite;
    }

    function getipanelcomponent($Ipanel_Pools_ID)
    {
    	$editipanelcomponent = $this->select_model->select_where("*","ipanel_pool_component","Ipanel_Pools_ID = '".$Ipanel_Pools_ID."'");
    	if($editipanelcomponent->num_rows())
    	{
    		$editipanelcomponent = $editipanelcomponent->row_array();
    	}
		else
		{
			$editipanelcomponent = NULL;
		}

		return $editipanelcomponent;
    }

    function getipaneloption($Ipanel_Pools_ID)
    {
    	$editipaneloption =  $this->select_model->select_where("*","ipanel_pool_option","Ipanel_Pools_ID = '".$Ipanel_Pools_ID."'");
		
    	if($editipaneloption->num_rows())
    	{
    		$editipaneloption = $editipaneloption->row_array();
    	}
		else
		{
			$editipaneloption = NULL;
		}

		return $editipaneloption;
    }

    function getcompositeoption($Composite_Pools_ID)
    {
    	$editcompositeoption =  $this->select_model->select_where("*","composite_pools_option","Composite_Pools_ID = '".$Composite_Pools_ID."'");
		
    	if($editcompositeoption->num_rows())
    	{
    		$editcompositeoption = $editcompositeoption->row_array();
    	}
		else
		{
			$editcompositeoption = NULL;
		}

		return $editcompositeoption;
    }

    function getexipanel_with_row($Ipanel_Pools_ID)
    {

		$exipanel = $this->select_model->select_where("*","ipanel_extra_area","Ipanel_Pools_ID = '".$Ipanel_Pools_ID."'");
		if($exipanel->num_rows())
		{
			$exipanel_row = $exipanel->num_rows();
			$exipanel = $exipanel->result_array();
		}
		else
		{
			$exipanel = NULL;
			$exipanel_row = 0; 
		}

		return array($exipanel,$exipanel_row);
    }

    function getcompositepooldata($Composite_Pools_Series_ID, $Composite_Color_ID)
    {
    	$wherearr = array("composite_pools_data.Composite_Pools_Series_ID = '".$Composite_Pools_Series_ID."'","composite_pools_data.Composite_Color_ID = '".$Composite_Color_ID."'");
    	$jarr = array("composite_color","composite_pools_series","supplier_product");
    	$jwarr = array("composite_pools_data.Composite_Color_ID = composite_color.Composite_Color_ID" ,"composite_pools_data.Composite_Pools_Series_ID = composite_pools_series.Composite_Pools_Series_ID" ,"composite_pools_data.Supplier_Product_Code = supplier_product.Supplier_Product_Code");

		$returndata = $this->select_model->selectwherejoin("*","composite_pools_data",$wherearr,$jarr,$jwarr,NULL,NULL);
		if($returndata->num_rows())
		{
			$returndata_row = $returndata->num_rows();
			$returndata = $returndata->row_array();
		}
		else
		{
			$returndata_row = 0;
			$returndata = NULL;
		}


    	return $returndata;
    }

    function getflitrationunit_with_row()
    {
		$wherearr = array("Supplier_Product_Code like 'FFI20-%'","Supplier_Product_Code like 'FFI21-%'");

		$flitrationunit = $this->select_model->select_orwhere("*","supplier_product",$wherearr);
		if($flitrationunit->num_rows())
		{
			$flitrationunit_row = $flitrationunit->num_rows();
			$flitrationunit = $flitrationunit->result_array();
		}
		else
		{
			$flitrationunit_row = 0;
			$flitrationunit = NULL;
		}

		return array($flitrationunit,$flitrationunit_row);
    }

	function getcompositeflitrationunit_with_row()
    {
		$wherearr = array("Supplier_Product_Code like 'FFI10-%'");

		$returndata = $this->select_model->select_orwhere("*","supplier_product",$wherearr);
		if($returndata->num_rows())
		{
			$returndata_row = $returndata->num_rows();
			$returndata = $returndata->result_array();
		}
		else
		{
			$returndata_row = 0;
			$returndata = NULL;
		}

		return array($returndata,$returndata_row);
    }

    function getfliterpumpunit_with_row()
    {
		$wherearr = array("Supplier_Product_Code like 'FPU30-%'");

		$fliterpumpunit = $this->select_model->select_orwhere("*","supplier_product",$wherearr);
		if($fliterpumpunit->num_rows())
		{
			$fliterpumpunit_row = $fliterpumpunit->num_rows();
			$fliterpumpunit = $fliterpumpunit->result_array();
		}
		else
		{
			$fliterpumpunit_row = 0;
			$fliterpumpunit = NULL;
		}

		return array($fliterpumpunit,$fliterpumpunit_row);
    }

    function getcompositepipe_with_row()
    {
		$wherearr = array("Supplier_Product_Code like 'FFI60-%'");

		$returndata = $this->select_model->select_orwhere("*","supplier_product",$wherearr);
		if($returndata->num_rows())
		{
			$returndata_row = $returndata->num_rows();
			$returndata = $returndata->result_array();
		}
		else
		{
			$returndata_row = 0;
			$returndata = NULL;
		}

		return array($returndata,$returndata_row);
    }

    function getspapumpunit_with_row()
    {
    	$wherearr = array("Supplier_Product_Code like 'FPU30-%'");

		$spapumpunit = $this->select_model->select_orwhere("*","supplier_product",$wherearr);
		if($spapumpunit->num_rows())
		{
			$spapumpunit_row = $spapumpunit->num_rows();
			$spapumpunit = $spapumpunit->result_array();
		}
		else
		{
			$spapumpunit_row = 0;
			$spapumpunit = NULL;
		}

		return array($spapumpunit,$spapumpunit_row);
    }

    function getpumproom_with_row()
    {
    	$wherearr = array("Supplier_Product_Code like 'FAC21-001%'");
		$pumproom =  $this->select_model->select_orwhere("*","supplier_product",$wherearr);
		if($pumproom->num_rows())
		{
			$pumproom_row = $pumproom->num_rows();
			$pumproom = $pumproom->result_array();
		}
		else
		{
			$pumproom_row = 0;
			$pumproom = NULL;
		}

		return array($pumproom,$pumproom_row);
    }

    function getsaltprice()
    {
    	$wherearr = array("Supplier_Product_Code = 'FAC30-00013'");
    	$jarr = array("product_unit");
    	$jcarr = array("supplier_product.Product_Unit_ID = product_unit.Product_Unit_ID");
		$salt =  $this->select_model->selectorwherejoin("*","supplier_product",$wherearr,$jarr,$jcarr,NULL,NULL);
		if($salt->num_rows())
		{
			$salt = $salt->row_array();
		}
		else
		{
			$salt = NULL;
		}

		return $salt;
    }

    function getrobot_with_row()
    {
    	$wherearr = array("Supplier_Product_Code like 'FPM00-0025%'");
    	$jarr = array("product_unit");
    	$jcarr = array("supplier_product.Product_Unit_ID = product_unit.Product_Unit_ID");
		$robot =  $this->select_model->selectorwherejoin("*","supplier_product",$wherearr,$jarr,$jcarr,NULL,NULL);
		if($robot->num_rows())
		{
			$robot_row = $robot->num_rows();
			$robot = $robot->result_array();
		}
		else
		{
			$robot_row = 0;
			$robot = NULL;
		}

		return array($robot,$robot_row);
    }

    function getallproduct_with_row()
    {
    	$product = $this->select_model->selectwherejoin("*","supplier_product",NULL,"product_unit","supplier_product.Product_Unit_ID = product_unit.Product_Unit_ID",NULL,NULL);
		if($product->num_rows())
		{
			$product_row = $product->num_rows();
			$product = $product->result_array();
		}
		else
		{
			$product_row = 0;
			$product = NULL;
		}

		return array($product,$product_row);
    }

    function getusefiltration($Ipanel_Pool_Component_Filtration, $Ipanel_Pool_Component_Filtration_Unit)
    {
			$Use_Filtration =  $this->select_model->select_where("*","supplier_product","Supplier_Product_Code = '".$Ipanel_Pool_Component_Filtration."'");
			if($Use_Filtration->num_rows())
			{
				$Use_Filtration = $Use_Filtration->row_array();
			}
			else
			{
				$Use_Filtration = NULL;
			}

			if(strpos($Use_Filtration['Supplier_Product_Name'], 'TE 20') !== false || strpos($Use_Filtration['Supplier_Product_Name'], 'TE20') !== false)
			{
				$te20count = $Ipanel_Pool_Component_Filtration_Unit;
			}
			else
			{
				$te20count = 0;
			}

			if(strpos($Use_Filtration['Supplier_Product_Name'], 'TE 25') !== false || strpos($Use_Filtration['Supplier_Product_Name'], 'TE25') !== false)
			{
				$te25count = $Ipanel_Pool_Component_Filtration_Unit;
			}
			else
			{
				$te25count = 0;
			}

			if(strpos($Use_Filtration['Supplier_Product_Name'], 'BIKINI') !== false || strpos($Use_Filtration['Supplier_Product_Name'], 'BIKINI') !== false)
			{
				$bikinicount = 1;
			}
			else
			{
				$bikinicount = 0;
			}

			return array($te20count,$te25count,$bikinicount);
    }

    function getlogo($Ipanel_Pool_Option_Logo)
    {
    	$uselogo = array();
    	$logo = json_decode($Ipanel_Pool_Option_Logo);
    	$logosize = sizeof($logo);
    	for($xx=0;$xx<$logosize;$xx++){
			foreach ($logo[$xx] as $key => $value) {
				$logoid = $key;
				$logocount = $value;
			}
			$logounit[$logoid] = $logocount;
		}

		foreach ($logounit as $key => $value) {
			$Use_logo =  $this->select_model->selectwherejoin("*","supplier_product","Supplier_Product_Code = '".$key."'","product_unit","supplier_product.Product_Unit_ID = product_unit.Product_Unit_ID",NULL,NULL);
			if($Use_logo->num_rows())
			{
				$Use_logo = $Use_logo->row_array();

				array_push($uselogo, $Use_logo);
			}
		}

		return array($uselogo,$logounit);
    }

    function getheatpump($area)
    {
    	$allheatpump =  $this->select_model->selectwherejoin("*","supplier_product","Supplier_Product_Code like 'FPU60-00%' ","product_unit","supplier_product.Product_Unit_ID = product_unit.Product_Unit_ID",NULL,NULL);
		if($allheatpump->num_rows())
		{
			$allheatpump_row = $allheatpump->num_rows();
			$allheatpump = $allheatpump->result_array();
		}
		else
		{
			$allheatpump = NULL;
		}

		if($allheatpump!=NULL)
		{
			$allwatt = array();
			foreach ($allheatpump as $key => $value) {
				$text = $value['Supplier_Product_Name_ENG'];
				preg_match_all("/\([^\]]*\)/", $text, $matches);
				preg_match_all('#([0-9\.]+)#', $matches[0][0], $wattnum);
				array_push($allwatt,$wattnum[0][0]);
			}
		}

		$usekw = $area/2;

		$maxheatpumpkw = max($allwatt);

		asort($allwatt);

		$allwatt = array_values($allwatt);



		if($area>0)
		{
			if($usekw<=$maxheatpumpkw)
			{
				foreach ($allwatt as $key => $value) {
					if($value>$usekw)
					{
						$useheatkw = $value;
						break;
					}
				}

				$useheatpump = array();
				foreach ($allheatpump as $key => $value) {
					$text = $value['Supplier_Product_Name_ENG'];
					preg_match_all("/\([^\]]*\)/", $text, $matches);

					$pos = strpos($matches[0][0], $useheatkw);



					if($pos !== false)
					{
						array_push($useheatpump,$value);
						
					}
				}

			}
			elseif($usekw>$maxheatpumpkw&&$usekw<=($maxheatpumpkw*2))
			{
				$kwtouse =  $usekw/2;
				foreach ($allwatt as $key => $value) {
					if($value>$kwtouse)
					{
						$useheatkw = $value;
						break;
					}
				}

				$useheatpump = array();
				foreach ($allheatpump as $key => $value) {
					$pos = strpos($value['Supplier_Product_Name_ENG'], $useheatkw);

					for ($i=0; $i < 2; $i++) { 
						if($pos !== false)
						{
							array_push($useheatpump,$value);
						}
					}

				}
			}
			else
			{
				$useheatpump = array();
				$pumptouse =  $usekw/$maxheatpumpkw;

				$splitenumberofpump = explode(".", $pumptouse);
				foreach ($allheatpump as $key => $value) {
					$pos = strpos($value['Supplier_Product_Name_ENG'], $maxheatpumpkw);

					if($pos !== false)
					{
						for($xx=0;$xx<$splitenumberofpump[0];$xx++)
						{
							array_push($useheatpump,$value);
						}
					}
				}

				$splitenumberofpump[1] = "0.".$splitenumberofpump[1];
				$kwtouse =  $splitenumberofpump[1]*$maxheatpumpkw;

				foreach ($allwatt as $key => $value) {
					if($value>=$kwtouse)
					{
						$useheatkw = $value;
						break;
					}
				}

				foreach ($allheatpump as $key => $value) {
					$pos = strpos($value['Supplier_Product_Name_ENG'], $useheatkw);

					if($pos !== false)
					{
						array_push($useheatpump,$value);
						
					}
				}
			}

			return $useheatpump;
		}
    }


	function unique_multidim_array($array, $key) { 
    $temp_array = array(); 
    $i = 0; 
    $key_array = array(); 
    
    foreach($array as $val) { 
        if (!in_array($val[$key], $key_array)) { 
            $key_array[$i] = $val[$key]; 
            $temp_array[$i] = $val; 
        } 
        $i++; 
    } 
    	return $temp_array; 
	}

	function calkm($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		if($editproject!=NULL)
		{
			$project_lat = $editproject['Project_Lat'];

			$project_lon = $editproject['Project_Lon'];

			$getuserlatlon = $this->select_model->select_where("*","branch","Branch_ID = '".$editproject['Branch_ID']."'");

			if($getuserlatlon->num_rows())
			{
				$getuserlatlon = $getuserlatlon->row_array();

				$user_lat = $getuserlatlon['Branch_Lat'];
				$user_lon = $getuserlatlon['Branch_Lon'];

				$distanct = $this->distanceGeoPoints($project_lat,$project_lon,$user_lat,$user_lon);
				//echo " Plat : ".$project_lat." Plon : ".$project_lon." Ulat : ".$user_lat." Ulon : ".$user_lon;
				return $distanct;
			}
			else
			{
				return 0;
			}
			
		}
		else
		{
			return 0;
		}
	}

	function distanceGeoPoints ($lat1, $lng1, $lat2, $lng2) {
	    $earthRadius = 3958.75;

	    $dLat = deg2rad($lat2-$lat1);
	    $dLng = deg2rad($lng2-$lng1);


	    $a = sin($dLat/2) * sin($dLat/2) +
	       cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
	       sin($dLng/2) * sin($dLng/2);
	    $c = 2 * atan2(sqrt($a), sqrt(1-$a));
	    $dist = $earthRadius * $c;

	    // from miles
	    $meterConversion = 1609;
	    $geopointDistance = $dist * $meterConversion;

	    return $geopointDistance;
	}

	function ConvertToUTF8($text){

	    $encoding = mb_detect_encoding($text, mb_detect_order(), false);

	    if($encoding == "UTF-8")
	    {
	        $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');    
	    }


	    $out = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);


	    return $out;
	}

	function branchdc($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		if($editproject!=NULL)
		{

			$getdc = $this->select_model->select_where("*","branch","Branch_ID = '".$editproject['Branch_ID']."'");

			if($getdc->num_rows())
			{
				$getdc = $getdc->row_array();

				$dc = $getdc['Branch_Discount'];

				return $dc;
			}
			else
			{
				return 0;
			}
			
		}
		else
		{
			return 0;
		}
	}

	function gettransport($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		if($editproject!=NULL)
		{

			$gettrans = $this->select_model->select_where("*","product_transport","Province_ID = '".$editproject['Province_ID']."'");
			if($gettrans->num_rows())
			{
				$gettrans = $gettrans->row_array();

				return array($gettrans['Product_Transport_Six_Wheel'],$gettrans['Product_Transport_Ten_Wheel'],$gettrans['Product_Transport_Eightteen_Wheel']);
			}
			else
			{
				return array(30000,30000,30000);
			}
			
		}
		else
		{
			return array(0,0,0);
		}
	}

	function checksaleprovince($Project_ID)
    {
    	$jcarr = array("branch");
    	$jvarr = array("branch.Branch_ID = user.Branch_ID");
    	$selprov = $this->select_model->selectwherejoin("*","user","User_ID = '".$this->session->userdata('User_ID')."'",$jcarr,$jvarr,NULL,NULL);
    	if($selprov->num_rows())
		{
			$selprov = $selprov->row_array();
		}
		else
		{
			$selprov = NULL;
		};

    	if($selprov != NULL)
    	{
    		$selprov = $selprov['Province_ID'];
    	}
    	else
    	{
    		redirect(base_url());
    	}
    }

    function calculate_ipanel_apc($Project_ID)
    {
    	$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$exipanel = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[0];
		$exipanel_row = $this->getexipanel_with_row($editipanel['Ipanel_Pools_ID'])[1];

		$le = $editipanel['Ipanel_Pools_Length'];
		$wi = $editipanel['Ipanel_Pools_Width'];
		$dep1 = $editipanel['Ipanel_Pools_Dept1'];
		$dep2 = $editipanel['Ipanel_Pools_Dept2'];
		$dep3 = $editipanel['Ipanel_Pools_Dept3'];
		$alldep = 0;
		$area = 0;
		$perimeter = 0;
		$capacity = 0;

		if($editipanel['Ipanel_Pools_Dept1']!=""&&$editipanel['Ipanel_Pools_Dept1']!=0&&$editipanel['Ipanel_Pools_Dept1']!=NULL)
		{
			$alldep++;
		}
		if($editipanel['Ipanel_Pools_Dept2']!=""&&$editipanel['Ipanel_Pools_Dept2']!=0&&$editipanel['Ipanel_Pools_Dept2']!=NULL)
		{
			$alldep++;
		}
		if($editipanel['Ipanel_Pools_Dept3']!=""&&$editipanel['Ipanel_Pools_Dept3']!=0&&$editipanel['Ipanel_Pools_Dept3']!=NULL)
		{
			$alldep++;
		}

		if($editipanel['Ipanel_Pools_Shape']=="Ovoid")
		{
			$Area = $le*$wi*0.917;
			$Perimeter = (2*($le+$wi))*0.91;
			$Capacity = ($Area*($dep1+$dep2+$dep3))/$alldep ;
		}
		else if($editipanel['Ipanel_Pools_Shape']=="Liberty")
		{
			$Area = $le*$wi*0.81;
			$Perimeter = (2*($le+$wi))*0.85;
			$Capacity = ($Area*($dep1+$dep2+$dep3))/$alldep ;
		}
		else
		{
			$Area = $le*$wi;
			$Perimeter = (2*($le+$wi));
			$Capacity = ($Area*($dep1+$dep2+$dep3))/$alldep ;
		}
		if($exipanel_row>0)
		{
			if($editipanel['Ipanel_Pools_Extra_Shape']=="Rectangular")
			{	
				$sumwl = 0;
				$sum2w = 0;

				foreach ($exipanel as $key => $value) {

					if($value['Ipanel_Extra_Area_Length']==""||$value['Ipanel_Extra_Area_Length']==null)
					{
						$value['Ipanel_Extra_Area_Length'] = 0;
					}
					if($value['Ipanel_Extra_Area_Width']==""||$value['Ipanel_Extra_Area_Width']==null)
					{
						$value['Ipanel_Extra_Area_Width'] = 0;
					}

					$sumwl = $sumwl+($value['Ipanel_Extra_Area_Length']*$value['Ipanel_Extra_Area_Width']);
					$sum2w = $sum2w+$value['Ipanel_Extra_Area_Width'];

				}

				$sum2w = 2*$sum2w;

				$Area = $Area+$sumwl;
				$Perimeter = $Perimeter+$sum2w;
				$Capacity = (($Area)*($dep1+$dep2+$dep3))/$alldep ;

			}
			else if($editipanel['Ipanel_Pools_Extra_Shape']=="Semicircle")
			{
				$sumrad = 0;
				$sumradup2 = 0;

				foreach ($exipanel as $key => $value) {

					if($value['Ipanel_Extra_Area_Radius']==""||$value['Ipanel_Extra_Area_Radius']==null)
					{
						$value['Ipanel_Extra_Area_Radius'] = 0;
					}

					$sumrad = $sumrad+$value['Ipanel_Extra_Area_Radius'];
					$sumradup2 = $sumradup2+($value['Ipanel_Extra_Area_Radius']*$value['Ipanel_Extra_Area_Radius']);
					
				}

				$Area = $Area+(  ( pi()/2 ) * $sumradup2 );
				$Perimeter = $Perimeter+( ( pi()-2 ) * $sumrad );
				$Capacity = (($Area)*($dep1+$dep2+$dep3))/$alldep ;
			}
		}

		return array($Area,$Perimeter,$Capacity);
    }

	function sum_spa_swim_heat_scul_pump($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$badu4716standardqty=0;
		$badu4722standardqty=0;
		$allheatpump=0;
		$sfp1500qty=0;

		if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']!=""&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']!=0)
		{
			if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']>0&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=4)
			{
				$badu4716standardqty += 1;
			}
			else
			{
				$badu4722standardqty += ceil($editipaneloption['Ipanel_Pool_Option_Spa_Jet']/6);
			}
		}

		$baduheatpump = 0;

		if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL)
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}

		if($heatpump!=NULL)
		{
			/*$allheatpump=0;
			$fixnum = 1;
			$heatpump_arr = array();

			foreach ($heatpump as $key => $value) 
			{
				array_push($heatpump_arr, $value['Supplier_Product_Code']);	
			}

			$heatpump_arr = array_count_values($heatpump_arr);

			$nodupheatpump = $this->unique_multidim_array($heatpump,"Supplier_Product_Code");


			foreach ($nodupheatpump as $key => $value) 
			{
				foreach ($heatpump_arr as $key2 => $value2) {
					if($key2==$value['Supplier_Product_Code'])
					{
						$heatpumpqty = $value2;
						$allheatpump += $heatpumpqty;
						break;
					}
					else
					{
						$heatpumpqty = $fixnum;
						$allheatpump += $fixnum;
						break;
					}
				}
			}*/
			if(sizeof($heatpump)==1)
			{
				$baduheatpump = 1;
			}
			else if(sizeof($heatpump)>1)
			{
				$baduheatpump = ceil(sizeof($heatpump)/2);
			}

		}

		if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
		{
			$sfp1500qty = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
		}

		if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>0&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!="")
		{
			if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/16")
			{
				$badu4716standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
			}
			elseif($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/22")
			{
				$badu4722standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
			}
		}

		$sum = $badu4716standardqty+$badu4722standardqty+$baduheatpump+$sfp1500qty;

		return $sum;
	}

	function sum_composite_spa_swim_heat_scul_mechanicalroom($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];
		$Dept1 = $editcompositepooldata['Composite_Pools_Data_Dept1'];
		$Dept2 = $editcompositepooldata['Composite_Pools_Data_Dept2'];
		$Dept3 = $editcompositepooldata['Composite_Pools_Data_Dept3'];

		$badu4716standardqty=0;
		$badu4722standardqty=0;
		$allheatpump=0;
		$sfp1500pumproomqty=0;
		$intelipumproom=0;
		$badusinglepumpqty=0;
		$badudoublepumpqty=0;

		if($editcompositeoption['Composite_Pools_Option_Spa_Jet']!=NULL&&$editcompositeoption['Composite_Pools_Option_Spa_Jet']!=""&&$editcompositeoption['Composite_Pools_Option_Spa_Jet']!=0)
		{
			if($editcompositeoption['Composite_Pools_Option_Spa_Jet']>0&&$editcompositeoption['Composite_Pools_Option_Spa_Jet']<=4)
			{
				$badu4716standardqty += 1;
			}
			else
			{
				$badu4722standardqty += ceil($editcompositeoption['Composite_Pools_Option_Spa_Jet']/6);
			}
		}

		$baduheatpump = 0;

		if($editcompositeoption['Composite_Pools_Option_Pool_Heating_System']!=NULL&&$editcompositeoption['Composite_Pools_Option_Pool_Heating_System']!="")
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}

		if($heatpump!=NULL)
		{
			if(sizeof($heatpump)==1)
			{
				$badu4716standardqty += 1;
			}
			else if(sizeof($heatpump)>1)
			{
				$badu4722standardqty += ceil(sizeof($heatpump)/2);
			}

		}

		if($editcompositeoption['Composite_Pools_Option_Swim_Jet_System']=="jdeyeball")
		{
			$sfp1500pumproomqty = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
			$intelipumproom = 0;
		}
		else if($editcompositeoption['Composite_Pools_Option_Swim_Jet_System']=="aquaflow")
		{
			$intelipumproom = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
			$sfp1500pumproomqty = 0;
		}

		if($editcompositeoption['Composite_Pools_Option_Pool_Sculpture']>0&&$editcompositeoption['Composite_Pools_Option_Pool_Sculpture']!=NULL&&$editcompositeoption['Composite_Pools_Option_Pool_Sculpture']!="")
		{
			if($editcompositeoption['Composite_Pools_Option_Pool_Sculpture_Pump']=="47/16")
			{
				$badu4716standardqty += $editcompositeoption['Composite_Pools_Option_Pool_Sculpture'];
			}
			elseif($editcompositeoption['Composite_Pools_Option_Pool_Sculpture_Pump']=="47/22")
			{
				$badu4722standardqty += $editcompositeoption['Composite_Pools_Option_Pool_Sculpture'];
			}
		}

		//$sum = $badu4716standardqty+$badu4722standardqty+$baduheatpump+$sfp1500qty;

		if(($badu4716standardqty+$badu4722standardqty)/2>0.5)
		{
			$badudoublepumpqty = floor( (($badu4716standardqty+$badu4722standardqty)/2) );
		}
		else
		{
			$badudoublepumpqty = 0;
		}

		if(($badu4716standardqty+$badu4722standardqty)/2>0.45)
		{
			$badusinglepumpqty = ceil( (($badu4716standardqty+$badu4722standardqty)/2) - $badudoublepumpqty );
		}
		else
		{
			$badusinglepumpqty = 0;
		}

		$sum = $sfp1500pumproomqty+$intelipumproom+$badusinglepumpqty+$badudoublepumpqty;

		return $sum;
	}

	function sum_composite_spa_swim_heat_scul_pump($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];
		$Dept1 = $editcompositepooldata['Composite_Pools_Data_Dept1'];
		$Dept2 = $editcompositepooldata['Composite_Pools_Data_Dept2'];
		$Dept3 = $editcompositepooldata['Composite_Pools_Data_Dept3'];

		$badu4716standardqty=0;
		$badu4722standardqty=0;
		$allheatpump=0;
		$sfp1500qty=0;
		$intelipumproom=0;

		if($editcompositeoption['Composite_Pools_Option_Spa_Jet']!=NULL&&$editcompositeoption['Composite_Pools_Option_Spa_Jet']!=""&&$editcompositeoption['Composite_Pools_Option_Spa_Jet']!=0)
		{
			if($editcompositeoption['Composite_Pools_Option_Spa_Jet']>0&&$editcompositeoption['Composite_Pools_Option_Spa_Jet']<=4)
			{
				$badu4716standardqty += 1;
			}
			else
			{
				$badu4722standardqty += ceil($editcompositeoption['Composite_Pools_Option_Spa_Jet']/6);
			}
		}

		if($editcompositeoption['Composite_Pools_Option_Pool_Heating_System']!=NULL&&$editcompositeoption['Composite_Pools_Option_Pool_Heating_System']!="")
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}

		if($heatpump!=NULL)
		{
			if(sizeof($heatpump)==1)
			{
				$badu4716standardqty += 1;
			}
			else if(sizeof($heatpump)>1)
			{
				$badu4722standardqty += ceil(sizeof($heatpump)/2);
			}

		}

		if($editcompositeoption['Composite_Pools_Option_Swim_Jet_System']=="jdeyeball")
		{
			$sfp1500qty = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
		}
		else if($editcompositeoption['Composite_Pools_Option_Swim_Jet_System']=="aquaflow")
		{
			$sfp1500qty = 0;
		}

		if($editcompositeoption['Composite_Pools_Option_Pool_Sculpture']>0&&$editcompositeoption['Composite_Pools_Option_Pool_Sculpture']!=NULL&&$editcompositeoption['Composite_Pools_Option_Pool_Sculpture']!="")
		{
			if($editcompositeoption['Composite_Pools_Option_Pool_Sculpture_Pump']=="47/16")
			{
				$badu4716standardqty += $editcompositeoption['Composite_Pools_Option_Pool_Sculpture'];
			}
			elseif($editcompositeoption['Composite_Pools_Option_Pool_Sculpture_Pump']=="47/22")
			{
				$badu4722standardqty += $editcompositeoption['Composite_Pools_Option_Pool_Sculpture'];
			}
		}

		$sum = $badu4716standardqty+$badu4722standardqty+$sfp1500qty;

		return $sum;
	}

	function calcable_10x2_5($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$sfp1500qty = 0;
		$badu4722standardqty = 0;
		$badu4716standardqty = 0;

		if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
		{
			$sfp1500qty = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
		}

		if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>0&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!="")
		{
			if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/16")
			{
				$badu4716standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
			}
			elseif($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/22")
			{
				$badu4722standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
			}
		}

		if($editipanel['Ipanel_Pools_Where']=="home")
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
		}
		else
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
		}

		$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

		$te20count = $Use_Filtration[0];
		$te25count = $Use_Filtration[1];
		$bikinicount = $Use_Filtration[2];

		return ceil( ($te20count+$te25count+$sfp1500qty+$badu4716standardqty+$badu4722standardqty)*20 );
	}


	function calculate_ipanel_structure($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);
		
		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate

		$ipanelstructure=NULL;
		$bolt=NULL;
		$panelclip=NULL;
		$struct60=NULL;
		$struct120=NULL;
		$fiberpanel=NULL;
		$outsidecerve=NULL;

		$te20count=0;
		$te25count=0;
		$bikinicount=0;

		$sumpricestructure=0;

		$ipanel_structure = array();
		$arrcount = 0;

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FST10-00102'){
					$ipanelstructure = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='RSS30-00300'){
					$bolt = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00802'){
					$panelclip = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00300'){
					$struct60 = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00310'){
					$struct120 = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00502'){
					$fiberpanel = $value;
					$arrcount++;
				}
				if($value['Supplier_Product_Code']=='FST10-00401'){
					$outsidecerve = $value;
					$arrcount++;
				}
			}

			$ipanelstructure['QTY']=0;
			$bolt['QTY']=0;
			$panelclip['QTY']=0;
			$struct60['QTY']=0;
			$struct120['QTY']=0;
			$fiberpanel['QTY']=0;
			$outsidecerve['QTY']=0;

			if($editipanel['Ipanel_Pools_Where']=="home")
			{
				$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
			}
			else
			{
				$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
			}

			$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

			$te20count = $Use_Filtration[0];
			$te25count = $Use_Filtration[1];
			$bikinicount = $Use_Filtration[2];

			$ipanelstructure['QTY'] = $Perimeter;
			$ipanelstructure['QTY'] = $ipanelstructure['QTY']-($te20count*0.75);
			$ipanelstructure['QTY'] = $ipanelstructure['QTY']-($te25count*0.75);

			if($editipanel['Ipanel_Pools_Shape']=="Ovoid")
			{
				$ipanelstructure['QTY'] = $ipanelstructure['QTY']-1;
			}
			if($editipanel['Ipanel_Pools_Shape']=="Rectangular")
			{
				$ipanelstructure['QTY'] = $ipanelstructure['QTY']-1;
			}

			$panelclip['QTY'] = ceil($ipanelstructure['QTY']/1.5)*5;

			if($editipanel['Ipanel_Pools_Shape']=="Ovoid")
			{
				$fiberpanel['QTY'] = $fiberpanel['QTY']+(1*4);
			}
			if($editipanel['Ipanel_Pools_Shape']=="Rectangular")
			{
				$fiberpanel['QTY'] = $fiberpanel['QTY']+(1*4);
			}

			$bolt['QTY'] = ($panelclip['QTY']*1.2)+($fiberpanel['QTY']+$outsidecerve['QTY']+$te25count+$te20count)*10;

			$struct60['QTY'] = ceil($ipanelstructure['QTY']/1.5)+$fiberpanel['QTY']+$outsidecerve['QTY']+$te25count+$te20count+1+$editipanelcomponent['Ipanel_Pool_Component_By_Pass_Jet']; //(1 = encappump + standardpump)
			$struct120['QTY'] = $struct60['QTY'];

			array_push($ipanel_structure, $ipanelstructure);
			array_push($ipanel_structure, $panelclip);
			array_push($ipanel_structure, $fiberpanel);
			array_push($ipanel_structure, $outsidecerve);
			array_push($ipanel_structure, $bolt);
			array_push($ipanel_structure, $struct60);
			array_push($ipanel_structure, $struct120);

			$sumpricestructure = $sumpricestructure+($ipanelstructure['QTY']*$ipanelstructure['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($panelclip['QTY']*$panelclip['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($fiberpanel['QTY']*$fiberpanel['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($outsidecerve['QTY']*$outsidecerve['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($bolt['QTY']*$bolt['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($struct60['QTY']*$struct60['Supplier_Product_Price']);
			$sumpricestructure = $sumpricestructure+($struct120['QTY']*$struct120['Supplier_Product_Price']);

			return array($ipanel_structure, $sumpricestructure);

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Structure</td></tr>";
			//end head
			/*foreach ($ipanel_structure as $key => $value) {
				echo "<tr style='border: 1px solid black;'>";
				echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Code']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Name']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['Product_Unit_Name']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['QTY']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['Supplier_Product_Price']."</td>";
				echo "<td style='border: 1px solid black;'>".$value['QTY']*$value['Supplier_Product_Price']."</td>";
				echo "</tr>";
			}
			

			echo $sumpricestructure;*/
		}
		else
		{
			return array(NULL, NULL);
		}

	}

	function calculate_ipanel_lightblue_wall($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate
		$wallliner = NULL;
		$walllinerlockstrip = NULL;
		$walllinerlockinsert = NULL;
		$infinitylinerlock = NULL;

		$sumpricewallliner = 0;

		$arraypoolwall = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI00-00509'){
					$wallliner = $value;
				}
				if($value['Supplier_Product_Code']=='FLI21-00301'){
					$walllinerlockstrip = $value;
				}
				if($value['Supplier_Product_Code']=='FLI21-00302'){
					$walllinerlockinsert = $value;
				}
				if($value['Supplier_Product_Code']=='FST10-00601'){
					$infinitylinerlock = $value;
				}
			}

			$infinitylinerlock['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] > 0&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!="")
			{
				
				$infinityedge_liner = ($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']*2)+2;

				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Build']=="BuildOnSite")
				{
					$infinityedge_liner += 2;

					$infinitylinerlock['QTY'] = $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']+2;
				}
			}
			else
			{
				$infinityedge_liner = 0;
			}


			$wallliner['QTY'] = ceil($Perimeter + 1 + $infinityedge_liner); 
			$walllinerlockstrip['QTY'] = ceil($Perimeter + 1);
			$walllinerlockinsert['QTY'] = ceil($Perimeter + 1);

			array_push($arraypoolwall, $wallliner);
			array_push($arraypoolwall, $walllinerlockstrip);
			array_push($arraypoolwall, $walllinerlockinsert);
			array_push($arraypoolwall, $infinitylinerlock);

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Light Blue Wall Liner</td></tr>";
			//end head
			$sumpricewallliner = $sumpricewallliner+($wallliner['QTY']*$wallliner['Supplier_Product_Price']);
			$sumpricewallliner = $sumpricewallliner+($walllinerlockstrip['QTY']*$walllinerlockstrip['Supplier_Product_Price']);
			$sumpricewallliner = $sumpricewallliner+($walllinerlockinsert['QTY']*$walllinerlockinsert['Supplier_Product_Price']);

			return array($arraypoolwall, $sumpricewallliner);


		}
		else
		{
			return array(NULL, NULL);
		}
	}

	function calculate_ipanel_lightblue_floor($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate
		$floorliner = NULL;

		$sumpricefloorliner = 0;

		$arrayfloorliner = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI00-00508'){
					$floorliner = $value;
				}
			}

			$floorliner['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']==NULL||$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] =="")
			{
				$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']  = 0;
			}

			$floorliner['QTY'] = ceil( (($Area+($Perimeter/2))/1.3) + $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] );

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Light Blue Floor Liner</td></tr>";
			//end head

			$sumpricefloorliner = $sumpricefloorliner+($floorliner['QTY']*$floorliner['Supplier_Product_Price']);

			array_push($arrayfloorliner, $floorliner);

			return array($arrayfloorliner, $sumpricefloorliner);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_davinci_wall($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate
		$wallliner = NULL;
		$walllinerlockstrip = NULL;
		$walllinerlockinsert = NULL;

		$sumpricewallliner = 0;

		$arrdavinciwall = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI00-00509'){
					$wallliner = $value;
				}
				if($value['Supplier_Product_Code']=='FLI21-00301'){
					$walllinerlockstrip = $value;
				}
				if($value['Supplier_Product_Code']=='FLI21-00302'){
					$walllinerlockinsert = $value;
				}
			}

			$wallliner['QTY'] = 0;
			$walllinerlockstrip['QTY'] = 0;
			$walllinerlockinsert['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] > 0)
			{
				$infinityedge_liner = ($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']*2)+2;
			}
			else
			{
				$infinityedge_liner = 0;
			}

			if($editipanel['Ipanel_Pools_Series']!="davinciblue")
			{
				$wallliner['QTY'] = ceil($Perimeter + 1 + $infinityedge_liner);
			}
			else
			{
				$wallliner['QTY'] = 0;
			}

			$walllinerlockstrip['QTY'] = ceil($Perimeter + 1 +$infinityedge_liner);
			$walllinerlockinsert['QTY'] = ceil($Perimeter + 1 +$infinityedge_liner);

			array_push($arrdavinciwall, $wallliner);
			array_push($arrdavinciwall, $walllinerlockstrip);
			array_push($arrdavinciwall, $walllinerlockinsert);

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Light Blue Wall Liner</td></tr>";
			//end head

			$sumpricewallliner = $sumpricewallliner+($wallliner['QTY']*$wallliner['Supplier_Product_Price']);
			$sumpricewallliner = $sumpricewallliner+($walllinerlockstrip['QTY']*$walllinerlockstrip['Supplier_Product_Price']);
			$sumpricewallliner = $sumpricewallliner+($walllinerlockinsert['QTY']*$walllinerlockinsert['Supplier_Product_Price']);

			return array($arrdavinciwall,$sumpricewallliner);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_davinci_floor($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		//set calculate
		$floorliner = NULL;

		$sumpricefloorliner = 0;

		$arrdavincifloor = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI00-00900'){
					$floorliner = $value;
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']==NULL||$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] =="")
			{
				$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']  = 0;
			}
			else
			{
				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Build']=="BuildOnSite")
				{
					$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] += 2;
				}
			}

			$floorliner['QTY'] = ceil( (($Area+($Perimeter/2))/1.6) + $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] );

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Davinchi Liner</td></tr>";
			//end head
			$sumpricefloorliner = $sumpricefloorliner+($floorliner['QTY']*$floorliner['Supplier_Product_Price']);

			array_push($arrdavincifloor, $floorliner);

			return array($arrdavincifloor,$sumpricefloorliner);
		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_filtration($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL)
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}

		
		//set calculate
		$te20panel = NULL;
		$te25panel = NULL;
		$bikinipanel = NULL;
		$badu4716standard = NULL;
		$badu4722standard = NULL;
		$badu4716encapsulate = NULL;
		$badu4722encapsulate = NULL;
		$badusinglepump = NULL;
		$badudoublepump = NULL;
		$standardpoolcontrol = NULL;
		$x20cl = NULL;
		$x30cl = NULL;

		$sumpriceflitration = 0;

		$arrfiltration = array();

		if($editipanel['Ipanel_Pools_Where']=="home")
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
		}
		else
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
		}

		if($product_row>0)
		{

			$Use_Filtration = $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);	

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FFI20-00715'){
					$te20panel = $value;
				}
				if($value['Supplier_Product_Code']=='FFI20-00708'){
					$te25panel = $value;
				}
				if($value['Supplier_Product_Code']=='FFI21-00504')
				{
					$bikinipanel = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00200'){
					$badu4716standard = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00201'){
					$badu4722standard = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00203'){
					$badu4716encapsulate = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00204'){
					$badu4722encapsulate = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00117'){
					$badusinglepump = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00122'){
					$badudoublepump = $value;
				}
				if($value['Supplier_Product_Code']=='FPU10-00104'){
					$standardpoolcontrol = $value;
				}
				if($value['Supplier_Product_Code']=='FAC30-00200'){
					$x20cl = $value;
				}
				if($value['Supplier_Product_Code']=='FAC30-00201'){
					$x30cl = $value;
				}
			}

			$te20panel['QTY'] = $Use_Filtration[0];
			$te25panel['QTY'] = $Use_Filtration[1];
			$bikinipanel['QTY'] = $Use_Filtration[2];
			$badu4716standard['QTY'] = 0;
			$badu4722standard['QTY'] = 0;
			$badu4716encapsulate['QTY'] = 0;
			$badu4722encapsulate['QTY'] = 0;
			$badusinglepump['QTY'] = 0;
			$badudoublepump['QTY'] = 0;
			$standardpoolcontrol['QTY'] = 0;
			$x20cl['QTY'] = 0;
			$x30cl['QTY'] = 0;

			if($editipanelcomponent['Ipanel_Pool_Component_Filter_Pump']=='Standard')
			{
				$badu4716standard['QTY'] = $te20panel['QTY'];
				$badu4722standard['QTY'] = $te25panel['QTY'];
				if($bikinipanel['QTY']>0)
				{
					$badu4716standard['QTY'] = $bikinipanel['QTY'];
				}
			}

			if($editipanelcomponent['Ipanel_Pool_Component_Filter_Pump']=='Endcapsulated')
			{
				$badu4716encapsulate['QTY'] = $te20panel['QTY'];
				$badu4722encapsulate['QTY'] = $te25panel['QTY'];
				if($bikinipanel['QTY']>0)
				{
					$badu4716encapsulate['QTY'] = $bikinipanel['QTY'];
				}
			}

			if($editipanelcomponent['Ipanel_Pool_Component_Control_Panel']=='salt')
			{
				$x20cl['QTY'] = $te20panel['QTY'];
				$x30cl['QTY'] = $te25panel['QTY'];
				if($bikinipanel['QTY']>0)
				{
					$x20cl['QTY'] = $bikinipanel['QTY'];
				}
			}
			else
			{
				if($te20panel['QTY']>=1)
				{
					$standardpoolcontrol['QTY'] = $te20panel['QTY'];
				}
				elseif($bikinipanel['QTY']>=1)
				{
					$standardpoolcontrol['QTY'] = $bikinipanel['QTY'];
				}
				elseif($te25panel['QTY']>=1)
				{
					$standardpoolcontrol['QTY'] = $te25panel['QTY'];
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=0)
			{
				$badu4716standard['QTY'] = $badu4716standard['QTY'];
				$badu4722standard['QTY'] = $badu4722standard['QTY'];
			}
			if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']>0&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=4)
			{
				$badu4716standard['QTY'] += 1;
			}
			else
			{
				$badu4722standard['QTY'] += ceil($editipaneloption['Ipanel_Pool_Option_Spa_Jet']/6);
			}

			if($heatpump!=NULL)
			{
				if(sizeof($heatpump)==1)
				{
					$badu4716standard['QTY'] += 1;
				}
				else if(sizeof($heatpump)>1)
				{
					$badu4722standard['QTY'] += ceil(sizeof($heatpump)/2);
				}

			}

			if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>0&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!=NULL)
			{
				if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/16")
				{
					$badu4716standard['QTY'] += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
				}
				elseif($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/22")
				{
					$badu4722standard['QTY'] += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
				}
			}

			if(($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY']-$te20panel['QTY']-$te25panel['QTY']+$bikinipanel['QTY'])/2>0.45)
			{
				$badudoublepump['QTY'] = floor( (($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY']-$te20panel['QTY']-$te25panel['QTY']+$bikinipanel['QTY'])/2) );
			}
			else
			{
				$badudoublepump['QTY'] = 0;
			}

			if(($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY']-$te20panel['QTY']-$te25panel['QTY']+$bikinipanel['QTY'])/2>0.45)
			{
				$badusinglepump['QTY'] = ceil( (($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY']-$te20panel['QTY']-$te25panel['QTY']+$bikinipanel['QTY'])/2) - $badudoublepump['QTY'] );
			}
			else
			{
				$badusinglepump['QTY'] = 0;
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Flitration</td></tr>";
			//end head

			$sumpriceflitration = $sumpriceflitration+($te20panel['QTY']*$te20panel['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($te25panel['QTY']*$te25panel['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($bikinipanel['QTY']*$bikinipanel['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badu4716standard['QTY']*$badu4716standard['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badu4722standard['QTY']*$badu4722standard['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badu4716encapsulate['QTY']*$badu4716encapsulate['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badu4722encapsulate['QTY']*$badu4722encapsulate['Supplier_Product_Price']);

			$useheatpump = array();

			if($heatpump!=NULL)
			{
				
				$fixnum = 1;
				$heatpump_arr = array();

				foreach ($heatpump as $key => $value) 
				{
					array_push($heatpump_arr, $value['Supplier_Product_Code']);	
				}

				$heatpump_arr = array_count_values($heatpump_arr);

				$nodupheatpump = $this->unique_multidim_array($heatpump,"Supplier_Product_Code");

				$useheatpump = array_values($nodupheatpump);

				foreach ($useheatpump as $key => $value) 
				{
					//$useheatpump[$key]['QTY'] = 0;
					foreach ($heatpump_arr as $key2 => $value2) {
						if($key2==$value['Supplier_Product_Code'])
						{
							$thisqty = $value2;
							$useheatpump[$key]['QTY'] = $value2;
							break;
						}
						else
						{
							$thisqty = $fixnum;
							$useheatpump[$key]['QTY'] = $fixnum;
							break;
						}
					}
					$sumpriceflitration = $sumpriceflitration+($thisqty*$value['Supplier_Product_Price']);
				}
			}

			$sumpriceflitration = $sumpriceflitration+($badusinglepump['QTY']*$badusinglepump['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($badudoublepump['QTY']*$badudoublepump['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($standardpoolcontrol['QTY']*$standardpoolcontrol['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($x20cl['QTY']*$x20cl['Supplier_Product_Price']);
			$sumpriceflitration = $sumpriceflitration+($x30cl['QTY']*$x30cl['Supplier_Product_Price']);

			array_push($arrfiltration, $te20panel);
			array_push($arrfiltration, $te25panel);
			array_push($arrfiltration, $bikinipanel);
			array_push($arrfiltration, $badu4716standard);
			array_push($arrfiltration, $badu4722standard);
			array_push($arrfiltration, $badu4716encapsulate);
			array_push($arrfiltration, $badu4722encapsulate);
			if($heatpump!=NULL)
			{
				foreach ($useheatpump as $key => $value) {
					array_push($arrfiltration, $value);
				}
			}
			array_push($arrfiltration, $badusinglepump);
			array_push($arrfiltration, $badudoublepump);
			array_push($arrfiltration, $standardpoolcontrol);
			array_push($arrfiltration, $x20cl);
			array_push($arrfiltration, $x30cl);

			
			return array($arrfiltration,$sumpriceflitration);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_underwaterlight($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$arrled = array();

		$colorled = NULL;
		$whiteled = NULL;
		$abs = NULL;

		$sumpriceled = 0;

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI51-00502'){
					$whiteled = $value;
				}
				if($value['Supplier_Product_Code']=='FLI51-00512'){
					$colorled = $value;
				}
				if($value['Supplier_Product_Code']=='FLI52-00101'){
					$abs = $value;
				}
			}

			$colorled['QTY'] = 0;
			$whiteled['QTY'] = 0;
			$abs['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Under_Water_Light']=="white")
			{
				$whiteled['QTY'] = $editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'];
			}
			elseif($editipaneloption['Ipanel_Pool_Option_Under_Water_Light']=="color")
			{
				$colorled['QTY'] = $editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'];
			}

			$abs['QTY'] = $colorled['QTY']+$whiteled['QTY'];

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>UNDER WATER LIGHT</td></tr>";
			//end head
			$sumpriceled = $sumpriceled+($colorled['QTY']*$colorled['Supplier_Product_Price']);
			$sumpriceled = $sumpriceled+($whiteled['QTY']*$whiteled['Supplier_Product_Price']);
			$sumpriceled = $sumpriceled+($abs['QTY']*$abs['Supplier_Product_Price']);

			array_push($arrled, $colorled);
			array_push($arrled, $whiteled);
			array_push($arrled, $abs);

			return array($arrled,$sumpriceled);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_spaoption($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$hmcp = NULL;
		$spajet = NULL;
		$hmcrp = NULL;
		$hmcrpf6 = NULL;

		$sumpricespaoption = 0;

		$arrspa = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FPU10-00102'){
					$hmcp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC20-00300'){
					$spajet = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00201'){
					$hmcrp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00202'){
					$hmcrpf6 = $value;
				}
			}

			$hmcp['QTY'] = 0;
			$spajet['QTY'] = 0;
			$hmcrp['QTY'] = 0;
			$hmcrpf6['QTY'] = 0;

			$spajet['QTY'] = $editipaneloption['Ipanel_Pool_Option_Spa_Jet'];


			if($spajet['QTY'] == NULL)
			{
				$spajet['QTY'] = 0;
			}
			if($spajet['QTY']>1)
			{
				$hmcp['QTY'] = 1;
			}

			if($spajet['QTY']>0&&$spajet['QTY']<=4)
			{
				$hmcrp['QTY'] = 1;
			}
			elseif($spajet['QTY']>4)
			{
				$hmcrpf6['QTY'] = $spajet['QTY']/6;
			}
			else
			{
				$hmcrp['QTY'] = 0;
				$hmcrpf6['QTY'] = 0;
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>SPA OPTION</td></tr>";
			//end head

			$sumpricespaoption = $sumpricespaoption+($hmcp['QTY']*$hmcp['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($spajet['QTY']*$spajet['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($hmcrp['QTY']*$hmcrp['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($hmcrpf6['QTY']*$hmcrpf6['Supplier_Product_Price']);

			array_push($arrspa, $hmcp);
			array_push($arrspa, $spajet);
			array_push($arrspa, $hmcrp);
			array_push($arrspa, $hmcrpf6);

			return array($arrspa,$sumpricespaoption);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_inletandfitting($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL)
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}
		
		$onga = NULL;
		$returneyeball = NULL;
		$eyeball = NULL;
		$speedupjet = NULL;

		$sumpriceinletandfitting = 0;

		$arrinlet = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI40-00800'){
					$onga = $value;
				}
				if($value['Supplier_Product_Code']=='FLI40-00102'){
					$returneyeball = $value;
				}
				if($value['Supplier_Product_Code']=='FLI40-00100'){
					$eyeball = $value;
				}
				if($value['Supplier_Product_Code']=='FFI60-00512'){
					$speedupjet = $value;
				}
			}

			$onga['QTY'] = 0;
			$returneyeball['QTY'] = 0;
			$eyeball['QTY'] = 0;
			$speedupjet['QTY'] = 0;

			$returneyeball['QTY'] = (ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15)*3)+$editipanelcomponent['Ipanel_Pool_Component_By_Pass_Jet'];

			if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
			{
				$eyeball['QTY'] = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
				$speedupjet['QTY'] = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
			}

			if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>=1)
			{
				$onga['QTY'] += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']*2;
			}

			if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
			{
				$onga['QTY'] += $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit']*2;
			}

			if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']>0&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=4)
			{
				$onga['QTY'] += 2;
			}
			else
			{
				$onga['QTY'] += (ceil($editipaneloption['Ipanel_Pool_Option_Spa_Jet']/6))*2;
			}

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>0)
			{
				$onga['QTY'] += 2;
			}

			if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']=="saparate")
			{
				$onga['QTY'] += ((ceil(sizeof($heatpump)/2))*2);
				$returneyeball['QTY'] += ((ceil(sizeof($heatpump)/2))*3);
			}
			else if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']=="bypass")
			{
				$eyeball['QTY'] += ((sizeof($heatpump))*2);
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>INLET & FITTING</td></tr>";
			//end head

			$sumpriceinletandfitting = $sumpriceinletandfitting+($onga['QTY']*$onga['Supplier_Product_Price']);
			$sumpriceinletandfitting = $sumpriceinletandfitting+($returneyeball['QTY']*$returneyeball['Supplier_Product_Price']);
			$sumpriceinletandfitting = $sumpriceinletandfitting+($eyeball['QTY']*$eyeball['Supplier_Product_Price']);
			$sumpriceinletandfitting = $sumpriceinletandfitting+($speedupjet['QTY']*$speedupjet['Supplier_Product_Price']);

			array_push($arrinlet, $onga);
			array_push($arrinlet, $returneyeball);
			array_push($arrinlet, $eyeball);
			array_push($arrinlet, $speedupjet);

			return array($arrinlet,$sumpriceinletandfitting);

		}
		else
		{
			return array(NULL,NULL);
		}
	}


	function calculate_ipanel_infinityedge($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$waterfall = NULL;
		$waterfallcontrol = NULL;

		$sumpriceinfinityedge = 0;

		$arrinfinity  = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FAC40-01502'){
					$waterfall = $value;
				}
				if($value['Supplier_Product_Code']=='FPU10-00109'){
					$waterfallcontrol = $value;
				}
			}

			$waterfall['QTY'] = 0;
			$waterfallcontrol['QTY'] = 0;

			if($editipanel['Ipanel_Pools_Series']!="davinciblue")
			{
				$waterfall['QTY'] = 0;
			}
			else
			{
				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Build']=="Compact")
				{
					$waterfall['QTY'] = floor($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']+1);
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>0)
			{
				$waterfallcontrol['QTY'] = 1;
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>INFINITY EDGE</td></tr>";
			//end head
			$sumpriceinfinityedge = $sumpriceinfinityedge+($waterfall['QTY']*$waterfall['Supplier_Product_Price']);
			$sumpriceinfinityedge = $sumpriceinfinityedge+($waterfallcontrol['QTY']*$waterfallcontrol['Supplier_Product_Price']);

			array_push($arrinfinity, $waterfall);
			array_push($arrinfinity, $waterfallcontrol);

			return array($arrinfinity, $sumpriceinfinityedge);
		}
		else
		{
			return array(NULL, NULL);
		}
	}

	function calculate_ipanel_otheroption($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$inteliswimjet = NULL;
		$sfp1500 = NULL;
		$ltppump = NULL;
		$ppppump = NULL;

		$sfppumproom = NULL;
		$sproomltp = NULL;
		$sproomppp = NULL;

		$doublepumproomltpsfp = NULL;
		$dproomltp = NULL;
		$dproomppp = NULL;

		$ltpandsfp = NULL;
		$pppandsfp = NULL;
		$pppandltp = NULL;
		
		$stainlesssteel3step = NULL;

		$sumpriceotheroption = 0;

		$arrother = array();

		if($editipaneloption['Ipanel_Pool_Option_Logo']!=NULL)
		{
			$uselogo = $this->getlogo($editipaneloption['Ipanel_Pool_Option_Logo']);
		}
		else
		{
			$uselogo = NULL;
		}

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FFI21-00303'){
					$inteliswimjet = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-10002'){
					$ltppump = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-12501'){
					$ppppump = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-20003'){
					$sfp1500 = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00100'){
					$sproomltp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00101'){
					$sproomppp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00103'){
					$sfppumproom = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00133'){
					$dproomltp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00134'){
					$dproomppp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00107'){
					$doublepumproomltpsfp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00108'){
					$ltpandsfp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00120'){
					$pppandsfp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00127'){
					$pppandltp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC10-00204'){
					$stainlesssteel3step = $value;
				}
			}

			$inteliswimjet['QTY'] = 0;
			$sfp1500['QTY'] = 0;
			$ltppump['QTY'] = 0;
			$ppppump['QTY'] = 0;

			$sfppumproom['QTY'] = 0;
			$sproomltp['QTY'] = 0;
			$sproomppp['QTY'] = 0;

			$doublepumproomltpsfp['QTY'] = 0;
			$dproomltp['QTY'] = 0;
			$dproomppp['QTY'] = 0;

			$ltpandsfp['QTY'] = 0;
			$pppandsfp['QTY'] = 0;
			$pppandltp['QTY'] = 0;
			$stainlesssteel3step['QTY'] = 0;

			if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="aquaflow")
			{
				$inteliswimjet['QTY'] = $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
			}

			if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>=1&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']<=6)
			{
				$ltppump['QTY'] = ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
			}
			else if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>=7&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']<=10)
			{
				$ppppump['QTY'] = ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
			}
			else if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>=11&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']<=14)
			{
				$sfp1500['QTY'] = ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
			}
			else
			{
				$sfp1500['QTY'] = floor($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
				$otherpump = $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'] - floor($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
				if($otherpump>=1&&$otherpump<=6)
				{
					$ltppump['QTY'] = 1;
				}
				else if($otherpump>=7&&$otherpump<=10)
				{
					$ppppump['QTY'] = 1;
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Swim_Jet_System']=="jdeyeball")
			{
				$sfp1500['QTY'] += $editipaneloption['Ipanel_Pool_Option_Swim_Jet_System_Unit'];
			}

			if(($sfp1500['QTY']/2)>0.45)
			{
				$doublepumproomltpsfp['QTY'] = floor ($sfp1500['QTY']/2);
			}

			if(($sfp1500['QTY']/2)>0.45)
			{
				$sfppumproom['QTY'] = ceil($sfp1500['QTY']/2)-$doublepumproomltpsfp['QTY'];
			}

			if($ppppump['QTY']>0||$ltppump['QTY']>0)
			{
				if($sfppumproom['QTY']>=1)
				{
					$sfppumproom['QTY'] = $sfppumproom['QTY'] -1;

					$ltpandsfp['QTY'] = 1;
					$pppandsfp['QTY'] = 1;
				}
				else
				{
					$sproomltp['QTY'] = 1;
					$sproomppp['QTY'] = 1;
				}
			}

			if($editipaneloption['Ipanel_Pool_Option_Ladder']=="Stainless")
			{
				$stainlesssteel3step['QTY'] = 	$editipaneloption['Ipanel_Pool_Option_Ladder_Unit'];
			}

			if($editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']!="No")
			{
				if($robot!=NULL)
				{
					foreach ($robot as $key => $value) {
						if($value['Supplier_Product_Code'] == $editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner'])
						{
							$robot = $value;
							$robot['QTY'] = 1;
						}
					}
					
				}

			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>OTHER OPTION</td></tr>";
			//end head

			$sumpriceotheroption = $sumpriceotheroption+($inteliswimjet['QTY']*$inteliswimjet['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($ppppump['QTY']*$ppppump['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($ltppump['QTY']*$ltppump['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sfp1500['QTY']*$sfp1500['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sproomltp['QTY']*$sproomltp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sproomppp['QTY']*$sproomppp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sfppumproom['QTY']*$sfppumproom['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($ltpandsfp['QTY']*$ltpandsfp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($pppandsfp['QTY']*$pppandsfp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($doublepumproomltpsfp['QTY']*$doublepumproomltpsfp['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($stainlesssteel3step['QTY']*$stainlesssteel3step['Supplier_Product_Price']);
			if($editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']!="No")
			{
				$sumpriceotheroption = $sumpriceotheroption+($robot['QTY']*$robot['Supplier_Product_Price']);
			}

			$setlogo = $uselogo[0];

			if($editipaneloption['Ipanel_Pool_Option_Logo']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Logo']!="")
			{
				foreach ($uselogo[0] as $key => $value) {

					foreach ($uselogo[1] as $key1 => $value1) {

						if($key1==$value['Supplier_Product_Code'])
						{
							$setlogo[$key]['QTY'] = $value1;
							$thisqty = $value1;
							break;
						}
						else
						{
							$setlogo[$key]['QTY'] = 0;
							$thisqty = 0;
						}
					}

					$sumpriceotheroption = $sumpriceotheroption+($thisqty*$value['Supplier_Product_Price']);

				}
			}

			array_push($arrother, $inteliswimjet);
			array_push($arrother, $ppppump);
			array_push($arrother, $ltppump);
			array_push($arrother, $sfp1500);
			array_push($arrother, $sproomltp);
			array_push($arrother, $sproomppp);
			array_push($arrother, $sfppumproom);
			array_push($arrother, $ltpandsfp);
			array_push($arrother, $pppandsfp);
			array_push($arrother, $doublepumproomltpsfp);
			array_push($arrother, $stainlesssteel3step);
			if($editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']!="No")
			{
				array_push($arrother, $robot);
			}

			if($editipaneloption['Ipanel_Pool_Option_Logo']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Logo']!="")
			{
				foreach ($setlogo as $key => $value) {
					array_push($arrother, $value);
				}
			}

			return array($arrother,$sumpriceotheroption);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_maintenent($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$vacuum12 = NULL;
		$vacuum15 = NULL;
		$vacuumhead = NULL;
		$leaf = NULL;
		$filterbag = NULL;
		$telescopic = NULL;
		$wall = NULL;
		$thermomiter = NULL;
		$testkid = NULL;

		$sumpricemaintenent = 0;

		$arrmaintain = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FPM00-00112'){
					$vacuum12 = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00123'){
					$vacuum15 = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00203'){
					$vacuumhead = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00303'){
					$leaf = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00400'){
					$filterbag = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00503'){
					$telescopic = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00604'){
					$wall = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00702'){
					$thermomiter = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00812'){
					$testkid = $value;
				}
			}

			$vacuum12['QTY'] = 0;
			$vacuum15['QTY'] = 0;
			$vacuumhead['QTY'] = 1;
			$leaf['QTY'] = 1;
			$filterbag['QTY'] = 1;
			$telescopic['QTY'] = 1;
			$wall['QTY'] = 1;
			$thermomiter['QTY'] = 1;
			$testkid['QTY'] = 1;

			if($editipanel['Ipanel_Pools_Length']<9)
			{
				$vacuum12['QTY'] = 1;
			}
			else
			{
				$vacuum15['QTY'] = 1;
			}

			if($editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories']>0)
			{
				$vacuumhead['QTY'] = $vacuumhead['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$leaf['QTY'] = $leaf['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$filterbag['QTY'] = $filterbag['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$telescopic['QTY'] = $telescopic['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$wall['QTY'] = $wall['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$thermomiter['QTY'] = $thermomiter['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
				$testkid['QTY'] = $testkid['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Maintenance_Accessories'];
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Maintenent Accessories</td></tr>";
			//end head

			$sumpricemaintenent = $sumpricemaintenent+($vacuum12['QTY']*$vacuum12['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($vacuum15['QTY']*$vacuum15['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($vacuumhead['QTY']*$vacuumhead['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($leaf['QTY']*$leaf['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($filterbag['QTY']*$filterbag['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($telescopic['QTY']*$telescopic['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($wall['QTY']*$wall['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($thermomiter['QTY']*$thermomiter['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($testkid['QTY']*$testkid['Supplier_Product_Price']);

			array_push($arrmaintain, $vacuum12);
			array_push($arrmaintain, $vacuum15);
			array_push($arrmaintain, $vacuumhead);
			array_push($arrmaintain, $leaf);
			array_push($arrmaintain, $filterbag);
			array_push($arrmaintain, $telescopic);
			array_push($arrmaintain, $wall);
			array_push($arrmaintain, $thermomiter);
			array_push($arrmaintain, $testkid);

			return array($arrmaintain,$sumpricemaintenent);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_ipanel_chemical($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$longlasting = NULL;
		$chlorine = NULL;
		$php = NULL;
		$phm = NULL;
		$jdclean = NULL;

		$sumpricechem = 0;

		$arrchem = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FCH00-00101'){
					$longlasting = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00111'){
					$chlorine = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00200'){
					$php = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00210'){
					$phm = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00300'){
					$jdclean = $value;
				}
			}

			$longlasting['QTY'] = 1;
			$chlorine['QTY'] = 1;
			$php['QTY'] = 1;
			$phm['QTY'] = 1;
			$jdclean['QTY'] = 1;

			if($editipanelcomponent['Ipanel_Pool_Component_Chemicals']>0)
			{
				$longlasting['QTY'] = $longlasting['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
				$chlorine['QTY'] = $chlorine['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
				$php['QTY'] = $php['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
				$phm['QTY'] = $phm['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
				$jdclean['QTY'] = $jdclean['QTY']*$editipanelcomponent['Ipanel_Pool_Component_Chemicals'];
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Chemical Treatment</td></tr>";
			//end head

			$sumpricechem = $sumpricechem+($longlasting['QTY']*$longlasting['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($chlorine['QTY']*$chlorine['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($php['QTY']*$php['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($phm['QTY']*$phm['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($jdclean['QTY']*$jdclean['Supplier_Product_Price']);

			array_push($arrchem,$longlasting);
			array_push($arrchem,$chlorine);
			array_push($arrchem,$php);
			array_push($arrchem,$phm);
			array_push($arrchem,$jdclean);

			return array($arrchem, $sumpricechem);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function structure_boq_ipanel($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$distanct = $this->calkm($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);
		
		$pumproom = $this->getpumproom_with_row()[0];
		$pumproom_row =  $this->getpumproom_with_row()[1];

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];


		//$wherearr = array("structure.Pool_Type = 'ipanel'","structure_by_province.Province_ID = '".$editproject['Province_ID']."'");
		/*$wherearr = array("structure.Pool_Type = 'ipanel'");
		$joincarr = array("product_unit","structure_by_province");
		$joinvarr = array("structure.Product_Unit_ID = product_unit.Product_Unit_ID","structure.Structure_ID = structure_by_province.Structure_ID");*/

		$wherearr = array("structure.Pool_Type = 'ipanel'");
		$joincarr = array("product_unit");
		$joinvarr = array("structure.Product_Unit_ID = product_unit.Product_Unit_ID");

		$boqipanel = $this->select_model->selectwherejoin("*","structure",$wherearr,$joincarr,$joinvarr,"Structure_Code","asc");


		if($boqipanel->num_rows())
		{
			$boqipanel_row = $boqipanel->num_rows();
			$boqipanel = $boqipanel->result_array();
		}
		else
		{
			$boqipanel = NULL;
			$boqipanel_row=0;
		}

		$boqprovipanel = $this->select_model->select_where("*","structure_by_province","structure_by_province.Province_ID = '".$editproject['Province_ID']."'");
		if($boqprovipanel->num_rows())
		{
			$boqprovipanel_row = $boqprovipanel->num_rows();
			$boqprovipanel = $boqprovipanel->result_array();
		}
		else
		{
			$boqprovipanel = NULL;
			$boqprovipanel_row=0;
		}

		$returnboqipanel = array();

		$elecpvcmatsum = 0;
		$elecpvclabsum = 0;
		$allpipe = 0;

		foreach ($boqipanel as $keyboq => $valueboq) {
			if($valueboq['Structure_Code']==20.5)
			{
				$aftermeterconcreteprice = $valueboq['Structure_Labour_Unit_Cost'];
			}
			if($valueboq['Structure_Code']==21.5)
			{
				$aftermeterkidpoolprice = $valueboq['Structure_Labour_Unit_Cost'];
			}
			if($valueboq['Structure_Code']==22.5)
			{
				$aftermeterinfinprice = $valueboq['Structure_Labour_Unit_Cost'];
			}
		}

		foreach ($boqipanel as $keyboq => $valueboq) {
			
			if($boqprovipanel_row>0)
			{
				foreach ($boqprovipanel as $keyprovboq => $valueprovboq) {
					if($valueprovboq['Structure_ID']==$valueboq['Structure_ID'])
					{
						$valueboq['Structure_Labour_Unit_Cost'] = $valueprovboq['Structure_By_Province_Labour_Unit_Cost'];
						$valueboq['Structure_Materials_Unit_Cost'] = $valueprovboq['Structure_By_Province_Materials_Unit_Cost'];
						break;
					}
				}
			}
			

			if($valueboq['Structure_Code']==1)
			{
				if( ($distanct/1000)<=50 )
				{
					$valueboq['QTY'] = 0;
				}
				else
				{
					$valueboq['QTY'] = 1;
				}
			}
			else if($valueboq['Structure_Code']==2)
			{
				if( ($distanct/1000)<=50 )
				{
					$valueboq['QTY'] = 0;
				}
				else
				{
					$valueboq['QTY'] = 1;
				}
			}
			else if($valueboq['Structure_Code']==3)
			{
				$valueboq['QTY'] = 1;
			}
			else if($valueboq['Structure_Code']==4)
			{
				$mac = 0;

				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Inground')
				{
					
					if($editipanelcomponent['Ipanel_Pool_Component_Dig']=='Machine')
					{
						$mac = 1;
					}
					else
					{
						$mac = 0;
						$valueboq['Structure_Code'] = 4.1;
					}

					$valueboq['Structure_Labour_Unit_Cost'] = ($valueboq['Structure_Labour_Unit_Cost']*$mac);
				}
				
				$valueboq['QTY'] = ceil( ($Capacity+($Area*0.2)+($Perimeter*1.4*0.8))*($mac) );
			}
			else if($valueboq['Structure_Code']==4.5)
			{
				$men = 0;
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Inground')
				{
					
					if($editipanelcomponent['Ipanel_Pool_Component_Dig']=='Men')
					{
						$men = 1;
						$valueboq['Structure_Code'] = 4;
					}
					else
					{
						$men = 0;
					}

					$valueboq['Structure_Labour_Unit_Cost'] = ($valueboq['Structure_Labour_Unit_Cost']*$men);
				}
				
				$valueboq['QTY'] = ceil( ($Capacity+($Area*0.2)+($Perimeter*1.4*0.8))*($men+$mac) );
			}
			else if($valueboq['Structure_Code']==5)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Inground')
				{
					$mac = 0;
					if($editipanelcomponent['Ipanel_Pool_Component_Dig']=='Machine')
					{
						$mac = 1;
					}
					else
					{
						$mac = 0;
						$valueboq['Structure_Code'] = 5.1;
					}

					$valueboq['Structure_Labour_Unit_Cost'] = (150*$mac);

					if($editipanelcomponent['Ipanel_Pool_Component_Pile']=='Have')
					{
						$concrete = ceil( (($Perimeter/2)+$Area)*0.05 );
						$concretebasementandwall = ceil( ((($Area+(0.5*$Perimeter))*0.15)+($Perimeter/8))*1.05 );

						if($editipanel['Ipanel_Pools_Where']=="home")
						{
							$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
						}
						else
						{
							$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
						}

						$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

						$te20count = $Use_Filtration[0];
						$te25count = $Use_Filtration[1];
						$bikinicount = $Use_Filtration[2];

						$valueboq['QTY'] = ceil(($Capacity+(($concrete+$concretebasementandwall)*2.4))+(2*($te20count+$te25count)));
					}
					else
					{
						$valueboq['QTY'] = 0;
					}
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==5.5)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Inground')
				{
					$men = 0;
					if($editipanelcomponent['Ipanel_Pool_Component_Dig']=='Men')
					{
						$men = 1;
						$valueboq['Structure_Code'] = 5;
					}
					else
					{
						$men = 0;
					}

					$valueboq['Structure_Labour_Unit_Cost'] = (150*$men);

					if($editipanelcomponent['Ipanel_Pool_Component_Pile']=='Have')
					{
						$concrete = ceil( (($Perimeter/2)+$Area)*0.05 );
						$concretebasementandwall = ceil( ((($Area+(0.5*$Perimeter))*0.15)+($Perimeter/8))*1.05 );

						if($editipanel['Ipanel_Pools_Where']=="home")
						{
							$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
						}
						else
						{
							$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
						}

						$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

						$te20count = $Use_Filtration[0];
						$te25count = $Use_Filtration[1];
						$bikinicount = $Use_Filtration[2];

						$valueboq['QTY'] = ceil(($Capacity+(($concrete+$concretebasementandwall)*2.4))+(2*($te20count+$te25count)));
					}
					else
					{
						$valueboq['QTY'] = 0;
					}
					
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==6)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = ceil($Area/6.25);
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==7)
			{
				$valueboq['QTY'] = ceil( (($Perimeter/2)+$Area)*0.05 );
			}
			else if($valueboq['Structure_Code']==8)
			{
				$valueboq['QTY'] = ceil( (($Perimeter/2)+$Area)*0.05 );
			}
			else if($valueboq['Structure_Code']==9)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te25count = $Use_Filtration[1];

				$valueboq['QTY'] = ceil( ($Perimeter+4)+(( $te25count )*2) );
			}
			else if($valueboq['Structure_Code']==10)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = ceil($Area/6.25)*4;
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==11)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']!='Customer')
				{
					$valueboq['QTY'] = ceil( ((($Perimeter/2)+($Area))*2)*1.2 );
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==12)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = ceil( ((($Perimeter/2)+($Area))*2)*1.2 );
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==13)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = ceil($Area/6.25)*2;
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==14)
			{
				$valueboq['QTY'] = ceil(($Perimeter*4)/7);
			}
			else if($valueboq['Structure_Code']==15)
			{
				$valueboq['QTY'] =  ceil(($Perimeter/9.5)*3);
			}
			else if($valueboq['Structure_Code']==16)
			{
				$sum1to16 = 0;

				if($editipanelcomponent['Ipanel_Pool_Component_Work']!='Customer')
				{
					$sum1to16 += ceil( ((($Perimeter/2)+($Area))*2)*1.2 );
				}
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$sum1to16 += ceil( ((($Perimeter/2)+($Area))*2)*1.2 );
				}
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$sum1to16 += ceil($Area/6.25)*2;
				}

				$sum1to16 += ceil(($Perimeter*4)/7);
				$sum1to16 += ceil(($Perimeter/9.5)*3);

				$valueboq['QTY'] = $sum1to16;

			}
			else if($valueboq['Structure_Code']==17)
			{
				$valueboq['QTY'] = $Perimeter;
			}
			else if($valueboq['Structure_Code']==18)
			{
				$valueboq['QTY'] = ceil( ((($Area+(0.5*$Perimeter))*0.15)+($Perimeter/8))*1.05 );
			}
			else if($valueboq['Structure_Code']==19)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']=='Customer')
				{
					$valueboq['QTY'] = (($Area+(0.5*$Perimeter))*0.05)+(ceil($Area/6.25)*0.2);
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==20)
			{

				$valueboq['QTY'] = 0;

				if($editipaneloption['Ipanel_Pool_Option_Spa_Seat']!=NULL||$editipaneloption['Ipanel_Pool_Option_Spa_Seat']!="")
				{
					$valueboq['QTY'] += $editipaneloption['Ipanel_Pool_Option_Spa_Seat'];
				}

				if($editipaneloption['Ipanel_Pool_Option_Ladder']=="Concrete")
				{
					$valueboq['QTY'] += $editipaneloption['Ipanel_Pool_Option_Ladder_Unit'];
				}
		
				if($valueboq['QTY']>0)
				{
					$valueboq['Structure_Labour_Unit_Cost'] = ($valueboq['Structure_Labour_Unit_Cost']+($valueboq['QTY']-1)*$aftermeterconcreteprice)/$valueboq['QTY'];
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 0;
				}
				
				//$valueboq['Structure_Labour_Unit_Cost'] = $valueboq['Structure_Labour_Unit_Cost'];
			}

			else if($valueboq['Structure_Code']==21)
			{
				$valueboq['QTY'] = 0;

				if($editipaneloption['Ipanel_Pool_Option_Kid_Pool']!=NULL||$editipaneloption['Ipanel_Pool_Option_Kid_Pool']!="")
				{
					$valueboq['QTY'] += $editipaneloption['Ipanel_Pool_Option_Kid_Pool'];
				}
				if($valueboq['QTY']>0)
				{
					$valueboq['Structure_Labour_Unit_Cost'] = ($valueboq['Structure_Labour_Unit_Cost']+($valueboq['QTY']-1)*$aftermeterkidpoolprice)/$valueboq['QTY'];
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 0;
				}

				//$valueboq['Structure_Labour_Unit_Cost'] = $valueboq['Structure_Labour_Unit_Cost'];

			}

			else if($valueboq['Structure_Code']==22)
			{
				$valueboq['QTY'] = 0;

				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!=NULL||$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!="")
				{
					$valueboq['QTY'] += $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'];
				}

				if($valueboq['QTY']>0)
				{
					if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Build']=="BuildOnSite")
					{
						//$valueboq['Structure_Labour_Unit_Cost'] = $valueboq['Structure_Labour_Unit_Cost'];
						$valueboq['Structure_Labour_Unit_Cost'] = ($valueboq['Structure_Labour_Unit_Cost']+($valueboq['QTY']-1)*$aftermeterinfinprice)/$valueboq['QTY'];
					}
					else
					{
						$valueboq['Structure_Labour_Unit_Cost'] = 0;
					}
					
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 0;
				}
			}

			else if($valueboq['Structure_Code']==23)
			{
				$valueboq['QTY'] = $Area;
			}
			else if($valueboq['Structure_Code']==24)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Work']!='Above')
				{
					$excavation = ceil( ($Capacity+($Area*0.2)+($Perimeter*1.4*0.8))*(1) );
					$sandorconcrete = ceil( (($Perimeter/2)+$Area)*0.05 );
					$concretebasementandwall = ceil( ((($Area+(0.5*$Perimeter))*0.15)+($Perimeter/8))*1.05 );
					$valueboq['QTY']  = ceil( $excavation - ($Capacity+($sandorconcrete*2)+$concretebasementandwall) );
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==25)
			{
				$valueboq['QTY'] = 0;

				//find Badu pump

				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}
				
				$Use_Filtration = $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);	

				$te20panelqty = $Use_Filtration[0];
				$te25panelqty = $Use_Filtration[1];
				$bikinipanelqty = $Use_Filtration[2];

				if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!="no"&&$editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!="")
				{
					$heatpump = $this->getheatpump($Area);
				}
				else
				{
					$heatpump = NULL;
				}

				$badu4716standardqty = 0;
				$badu4722standardqty = 0;
				$badu4716encapsulateqty = 0;
				$badu4722encapsulateqty = 0;
				$badudoublepumpqty = 0;
				$badusinglepumpqty = 0;
				$doublepumproomltpsfpqty = 0;
				$sfppumproomqty = 0;
				$sfp1500qty = 0;

				if($editipanelcomponent['Ipanel_Pool_Component_Filter_Pump']=='Standard')
				{
					$badu4716standardqty = $te20panelqty;
					$badu4722standardqty = $te25panelqty;
					if($bikinipanelqty>0)
					{
						$badu4716standardqty = $bikinipanelqty;
					}
				}

				if($editipanelcomponent['Ipanel_Pool_Component_Filter_Pump']=='Endcapsulated')
				{
					$badu4716encapsulateqty = $te20panelqty;
					$badu4722encapsulateqty = $te25panelqty;
					if($bikinipanelqty>0)
					{
						$badu4716encapsulateqty = $bikinipanelqty;
					}
				}

				if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=0)
				{
					$badu4716standardqty = $badu4716standardqty;
					$badu4722standardqty = $badu4722standardqty;
				}
				if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']>0&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']<=4)
				{
					$badu4716standardqty += 1;
				}
				else
				{
					$badu4722standardqty += ceil($editipaneloption['Ipanel_Pool_Option_Spa_Jet']/6);
				}

				if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']>0&&$editipaneloption['Ipanel_Pool_Option_Pool_Sculpture']!=NULL)
				{
					if($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/16")
					{
						$badu4716standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
					}
					elseif($editipaneloption['Ipanel_Pool_Option_Pool_Sculpture_Pump']=="47/22")
					{
						$badu4722standardqty += $editipaneloption['Ipanel_Pool_Option_Pool_Sculpture'];
					}
				}

				if($heatpump!=NULL)
				{
					if(sizeof($heatpump)==1)
					{
						$badu4716standardqty += 1;
					}
					else if(sizeof($heatpump)>1)
					{
						$badu4722standardqty += ceil(sizeof($heatpump)/2);
					}

				}

				if(($badu4716standardqty+$badu4722standardqty+$badu4716encapsulateqty+$badu4722encapsulateqty-$te20panelqty-$te25panelqty)/2>0.45)
				{
					$badudoublepumpqty = floor( (($badu4716standardqty+$badu4722standardqty+$badu4716encapsulateqty+$badu4722encapsulateqty-$te20panelqty-$te25panelqty)/2) );
				}
				else
				{
					$badudoublepumpqty = 0;
				}

				if(($badu4716standardqty+$badu4722standardqty+$badu4716encapsulateqty+$badu4722encapsulateqty-$te20panelqty-$te25panelqty)/2>0.45)
				{
					$badusinglepumpqty = ceil( (($badu4716standardqty+$badu4722standardqty+$badu4716encapsulateqty+$badu4722encapsulateqty-$te20panelqty-$te25panelqty)/2) - $badudoublepumpqty );
				}
				else
				{
					$badudoublepumpqty = 0;
				}

				//end find badu pump

				//find sfp pump
				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>1)
				{
					$sfp1500qty = ceil($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']/15);
				}

				if(($sfp1500qty/2)>0.45)
				{
					$doublepumproomltpsfpqty = floor ($sfp1500qty/2);
				}
				else
				{
					$doublepumproomltpsfpqty = 0;
				}

				if(($sfp1500qty/2)>0.45)
				{
					$sfppumproomqty = ceil($sfp1500qty/2)-$doublepumproomltpsfpqty;
				}
				else
				{
					$sfppumproomqty = 0;
				}
				//end find sfp pump

				//echo "badusinglepumpqty : ".$badusinglepumpqty." badudoublepumpqty : ".$badudoublepumpqty." sfppumproomqty : ".$sfppumproomqty." doublepumproomltpsfpqty : ".$doublepumproomltpsfpqty;

				$valueboq['QTY'] = $badusinglepumpqty + $badudoublepumpqty + $sfppumproomqty + $doublepumproomltpsfpqty;

			}
			else if($valueboq['Structure_Code']==26)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				$valueboq['QTY'] = $te20count+$te25count+$bikinicount;
			}
			else if($valueboq['Structure_Code']==27)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				$valueboq['QTY'] = $te20count+$te25count+$bikinicount;
			}
			else if($valueboq['Structure_Code']==28)
			{
				$valueboq['QTY'] = 20;

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==29)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				$valueboq['QTY'] = ceil( ($te20count*10) );

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==30)
			{
				$valueboq['QTY'] = 20;

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==31)
			{
				if($editipaneloption['Ipanel_Pool_Option_Spa_Jet']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Spa_Jet']!="")
				{
					$valueboq['QTY'] = $editipaneloption['Ipanel_Pool_Option_Spa_Jet']*1.5;
				}
				else
				{
					$valueboq['QTY'] = 0;
				}

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==32)
			{
				if($editipaneloption['Ipanel_Pool_Option_Pool_Heating_System']!=NULL)
				{
					$heatpump = $this->getheatpump($Area);
				}
				else
				{
					$heatpump = NULL;
				}

				if($heatpump!=NULL)
				{
					$allheatpump=0;
					$fixnum = 1;
					$heatpump_arr = array();

					foreach ($heatpump as $key => $value) 
					{
						array_push($heatpump_arr, $value['Supplier_Product_Code']);	
					}

					$heatpump_arr = array_count_values($heatpump_arr);


					$nodupheatpump = $this->unique_multidim_array($heatpump,"Supplier_Product_Code");


					foreach ($nodupheatpump as $key => $value) 
					{
						foreach ($heatpump_arr as $key2 => $value2) {
							if($key2==$value['Supplier_Product_Code'])
							{
								$heatpumpqty = $value2;
								$allheatpump += $heatpumpqty;
								break;
							}
							else
							{
								$heatpumpqty = $fixnum;
								$allheatpump += $fixnum;
								break;
							}
						}
					}
				}
				// $valueboq['QTY'] = $allheatpump;

				$valueboq['QTY'] = 0;

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==33)
			{
				$valueboq['QTY'] = ceil( ($this->sum_spa_swim_heat_scul_pump($Project_ID))*( 10+($Perimeter/2) ) );

				$allpipe += ($valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost']) + ($valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost']);
			}
			else if($valueboq['Structure_Code']==34)
			{
				$valueboq['QTY'] = 1;
				$valueboq['Structure_Labour_Unit_Cost'] = $allpipe*0.2;
			}
			else if($valueboq['Structure_Code']==35)
			{
				$cable2x2_5 = ceil( ($editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'])*(($Perimeter*0.5)+20) );
				$cable2x4 = 0;
				$cable3x1_5 = $this->sum_spa_swim_heat_scul_pump($Project_ID)*15;
				$cable10x2_5 = $this->calcable_10x2_5($Project_ID);

				$valueboq['QTY'] = $cable2x2_5+$cable2x4+$cable3x1_5+$cable10x2_5;

				$elecpvcmatsum = $valueboq['QTY']*$valueboq['Structure_Materials_Unit_Cost'];
				$elecpvclabsum = $valueboq['QTY']*$valueboq['Structure_Labour_Unit_Cost'];
			}
			else if($valueboq['Structure_Code']==36)
			{
				$valueboq['QTY'] = ceil( ($editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'])*(($Perimeter*0.5)+20) );
			}
			else if($valueboq['Structure_Code']==37)
			{
				$valueboq['QTY'] = 0;
			}
			else if($valueboq['Structure_Code']==38)
			{

				$valueboq['QTY'] = $this->sum_spa_swim_heat_scul_pump($Project_ID)*15;

			}
			else if($valueboq['Structure_Code']==39)
			{
				$valueboq['QTY'] = $this->calcable_10x2_5($Project_ID);
			}
			else if($valueboq['Structure_Code']==40)
			{
				$valueboq['Structure_Labour_Unit_Cost'] = ($elecpvcmatsum + $elecpvclabsum) *0.2;
				$valueboq['QTY'] = 1;

			}
			else if($valueboq['Structure_Code']==41)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				if($editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']>0)
				{
					$infin = $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']+2;
				}
				else
				{
					$infin = 0;
				}

				$valueboq['QTY'] = ceil( ($Perimeter+(3.5*($te20count+$te25count+$bikinicount))+$infin)+1);

				if($editipanel['Ipanel_Pools_Shape']=='Rectangular')
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 200;
				}
				else
				{
					$valueboq['Structure_Labour_Unit_Cost'] = 250;
				}
				
			}
			else
			{
				$valueboq['QTY'] = 0;
			}

			if(strpos($valueboq['Structure_Code'], '.') !== false)
			{

			}
			else
			{
				$returnboqipanel[$keyboq] = $valueboq;
			}
			
		}

		return $returnboqipanel;
	}

	public function getsystemandfinal($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editipanel = $this->getipanel($Project_ID);

		$distanct = $this->calkm($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		/*$wherearr = array("system_final.Pool_Type = 'ipanel'");
		$joincarr = array("product_unit","system_final_by_province");
		$joinvarr = array("system_final.Product_Unit_ID = product_unit.Product_Unit_ID","system_final.System_Final_ID = system_final_by_province.System_Final_ID");

		$system_final = $this->select_model->selectwherejoin("*","system_final",$wherearr,$joincarr,$joinvarr,NULL,NULL);

		if($system_final->num_rows())
		{
			$system_final_row = $system_final->num_rows();
			$system_final = $system_final->result_array();
		}
		else
		{
			$system_final = NULL;
			$system_final_row=0;
		}*/

		$wherearr = array("system_final.Pool_Type = 'ipanel'");
		$joincarr = array("product_unit");
		$joinvarr = array("system_final.Product_Unit_ID = product_unit.Product_Unit_ID");

		$boqipanel = $this->select_model->selectwherejoin("*","system_final",$wherearr,$joincarr,$joinvarr,NULL,NULL);


		if($boqipanel->num_rows())
		{
			$system_final_row = $boqipanel->num_rows();
			$system_final = $boqipanel->result_array();
		}
		else
		{
			$system_final = NULL;
			$system_final_row=0;
		}

		$system_finalprovipanel = $this->select_model->select_where("*","system_final_by_province","system_final_by_province.Province_ID = '".$editproject['Province_ID']."'");
		if($system_finalprovipanel->num_rows())
		{
			$system_finalprovipanel_row = $system_finalprovipanel->num_rows();
			$system_finalprovipanel = $system_finalprovipanel->result_array();
		}
		else
		{
			$system_finalprovipanel = NULL;
			$system_finalprovipanel_row=0;
		}

		$returnsystemandfinal = array();

		foreach ($system_final as $syskey => $sysvalue) {

			if($system_finalprovipanel_row>0)
			{
				foreach ($system_finalprovipanel as $keyprovsaf => $valueprovsaf) {
					if($valueprovsaf['System_Final_ID']==$sysvalue['System_Final_ID'])
					{
						$sysvalue['System_Final_Labour_Cost'] = $valueprovsaf['System_Final_By_Province_Labour_Unit_Cost'];
						$sysvalue['System_Final_Materials_Cost'] = $valueprovsaf['System_Final_By_Province_Materials_Unit_Cost'];
						break;
					}
				}
			}
			

			if($sysvalue['System_Final_Code']==1)
			{
				$sysvalue['QTY'] = $Area;
			}
			elseif($sysvalue['System_Final_Code']==2)
			{
				$sysvalue['QTY'] = $Perimeter;
			}
			elseif($sysvalue['System_Final_Code']==3)
			{
				$sysvalue['QTY'] = 1;
			}
			elseif($sysvalue['System_Final_Code']==4)
			{
				if($editipanel['Ipanel_Pools_Where']=="home")
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
				}
				else
				{
					$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
				}

				$Use_Filtration =  $this->getusefiltration($editipanelcomponent['Ipanel_Pool_Component_Filtration'], $Ipanel_Pool_Component_Filtration_Unit);

				$te20count = $Use_Filtration[0];
				$te25count = $Use_Filtration[1];
				$bikinicount = $Use_Filtration[2];

				$sysvalue['QTY'] = $te20count+$te25count;
			}
			elseif($sysvalue['System_Final_Code']==5)
			{
				$sysvalue['QTY'] = $this->sum_spa_swim_heat_scul_pump($Project_ID);
			}
			elseif($sysvalue['System_Final_Code']==6)
			{
				if($editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit']!=""&&$editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit']!=0)
				{
					$sysvalue['QTY'] = $editipaneloption['Ipanel_Pool_Option_Under_Water_Light_Unit'];
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
				
			}
			elseif($sysvalue['System_Final_Code']==7)
			{
				if($editipanelcomponent['Ipanel_Pool_Component_Control_Panel']=="salt")
				{
					$sysvalue['QTY'] = ceil($Capacity*4/25);
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
			}
			elseif($sysvalue['System_Final_Code']==8)
			{
				$sysvalue['QTY'] = 1;
			}
			elseif($sysvalue['System_Final_Code']==9)
			{
				
				if( ($distanct/1000)<=50 )
				{
					$sysvalue['QTY'] = 0;
					//$sysvalue['System_Final_Labour_Cost'] = 0;
				}
				else
				{
					$sysvalue['QTY'] = 1;
					//$sysvalue['System_Final_Labour_Cost'] = 10000;
				}
			}
			elseif($sysvalue['System_Final_Code']==10)
			{
				if($editipaneloption['Ipanel_Pool_Option_Ladder']=="Stainless")
				{
					$sysvalue['QTY'] = $editipaneloption['Ipanel_Pool_Option_Ladder_Unit'];
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
			}
			elseif($sysvalue['System_Final_Code']==11)
			{
				if($editipaneloption['Ipanel_Pool_Option_Ladder_Handrail']!=""&&$editipaneloption['Ipanel_Pool_Option_Ladder_Handrail']!=NULL&&$editipaneloption['Ipanel_Pool_Option_Ladder_Handrail']!=0)
				{
					$sysvalue['QTY'] = $editipaneloption['Ipanel_Pool_Option_Ladder_Handrail'];
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
			}
			elseif($sysvalue['System_Final_Code']==12)
			{
				if($editipaneloption['Ipanel_Pool_Option_Logo']!=""&&$editipaneloption['Ipanel_Pool_Option_Logo']!=NULL)
				{
					$uselogo = json_decode($editipaneloption['Ipanel_Pool_Option_Logo']); 
   					$uselogosize = sizeof($uselogo);
					$sysvalue['QTY'] = $uselogosize;
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
			}
			elseif($sysvalue['System_Final_Code']==13)
			{
				$sysvalue['QTY'] = ceil(($distanct/1000));
			}
			else
			{
				$sysvalue['QTY'] = 0;
			}


			$returnsystemandfinal[$syskey] = $sysvalue;
		}

		return $returnsystemandfinal;
	}

	function getsummarypeiceipanel($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcustomer = $this->getcustomer($editproject['Customer_ID']);
		$editcustomer_contact = $this->getcustomer_contact($Project_ID,$editproject['Customer_ID']);

		$editipanel = $this->getipanel($Project_ID);

		$distanct = $this->calkm($Project_ID);

		$editipanelcomponent = $this->getipanelcomponent($editipanel['Ipanel_Pools_ID']);

		$editipaneloption =  $this->getipaneloption($editipanel['Ipanel_Pools_ID']);

		$flitrationunit = $this->getflitrationunit_with_row()[0];
		$flitrationunit_row = $this->getflitrationunit_with_row()[1];

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		$saltdata = $this->getsaltprice();

		$Area = $this->calculate_ipanel_apc($Project_ID)[0];
		$Perimeter = $this->calculate_ipanel_apc($Project_ID)[1];
		$Capacity = $this->calculate_ipanel_apc($Project_ID)[2];

		if($editipanelcomponent['Ipanel_Pool_Component_Filtration']!=NULL)
		{
			foreach ($flitrationunit as $key => $value) {
				if($value['Supplier_Product_Code'] == $editipanelcomponent['Ipanel_Pool_Component_Filtration'])
				{
					//$showflitrationunit = $editipanelcomponent['Ipanel_Pool_Component_Filtration']." - ".$value['Supplier_Product_Name_ENG']." = ".$value['Supplier_Product_Price'];
					$showflitrationunit = $value['Supplier_Product_Name_ENG'];
				}
			}
		}
		else
		{
			$showflitrationunit = NULL;
		}


		if($editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']!=NULL)
		{
			foreach ($robot as $key => $value) {
				if($value['Supplier_Product_Code'] == $editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner'])
				{
					//$showrobot = $editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']." - ".$value['Supplier_Product_Name_ENG']." = ".$value['Supplier_Product_Price'];
					$showrobot = $value['Supplier_Product_Name_ENG'];
				}
			}
		}
		else
		{
			$showrobot = NULL;
		}

		if($editipanel['Ipanel_Pools_Where']=="home")
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/100);
		}
		else
		{
			$Ipanel_Pool_Component_Filtration_Unit = ceil($Capacity/75);
		}
		
		$ipanel_structure = $this->calculate_ipanel_structure($Project_ID);
		
		if($editipanel['Ipanel_Pools_Series']=="smartpool"||$editipanel['Ipanel_Pools_Series']=="ecopool")
		{
			$wall =	$this->calculate_ipanel_lightblue_wall($Project_ID);
			$floor = $this->calculate_ipanel_lightblue_floor($Project_ID);
		}
		else
		{
			$wall =	$this->calculate_ipanel_davinci_wall($Project_ID);
			$floor = $this->calculate_ipanel_davinci_floor($Project_ID);
		}

		$filtration = $this->calculate_ipanel_filtration($Project_ID);
		$underwaterlight = $this->calculate_ipanel_underwaterlight($Project_ID);
		$spaoption = $this->calculate_ipanel_spaoption($Project_ID);
		$inletandfitting = $this->calculate_ipanel_inletandfitting($Project_ID);
		$infinityedge = $this->calculate_ipanel_infinityedge($Project_ID);
		$otheroption = $this->calculate_ipanel_otheroption($Project_ID);
		$maintenent = $this->calculate_ipanel_maintenent($Project_ID);
		$chemical = $this->calculate_ipanel_chemical($Project_ID);

		$use_ipanel_structure = $ipanel_structure[0];
		$cost_ipanel_structure = $ipanel_structure[1];

		$use_wall = $wall[0];
		$cost_wall = $wall[1];

		$use_floor = $floor[0];
		$cost_floor = $floor[1];

		$use_filtration = $filtration[0];
		$cost_filtration = $filtration[1];

		$use_underwaterlight = $underwaterlight[0];
		$cost_underwaterlight = $underwaterlight[1];

		$use_spaoption = $spaoption[0];
		$cost_spaoption = $spaoption[1];
		
		$use_inletandfitting = $inletandfitting[0];
		$cost_inletandfitting = $inletandfitting[1];

		$use_infinityedge = $infinityedge[0];
		$cost_infinityedge = $infinityedge[1];
		
		$use_otheroption = $otheroption[0];
		$cost_otheroption = $otheroption[1];

		$use_maintenent = $maintenent[0];
		$cost_maintenent = $maintenent[1];

		$use_chemical = $chemical[0];
		$cost_chemical = $chemical[1];

		$alljdprodsummary = $cost_ipanel_structure+$cost_wall+$cost_floor+$cost_filtration+$cost_underwaterlight+$cost_underwaterlight+$cost_spaoption+$cost_inletandfitting+$cost_infinityedge+$cost_otheroption+$cost_maintenent+$cost_chemical;

		$ipanelboq = $this->structure_boq_ipanel($Project_ID);

		$totlemat = 0;
		$totlelabour = 0;
		$totleall = 0;

		$totlematsaf = 0;
		$totlelaboursaf = 0;
		$totleallsaf = 0;

		$eleccable2x2_5qty = 0;
		$eleccable2x4qty = 0;
		$eleccable3x1_5qty = 0;
		$eleccable10x2_5qty = 0;

		$eleccable2x2_5price = 0;
		$eleccable2x4price = 0;
		$eleccable3x1_5price = 0;
		$eleccable10x2_5price = 0;

		$maincopping = 0;

		$transportprice = $this->gettransport($Project_ID);

		$getsystemandfinal = $this->getsystemandfinal($Project_ID);

		$priceprodwithdc = $alljdprodsummary - (($alljdprodsummary * $this->branchdc($Project_ID))/100);

		foreach ($ipanelboq as $key => $value) {
			$cprodname = strlen($value['Structure_Name']);
			$showprd = NULL;
			if(strlen(iconv( 'UTF-8','cp874' , $value['Structure_Name']))>50)
			{
				//$showprd = substr($value['Structure_Name'], 0,114);
				$showprd = mb_substr($value['Structure_Name'],0,50);
				$showprd = $this->ConvertToUTF8($showprd);
				$showprd = $showprd."...";
			}
			else
			{
				$showprd = $value['Structure_Name'];
			}

			if($value['Structure_Code']=="36"){
				$eleccable2x2_5qty = $value['QTY'];
				$eleccable2x2_5price = $value['Structure_Materials_Unit_Cost'];
				$value['Structure_Materials_Unit_Cost'] = 0;
			}
			if($value['Structure_Code']=="37"){
				$eleccable2x4qty = $value['QTY'];
				$eleccable2x4price = $value['Structure_Materials_Unit_Cost'];
				$value['Structure_Materials_Unit_Cost'] = 0;
			}
			if($value['Structure_Code']=="38"){
				$eleccable3x1_5qty = $value['QTY'];
				$eleccable3x1_5price = $value['Structure_Materials_Unit_Cost'];
				$value['Structure_Materials_Unit_Cost'] = 0;
			}
			if($value['Structure_Code']=="39"){
				$eleccable10x2_5qty = $value['QTY'];
				$eleccable10x2_5price = $value['Structure_Materials_Unit_Cost'];
				$value['Structure_Materials_Unit_Cost'] = 0;
			}
			if($value['Structure_Code']=="41"){
				$maincopping = $value['QTY'];
			}


			$totlemat += $value['Structure_Materials_Unit_Cost']*$value['QTY'];
			$totlelabour += $value['Structure_Labour_Unit_Cost']*$value['QTY'];
			$totleall += ($value['QTY']*$value['Structure_Labour_Unit_Cost'])+($value['Structure_Materials_Unit_Cost']*$value['QTY']);
		}

		foreach ($getsystemandfinal as $key => $value) {
			$cprodname = strlen($value['System_Final_Name']);
			$showprd = NULL;
			if(strlen(iconv( 'UTF-8','cp874' , $value['System_Final_Name']))>48)
			{
				//$showprd = substr($value['Structure_Name'], 0,114);
				$showprd = mb_substr($value['System_Final_Name'],0,48);
				$showprd = $this->ConvertToUTF8($showprd);
				$showprd = $showprd."...";
			}
			else
			{
				$showprd = $value['System_Final_Name'];
			}

			$totlematsaf += $value['System_Final_Materials_Cost']*$value['QTY'];
			$totlelaboursaf += $value['System_Final_Labour_Cost']*$value['QTY'];
			$totleallsaf += ($value['QTY']*$value['System_Final_Labour_Cost'])+($value['System_Final_Materials_Cost']*$value['QTY']);
		}

		$sumallle = 0;

		$sumallle += $eleccable3x1_5qty*$eleccable3x1_5price;

		$sumallle += $eleccable2x2_5qty*$eleccable2x2_5price;

		$sumallle += $eleccable2x4qty*$eleccable2x4price;

		$sumallle += $eleccable10x2_5qty*$eleccable10x2_5price;

        $wherearr = array("Supplier_Product_Code like 'FST10-007%'");
        $allcoping = $this->select_model->select_orwhere("*","supplier_product",$wherearr);
        if($allcoping->num_rows())
        {
            $allcoping_row = $allcoping->num_rows();
            $allcoping = $allcoping->result_array();
        }
        else
        {
            $allcoping_row = 0;
            $allcoping = NULL;
        }

        $usecoping = NULL;
        $infincoping = NULL;

        foreach ($allcoping as $key => $value) {
            if($editipanelcomponent['Ipanel_Pool_Component_Coping']==$value['Supplier_Product_Code'])
            {
                $usecoping = $value;
            }

            if(strpos($value['Supplier_Product_Name'], 'จีน') !== false)
            {
                $infincoping = $value;
            }
        }


        $cprodname = strlen($infincoping['Supplier_Product_Name']);
        $cprodname2 = strlen($usecoping['Supplier_Product_Name']);
        $showprd = NULL;
        $showprd2 = NULL;

        if(strlen(iconv( 'UTF-8','cp874' , $usecoping['Supplier_Product_Name']))>45)
        {
            $showprd = substr($usecoping['Supplier_Product_Name'], 0,108);
            $showprd = $this->ConvertToUTF8($showprd);
            $showprd = $showprd."...";
        }
        else
        {
            $showprd = $usecoping['Supplier_Product_Name'];
        }

        if(strlen(iconv( 'UTF-8','cp874' , $infincoping['Supplier_Product_Name']))>45)
        {
            $showprd2 = substr($infincoping['Supplier_Product_Name'], 0,108);
            $showprd2 = $this->ConvertToUTF8($showprd2);
            $showprd2 = $showprd2."...";
        }
        else
        {
            $showprd2 = $infincoping['Supplier_Product_Name'];
        }

        if($usecoping!=NULL)
        {
        	if($editipanelcomponent['Ipanel_Pool_Component_Coping']!="FST10-00720")
        	{
	            $sumallle += $maincopping*$usecoping['Supplier_Product_Price'];
        	}
            else
            {
	            $sumallle += ($maincopping - $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set'])*$usecoping['Supplier_Product_Price'];
           		$sumallle += $editipaneloption['Ipanel_Pool_Option_Infinity_Eage_Set']*$infincoping['Supplier_Product_Price'];
            }
          
        }

		if($editipanelcomponent['Ipanel_Pool_Component_Control_Panel']=='salt')
		{
			$salt = ceil($Capacity*4/25);
		}
		else
		{
			$salt = 0;
		}

		if($saltdata!=NULL)
		{
			$sumallle += ceil($salt)*$saltdata['Supplier_Product_Price'];
		}
		else
		{
			$sumallle += ceil($salt)*300;
		}

		$sumallle += $Capacity*250;

		$showprice = ( ( $priceprodwithdc + ($totleall+($totleall*0.135)+(($totleall+$totleall*0.135)*0.07))+
		($totleallsaf+($totleallsaf*0.135)+(($totleallsaf+$totleallsaf*0.135)*0.07)) + ($sumallle+$transportprice[1]) ) *2 );

		return $showprice;
	}

	function calculate_composite_worksystem($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		if($editcompositeoption['Composite_Pools_Option_Pool_Heating_System']!=NULL)
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}

		//set calculate
		$compositefilter = NULL;
		$compositefilterdouble = NULL;
		$badu4716standard = NULL;
		$badu4722standard = NULL;
		$badu4716encapsulate = NULL;
		$badu4722encapsulate = NULL;
		$badusinglepump = NULL;
		$badudoublepump = NULL;
		$standardpoolcontrol = NULL;
		$x20cl = NULL;
		$x30cl = NULL;
		$colorled = NULL;
		$whiteled = NULL;
		$junctionbox = NULL;

		$sumpriceworksystem = 0;

		$arrworksystem = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FFI10-00207'){
					$compositefilter = $value;
				}
				if($value['Supplier_Product_Code']=='FFI10-00211'){
					$compositefilterdouble = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00200'){
					$badu4716standard = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00201'){
					$badu4722standard = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00203'){
					$badu4716encapsulate = $value;
				}
				if($value['Supplier_Product_Code']=='FPU30-00204'){
					$badu4722encapsulate = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00117'){
					$badusinglepump = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00122'){
					$badudoublepump = $value;
				}
				if($value['Supplier_Product_Code']=='FPU10-00104'){
					$standardpoolcontrol = $value;
				}
				if($value['Supplier_Product_Code']=='FAC30-00200'){
					$x20cl = $value;
				}
				if($value['Supplier_Product_Code']=='FAC30-00201'){
					$x30cl = $value;
				}
				if($value['Supplier_Product_Code']=='FLI51-00502'){
					$colorled = $value;
				}
				if($value['Supplier_Product_Code']=='FLI51-00512'){
					$whiteled = $value;
				}
				if($value['Supplier_Product_Code']=='FLI52-00101'){
					$abs = $value;
				}
			}

			$compositefilter['QTY'] = 0;
			$compositefilterdouble['QTY'] = 0;
			$badu4716standard['QTY'] = 0;
			$badu4722standard['QTY'] = 0;
			$badu4716encapsulate['QTY'] = 0;
			$badu4722encapsulate['QTY'] = 0;
			$badusinglepump['QTY'] = 0;
			$badudoublepump['QTY'] = 0;
			$standardpoolcontrol['QTY'] = 0;
			$x20cl['QTY'] = 0;
			$x30cl['QTY'] = 0;
			$colorled['QTY'] = 0;
			$whiteled['QTY'] = 0;
			$abs['QTY'] = 0;

			if($editcompositepooldata['Composite_Pools_Data_Filtration']=="FFI10-00207")
			{
				$compositefilter['QTY'] = $editcompositepooldata['Composite_Pools_Data_Filtration_Unit'];
			}
			elseif($editcompositepooldata['Composite_Pools_Data_Filtration']=="FFI10-002011")
			{
				$compositefilterdouble['QTY'] = $editcompositepooldata['Composite_Pools_Data_Filtration_Unit'];
			}

			if($editcompositepooldata['Composite_Pools_Data_Pump']=="FPU30-00203")
			{
				$badu4716encapsulate['QTY'] = $editcompositepooldata['Composite_Pools_Data_Pump_Unit'];
			}
			elseif($editcompositepooldata['Composite_Pools_Data_Pump']=="FPU30-00204")
			{
				$badu4722encapsulate['QTY'] = $editcompositepooldata['Composite_Pools_Data_Pump_Unit'];
			}

			if($editcomposite['Composite_Pools_Control_Panel']=='salt')
			{
				$x20cl['QTY'] = $compositefilter['QTY'];
				$x30cl['QTY'] = $compositefilterdouble['QTY'];
			}
			else
			{
				if($compositefilter['QTY']>=1)
				{
					$standardpoolcontrol['QTY'] = $compositefilter['QTY'];
				}
				else
				{
					$standardpoolcontrol['QTY'] = $compositefilterdouble['QTY'];
				}
			}

			if($editcompositeoption['Composite_Pools_Option_Spa_Jet']<=0)
			{
				$badu4716standard['QTY'] = $badu4716standard['QTY'];
				$badu4722standard['QTY'] = $badu4722standard['QTY'];
			}
			if($editcompositeoption['Composite_Pools_Option_Spa_Jet']>0&&$editcompositeoption['Composite_Pools_Option_Spa_Jet']<=4)
			{
				$badu4716standard['QTY'] += 1;
			}
			else
			{
				$badu4722standard['QTY'] += ceil($editcompositeoption['Composite_Pools_Option_Spa_Jet']/6);
			}

			if($heatpump!=NULL)
			{
				if(sizeof($heatpump)==1)
				{
					$badu4716standard['QTY'] += 1;
				}
				else if(sizeof($heatpump)>1)
				{
					$badu4722standard['QTY'] += ceil(sizeof($heatpump)/2);
				}

			}

			if($editcompositeoption['Composite_Pools_Option_Pool_Sculpture']>0&&$editcompositeoption['Composite_Pools_Option_Pool_Sculpture']!=NULL)
			{
				if($editcompositeoption['Composite_Pools_Option_Pool_Sculpture_Pump']=="47/16")
				{
					$badu4716standard['QTY'] += $editcompositeoption['Composite_Pools_Option_Pool_Sculpture'];
				}
				elseif($editcompositeoption['Composite_Pools_Option_Pool_Sculpture_Pump']=="47/22")
				{
					$badu4722standard['QTY'] += $editcompositeoption['Composite_Pools_Option_Pool_Sculpture'];
				}
			}

			if(($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY'])/2>0.5)
			{
				$badudoublepump['QTY'] = floor( (($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY'])/2) );
			}
			else
			{
				$badudoublepump['QTY'] = 0;
			}

			if(($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY'])/2>0.45)
			{
				$badusinglepump['QTY'] = ceil( (($badu4716standard['QTY']+$badu4722standard['QTY']+$badu4716encapsulate['QTY']+$badu4722encapsulate['QTY'])/2) - $badudoublepump['QTY'] );
			}
			else
			{
				$badusinglepump['QTY'] = 0;
			}

			if($editcompositeoption['Composite_Pools_Option_Under_Water_Light']=="white")
			{
				$whiteled['QTY'] = $editcompositeoption['Composite_Pools_Option_Under_Water_Light_Unit'];
			}
			elseif($editcompositeoption['Composite_Pools_Option_Under_Water_Light']=="color")
			{
				$colorled['QTY'] = $editcompositeoption['Composite_Pools_Option_Under_Water_Light_Unit'];
			}

			$abs['QTY'] = $colorled['QTY']+$whiteled['QTY'];

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Flitration</td></tr>";
			//end head

			$sumpriceworksystem = $sumpriceworksystem+($compositefilter['QTY']*$compositefilter['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($compositefilterdouble['QTY']*$compositefilterdouble['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($badu4716standard['QTY']*$badu4716standard['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($badu4722standard['QTY']*$badu4722standard['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($badu4716encapsulate['QTY']*$badu4716encapsulate['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($badu4722encapsulate['QTY']*$badu4722encapsulate['Supplier_Product_Price']);

			$useheatpump = array();

			if($heatpump!=NULL)
			{
				
				$fixnum = 1;
				$heatpump_arr = array();

				foreach ($heatpump as $key => $value) 
				{
					array_push($heatpump_arr, $value['Supplier_Product_Code']);	
				}

				$heatpump_arr = array_count_values($heatpump_arr);

				$nodupheatpump = $this->unique_multidim_array($heatpump,"Supplier_Product_Code");

				$useheatpump = array_values($nodupheatpump);

				foreach ($useheatpump as $key => $value) 
				{
					//$useheatpump[$key]['QTY'] = 0;
					foreach ($heatpump_arr as $key2 => $value2) {
						if($key2==$value['Supplier_Product_Code'])
						{
							$thisqty = $value2;
							$useheatpump[$key]['QTY'] = $value2;
							break;
						}
						else
						{
							$thisqty = $fixnum;
							$useheatpump[$key]['QTY'] = $fixnum;
							break;
						}
					}
					$sumpriceworksystem = $sumpriceworksystem+($thisqty*$value['Supplier_Product_Price']);
				}
			}

			$sumpriceworksystem = $sumpriceworksystem+($badusinglepump['QTY']*$badusinglepump['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($badudoublepump['QTY']*$badudoublepump['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($standardpoolcontrol['QTY']*$standardpoolcontrol['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($x20cl['QTY']*$x20cl['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($x30cl['QTY']*$x30cl['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($colorled['QTY']*$colorled['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($whiteled['QTY']*$whiteled['Supplier_Product_Price']);
			$sumpriceworksystem = $sumpriceworksystem+($abs['QTY']*$abs['Supplier_Product_Price']);

			array_push($arrworksystem, $compositefilter);
			array_push($arrworksystem, $compositefilterdouble);
			array_push($arrworksystem, $badu4716standard);
			array_push($arrworksystem, $badu4722standard);
			array_push($arrworksystem, $badu4716encapsulate);
			array_push($arrworksystem, $badu4722encapsulate);

			if($heatpump!=NULL)
			{
				foreach ($useheatpump as $key => $value) {
					array_push($arrworksystem, $value);
				}
			}
			array_push($arrworksystem, $badusinglepump);
			array_push($arrworksystem, $badudoublepump);
			array_push($arrworksystem, $standardpoolcontrol);
			array_push($arrworksystem, $x20cl);
			array_push($arrworksystem, $x30cl);
			array_push($arrworksystem, $colorled);
			array_push($arrworksystem, $whiteled);
			array_push($arrworksystem, $abs);

			
			return array($arrworksystem,$sumpriceworksystem);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_composite_spaoption($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		if($editcompositeoption['Composite_Pools_Option_Pool_Heating_System']!=NULL)
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}


		$hmcp = NULL;
		$spajet = NULL;
		$spapipe4head = NULL;
		$spapipe6head = NULL;
		$lidonga = NULL;
		$windbutton = NULL;
		$windlinepvc = NULL;
		$windspaswitch15amp = NULL;
		$windheadonga = NULL;

		$sumpricespaoption = 0;

		$arrspa = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FPU10-00102'){
					$hmcp = $value;
				}
				if($value['Supplier_Product_Code']=='FAC20-00300'){
					$spajet = $value;
				}
				if($value['Supplier_Product_Code']=='FFI60-00923'){
					$spapipe4head = $value;
				}
				if($value['Supplier_Product_Code']=='FFI60-00924'){
					$spapipe6head = $value;
				}
				if($value['Supplier_Product_Code']=='FLI40-00800'){
					$lidonga = $value;
				}
				if($value['Supplier_Product_Code']=='FLI40-00702'){
					$windbutton = $value;
				}
				if($value['Supplier_Product_Code']=='RPI00-00201'){
					$windlinepvc = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00301'){
					$windspaswitch15amp = $value;
				}
				if($value['Supplier_Product_Code']=='FLI40-00601'){
					$windheadonga = $value;
				}
			}

			$hmcp['QTY'] = 0;
			$spajet['QTY'] = 0;
			$spapipe4head['QTY'] = 0;
			$spapipe6head['QTY'] = 0;
			$lidonga['QTY'] = 0;
			$windbutton['QTY'] = 0;
			$windlinepvc['QTY'] = 0;
			$windspaswitch15amp['QTY'] = 0;
			$windheadonga['QTY'] = 0;

			$spajet['QTY'] = $editcompositeoption['Composite_Pools_Option_Spa_Jet'];


			if($spajet['QTY'] == NULL)
			{
				$spajet['QTY'] = 0;
			}

			if($spajet['QTY']>1)
			{
				$hmcp['QTY'] = 1;
			}

			if($spajet['QTY']>0&&$spajet['QTY']<=4)
			{
				$spapipe4head['QTY'] = 1;
			}
			elseif($spajet['QTY']>4)
			{
				$spapipe6head['QTY'] = $spajet['QTY']/6;
			}
			else
			{
				$spapipe4head['QTY'] = 0;
				$spapipe6head['QTY'] = 0;
			}

			if($editcompositeoption['Composite_Pools_Option_Pool_Sculpture']>=1)
			{
				$lidonga['QTY'] += $editcompositeoption['Composite_Pools_Option_Pool_Sculpture']*2;
			}

			if($editcompositeoption['Composite_Pools_Option_Swim_Jet_System']=="jdeyeball")
			{
				$lidonga['QTY'] += $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit']*2;
			}

			if($editcompositeoption['Composite_Pools_Option_Spa_Jet']>0&&$editcompositeoption['Composite_Pools_Option_Spa_Jet']<=4)
			{
				$lidonga['QTY'] += 2;
			}
			else
			{
				$lidonga['QTY'] += (ceil($editcompositeoption['Composite_Pools_Option_Spa_Jet']/6))*2;
			}

			if($editcompositeoption['Composite_Pools_Option_Pool_Heating_System']=="saparate")
			{
				$lidonga['QTY'] += ((ceil(sizeof($heatpump)/2))*2);
			}

			$windbutton['QTY'] += $hmcp['QTY'];
			$windlinepvc['QTY'] += $hmcp['QTY'];
			$windspaswitch15amp['QTY'] += $hmcp['QTY'];
			$windheadonga['QTY'] += $hmcp['QTY']*2;

			if($editcompositeoption['Composite_Pools_Option_Swim_Jet_System']=='jdeyeball')
			{
				$windbutton['QTY'] += 1;
				$windspaswitch15amp['QTY'] += 1;
				$windlinepvc['QTY'] += 1;
			}
	

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>SPA OPTION</td></tr>";
			//end head
			$sumpricespaoption = $sumpricespaoption+($hmcp['QTY']*$hmcp['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($spajet['QTY']*$spajet['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($spapipe4head['QTY']*$spapipe4head['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($spapipe6head['QTY']*$spapipe6head['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($lidonga['QTY']*$lidonga['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($windbutton['QTY']*$windbutton['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($windlinepvc['QTY']*$windlinepvc['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($windspaswitch15amp['QTY']*$windspaswitch15amp['Supplier_Product_Price']);
			$sumpricespaoption = $sumpricespaoption+($windheadonga['QTY']*$windheadonga['Supplier_Product_Price']);

			array_push($arrspa, $hmcp);
			array_push($arrspa, $spajet);
			array_push($arrspa, $spapipe4head);
			array_push($arrspa, $spapipe6head);
			array_push($arrspa, $lidonga);
			array_push($arrspa, $windbutton);
			array_push($arrspa, $windlinepvc);
			array_push($arrspa, $windspaswitch15amp);
			array_push($arrspa, $windheadonga);

			return array($arrspa,$sumpricespaoption);

		}
		else
		{
			return array(NULL,NULL);
		}
	}
	
	function calculate_composite_swimjet($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$inteliswimjet = NULL;
		$swimjetpumproom = NULL;
		$swimjetpipe = NULL;

		$sumpriceswimjetoption = 0;

		$arrswimjet = array();

		if($product_row>0)
		{

			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FFI21-00303'){
					$inteliswimjet = $value;
				}
				if($value['Supplier_Product_Code']=='FAC21-00124'){
					$swimjetpumproom = $value;
				}
				if($value['Supplier_Product_Code']=='FFI60-00916'){
					$swimjetpipe = $value;
				}
			}

			$inteliswimjet['QTY'] = 0;
			$swimjetpumproom['QTY'] = 0;
			$swimjetpipe['QTY'] = 0;

			if($editcompositeoption['Composite_Pools_Option_Swim_Jet_System']=='aquaflow')
			{
				$inteliswimjet['QTY'] = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
				$swimjetpumproom['QTY'] = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
				$swimjetpipe['QTY'] = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
			}
	

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>SPA OPTION</td></tr>";
			//end head
			$sumpriceswimjetoption = $sumpriceswimjetoption+($inteliswimjet['QTY']*$inteliswimjet['Supplier_Product_Price']);
			$sumpriceswimjetoption = $sumpriceswimjetoption+($swimjetpumproom['QTY']*$swimjetpumproom['Supplier_Product_Price']);
			$sumpriceswimjetoption = $sumpriceswimjetoption+($swimjetpipe['QTY']*$swimjetpipe['Supplier_Product_Price']);

			array_push($arrswimjet, $inteliswimjet);
			array_push($arrswimjet, $swimjetpumproom);
			array_push($arrswimjet, $swimjetpipe);

			return array($arrswimjet,$sumpriceswimjetoption);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_composite_otheroption($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$returneyeball = NULL;
		$eyeball = NULL;
		$speedupjet = NULL;
		$crosscurrentpipe = NULL;
		$sfp1500pump = NULL;
		$sfppumproom = NULL;
		$stainlesssteel3step = NULL;

		$sumpriceotheroption = 0;

		$arrotheroption = array();

		if($editcompositeoption['Composite_Pools_Option_Pool_Heating_System']!=NULL)
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}

		$robot = $this->getrobot_with_row()[0];
		$robot_row = $this->getrobot_with_row()[1];

		if($product_row>0)
		{
			
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FLI40-00102'){
					$returneyeball = $value;
				}
				if($value['Supplier_Product_Code']=='FLI40-00100'){
					$eyeball = $value;
				}
				if($value['Supplier_Product_Code']=='FFI60-00512'){
					$speedupjet = $value;
				}
				if($value['Supplier_Product_Code']=="FFI60-00910")
				{
					$crosscurrentpipe = $value;
				}
				if($value['Supplier_Product_Code']=="FPU30-20003")
				{
					$sfp1500pump = $value;
				}
				if($value['Supplier_Product_Code']=="FAC21-00103")
				{
					$sfppumproom = $value;
				}
				if($value['Supplier_Product_Code']=='FAC10-00204'){
					$stainlesssteel3step = $value;
				}
			}

			$returneyeball['QTY'] = 0;
			$eyeball['QTY'] = 0;
			$speedupjet['QTY'] = 0;
			$crosscurrentpipe['QTY'] = 0;
			$sfp1500pump['QTY'] = 0;
			$sfppumproom['QTY'] = 0;
			$stainlesssteel3step['QTY'] = 0;

			if($editcompositeoption['Composite_Pools_Option_Swim_Jet_System']=="jdeyeball")
			{
				$eyeball['QTY'] = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
				$speedupjet['QTY'] = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
				$sfp1500pump['QTY'] = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
				$sfppumproom['QTY'] = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit'];
			}

			if($editcompositeoption['Composite_Pools_Option_Pool_Heating_System']=="saparate")
			{
				$returneyeball['QTY'] += ((ceil(sizeof($heatpump)/2))*3);
			}
			else if($editcompositeoption['Composite_Pools_Option_Pool_Heating_System']=="bypass")
			{
				$eyeball['QTY'] += ((sizeof($heatpump))*2);
			}

			$crosscurrentpipe['QTY'] = $editcompositeoption['Composite_Pools_Option_Swim_Jet_System_Unit']+$editcompositeoption['Composite_Pools_Option_Pool_Sculpture'];

			if($editcompositeoption['Composite_Pools_Option_Ladder']!=""&&$editcompositeoption['Composite_Pools_Option_Ladder']!="No"&&$editcompositeoption['Composite_Pools_Option_Ladder']!=NULL)
			{
				$stainlesssteel3step['QTY'] = $editcompositeoption['Composite_Pools_Option_Ladder_Unit'];
			}

			if($editcompositeoption['Composite_Pools_Option_Robotic_Cleaner']!=NULL&&$editcompositeoption['Composite_Pools_Option_Robotic_Cleaner']!="No")
			{
				if($robot!=NULL)
				{
					foreach ($robot as $key => $value) {
						if($value['Supplier_Product_Code'] == $editcompositeoption['Composite_Pools_Option_Robotic_Cleaner'])
						{
							$robot = $value;
							$robot['QTY'] = 1;
						}
					}
					
				}

			}
			

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Other OPTION</td></tr>";
			//end head
			$sumpriceotheroption = $sumpriceotheroption+($returneyeball['QTY']*$returneyeball['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($eyeball['QTY']*$eyeball['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($speedupjet['QTY']*$speedupjet['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($crosscurrentpipe['QTY']*$crosscurrentpipe['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sfp1500pump['QTY']*$sfp1500pump['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($sfppumproom['QTY']*$sfppumproom['Supplier_Product_Price']);
			$sumpriceotheroption = $sumpriceotheroption+($stainlesssteel3step['QTY']*$stainlesssteel3step['Supplier_Product_Price']);
			if($editcompositeoption['Composite_Pools_Option_Robotic_Cleaner']!=NULL)
			{
				$sumpriceotheroption = $sumpriceotheroption+($robot['QTY']*$robot['Supplier_Product_Price']);
			}

			array_push($arrotheroption, $returneyeball);
			array_push($arrotheroption, $eyeball);
			array_push($arrotheroption, $speedupjet);
			array_push($arrotheroption, $crosscurrentpipe);
			array_push($arrotheroption, $sfp1500pump);
			array_push($arrotheroption, $sfppumproom);
			array_push($arrotheroption, $stainlesssteel3step);

			if($editcompositeoption['Composite_Pools_Option_Robotic_Cleaner']!=NULL)
			{
				array_push($arrotheroption, $robot);
			}


			return array($arrotheroption,$sumpriceotheroption);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_composite_maintenent($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$vacuum12 = NULL;
		$vacuumhead = NULL;
		$leaf = NULL;
		$filterbag = NULL;
		$telescopic = NULL;
		$wall = NULL;
		$thermomiter = NULL;
		$testkid = NULL;

		$sumpricemaintenent = 0;

		$arrmaintain = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FPM00-00112'){
					$vacuum12 = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00215'){
					$vacuumhead = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00303'){
					$leaf = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00400'){
					$filterbag = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00503'){
					$telescopic = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00604'){
					$wall = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00702'){
					$thermomiter = $value;
				}
				if($value['Supplier_Product_Code']=='FPM00-00812'){
					$testkid = $value;
				}
			}

			$vacuum12['QTY'] = 1;
			$vacuumhead['QTY'] = 1;
			$leaf['QTY'] = 1;
			$filterbag['QTY'] = 1;
			$telescopic['QTY'] = 1;
			$wall['QTY'] = 1;
			$thermomiter['QTY'] = 1;
			$testkid['QTY'] = 1;

			if($editcomposite['Composite_Pools_Maintenance_Accessories']>0)
			{
				$vacuumhead['QTY'] = $vacuumhead['QTY']*$editcomposite['Composite_Pools_Maintenance_Accessories'];
				$leaf['QTY'] = $leaf['QTY']*$editcomposite['Composite_Pools_Maintenance_Accessories'];
				$filterbag['QTY'] = $filterbag['QTY']*$editcomposite['Composite_Pools_Maintenance_Accessories'];
				$telescopic['QTY'] = $telescopic['QTY']*$editcomposite['Composite_Pools_Maintenance_Accessories'];
				$wall['QTY'] = $wall['QTY']*$editcomposite['Composite_Pools_Maintenance_Accessories'];
				$thermomiter['QTY'] = $thermomiter['QTY']*$editcomposite['Composite_Pools_Maintenance_Accessories'];
				$testkid['QTY'] = $testkid['QTY']*$editcomposite['Composite_Pools_Maintenance_Accessories'];
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Maintenent Accessories</td></tr>";
			//end head

			$sumpricemaintenent = $sumpricemaintenent+($vacuum12['QTY']*$vacuum12['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($vacuumhead['QTY']*$vacuumhead['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($leaf['QTY']*$leaf['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($filterbag['QTY']*$filterbag['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($telescopic['QTY']*$telescopic['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($wall['QTY']*$wall['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($thermomiter['QTY']*$thermomiter['Supplier_Product_Price']);
			$sumpricemaintenent = $sumpricemaintenent+($testkid['QTY']*$testkid['Supplier_Product_Price']);

			array_push($arrmaintain, $vacuum12);
			array_push($arrmaintain, $vacuumhead);
			array_push($arrmaintain, $leaf);
			array_push($arrmaintain, $filterbag);
			array_push($arrmaintain, $telescopic);
			array_push($arrmaintain, $wall);
			array_push($arrmaintain, $thermomiter);
			array_push($arrmaintain, $testkid);

			return array($arrmaintain,$sumpricemaintenent);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function calculate_composite_chemical($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];

		$product = $this->getallproduct_with_row()[0];
		$product_row = $this->getallproduct_with_row()[1];

		$longlasting = NULL;
		$chlorine = NULL;
		$php = NULL;
		$phm = NULL;
		$jdclean = NULL;

		$sumpricechem = 0;

		$arrchem = array();

		if($product_row>0)
		{
			foreach ($product as $key => $value) {
				if($value['Supplier_Product_Code']=='FCH00-00101'){
					$longlasting = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00111'){
					$chlorine = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00200'){
					$php = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00210'){
					$phm = $value;
				}
				if($value['Supplier_Product_Code']=='FCH00-00300'){
					$jdclean = $value;
				}
			}

			$longlasting['QTY'] = 1;
			$chlorine['QTY'] = 1;
			$php['QTY'] = 1;
			$phm['QTY'] = 1;
			$jdclean['QTY'] = 1;

			if($editcomposite['Composite_Pools_Chemicals']>0)
			{
				$longlasting['QTY'] = $longlasting['QTY']*$editcomposite['Composite_Pools_Chemicals'];
				$chlorine['QTY'] = $chlorine['QTY']*$editcomposite['Composite_Pools_Chemicals'];
				$php['QTY'] = $php['QTY']*$editcomposite['Composite_Pools_Chemicals'];
				$phm['QTY'] = $phm['QTY']*$editcomposite['Composite_Pools_Chemicals'];
				$jdclean['QTY'] = $jdclean['QTY']*$editcomposite['Composite_Pools_Chemicals'];
			}

			//head
			//echo "<tr><td colspan='6' style='border: 1px solid black;'>Chemical Treatment</td></tr>";
			//end head

			$sumpricechem = $sumpricechem+($longlasting['QTY']*$longlasting['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($chlorine['QTY']*$chlorine['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($php['QTY']*$php['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($phm['QTY']*$phm['Supplier_Product_Price']);
			$sumpricechem = $sumpricechem+($jdclean['QTY']*$jdclean['Supplier_Product_Price']);

			array_push($arrchem,$longlasting);
			array_push($arrchem,$chlorine);
			array_push($arrchem,$php);
			array_push($arrchem,$phm);
			array_push($arrchem,$jdclean);

			return array($arrchem, $sumpricechem);

		}
		else
		{
			return array(NULL,NULL);
		}
	}

	function structure_boq_composite($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

        $distanct = $this->calkm($Project_ID);

        $Length = $editcompositepooldata['Composite_Pools_Data_Length'];
		$Width = $editcompositepooldata['Composite_Pools_Data_Width'];
		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];
		$Dept1 = $editcompositepooldata['Composite_Pools_Data_Dept1'];
		$Dept2 = $editcompositepooldata['Composite_Pools_Data_Dept2'];
		$Dept3 = $editcompositepooldata['Composite_Pools_Data_Dept3'];

		if($editcompositeoption['Composite_Pools_Option_Pool_Heating_System']!=NULL&&$editcompositeoption['Composite_Pools_Option_Pool_Heating_System']!="")
		{
			$heatpump = $this->getheatpump($Area);
		}
		else
		{
			$heatpump = NULL;
		}

		if($heatpump!=NULL)
		{
			$heatpumpqty = sizeof($heatpump);
		}

		$wherearr = array("structure.Pool_Type = 'composite'");
		$joincarr = array("product_unit");
		$joinvarr = array("structure.Product_Unit_ID = product_unit.Product_Unit_ID");

		$boqcomposite = $this->select_model->selectwherejoin("*","structure",$wherearr,$joincarr,$joinvarr,"Structure_Code","asc");


		if($boqcomposite->num_rows())
		{
			$boqcomposite_row = $boqcomposite->num_rows();
			$boqcomposite = $boqcomposite->result_array();
		}
		else
		{
			$boqcomposite = NULL;
			$boqcomposite_row=0;
		}

		$boqprovcomposite = $this->select_model->select_where("*","structure_by_province","structure_by_province.Province_ID = '".$editproject['Province_ID']."'");
		if($boqprovcomposite->num_rows())
		{
			$boqprovcomposite_row = $boqprovcomposite->num_rows();
			$boqprovcomposite = $boqprovcomposite->result_array();
		}
		else
		{
			$boqprovcomposite = NULL;
			$boqprovcomposite_row=0;
		}

		$returnboqcomposite = array();

		$elecpvcmatsum = 0;
		$elecpvclabsum = 0;
		$allpipe = 0;

		foreach ($boqcomposite as $keyboq => $valueboq) {
			if($valueboq['Structure_Code']==20.5)
			{
				$aftermeterconcreteprice = $valueboq['Structure_Labour_Unit_Cost'];
			}
			if($valueboq['Structure_Code']==21.5)
			{
				$aftermeterkidpoolprice = $valueboq['Structure_Labour_Unit_Cost'];
			}
			if($valueboq['Structure_Code']==22.5)
			{
				$aftermeterinfinprice = $valueboq['Structure_Labour_Unit_Cost'];
			}
		}

		foreach ($boqcomposite as $keyboq => $valueboq) {
			
			if($boqprovcomposite_row>0)
			{
				foreach ($boqprovcomposite as $keyprovboq => $valueprovboq) {
					if($valueprovboq['Structure_ID']==$valueboq['Structure_ID'])
					{
						$valueboq['Structure_Labour_Unit_Cost'] = $valueprovboq['Structure_By_Province_Labour_Unit_Cost'];
						$valueboq['Structure_Materials_Unit_Cost'] = $valueprovboq['Structure_By_Province_Materials_Unit_Cost'];
						break;
					}
				}
			}
			

			if($valueboq['Structure_Code']==1)
			{
				if( ($distanct/1000)<=50 )
				{
					$valueboq['QTY'] = 0;
				}
				else
				{
					$valueboq['QTY'] = 1;
				}
			}
			else if($valueboq['Structure_Code']==2)
			{
				if( ($distanct/1000)<=50 )
				{
					$valueboq['QTY'] = 0;
				}
				else
				{
					$valueboq['QTY'] = 1;
				}
			}
			else if($valueboq['Structure_Code']==3)
			{
				if( ($distanct/1000)<=50 )
				{
					$valueboq['QTY'] = 1;
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==4)
			{
				$mac = 0;

				if($editcomposite['Composite_Pools_Work']=='Inground')
				{
					
					if($editcomposite['Composite_Pools_Dig']=='Machine')
					{
						$mac = 1;
					}
					else
					{
						$mac = 0;
						$valueboq['Structure_Code'] = 4.1;
					}

					$valueboq['Structure_Labour_Unit_Cost'] = ($valueboq['Structure_Labour_Unit_Cost']*$mac);
				}

				if($editcomposite['Composite_Pools_Pile']=='Have')
				{
					$pile = 1;
				}
				else
				{
					$pile = 0;
				}
				
				$valueboq['QTY'] = ceil( ($Capacity+($Area*0.2*$pile)+($Perimeter*($Dept1+$Dept2+$Dept3)*0.25))*($mac) );
			}
			else if($valueboq['Structure_Code']==4.5)
			{
				$men = 0;
				if($editcomposite['Composite_Pools_Work']=='Inground')
				{
					
					if($editcomposite['Composite_Pools_Dig']=='Men')
					{
						$men = 1;
						$valueboq['Structure_Code'] = 4;
					}
					else
					{
						$men = 0;
					}

					if($editcomposite['Composite_Pools_Pile']=='Have')
					{
						$pile = 1;
					}
					else
					{
						$pile = 0;
					}

					$valueboq['Structure_Labour_Unit_Cost'] = ($valueboq['Structure_Labour_Unit_Cost']*$men);
				}
				
				$valueboq['QTY'] = ceil( ($Capacity+($Area*0.2*$pile)+($Perimeter*($Dept1+$Dept2+$Dept3)*0.25))*($men) );
			}
			else if($valueboq['Structure_Code']==5)
			{
				if($editcomposite['Composite_Pools_Work']=='Inground')
				{
					if($editcomposite['Composite_Pools_Pile']=='Have')
					{
						$boq7 = ceil( (($Perimeter/2)+$Area)*0.05 );
						$boq9 = ceil( ($Length*$Width*0.15) );

						$valueboq['QTY'] = ceil( (($Capacity+($boq7+$boq9)*2.4)/1.2)+2 );
					}
					else
					{
						$valueboq['QTY'] = 0;
					}	
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
				
			}
			else if($valueboq['Structure_Code']==6)
			{
				$valueboq['QTY'] = ceil( (($Perimeter/2)+$Area)*0.05 );
			}
			else if($valueboq['Structure_Code']==7)
			{
				if($editcomposite['Composite_Pools_Work']=='Inground')
				{
					if($editcomposite['Composite_Pools_Pile']=='Have')
					{
						$valueboq['QTY'] = ceil( (($Perimeter/2)+$Area)*0.05 );
					}
					else
					{
						$valueboq['QTY'] = 0;
					}	
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==8)
			{
				if($editcomposite['Composite_Pools_Work']=='Inground')
				{
					if($editcomposite['Composite_Pools_Pile']=='Have')
					{
						$valueboq['QTY'] = ceil( ($Length*$Width) );
					}
					else
					{
						$valueboq['QTY'] = 0;
					}	
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==9)
			{
				if($editcomposite['Composite_Pools_Work']=='Inground')
				{
					if($editcomposite['Composite_Pools_Pile']=='Have')
					{
						$valueboq['QTY'] = ceil( ($Length*$Width*0.15) );
					}
					else
					{
						$valueboq['QTY'] = 0;
					}	
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
			}
			else if($valueboq['Structure_Code']==10)
			{
					$valueboq['QTY'] = $Capacity;
			}
			else if($valueboq['Structure_Code']==11)
			{
				if($editcomposite['Composite_Pools_Work']=='Inground')
				{
					if($editcomposite['Composite_Pools_Pile']=='Have')
					{
						$pile = 1;
					}
					else
					{
						$pile = 0;
					}

					$boq4 = ceil( ($Capacity+($Area*0.2*$pile)+($Perimeter*($Dept1+$Dept2+$Dept3)*0.25))*(1) );

					$boq6 = $valueboq['QTY'] = ceil( (($Perimeter/2)+$Area)*0.05 );

					if($editcomposite['Composite_Pools_Pile']=='Have')
					{
						$boq7 = ceil( (($Perimeter/2)+$Area)*0.05 );
						$boq9 = ceil( ($Perimeter*$Area*0.15) );
					}
					else
					{
						$boq7 = 0;
						$boq9 = 0;
					}
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
				
			}
			else if($valueboq['Structure_Code']==12)
			{
				$valueboq['QTY'] = $this->sum_composite_spa_swim_heat_scul_mechanicalroom($Project_ID);
			}
			else if($valueboq['Structure_Code']==13)
			{
				$valueboq['QTY'] = 20;
			}
			else if($valueboq['Structure_Code']==14)
			{
				$valueboq['QTY'] = 10;
			}
			else if($valueboq['Structure_Code']==15)
			{
				$valueboq['QTY'] =  20;
			}
			else if($valueboq['Structure_Code']==16)
			{
				$allexpump = $this->sum_composite_spa_swim_heat_scul_pump($Project_ID);
				$valueboq['QTY'] = ceil($allexpump*(10+($Perimeter/2)));
			}
			else if($valueboq['Structure_Code']==17)
			{
				//$valueboq['QTY'] = $heatpumpqty*30;
				$valueboq['QTY'] = 0;
			}
			else if($valueboq['Structure_Code']==18)
			{
				$valueboq['QTY'] = 1;
			}
			else if($valueboq['Structure_Code']==19)
			{
				$valueboq['QTY'] = ceil($Perimeter);
			}
			else if($valueboq['Structure_Code']==20)
			{
				$allexpump = $this->sum_composite_spa_swim_heat_scul_pump($Project_ID);
				$boq21 = $allexpump*15;
				$boq22 = ceil($editcompositeoption['Composite_Pools_Option_Under_Water_Light_Unit']*(($Perimeter*0.5)+20));
				$boq23 = 20;

				$valueboq['QTY'] = $boq21+$boq22+$boq23;
			}
			else if($valueboq['Structure_Code']==21)
			{
				$allexpump = $this->sum_composite_spa_swim_heat_scul_pump($Project_ID);
				$valueboq['QTY'] = $allexpump*15;
			}
			else if($valueboq['Structure_Code']==22)
			{
				$valueboq['QTY'] = ceil($editcompositeoption['Composite_Pools_Option_Under_Water_Light_Unit']*(($Perimeter*0.5)+20));
			}
			else if($valueboq['Structure_Code']==23)
			{
				$valueboq['QTY'] = 20;
			}
			else if($valueboq['Structure_Code']==24)
			{
				$valueboq['QTY'] = 1;
			}
			else if($valueboq['Structure_Code']==25)
			{
				if($editcomposite['Composite_Pools_Coping']!="None"&&$editcomposite['Composite_Pools_Coping']!=""&&$editcomposite['Composite_Pools_Coping']!=NULL)
				{
					$valueboq['QTY'] = ($Perimeter+3.5);
				}
				else
				{
					$valueboq['QTY'] = 0;
				}
				
			}
			else
			{
				$valueboq['QTY'] = 0;
			}

			if(strpos($valueboq['Structure_Code'], '.') !== false)
			{

			}
			else
			{
				$returnboqcomposite[$keyboq] = $valueboq;
			}
			
		}

		return $returnboqcomposite;
	}

	function getsystemandfinal_composite($Project_ID)
	{
		$editproject = $this->getproj($Project_ID);

		$editcomposite = $this->getcomposite($Project_ID);

        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);

		$Area = $editcompositepooldata['Composite_Pools_Data_Area'];
		$Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
		$Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];
		$Dept1 = $editcompositepooldata['Composite_Pools_Data_Dept1'];
		$Dept2 = $editcompositepooldata['Composite_Pools_Data_Dept2'];
		$Dept3 = $editcompositepooldata['Composite_Pools_Data_Dept3'];

		$wherearr = array("system_final.Pool_Type = 'composite'");
		$joincarr = array("product_unit");
		$joinvarr = array("system_final.Product_Unit_ID = product_unit.Product_Unit_ID");

		$sysfinalcomposite = $this->select_model->selectwherejoin("*","system_final",$wherearr,$joincarr,$joinvarr,NULL,NULL);


		if($sysfinalcomposite->num_rows())
		{
			$system_finalcomposite_row = $sysfinalcomposite->num_rows();
			$system_finalcomposite = $sysfinalcomposite->result_array();
		}
		else
		{
			$system_finalcomposite = NULL;
			$system_finalcomposite_row=0;
		}

		$system_finalprovcomposite = $this->select_model->select_where("*","system_final_by_province","system_final_by_province.Province_ID = '".$editproject['Province_ID']."'");
		if($system_finalprovcomposite->num_rows())
		{
			$system_finalprovcomposite_row = $system_finalprovcomposite->num_rows();
			$system_finalprovcomposite = $system_finalprovcomposite->result_array();
		}
		else
		{
			$system_finalprovcomposite = NULL;
			$system_finalprovcomposite_row=0;
		}

		$returnsystemandfinal = array();

		foreach ($system_finalcomposite as $syskey => $sysvalue) {

			if($system_finalprovcomposite_row>0)
			{
				foreach ($system_finalprovcomposite as $keyprovsaf => $valueprovsaf) {
					if($valueprovsaf['System_Final_ID']==$sysvalue['System_Final_ID'])
					{
						$sysvalue['System_Final_Labour_Cost'] = $valueprovsaf['System_Final_By_Province_Labour_Unit_Cost'];
						$sysvalue['System_Final_Materials_Cost'] = $valueprovsaf['System_Final_By_Province_Materials_Unit_Cost'];
						break;
					}
				}
			}
			

			if($sysvalue['System_Final_Code']==1)
			{
				$sysvalue['QTY'] = 1;
			}
			elseif($sysvalue['System_Final_Code']==2)
			{
				$sysvalue['QTY'] =  $this->sum_composite_spa_swim_heat_scul_mechanicalroom($Project_ID);
			}
			elseif($sysvalue['System_Final_Code']==3)
			{
				if($editcompositeoption['Composite_Pools_Option_Under_Water_Light_Unit']>2)
				{
					$sysvalue['QTY'] = $editcompositeoption['Composite_Pools_Option_Under_Water_Light_Unit']-2;
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
				
			}
			elseif($sysvalue['System_Final_Code']==4)
			{
				if($editcomposite['Composite_Pools_Control_Panel']=="salt")
				{
					$sysvalue['QTY'] = ceil($Capacity*4/25);
				}
				else
				{
					$sysvalue['QTY'] = 0;
				}
			}
			elseif($sysvalue['System_Final_Code']==5)
			{
				$sysvalue['QTY'] = $Capacity*0.5;
			}
			else
			{
				$sysvalue['QTY'] = 0;
			}


			$returnsystemandfinal[$syskey] = $sysvalue;
		}

		return $returnsystemandfinal;
	}

	function getsummarycomposite($Project_ID)
	{
 		$Project_ID = $this->input->get('pid');

        $editproject = $this->getproj($Project_ID);

        $editcustomer = $this->getcustomer($editproject['Customer_ID']);
        $editcustomer_contact = $this->getcustomer_contact($Project_ID,$editproject['Customer_ID']);

        $editcomposite = $this->getcomposite($Project_ID);
        $editcompositeoption =  $this->getcompositeoption($editcomposite['Composite_Pools_ID']);

        $distanct = $this->calkm($Project_ID);

        $editcompositepooldata = $this->getcompositepooldata($editcomposite['Composite_Pools_Series_ID'],$editcomposite['Composite_Color_ID']);
        $editcompositepooldata['QTY'] = 1;

        $flitrationunit = $this->getcompositeflitrationunit_with_row()[0];
        $flitrationunit_row = $this->getcompositeflitrationunit_with_row()[1];

        
        $Area = $editcompositepooldata['Composite_Pools_Data_Area'];
        $Perimeter = $editcompositepooldata['Composite_Pools_Data_Perimeter'];
        $Capacity = $editcompositepooldata['Composite_Pools_Data_Volume'];

        $robot = $this->getrobot_with_row()[0];
        $robot_row = $this->getrobot_with_row()[1];

        $saltdata = $this->getsaltprice();

        if($editcompositeoption['Composite_Pools_Option_Robotic_Cleaner']!=NULL)
        {
            foreach ($robot as $key => $value) {
                if($value['Supplier_Product_Code'] == $editcompositeoption['Composite_Pools_Option_Robotic_Cleaner'])
                {
                    //$showrobot = $editipaneloption['Ipanel_Pool_Option_Robotic_Cleaner']." - ".$value['Supplier_Product_Name_ENG']." = ".$value['Supplier_Product_Price'];
                    $showrobot = $value['Supplier_Product_Name_ENG'];
                }
            }
        }
        else
        {
            $showrobot = NULL;
        }

        $worksystem = $this->calculate_composite_worksystem($Project_ID);
        $spaoption = $this->calculate_composite_spaoption($Project_ID);
        $swimjet = $this->calculate_composite_swimjet($Project_ID);
        $otheroption = $this->calculate_composite_otheroption($Project_ID);
        $maintenent = $this->calculate_composite_maintenent($Project_ID);
        $chemical = $this->calculate_composite_chemical($Project_ID);

        $payment = $this->getpayment($Project_ID);

        $use_worksystem = $worksystem[0];
        $cost_worksystem = $worksystem[1];

        $use_spaoption = $spaoption[0];
        $cost_spaoption = $spaoption[1];
        
        $use_swimjet = $swimjet[0];
        $cost_swimjet = $swimjet[1];
        
        $use_otheroption = $otheroption[0];
        $cost_otheroption = $otheroption[1];

        $use_maintenent = $maintenent[0];
        $cost_maintenent = $maintenent[1];

        $use_chemical = $chemical[0];
        $cost_chemical = $chemical[1];

        $alljdprodsummary = $editcompositepooldata['Supplier_Product_Price']+$cost_worksystem+$cost_spaoption+$cost_swimjet+$cost_otheroption+$cost_maintenent+$cost_chemical;
        $compositeboq = $this->structure_boq_composite($Project_ID);
        $compositesystemandfinal = $this->getsystemandfinal_composite($Project_ID);

        $totlemat = 0;
        $totlelabour = 0;
        $totleall = 0;

        $totlematsaf = 0;
        $totlelaboursaf = 0;
        $totleallsaf = 0;

        $eleccable2x2_5qty = 0;
        $eleccable2x4qty = 0;
        $eleccable3x1_5qty = 0;
        $eleccable10x2_5qty = 0;

        $eleccable2x2_5price = 0;
        $eleccable2x4price = 0;
        $eleccable3x1_5price = 0;
        $eleccable10x2_5price = 0;

        $maincopping = 0;

        $transportprice = $this->gettransport($Project_ID);

        foreach ($compositeboq as $key => $value) {
            if($value['Structure_Code']=="21"){
                $eleccable3x1_5qty = $value['QTY'];
                $eleccable3x1_5price = $value['Structure_Materials_Unit_Cost'];
                $value['Structure_Materials_Unit_Cost'] = 0;
            }
            if($value['Structure_Code']=="22"){
                $eleccable2x2_5qty = $value['QTY'];
                $eleccable2x2_5price = $value['Structure_Materials_Unit_Cost'];
                $value['Structure_Materials_Unit_Cost'] = 0;
            }
            if($value['Structure_Code']=="23"){
                $eleccable10x2_5qty = $value['QTY'];
                $eleccable10x2_5price = $value['Structure_Materials_Unit_Cost'];
                $value['Structure_Materials_Unit_Cost'] = 0;
            }
            if($value['Structure_Code']=="25"){
                $maincopping = $value['QTY'];
            }

            $totlemat += $value['Structure_Materials_Unit_Cost']*$value['QTY'];
            $totlelabour += $value['Structure_Labour_Unit_Cost']*$value['QTY'];
            $totleall += ($value['QTY']*$value['Structure_Labour_Unit_Cost'])+($value['Structure_Materials_Unit_Cost']*$value['QTY']);
        }
  
        foreach ($compositesystemandfinal as $key => $value) {
            $totlematsaf += $value['System_Final_Materials_Cost']*$value['QTY'];
            $totlelaboursaf += $value['System_Final_Labour_Cost']*$value['QTY'];
            $totleallsaf += ($value['QTY']*$value['System_Final_Labour_Cost'])+($value['System_Final_Materials_Cost']*$value['QTY']);
        }

        $priceprodwithdc = $alljdprodsummary - (($alljdprodsummary * $this->branchdc($Project_ID))/100);

        $sumallle = 0;

        $sumallle += $eleccable3x1_5qty*$eleccable3x1_5price;

        $sumallle += $eleccable2x2_5qty*$eleccable2x2_5price;

        $sumallle += $eleccable10x2_5qty*$eleccable10x2_5price;

        $usecoping = NULL;

        $wherearr = array("Supplier_Product_Code like 'FST10-007%'");
        $allcoping = $this->select_model->select_orwhere("*","supplier_product",$wherearr);
        if($allcoping->num_rows())
        {
            $allcoping_row = $allcoping->num_rows();
            $allcoping = $allcoping->result_array();
        }
        else
        {
            $allcoping_row = 0;
            $allcoping = NULL;
        }

        foreach ($allcoping as $key => $value) {
            if($editcomposite['Composite_Pools_Coping']==$value['Supplier_Product_Code'])
            {
                $usecoping = $value;
            }
        }


        $cprodname = strlen($usecoping['Supplier_Product_Name']);
        $showprd = NULL;
        if(strlen(iconv( 'UTF-8','cp874' , $usecoping['Supplier_Product_Name']))>45)
        {
            $showprd = substr($usecoping['Supplier_Product_Name'], 0,108);
            $showprd = $this->ConvertToUTF8($showprd);
            $showprd = $showprd."...";
        }
        else
        {
            $showprd = $usecoping['Supplier_Product_Name'];
        }

        if($usecoping!=NULL)
        {
            $sumallle += $maincopping*$usecoping['Supplier_Product_Price'];
        }

        if($editcomposite['Composite_Pools_Control_Panel']=='salt')
        {
            $salt = ceil($Capacity*4/25);
        }
        else
        {
            $salt = 0;
        }

        if($saltdata!=NULL)
        {
        	$sumallle += ceil($salt)*$saltdata["Supplier_Product_Price"];
        }
        else
        {
        	$sumallle += ceil($salt)*300;
        }
        

        $sumallle += $Capacity*250;

         $showprice = ( ( $priceprodwithdc + ($totleall+($totleall*0.135)+(($totleall+$totleall*0.135)*0.07))+
        ($totleallsaf+($totleallsaf*0.135)+(($totleallsaf+$totleallsaf*0.135)*0.07)) + ($sumallle+$transportprice[1]) ) *2 );

         return $showprice;
	}

	function getpayment($Project_ID)
	{
		$getpayment = $this->select_model->select_where("*","payment","Project_ID = '".$Project_ID."'");
		if($getpayment->num_rows())
		{
			return $getpayment->row_array();
		}
		else
		{
			return NULL;
		}
	}

}