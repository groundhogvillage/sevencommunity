$(document).ready(function(){
  tinymce.init({
    selector:'textarea',
    oninit : "setPlainText",
    plugins: [
      'autolink lists link preview hr anchor',
      'searchreplace wordcount code fullscreen',
      'insertdatetime contextmenu',
      'paste textcolor colorpicker textpattern toc'
    ],
    toolbar1: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ',
    toolbar2: 'forecolor backcolor | link'
  });
});
