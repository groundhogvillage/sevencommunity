$(".edtfile").click(function(){
  var mainid = $("#mainid").val();
  var subid = $("#subid").val();

  $("body").addClass("loading");

  var id = $(this).attr('id').split("_");
  var id1 = id[1];

  $.ajax({
    type: "GET",
    url: base_url + "cms/editfile?idedt="+id1+"&type=file",
    }).done(function(msg){
      data = $.parseJSON(msg);
      $("#fileid").val(data.id);
      $("#oldfile").val(data.path);
      $("#filename").val(data.name);
  });

  $("body").removeClass("loading");

});

$(".delthisfile").click(function(){
  var mainid = $("#mainid").val();
  var subid = $("#subid").val();

  $("body").addClass("loading");

  var id = $(this).attr('id').split("_");
  var id1 = id[1];

  $.ajax({
    type: "GET",
    url: base_url + "cms/deletefile?iddel="+id1+"&type=file",
    }).done(function(msg){
    if(msg == "Delete Complete"){
    //  window.location.href = window.location.href;
      $('#deleteddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

      $.ajax({
        type: "GET",
        url: base_url + "cms/getfilecontent?pageid="+mainid+"&subid="+subid+"&type=file"
       }).done(function(msg2){

        data = $.parseJSON(msg2);

        if(data.alldata_row > 0){
          $("#reviewfile").html(data.retdata);
        }
        else
        {
          $("#reviewfile").html("");
        }
      });

      /*$.getScript( base_url + "assets/js/cms/defaultrefreshdelfile.js", function( data, textStatus, jqxhr ) {
        console.log( data ); // Data returned
        console.log( textStatus ); // Success
        console.log( jqxhr.status ); // 200
        console.log( "Load was performed." );
      });*/

      $("body").removeClass("loading"); 

    }
    else
    {
      alert(msg);
      $("body").removeClass("loading");
    //$("#showerr").text(msg);
    }
  });

  $("body").removeClass("loading");

});

$(document).ready(function(){
    $('.reorder_linkfile').on('click',function(){
        $("ul.reorder-file-list").sortable({ tolerance: 'pointer' });
        $('.reorder_linkfile').html('save reorderingfile');
        $('.reorder_linkfile').attr("id","save_reorderfile");
        $('#reorder-helperfile').slideDown('slow');
        $('.image_linkfile').attr("href","javascript:void(0);");
        $('.image_linkfile').css("cursor","move");
        $("#save_reorderfile").click(function( e ){
            if( !$("#save_reorderfile i").length ){
                $(this).html('').prepend('<img src="../assets/images/loading.gif"/>');
                $("ul.reorder-file-list").sortable('destroy');
                $("#reorder-helperfile").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');

                var mainid = $("#mainid").val();
                var subid = $("#subid").val();

                var h = [];
                $("ul.reorder-file-list li").each(function() {  h.push($(this).attr('id').substr(8));  });

                $.ajax({
                    type: "POST",
                    url: base_url + "cms/reorderbytype",
                    data: {order: h, type: "file", mainid: mainid, subid: subid},
                    success: function(result){
                      //  alert(result);
                    //  window.location.reload();
                    if(result=="complete")
                    {
                      $.ajax({
                        type: "GET",
                        url: base_url + "cms/getfilecontent?pageid="+mainid+"&subid="+subid+"&type=file"
                       }).done(function(msg2){

                        data = $.parseJSON(msg2);

                        if(data.alldata_row > 0){
                          $("#reviewfile").html(data.retdata);
                        }
                        else
                        {
                          $("#reviewfile").html("");
                        }
                      });
                    }

                    }
                });



                return false;
            }
            e.preventDefault();
        });
    });
});
