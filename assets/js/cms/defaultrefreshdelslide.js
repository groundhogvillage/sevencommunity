$(".edtslide").click(function(){
  var idpage = $("#mainid").val();

  $("body").addClass("loading");

  var id = $(this).attr('id').split("_");
  var id1 = id[1];

  $.ajax({
    type: "GET",
    url: base_url + "cms/editfile?idedt="+id1+"&type=slide",
    }).done(function(msg){
      data = $.parseJSON(msg);
      $("#slideid").val(data.id);
      $("#oldslide").val(data.path);
      $("#slidename").val(data.name);
  });

  $("body").removeClass("loading");

});


$(".delthisslide").click(function(){
  var mainid = $("#mainid").val();
  var subid = $("#subid").val();

  $("body").addClass("loading");

  var id = $(this).attr('id').split("_");
  var id1 = id[1];

  $.ajax({
    type: "GET",
    url: base_url + "cms/deletefile?iddel="+id1+"&type=slide",
    }).done(function(msg){
    if(msg == "Delete Complete"){
      //  window.location.href = window.location.href;
      $('#deleteddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

       $.ajax({
         type: "GET",
         url: base_url + "cms/getfilecontent?pageid="+mainid+"&subid="+subid+"&type=slide"
        }).done(function(msg2){

         data = $.parseJSON(msg2);

         if(data.alldata_row > 0){
           $("#reviewslide").html(data.retdata);
         }
         else
         {
           $("#reviewslide").html("");
         }
       });

       $("body").removeClass("loading");

       //$.getScript( base_url + "assets/js/cms/defaultrefreshdelslide.js", function( data, textStatus, jqxhr ) { });
    }
    else
    {
      alert(msg);
      //$("#showerr").text(msg);
      $("body").removeClass("loading"); 
    }
  });
});


$(document).ready(function(){
    $('.reorder_linkslide').on('click',function(){
        $("ul.reorder-slide-list").sortable({ tolerance: 'pointer' });
        $('.reorder_linkslide').html('save reorderingslide');
        $('.reorder_linkslide').attr("id","save_reorderslide");
        $('#reorder-helperslide').slideDown('slow');
        $('.image_linkslide').attr("href","javascript:void(0);");
        $('.image_linkslide').css("cursor","move");
        $("#save_reorderslide").click(function( e ){
            if( !$("#save_reorderslide i").length ){
                $(this).html('').prepend('<img src="../assets/images/loading.gif"/>');
                $("ul.reorder-slide-list").sortable('destroy');
                $("#reorder-save_reorderslide").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');

                var mainid = $("#mainid").val();
                var subid = $("#subid").val();

                var h = [];
                $("ul.reorder-slide-list li").each(function() {  h.push($(this).attr('id').substr(9));  });

                $.ajax({
                    type: "POST",
                    url: base_url + "cms/reorderbytype",
                    data: {order: h, type: "slide", mainid: mainid, subid: subid},
                    success: function(result){
                      //  alert(result);
                      //  window.location.reload();
                      if(result=="complete")
                      {
                        $.ajax({
                          type: "GET",
                          url: base_url + "cms/getfilecontent?pageid="+mainid+"&subid="+subid+"&type=slide"
                         }).done(function(msg2){

                          data = $.parseJSON(msg2);

                          if(data.alldata_row > 0){
                            $("#reviewslide").html(data.retdata);
                          }
                          else
                          {
                            $("#reviewslide").html("");
                          }
                        });
                      }

                    }
                });



                return false;
            }
            e.preventDefault();
        });
    });
});
