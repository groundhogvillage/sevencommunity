var host = window.location.origin;

if(host=="http://localhost"){
  var base_url = "http://localhost/sevencommunity/";
}
else
{
   var base_url = host+"/";
}

document.getElementById("defaultOpen").click();

$("#defform").submit(function(){

 tinyMCE.triggerSave();
 var ref = $(this).find("[required]");
 var x = 0;

 $("body").addClass("loading");

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

 $.ajax({
   type: "POST",
   url: base_url + "cms/updatecmsdata_sub",
   data: new FormData( this ),
   processData: false,
   contentType: false,
   async: true
   }).done(function(msg){
     if(msg == "ข้อมูลได้ทำการเพิ่มแล้ว")
     {
       window.location.href = window.location.href;
       $("body").removeClass("loading");
     }
     else if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว")
     {
       $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');
       $("body").removeClass("loading");
     }
     else
     {
       alert(msg);
       $("body").removeClass("loading");
     //$("#showerr").text(msg);
     }
 });
   return false;

});


$("#defformfile").submit(function(){
 tinyMCE.triggerSave();
 var mainid = $("#mainid").val();
 var subid = $("#subid").val();

 var ref = $(this).find("[required]");
 var x = 0;

 $("body").addClass("loading");

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

   $.ajax({
     type: "POST",
     url: base_url + "cms/updatecmsfile",
     data: new FormData( this ),
     processData: false,
     contentType: false,
     async: true
     }).done(function(msg){
     if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"||msg == "ข้อมูลได้ทำการเพิ่มแล้ว"){
       //window.location.href = window.location.href;
       $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

       $.ajax({
         type: "GET",
         url: base_url + "cms/getfilecontent?pageid="+mainid+"&subid="+subid+"&type=file"
         }).done(function(msg2){

          data = $.parseJSON(msg2);

          if(data.alldata_row > 0){
            $("#reviewfile").html(data.retdata);
          }
          else
          {
            $("#reviewfile").html("");
          }

          $("#fileid").val("");
          $("#oldfile").val("");
          $("#filename").val("");
          $("#uploadfile").val("");
          $("body").removeClass("loading");
       });

     }
     else
     {
       alert(msg);
       $("body").removeClass("loading");
     //$("#showerr").text(msg);
     }
   });
   return false;

});


$("#defformpic").submit(function(){
 tinyMCE.triggerSave();
 var mainid = $("#mainid").val();
 var subid = $("#subid").val();

 var ref = $(this).find("[required]");
 var x = 0;

 $("body").addClass("loading");

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

   $.ajax({
     type: "POST",
     url: base_url + "cms/updatecmspicture",
     data: new FormData( this ),
     processData: false,
     contentType: false,
     async: true
     }).done(function(msg){
     if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"||msg == "ข้อมูลได้ทำการเพิ่มแล้ว"){
       //window.location.href = window.location.href;
       $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

       $.ajax({
         type: "GET",
         url: base_url + "cms/getfilecontent?pageid="+mainid+"&subid="+subid+"&type=picture"
        }).done(function(msg2){

         data = $.parseJSON(msg2);

         if(data.alldata_row > 0){
           $("#reviewpicture").html(data.retdata);
           $("#allpic").val(data.alldata_row);
         }
         else
         {
           $("#reviewpicture").html("");
           $("#allpic").val("0");
         }
       });


       $("#pictureid").val("");
       $("#oldpic").val("");
       $("#picturename").val("");
       tinyMCE.get('picturedescription').setContent("");
       //$("#picturedescription").val("");
       $("#uploadpicture").val("");
       $("body").removeClass("loading");
     }
     else
     {
       alert(msg);
       $("body").removeClass("loading");
     //$("#showerr").text(msg);
     }
   });
   return false;

});


$("#defformslide").submit(function(){
 tinyMCE.triggerSave();
 var mainid = $("#mainid").val();
 var subid = $("#subid").val();

 var ref = $(this).find("[required]");
 var x = 0;

 $("body").addClass("loading");

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

   $.ajax({
     type: "POST",
     url: base_url + "cms/updatecmsslide",
     data: new FormData( this ),
     processData: false,
     contentType: false,
     async: true
     }).done(function(msg){
     if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"||msg == "ข้อมูลได้ทำการเพิ่มแล้ว"){
       $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

       $.ajax({
         type: "GET",
         url: base_url + "cms/getfilecontent?pageid="+mainid+"&subid="+subid+"&type=slide"
       }).done(function(msg2){

        data = $.parseJSON(msg2);

        if(data.alldata_row > 0){
          $("#reviewslide").html(data.retdata);
        }
        else
        {
          $("#reviewslide").html("");
        }

        $("#slideid").val("");
        $("#oldslide").val("");
        $("#slidename").val("");
        $("#uploadslide").val("");
        $("body").removeClass("loading");

      });
     }
     else
     {
       alert(msg);
       $("body").removeClass("loading");
     //$("#showerr").text(msg);
     }
   });
   return false;

});


$("#defformattr").submit(function(){

 var ref = $(this).find("[required]");
 var x = 0;

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

 $.ajax({
   type: "POST",
   url: base_url + "cms/updatecmsattr",
   data: new FormData( this ),
   processData: false,
   contentType: false,
   async: true
   }).done(function(msg){
   if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"||msg == "ข้อมูลได้ทำการเพิ่มแล้ว"){
     window.location.href = window.location.href;
     $("body").removeClass("loading");
     //$('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');
   }
   else
   {
     alert(msg);
     $("body").removeClass("loading");
   //$("#showerr").text(msg);
   }
 });
   return false;

});

function openlang(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" activetabs", "");
    }

    if(cityName!="TH"&&cityName!="CN"&&cityName!="EN")
    {
      document.getElementById("submitfortxtdata").style.display = "none";
    }
    else
    {
      document.getElementById("submitfortxtdata").style.display = "block";
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " activetabs";
}

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading"); },
    ajaxStop: function() { $body.removeClass("loading"); }
});

function resetform() {
  document.getElementById("defform").reset();
}

function resetformpicture() {
  window.location.href = window.location.href;
  //document.getElementById("defformpic").reset();
}

function resetformslide() {
  window.location.href = window.location.href;
  //document.getElementById("defformslide").reset();
}

function resetformfile() {
  window.location.href = window.location.href;
  //document.getElementById("defformfile").reset();
}

function resetformattr() {
  window.location.href = window.location.href;
  //document.getElementById("defformattr").reset();
}


$("#uploadpicture").change(function(event){
	event.preventDefault();
	var $fileUpload = $("#uploadpicture");
	if (parseInt($fileUpload.get(0).files.length)>($("#maxpic").val()-$("#allpic").val()) ){
	 alert("You can only upload a maximum of "+$("#maxpic").val()+" files");
	 $("#uploadpicture").val("");
 }
 ValidateFileUpload();
 return false;
});

function ValidateFileUpload() {

var fuData = document.getElementById('uploadpicture');
	for (var i = 0; i < fuData.files.length; ++i) {
		var FileUploadPath = fuData.files.item(i).name;
    var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
    if (Extension == "gif" || Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
            if (fuData.files && fuData.files.item(i)) {
                var size = fuData.files.item(i).size;
                if(size > 4096000){
                     alert("Maximum file size exceeds (Max 4MB.)");
										 $("#uploadpicture").val("");
                    return;
                }else{
                }
            }
    }
		else {
        alert("Photo only allows file types of PNG, JPG, JPEG and GIF. ");
				$("#uploadpicture").val("");
    }
	}
}
