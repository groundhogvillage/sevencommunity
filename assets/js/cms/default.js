var host = window.location.origin;

if(host=="http://localhost"){
  var base_url = "http://localhost/sevencommunity/";
}
else
{
   var base_url = host+"/";
}

document.getElementById("defaultOpen").click();

$("#defform").submit(function(){
 tinyMCE.triggerSave();
 var ref = $(this).find("[required]");
 var x = 0;

 $("body").addClass("loading");

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

 $.ajax({
   type: "POST",
   url: base_url + "cms/updatecmsdata",
   data: new FormData( this ),
   processData: false,
   contentType: false,
   async: true
   }).done(function(msg){
   if(msg == "ข้อมูลได้ทำการเพิ่มแล้ว")
   {
     window.location.href = window.location.href;
     $("body").removeClass("loading");
   }
   else if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว")
   {
     $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');
     $("body").removeClass("loading");
   }
   else
   {
     $("body").removeClass("loading"); 
     alert(msg);
   }
 });


   return false;

});


$("#defformfile").submit(function(){
 tinyMCE.triggerSave();
 var idpage = $("#mainid").val();

 var ref = $(this).find("[required]");
 var x = 0;

 $("body").addClass("loading");

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

   $.ajax({
     type: "POST",
     url: base_url + "cms/updatecmsfile",
     data: new FormData( this ),
     processData: false,
     contentType: false,
     async: true
     }).done(function(msg){
     if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"||msg == "ข้อมูลได้ทำการเพิ่มแล้ว"){
       //window.location.href = window.location.href;
       $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

       $.ajax({
         type: "GET",
         url: base_url + "cms/getfilecontent?pageid="+idpage+"&type=file"
         }).done(function(msg2){

          data = $.parseJSON(msg2);

          if(data.alldata_row > 0){
            $("#reviewfile").html(data.retdata);
          }
          else
          {
            $("#reviewfile").html("");
          }

          $("#fileid").val("");
          $("#oldfile").val("");
          $("#filename").val("");
          $("#uploadfile").val("");

       });
       $("body").removeClass("loading");
     }
     else
     {
       alert(msg);
       $("body").removeClass("loading");
     //$("#showerr").text(msg);
     }
   });


   return false;

});


$("#defformpic").submit(function(){
 tinyMCE.triggerSave();
 var idpage = $("#mainid").val();

 var ref = $(this).find("[required]");
 var x = 0;

 $("body").addClass("loading");

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

   $.ajax({
     type: "POST",
     url: base_url + "cms/updatecmspicture",
     data: new FormData( this ),
     processData: false,
     contentType: false,
     async: true
     }).done(function(msg){
     if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"||msg == "ข้อมูลได้ทำการเพิ่มแล้ว"){
       //window.location.href = window.location.href;
       $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

       $.ajax({
         type: "GET",
         url: base_url + "cms/getfilecontent?pageid="+idpage+"&type=picture"
        }).done(function(msg2){

         data = $.parseJSON(msg2);

         if(data.alldata_row > 0){
           $("#reviewpicture").html(data.retdata);
         }
         else
         {
           $("#reviewpicture").html("");
         }
       });

       $("#pictureid").val("");
       $("#oldpic").val("");
       $("#picturename").val("");
       $("#uploadpicture").val("");
       $("body").removeClass("loading");
     }
     else
     {
       alert(msg);
       $("body").removeClass("loading");
     //$("#showerr").text(msg);
     }
   });

   return false;

});


$("#defformslide").submit(function(){
 tinyMCE.triggerSave();
 var idpage = $("#mainid").val();

 var ref = $(this).find("[required]");
 var x = 0;

 $("body").addClass("loading");

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

   $.ajax({
     type: "POST",
     url: base_url + "cms/updatecmsslide",
     data: new FormData( this ),
     processData: false,
     contentType: false,
     async: true
     }).done(function(msg){
     if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"||msg == "ข้อมูลได้ทำการเพิ่มแล้ว"){
       $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

       $.ajax({
         type: "GET",
         url: base_url + "cms/getfilecontent?pageid="+idpage+"&type=slide"
       }).done(function(msg2){

        data = $.parseJSON(msg2);

        if(data.alldata_row > 0){
          $("#reviewslide").html(data.retdata);
        }
        else
        {
          $("#reviewslide").html("");
        }

        $("#slideid").val("");
        $("#oldslide").val("");
        $("#slidename").val("");
        $("#uploadslide").val("");
        $("body").removeClass("loading");
      });
     }
     else
     {
       alert(msg);
       $("body").removeClass("loading");
     //$("#showerr").text(msg);
     }
   });

   return false;

});

$("#defformattr").submit(function(){
 tinyMCE.triggerSave();
 var ref = $(this).find("[required]");
 var x = 0;

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

 $.ajax({
   type: "POST",
   url: base_url + "cms/updatecmsattr",
   data: new FormData( this ),
   processData: false,
   contentType: false,
   async: true
   }).done(function(msg){
   if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"||msg == "ข้อมูลได้ทำการเพิ่มแล้ว"){
     window.location.href = window.location.href;
     //$('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');
   }
   else
   {
     alert(msg);
   //$("#showerr").text(msg);
   }
 });
   return false;

});


function openlang(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" activetabs", "");
    }

    if(cityName!="TH"&&cityName!="CN"&&cityName!="EN")
    {
      document.getElementById("submitfortxtdata").style.display = "none";
    }
    else
    {
      document.getElementById("submitfortxtdata").style.display = "block";
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " activetabs";
}

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading"); },
    ajaxStop: function() { $body.removeClass("loading"); }
});

function resetform() {
  document.getElementById("defform").reset();
}

function resetformpicture() {
  document.getElementById("defformpic").reset();
}

function resetformslide() {
  document.getElementById("defformslide").reset();
}

function resetformfile() {
  document.getElementById("defformfile").reset();
}

function resetformcommend() {
  document.getElementById("defformcommend").reset();
}

function resetformattr() {
  document.getElementById("defformattr").reset();
}


$("#addsubpage").click(function(){

  if($("#popup2").css("display")=="none")
  {
    $("#popup2").css("display","block");
  }
  else
  {
    $("#popup2").css("display","none");
  }

  return false;

});


$("#closepopup").click(function(){

  $("#popup2").css("display","none");

  return false;

});

$("#addsubform").submit(function(){

  var ref = $(this).find("[required]");
  var x = 0;

    $(ref).each(function(){
        if ( $(this).val() == '' )
        {
          x++;
        }
    });

    if(x>0)
    {
      alert("Required field should not be blank.");
      return false;
    }

  $.ajax({
    type: "POST",
    url: base_url + "cms/addsubpage",
    data: new FormData( this ),
    processData: false,
    contentType: false,
    async: true
    }).done(function(msg){
    if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"||msg == "ข้อมูลได้ทำการเพิ่มแล้ว")
    {
      window.location.href = window.location.href;
    }
    else
    {
      alert(msg);
    }
  });
    return false;

});
