var host = window.location.origin;

if(host=="http://localhost"){
  var base_url = "http://localhost/sevencommunity/";
}
else
{
   var base_url = host+"/";
}

$( document ).ready(function() {

	 $("#signup").submit(function(){

	 	var ref = $(this).find("[required]");
		var x = 0;

	    $(ref).each(function(){
	        if ( $(this).val() == '' )
	        {
	        	x++;
	        }
	    });

	    if(x>0)
	    {
	    	alert("Required field should not be blank.");
	    	return false;
	    }

	 	if( $("#reinputPassword").val() != $('#inputPassword').val() ){
			alert("Password Not Match");
			return false;
		}

		$.ajax({
			type: "POST",
			url: base_url + "index/register_update_data",
			data: new FormData( this ),
			processData: false,
			contentType: false,
			async: true
			}).done(function(msg){
			if(msg == "ข้อมูลได้ทำการเพิ่มแล้ว"){
		        //$('#submitbutton').prop('disabled', false); //TO ENABLE
				//alert(msg+" รอ Email ตอบกลับจากผู้ดูแลระบบเพื่อใช้งานการ Login");
				window.location.href = base_url+"index/waitaccept";
	     	}else{
				alert(msg);
				//$("#showerr").text(msg);
	      	}
		});
	    return false;

	 });

});

/*
$(function() {
	$( ".datepicker_bd" ).datepicker({
	  dateFormat: "dd/mm/yy",
	  defaultDate: '01/01/1991',
	  yearRange: "-100:+0",
	  changeMonth: true,
	  changeYear: true,
	  showButtonPanel: true
	});
});

$( ".clickshowcalendar" ).click(function() {
	$( ".datepicker_bd" ).focus();
});
*/
function resetform() {
    document.getElementById("signup").reset();
}
