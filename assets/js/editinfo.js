var host = window.location.origin;

if(host=="http://localhost"){
  var base_url = "http://localhost/sevencommunity/";
}
else
{
   var base_url = host+"/";
}

$('#popupopen').click(function () {
	document.getElementById('popup_a').style.display = 'block';
});

$('#cancelpopupbutton, #xpopupbutton').click(function () {
	document.getElementById('popup_a').style.display = 'none';
});


$( document ).ready(function() {

	 $("#editinfo").submit(function(){

	 	var ref = $(this).find("[required]");
		var x = 0;

	    $(ref).each(function(){
	        if ( $(this).val() == '' )
	        {
	        	x++;
	        }
	    });

	    if(x>0)
	    {
	    	alert("Required field should not be blank.");
	    	return false;
	    }

		$.ajax({
			type: "POST",
			url: base_url + "main/editinfodata",
			data: new FormData( this ),
			processData: false,
			contentType: false,
			async: true
			}).done(function(msg){
			if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"){
				window.location.href = base_url+"main/editinfo?process=updated";
	     	}else{
				alert(msg);
	      	}
		});
	    return false;

	 });

	 $("#changepass").submit(function(){

	 	if( $("#reinputPassword").val() != $('#inputPassword').val() ){
			alert("Password Not Match");
			return false;
		}
		else
		{
			$.ajax({
				type: "POST",
				url: base_url + "main/changepassword",
				data: new FormData( this ),
				processData: false,
				contentType: false,
				async: true
				}).done(function(msg){
				if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว"){
					window.location.href = base_url+"main/editinfo?process=passchange";
		     	}else{
		     		alert(msg);
					//window.location.href = base_url+"main/editinfo?process=passnotchange";
		      	}
			});

		}

		 return false;

	 });

});
