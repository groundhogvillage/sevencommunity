/*$(".edtpic").click(function(){
  var idpage = $("#mainid").val();

  $("body").addClass("loading");

  var id = $(this).attr('id').split("_");
  var id1 = id[1];

  $.ajax({
    type: "GET",
    url: base_url + "cms/editfile?idedt="+id1+"&type=picture",
    }).done(function(msg){
      data = $.parseJSON(msg);
      $("#pictureid").val(data.id);
      $("#oldpic").val(data.path);
      $("#picturename").val(data.name);
      tinyMCE.get('picturedescription').setContent(data.description);
      //$("#picturedescription").val(data.description);
  });

  $("body").removeClass("loading");

});*/


$(".delthispic").click(function(){
  var news_id = $("#news_id").val();

  var id = $(this).attr('id').split("_");
  var id1 = id[1];

  $("body").addClass("loading");

  $.ajax({
    type: "GET",
    url: base_url + "community/delnewspicture?iddel="+id1+"&type=picture",
    }).done(function(msg){
    if(msg == "Delete Complete"){
    //  window.location.href = window.location.href;
    $('#deleteddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

      $.ajax({
        type: "GET",
        url: base_url + "community/getnewspictureorder?news_id="+news_id
       }).done(function(msg2){

        data = $.parseJSON(msg2);

        if(data.alldata_row > 0){
          $("#reviewpicture").html(data.retdata);
        }
        else
        {
          $("#reviewpicture").html("");
        }
      });

      $("body").removeClass("loading");
    }
    else
    {
      alert(msg);
      $("body").removeClass("loading");
    //$("#showerr").text(msg);
    }
  });
});

$(document).ready(function(){
    $('.reorder_linkpicture').on('click',function(){
        $("ul.reorder-picture-list").sortable({ tolerance: 'pointer' });
        $('.reorder_linkpicture').html('save reorderingpicture');
        $('.reorder_linkpicture').attr("id","save_reorderpicture");
        $('#reorder-helperpicture').slideDown('slow');
        $('.image_linkpicture').attr("href","javascript:void(0);");
        $('.image_linkpicture').css("cursor","move");
        $("#save_reorderpicture").click(function( e ){
            if( !$("#save_reorderpicture i").length ){
                $(this).html('').prepend('<img src="../assets/images/loading.gif"/>');
                $("ul.reorder-picture-list").sortable('destroy');
                $("#reorder-helperpicture").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');

                var news_id = $("#news_id").val();

                var h = [];
                $("ul.reorder-picture-list li").each(function() {  h.push($(this).attr('id').substr(11));  });

                $.ajax({
                    type: "POST",
                    url: base_url + "community/reordernewspicture",
                    data: {order: h, type: "news_picture", news_id: news_id},
                    success: function(result){
                      //  alert(result);
                      // window.location.reload();
                      if(result=="complete")
                      {
                        $.ajax({
                          type: "GET",
                          url: base_url + "community/getnewspictureorder?news_id="+news_id
                         }).done(function(msg2){

                          data = $.parseJSON(msg2);

                          if(data.alldata_row > 0){
                            $("#reviewpicture").html(data.retdata);
                          }
                          else
                          {
                            $("#reviewpicture").html("");
                          }
                        });
                      }
                      else
                      {
                          alert(result);
                      }

                    }
                });



                return false;
            }
            e.preventDefault();
        });
    });
});
