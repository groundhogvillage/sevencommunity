var host = window.location.origin;

if(host=="http://localhost"){
  var base_url = "http://localhost/sevencommunity/";
}
else
{
   var base_url = host+"/";
}

$("#defform").submit(function(){

 $("body").addClass("loading");

 tinyMCE.triggerSave();

 var type = $("#type").val();
 var main = $("#page_id").val();

 var ref = $(this).find("[required]");
 var x = 0;

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

 $.ajax({
   type: "POST",
   url: base_url + "community/updateitemcomm",
   data: new FormData( this ),
   processData: false,
   contentType: false,
   async: true
   }).done(function(msg){
   if(msg == "ข้อมูลได้ทำการเพิ่มแล้ว")
   {
     window.location.href =  base_url + "community/itemcommlist?main="+main+"&type="+type;
     $("body").removeClass("loading");
   }
   else if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว")
   {
     $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');
     window.location.href = window.location.href;
     $("body").removeClass("loading");
   }
   else
   {
     alert(msg);
     $("body").removeClass("loading");
   }
 });
   return false;

});


$("#fileToUpload").change(function(event){
	event.preventDefault();
	var $fileUpload = $("#fileToUpload");
	if (parseInt($fileUpload.get(0).files.length)>(5-$("#item_picture_row").val()) ){
	 alert("You can only upload a maximum of 5 files");
	 $("#fileToUpload").val("");
 }
 ValidateFileUpload();
 return false;
});

function ValidateFileUpload() {

var fuData = document.getElementById('fileToUpload');
	for (var i = 0; i < fuData.files.length; ++i) {
		var FileUploadPath = fuData.files.item(i).name;
    var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
    if (Extension == "gif" || Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
            if (fuData.files && fuData.files.item(i)) {
                var size = fuData.files.item(i).size;
                if(size > 4096000){
                     alert("Maximum file size exceeds (Max 4MB.)");
										 $("#fileToUpload").val("");
                    return;
                }else{
                  /*  var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(fuData.files[0]);*/
                }
            }
    }
		else {
        alert("Photo only allows file types of PNG, JPG, JPEG and GIF. ");
				$("#fileToUpload").val("");
    }
	}
}

function resetform() {
  document.getElementById("defform").reset();
}


if (window.location.protocol != "https:") {
  if(($('#lat').val()==0||$('#lat').val()==""||$('#lat').val()==null)&&($('#lon').val()==0||$('#lon').val()==""||$('#lon').val()==null))
  {
    $('#lat').val('7.900577973795131');
    $('#lon').val('98.38213920593262');
  }
}

$(document).ready(function() {

function success(position) {

  var coords = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

  var options = {
    zoom: 15,
    center: coords,
    mapTypeControl: false,
    scrollwheel: true,
    disableDefaultUI: true,
		draggable:true,
    navigationControlOptions: {
    	style: google.maps.NavigationControlStyle.SMALL
    },
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map-canvas"), options);

  var marker = new google.maps.Marker({
      position: coords,
			draggable:true,
    	animation: google.maps.Animation.DROP,
      map: map
  });

  $('#lat').val(position.coords.latitude);

  $('#lon').val(position.coords.longitude);

	initialize();

}

function initialize() {
   if($('#lat').val()!=0&&$('#lat').val()!=""&&$('#lat').val()!=null&&$('#lon').val()!=0&&$('#lon').val()!=""&&$('#lon').val()!=null)
   {
   		var lat = $('#lat').val();
   		var lon = $('#lon').val();
			if($('#setzoom').val()==null||$('#setzoom').val()==""||$('#setzoom').val()==0)
			{
				 setzoom = 15;
			}
			else
			{
				 setzoom = parseFloat($('#setzoom').val());
			}
   }
   else
   {
   		navigator.geolocation.watchPosition(success);
   }

  var myLatlng = new google.maps.LatLng(lat,lon);
  var mapOptions = {
    disableDefaultUI: false,
    scrollwheel: true,
		draggable:true,
    zoom: setzoom,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
			draggable:true,
    	animation: google.maps.Animation.DROP,
      map: map
  });

	google.maps.event.addListener(marker, 'dragend', function()
	{
	    geocodePosition(marker.getPosition());
			/*alert( "Latitude: "+event.latLng.lat()+" "+", longitude: "+event.latLng.lng() );
			$('#lat').val(event.latLng.lat());
			$('#lon').val(event.latLng.lng());*/
	});

	function geocodePosition(pos)
	{
	   geocoder = new google.maps.Geocoder();
	   geocoder.geocode
	    ({
	        latLng: pos
	    },
	        function(results, status)
	        {
	            if (status == google.maps.GeocoderStatus.OK)
	            {
								//alert(results[0].geometry.location.lng());
								$('#lat').val(results[0].geometry.location.lat());
								$('#lon').val(results[0].geometry.location.lng());
	                /*$("#mapSearchInput").val(results[0].formatted_address);
	                $("#mapErrorMsg").hide(100);*/
	            }
	            else
	            {
	              //  $("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
	            }
	        }
	    );
	}

	/*function moveMarker( map, marker ) {
	    marker.setPosition( new google.maps.LatLng( lat, lon ) );
	    map.panTo( new google.maps.LatLng( lat, lon ) );
	    initialize();
	};

	google.maps.event.addListener(map, 'click', function(event) {
			//alert( "Latitude: "+event.latLng.lat()+" "+", longitude: "+event.latLng.lng() );
			$('#lat').val(event.latLng.lat());
			$('#lot').val(event.latLng.lng());
	        moveMarker( map, marker );
	    });

	google.maps.event.addListener(map, 'zoom_changed', function(event) {
			//alert( "Latitude: "+event.latLng.lat()+" "+", longitude: "+event.latLng.lng() );
				$('#setzoom').val(map.getZoom());
			});
*/
	google.maps.event.addListener(marker, 'click', function() {
		window.open(
		  'https://www.google.co.th/maps',
		  '_blank' // <- This is what makes it open in a new window.
		);
	});

}

	$('#getlocation').click(function () {
		if (navigator.geolocation) {
	 	 navigator.geolocation.watchPosition(success);
		} else {
		  error('Geo Location is not supported');
		}
		return false;
	});

	$('#updatelocation').click(function () {
		initialize();
		return false;
	});


	google.maps.event.addDomListener(window, 'load', initialize);
});



$(".delthispic").click(function(){
  var item_id = $("#item_id").val();

  var id = $(this).attr('id').split("_");
  var id1 = id[1];

  $("body").addClass("loading");

  $.ajax({
    type: "GET",
    url: base_url + "community/delitempicture?iddel="+id1+"&type=picture",
    }).done(function(msg){
    if(msg == "Delete Complete"){
    //  window.location.href = window.location.href;
    $('#deleteddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

      $.ajax({
        type: "GET",
        url: base_url + "community/getitempictureorder?item_id="+item_id
       }).done(function(msg2){

        data = $.parseJSON(msg2);

        if(data.alldata_row > 0){
          $("#reviewpicture").html(data.retdata);
        }
        else
        {
          $("#reviewpicture").html("");
        }
      });

      $("body").removeClass("loading");
    }
    else
    {
      alert(msg);
      $("body").removeClass("loading");
    //$("#showerr").text(msg);
    }
  });
});

$(document).ready(function(){
    $('.reorder_linkpicture').on('click',function(){
        $("ul.reorder-picture-list").sortable({ tolerance: 'pointer' });
        $('.reorder_linkpicture').html('save reorderingpicture');
        $('.reorder_linkpicture').attr("id","save_reorderpicture");
        $('#reorder-helperpicture').slideDown('slow');
        $('.image_linkpicture').attr("href","javascript:void(0);");
        $('.image_linkpicture').css("cursor","move");
        $("#save_reorderpicture").click(function( e ){
            if( !$("#save_reorderpicture i").length ){
                $(this).html('').prepend('<img src="../assets/images/loading.gif"/>');
                $("ul.reorder-picture-list").sortable('destroy');
                $("#reorder-helperpicture").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');

                var item_id = $("#item_id").val();

                var h = [];
                $("ul.reorder-picture-list li").each(function() {  h.push($(this).attr('id').substr(11));  });

                $.ajax({
                    type: "POST",
                    url: base_url + "community/reorderitempicture",
                    data: {order: h, type: "item_picture", item_id: item_id},
                    success: function(result){
                      //  alert(result);
                      // window.location.reload();
                      if(result=="complete")
                      {
                        $.ajax({
                          type: "GET",
                          url: base_url + "community/getitempictureorder?item_id="+item_id
                         }).done(function(msg2){

                          data = $.parseJSON(msg2);

                          if(data.alldata_row > 0){
                            $("#reviewpicture").html(data.retdata);
                          }
                          else
                          {
                            $("#reviewpicture").html("");
                          }
                        });
                      }
                      else
                      {
                          alert(result);
                      }

                    }
                });



                return false;
            }
            e.preventDefault();
        });
    });
});
