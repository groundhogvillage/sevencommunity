var host = window.location.origin;

if(host=="http://localhost"){
  var base_url = "http://localhost/sevencommunity/";
}
else
{
   var base_url = host+"/";
}

$("#defform").submit(function(){

 $("body").addClass("loading");

 tinyMCE.triggerSave();

 var type = $("#type").val();
 var main = $("#page_id").val();

 var ref = $(this).find("[required]");
 var x = 0;

   $(ref).each(function(){
       if ( $(this).val() == '' )
       {
         x++;
       }
   });

   if(x>0)
   {
     alert("Required field should not be blank.");
     return false;
   }

 $.ajax({
   type: "POST",
   url: base_url + "community/updatenewscomm",
   data: new FormData( this ),
   processData: false,
   contentType: false,
   async: true
   }).done(function(msg){
   if(msg == "ข้อมูลได้ทำการเพิ่มแล้ว")
   {
     window.location.href =  base_url + "community/newscommlist?main="+main+"&type="+type;
     $("body").removeClass("loading");
   }
   else if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว")
   {
     $('#updateddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');
     window.location.href = window.location.href;
     $("body").removeClass("loading");
   }
   else
   {
     alert(msg);
     $("body").removeClass("loading");
   }
 });
   return false;

});


$("#fileToUpload").change(function(event){
	event.preventDefault();
	var $fileUpload = $("#fileToUpload");
	if (parseInt($fileUpload.get(0).files.length)>(5-$("#news_picture_row").val()) ){
	 alert("You can only upload a maximum of 5 files");
	 $("#fileToUpload").val("");
 }
 ValidateFileUpload();
 return false;
});

function ValidateFileUpload() {

var fuData = document.getElementById('fileToUpload');
	for (var i = 0; i < fuData.files.length; ++i) {
		var FileUploadPath = fuData.files.item(i).name;
    var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
    if (Extension == "gif" || Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
            if (fuData.files && fuData.files.item(i)) {
                var size = fuData.files.item(i).size;
                if(size > 4096000){
                     alert("Maximum file size exceeds (Max 4MB.)");
										 $("#fileToUpload").val("");
                    return;
                }else{
                  /*  var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(fuData.files[0]);*/
                }
            }
    }
		else {
        alert("Photo only allows file types of PNG, JPG, JPEG and GIF. ");
				$("#fileToUpload").val("");
    }
	}
}

function resetform() {
  document.getElementById("defform").reset();
}

$(".delthispic").click(function(){
  var news_id = $("#news_id").val();

  var id = $(this).attr('id').split("_");
  var id1 = id[1];

  $("body").addClass("loading");

  $.ajax({
    type: "GET",
    url: base_url + "community/delnewspicture?iddel="+id1+"&type=picture",
    }).done(function(msg){
    if(msg == "Delete Complete"){
    //  window.location.href = window.location.href;
    $('#deleteddatacomplete').fadeIn('fast').delay(1000).fadeOut('fast');

      $.ajax({
        type: "GET",
        url: base_url + "community/getnewspictureorder?news_id="+news_id
       }).done(function(msg2){

        data = $.parseJSON(msg2);

        if(data.alldata_row > 0){
          $("#reviewpicture").html(data.retdata);
        }
        else
        {
          $("#reviewpicture").html("");
        }
      });

      $("body").removeClass("loading");
    }
    else
    {
      alert(msg);
      $("body").removeClass("loading");
    //$("#showerr").text(msg);
    }
  });
});

$(document).ready(function(){
    $('.reorder_linkpicture').on('click',function(){
        $("ul.reorder-picture-list").sortable({ tolerance: 'pointer' });
        $('.reorder_linkpicture').html('save reorderingpicture');
        $('.reorder_linkpicture').attr("id","save_reorderpicture");
        $('#reorder-helperpicture').slideDown('slow');
        $('.image_linkpicture').attr("href","javascript:void(0);");
        $('.image_linkpicture').css("cursor","move");
        $("#save_reorderpicture").click(function( e ){
            if( !$("#save_reorderpicture i").length ){
                $(this).html('').prepend('<img src="../assets/images/loading.gif"/>');
                $("ul.reorder-picture-list").sortable('destroy');
                $("#reorder-helperpicture").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');

                var news_id = $("#news_id").val();

                var h = [];
                $("ul.reorder-picture-list li").each(function() {  h.push($(this).attr('id').substr(11));  });

                $.ajax({
                    type: "POST",
                    url: base_url + "community/reordernewspicture",
                    data: {order: h, type: "news_picture", news_id: news_id},
                    success: function(result){
                      //  alert(result);
                      // window.location.reload();
                      if(result=="complete")
                      {
                        $.ajax({
                          type: "GET",
                          url: base_url + "community/getnewspictureorder?news_id="+news_id
                         }).done(function(msg2){

                          data = $.parseJSON(msg2);

                          if(data.alldata_row > 0){
                            $("#reviewpicture").html(data.retdata);
                          }
                          else
                          {
                            $("#reviewpicture").html("");
                          }
                        });
                      }
                      else
                      {
                          alert(result);
                      }

                    }
                });



                return false;
            }
            e.preventDefault();
        });
    });
});
