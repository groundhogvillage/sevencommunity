var host = window.location.origin;

if(host=="http://localhost"){
  var base_url = "http://localhost/sevencommunity/";
}
else
{
   var base_url = host+"/";
}

$( document ).ready(function() {

	 $("#editinfo").submit(function(){

	 	var ref = $(this).find("[required]");
		var x = 0;

	    $(ref).each(function(){
	        if ( $(this).val() == '' )
	        {
	        	x++;
	        }
	    });

	    if(x>0)
	    {
	    	alert("Required field should not be blank.");
	    	return false;
	    }

		$.ajax({
			type: "POST",
			url: base_url + "main/edituserdata",
			data: new FormData( this ),
			processData: false,
			contentType: false,
			async: true
			}).done(function(msg){
			if(msg == "ข้อมูลได้ทำการแก้ไขแล้ว")
      {
			     window.location.href = base_url+"main/edituser?User_ID="+$("#User_ID").val()+"&process=updated";
     	}
      else if(msg == "ข้อมูลได้ทำการเพิ่มแล้ว")
      {
        window.location.href = base_url+"main/userlist"
      }
      else
      {
			  alert(msg);
      }
		});
	    return false;

	 });

});
