$(function(){
var height = $("#before-fixed-navbar").height();
if ($(window).scrollTop() > height-1) {
  $(".fixed-navbar").css("display","block");
}
else  {
  $(".fixed-navbar").css("display","none");
}
});

$( window ).scroll(function() {
var height = $("#before-fixed-navbar").height();
if ($(window).scrollTop() > height-1) {
  $(".fixed-navbar").css("display","block");
}
else  {
  $(".fixed-navbar").css("display","none");
}
});

$(window).resize(function(){
var height = $("#before-fixed-navbar").height();
if ($(window).scrollTop() > height-1) {
  $(".fixed-navbar").css("display","block");
}
else  {
  $(".fixed-navbar").css("display","none");
}
});

//jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".fixed-navbar").addClass("top-nav-collapse");
    } else {
        $(".fixed-navbar").removeClass("top-nav-collapse");
    }
});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $(document).on('click', 'a.page-scroll', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

jQuery(document).ready(function($) {

  $('#myCarousel').carousel({
          interval: 5000
  });

  //Handles the carousel thumbnails
 $('[id^=carousel-selector-]').click( function(){
      var id = this.id.substr(this.id.lastIndexOf("-") + 1);
      var id = parseInt(id);
      $('#myCarousel').carousel(id);
  });

  // Text
  $('#carousel-text').html($('#slide-content-0').html());

  // When the carousel slides, auto update the text
  $('#myCarousel').on('slid.bs.carousel', function (e) {
           var id = $('.item.comm.active').data('slide-number');
          $('#carousel-text').html($('#slide-content-'+id).html());
  });
});

$(function() {
  var selectedClass = "";
    $(".fil-cat").click(function(){
      selectedClass = $(this).attr("data-rel");

      $("#product-hilight").fadeTo(200, 0.1);

      $("#product-hilight li").not("."+selectedClass).fadeOut().removeClass('scale-anm');

      setTimeout(function() {

        $("."+selectedClass).fadeIn().addClass('scale-anm');

        $("#product-hilight").fadeTo(400, 1);
      }, 400);

    });
});
