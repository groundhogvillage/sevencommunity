var host = window.location.origin;

if(host=="http://localhost"){
  var base_url = "http://localhost/sevencommunity/";
}
else
{
   var base_url = host+"/";
}

$(document).ready(function() {

  $('#closebox_on_map').click( function(){
    $("#box_on_map").css("display","none");
  });

  $.ajax({
    type: "GET",
    url: base_url + "index/getmapcommunity?commid="+$("#commid").val()
    }).done(function(msg){

     data = $.parseJSON(msg);

     if(data.item_row > 0){

       /*console.log(data.item[0]['name']+" , "+data.item[0]['lat']+" , "+data.item[0]['lon']+" , "+data.item[0]['id']);*/

       var locations = [];
       for (var i = 0; i < data.item.length; i++) {
         locations[i] = [data.item[i]['name'], data.item[i]['lat'], data.item[i]['lon'], data.item[i]['id']]
       }

       /*var locations = [
         ['Bondi Beach', -33.890542, 151.274856, 4],
         ['Coogee Beach', -33.923036, 151.259052, 5],
         ['Cronulla Beach', -34.028249, 151.157507, 3],
         ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
         ['Maroubra Beach', -33.950198, 151.259302, 1]
       ];*/
     }
     else
     {
       var locations = [ ['Phuket Town', 7.900577973795131, 98.38213920593262, 0] ];
     }


       var drgflag=true;

       if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
       var drgflag=true;
       }

       function initMap() {

               var map = new google.maps.Map(document.getElementById('map'), {
                 zoom: 12,
                 center: new google.maps.LatLng(locations[0][1], locations[0][2])
               });

               // Create the search box and link it to the UI element.
               var box_on_map = /** @type {HTMLInputElement} */(
                 document.getElementById('box_on_map'));
                 map.controls[google.maps.ControlPosition.TOP_LEFT].push(box_on_map);

               var infowindow = new google.maps.InfoWindow();


               markers = new google.maps.Marker({
                 position: new google.maps.LatLng(locations[0][1], locations[0][2]),
                 map: map
               });
               infowindow.setContent("<a target='_blank' href='https://www.google.com/maps/dir//"+locations[0][1]+","+locations[0][2]+"'>"+"นำทางไป "+locations[0][0]+" </a>");
               infowindow.open(map, markers);

               var marker, i;

               for (i = 0; i < locations.length; i++) {

                 marker = new google.maps.Marker({
                   position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                   map: map
                 });

                 google.maps.event.addListener(marker, 'click', (function(marker, i) {
                   return function() {
                     //console.log(locations[i][3]);
                     $.ajax({
                       type: "GET",
                       url: base_url + "index/getmaponitemcommunity?itemid="+locations[i][3]
                       }).done(function(msg){

                        data = $.parseJSON(msg);
                        //console.log(data);

                        $("#box_on_map").html(data);
                        $("#box_on_map").css("display","block");

                        $('#closebox_on_map').click( function(){
                          $("#box_on_map").css("display","none");
                        });
                      });

                     infowindow.setContent("<a target='_blank' href='https://www.google.com/maps/dir//"+locations[i][1]+","+locations[i][2]+"'>"+"นำทางไป "+locations[i][0]+" </a>");
                     infowindow.open(map, marker);
                   }
                 })(marker, i));
               }

               map.setOptions({ scrollwheel: false,draggable:drgflag });
       }

       initMap();

  });


});
