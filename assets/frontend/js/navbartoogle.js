jQuery(document).ready(function($) {
  var w = window.innerWidth;
  if(w>=601)
  {
    $("#onmore600").css("display","block");
    $("#onmore300").css("display","none");
    $("#onless300").css("display","none");
  }
  else if(w>=318&&w<=600)
  {
    $("#onmore600").css("display","none");
    $("#onmore300").css("display","block");
    $("#onless300").css("display","none");
  }
  else if(w<=317)
  {
    $("#onmore600").css("display","none");
    $("#onmore300").css("display","none");
    $("#onless300").css("display","block");
  }
});


window.addEventListener("resize", function() {
  var w = window.innerWidth;
  setTimeout(function(){ $(".text_hoverlay").removeAttr("style"); },100);
  $(".navbar-toggle").attr("aria-expanded","false");
  $(".navbar-toggle").addClass("collapsed");
  $(".navbar-collapse").removeClass("in");
  if(w>=601)
  {
    $("#onmore600").css("display","block");
    $("#onmore300").css("display","none");
    $("#onless300").css("display","none");
  }
  else if(w>=318&&w<=600)
  {
    $("#onmore600").css("display","none");
    $("#onmore300").css("display","block");
    $("#onless300").css("display","none");
  }
  else if(w<=317)
  {
    $("#onmore600").css("display","none");
    $("#onmore300").css("display","none");
    $("#onless300").css("display","block");
  }

});


$(".navbar-toggle").click(function () {
  var x = $(".navbar-toggle").attr("aria-expanded");
  var w = window.innerWidth;
  $(".text_hoverlay").css("display","none");
//  alert(w);
  if(w>=1200)
  {
    $("#onmore600").css("display","block");
    $("#onmore300").css("display","none");
    $("#onless300").css("display","none");
    $(".text_hoverlay").css("top","263px");
  }
  else if(w>=768&&w<=1199)
  {
    $("#onmore600").css("display","block");
    $("#onmore300").css("display","none");
    $("#onless300").css("display","none");
    if(x==undefined||x=="false")
    {
      $(".text_hoverlay").css("top","653px");
    }
    else
    {
      $(".text_hoverlay").css("top","210px");
    }
  }
  else if(w>=601&&w<=767)
  {
    $("#onmore600").css("display","block");
    $("#onmore300").css("display","none");
    $("#onless300").css("display","none");
    if(x==undefined||x=="false")
    {
      $(".text_hoverlay").css("top","669px");
    }
    else
    {
      $(".text_hoverlay").css("top","210px");
    }
  }
  else if(w>=318&&w<=600)
  {
    $("#onmore600").css("display","none");
    $("#onmore300").css("display","block");
    $("#onless300").css("display","none");
    if(x==undefined||x=="false")
    {
      $(".text_hoverlay").css("top","669px");
    }
    else
    {
      $(".text_hoverlay").css("top","210px");
    }
  }
  else if(w<=317)
  {
    $("#onmore600").css("display","none");
    $("#onmore300").css("display","none");
    $("#onless300").css("display","block");
    if(x==undefined||x=="false")
    {
      $(".text_hoverlay").css("top","719px");
    }
    else
    {
      $(".text_hoverlay").css("top","260px");
    }
  }


  setTimeout(function(){ $(".text_hoverlay").css("display","block"); },400);
});

  $('#shmaincontent').on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({ scrollTop: $($("#shmaincontent")).offset().top}, 500, 'linear');
	});
